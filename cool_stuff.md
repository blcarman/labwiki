[tmux tutorial](https://danielmiessler.com/study/tmux/)

pwmake: generate random pw with certain amount of entropy

lsof: list open files

lsof +L1: list open files that have been deleted from disk

cron.weekly: nice weekly summary of linux stuff, not much fluff, https://www.cronweekly.com/

[Bash style guide](https://bluepenguinlist.com/2016/11/04/bash-scripting-tutorial/)

[auditd tool](https://slack.engineering/syscall-auditing-at-scale-e6a3ca8ac1b8#.d09q8koiu)

[database changes done right](http://thedailywtf.com/articles/Database-Changes-Done-Right)

[borg to cloud backup](https://opensource.com/article/17/10/backing-your-machines-borg)

[python ssl tester](https://github.com/iSECPartners/sslyze)

[performance checklist thing](http://www.brendangregg.com/USEmethod/use-rosetta.html)

virtualbox shortcuts

leave seamless right ctrl + L

sendmail config: `m4 /etc/mail/sendmail.mc > /etc/mail/sendmail.cf`

Lynis security thing (need to check it out)

rescan disk to extend `echo 1 > /sys/block/sde/device/rescan`

Jinja string concatenation use `~` instead of `+`, [reference](http://jinja.pocoo.org/docs/2.10/templates/#other-operators)

bash read csv files: https://www.cyberciti.biz/faq/unix-linux-bash-read-comma-separated-cvsfile/

[Mermaid: Diagrams as code](https://mermaid-js.github.io/mermaid/#/)

[kubernetes with bgp](https://blog.alexellis.io/bare-metal-kubernetes-with-k3s/)

[gremlin](https://www.gremlin.com/community/tutorials/how-to-use-and-install-gremlin-on-amazon-linux/)

spiffe

https://timetagger.app/ neat toggl alternative, might be fun to try adding some additional fields like git commits or documentation links

https://www.jupiterbroadcasting.com/show/error/

smallstep: ssh certs
