A friend recently started looking at upgrading their NAS. Already owning 2 synologys they were looking at another one. However I put forth the challenge that I could do better at half the price. The synology/freenas mini price is around $800 so we need to stay under $400 for me to meet the challenge. The obvious difference between a new synology and a whitebox is the used hardware and lack of a friendly gui. Well, its storage, you really don't need a gui if you have even a passing familiarity with linux. And the low price of used allows you to set some money aside if something breaks. 

## Easy Mode ##

Ebay has complete (minus disks) 8 bay Dell R520's for roughly $400, conveniently at the upper limit of our challenged range. 

## Maximum Effort ##

Piecing things together individually gives a bit more freedom and allows future upgrades though does require more effort and can easily exceed the target price as you get sucked into the little jumps here and there. Alternatively, if you build your own desktops you can re-use old parts from that as you upgrade. For example, my storage server is running an old AMD Phenom II cpu and micro-atx motherboard and will get upgraded to a ryzen cpu/mobo when I finally identify the gremlin that's been in my desktop system. 

* Chassis: [Supermicro CSE-825](https://www.ebay.com/itm/SuperMicro-2U-CSE-825-Server-Chassis-w-2x-PWS-721P-1R-Power-Supplies-SAS825TQ/401638724838?epid=1400249394&hash=item5d838894e6:g:q6sAAOSwvD5aAOw-:rk:7:pf:0) $135 
* Power Supplies: 2x 2x PWS-721P-1R (included with chassis) 
* Disk Trays: included with chassis
* Motherboard: [Supermicro X9DRH-7TF](https://www.ebay.com/itm/Supermicro-X9DRH-7TF-Dual-Socket-XEON-LGA2011-Extended-ATX-Server-Motherboard/372275874215?epid=1956131701&hash=item56ad5f09a7:g:yFcAAOSwaPZazqgy:rk:14:pf:0&LH_BIN=1) $300 (we can dream right?)
* alt mobo: [Supermicro MDC-X10SRL](https://www.ebay.com/itm/Supermicro-Motherboard-MBD-X10SRL-F-B-Xeon-E5-1600-2600-v3-LGA2011-C612/392149538759?epid=1737634645&hash=item5b4def17c7:g:2X8AAOSwKQFbxrDm:rk:10:pf:0&LH_BIN=1) $225
* HBA: included with motherboard above or [LSI SAS 9211-8i](https://www.ebay.com/itm/LSI-SAS9211-8I-6GB-S-HALF-HEIGHT-LOW-PROFILE-BRACKET-HBA-SAS-RAID-CARD-3WDV5-USA/391800872669?hash=item5b3926dedd:g:kHkAAOSwTMZbetD7:rk:17:pf:0&LH_BIN=1) $48
* CPU: [xeon e5-1620 v3](https://www.ebay.com/itm/Intel-Xeon-E5-1620-v3-3-5GHz-Quad-Core-Socket-R3-LGA2011-3-SR20P-CPU-Processor/292836344026?epid=1255339314&hash=item442e6800da:g:2zgAAOSwoZlb~bHt:rk:3:pf:0) $78
* RAM: 32 GB would be nice, but could easily start with 16

* Desktop Motherboard: [Gigabyte B450 AORUS ELITE](https://www.newegg.com/Product/Product.aspx?Item=N82E16813145085) $100 (shop around)
* Desktop CPU: [Ryzen 3 2200G](https://www.newegg.com/Product/Product.aspx?Item=N82E16819113481) $100 (shop around)
* Desktop RAM: [8 GB Corsair DDR4](https://www.newegg.com/Product/Product.aspx?Item=N82E16820233982) $60, buy 1-4 depending on budget

Total: $395

Some of you might be complaining about the non-ECC RAM and desktop parts, but we're not building an enterprise grade NAS. Also this cpu will still be overkill for what a NAS needs and we get an NVME slot out of it, so we could even do some fun stuff with caching drives if we wanted. Also with the 2200g we don't have to burn a pcie slot with a graphics card.

## Drives and Backups ##

So these posts always assume or pretned that backups just magically happen, but you probably also need to invest in usb drives or a second nas or something to do backups with and the solution will often depend on how much crap you plan to store.