# Storage 2020 #

I picked up a 12 TB USB drive for backups so it's time to finally make some updates to the storage server.

* sub-volumes
* snapshots
* read-only subvolume for jellyfin?
* upgrade to fedora server?
* borg backups moved to btrfs? kind of liked not having these on the btrfs but would make physical backups easier possibly


## Layout ##

Currently I have a mess of things spread all over the place. For example I have about 3 or 4 backups folders (and a random archive folder) this should help reduce the clutter. Also I think each of these top level things will be a subvolume. Maybe ISOs and VMs get top level things too, since only VMs should be nocow.

├── backups
├── export
│   ├── user1
│   └── user2
├── lab
│   ├── software
│   ├── ISOs
│   └── VMs
├── media
│   └── movies
│   └── music
│   └── pictures
└── snapshots

### Backups ###

## Hardware (Dream Build) ##

* [AsRock Rack X470D4U](https://www.newegg.com/asrock-rack-x470d4u-amd-ryzen-2nd-generation-series-processors/p/N82E16813140023) $260
* AMD R5 2400G (I already had it) $80?
* 2x16 GB RAM (steal half from the desktop since I really don't end up using 64GB much) $120ish
* SAS hba LSI 1068E (SAS 3Gb/s) (keep from existing server, wouldn't even need it if I hadn't bought those 4x4TB SAS drives a while back) ?
* 2x10gb nic (keep from existing, I think its an intel x540?) ?
* 2x1TB nvme drives in software raid 1 $200-300 or would a small cache drive be better?
* spinning drives (random mix from existing server)
* supermicro 2u 12 bay chassis (from existing server)


size before replacing last 1 TB with 4 TB `/dev/sde                 6.6T  4.0T  1.7T  71% /mnt/warehouse` after `/dev/sde                 8.0T  4.0T  3.7T  53% /mnt/warehouse`

## Software ##

CentOS 7 is proving to be limiting with its older versions of things. Upgrading to a mainline kernel created as many problems as it solved. I would consider moving to freenas but would like to run containers and VMs if needed. So I'm considering keeping the existing btrfs volume and using Fedora Server for the OS instead. Also considering adding more of the config to ansible and building a test VM. Not adding to chef server since chef server is dependent on the warehouse.

Also considering changing the management IP to be dhcp static mapped, would make config after install slightly easier.

side note: have vmware datastore mounted at lab level so its stops reporting double the storage size

web server for software