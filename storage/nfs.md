## NFS-Ganesha on Gluster ##

Guide for creating an HA NFS cluster and any caveats. This will hopefully be a full step by step guide whereas others skip over various parts.

We're using the work lab for this one since I have 2 storage servers there. Each storage server has 6 1gb NICs, CPU and RAM differ on each but basically a quad core and 8-12 GB RAM. The storage is a bit goofy as well with 11 2 TB disks between the 2. So we'll setup RAID10 on each giving us 4 TB on each that's usable for gluster and some random leftovers. If we needed the storage capacity I would be tempted to go RAID1 on the individual servers and have gluster provide the redundancy.

### Preparing the hosts ###

We're starting with minimal CentOS 7 installs on each host. Originally I wasn't going to try to save any of the original data, but it might be possible after all. To make it even more fun we're going to keep Fedora on the original host. Fedora uses a newer version of glusterfs so this might get interesting. 

#### Prepare the Underlying Storage ####

On the Fedora host we'll use LVM to combine 2 RAID 1 virtual disks that are each 2 TB (this isn't the recommended method, but was most convenient based on the server's existing config). For the Centos host we had a 4 TB virtual disk. In any case the important piece is that the bricks are the same size and using xfs as the base filesystem.



#### Configure NIC Teaming ####

Using teamd and configured via Network Manager using the LACP for the runner with 4 connections going to a 3750 switch stack port-channel with 2 connections to each switch. 

Example Switch configuration

```

interface Port-channel1
 description hp_storage
  switchport access vlan 22
   switchport mode access
   !
   interface GigabitEthernet1/0/26
    description STORAGE
     switchport access vlan 22
      switchport mode access
       channel-group 1 mode active
       !

```  

#### Installation ####
       
1. yum install epel-release
1. wget http://download.gluster.org/pub/gluster/glusterfs/nfs-ganesha/2.3.0/EPEL.repo/nfs-ganesha.repo    
1. mv nfs-ganesha.repo /etc/yum.repos.d/
1. yum install centos-release-gluster glusterfs-server glusterfs-cli glusterfs-ganesha nfs-ganesha-xfs
       
#### Gluster Configuration ####

#### NFS Ganesha Configuration ####

#### Testing and Failover ####
       
## Kerberized NFS ##
       
I have an nfs share that was working fine with a Fedora client that randomly stopped working at some point. I stood up a CentOS VM to test with and it worked correctly without any set up really. One of the oddities is that when doing an ls -lh command on the Fedora client the users are listed as user@domain instead of just user as appear on both the nfs server and the other centos test box. I'm not sure if this really matters since the uid are correct and I believe it was working in the past with Fedora still doing the full user@domain. In any case it appears the issue is on the Fedora client side rather than something with the server. 
       
This just furthers the justification for a test/staging environment and making all config changes via ansible, so that there is less question of what changes to cause things to stop working. Better monitoring/testing would also help, in this case maybe a periodic test to create a file on the nfs share to verify its mounted and working or even running the ls command to see if it returns the user instead of nobody/root like its doing in the broken state. 
       
And nfs client is mostly fixed. Turns out my workstation is not automatically getting (or perhaps keeping) kerberos tickets. In addition to this I had to modify and start/enable the nfs-idmapd.service file with 
       
```
       [Install]
       WantedBy=multi-user.target
```
per https://www.centos.org/forums/viewtopic.php?t=53896. 