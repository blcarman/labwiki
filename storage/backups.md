It's probably overdue that I come up with a real backup scheme. I don't have that much data I care about it, but it would be nice to have a real system and with de-dup I think I could do pretty good.

### What? ###

The desktop will be fairly simple. I really only care about a few system config files like the networkd config and the stuff in my home directory that isn't a download.

There are only a few VMs that I care about, the first being this wiki and the 2nd being the windows VM which doesn't have any real data but the license does make it annoying to reinstall. 

The warehouse has a ton of data on it (about 3 TB) but not a whole lot that I really care about. Really I just want my personal documents and pictures. The rest of the media and VMs are safe enough with the btrfs raid 10. 

### How? ###

Since a power issue at my house is likely to damage both my desktop and servers it makes sense that my backups be disconnected. I don't trust the cloud or want to pay that much so a USB drive will suffice. I also don't want to attach and detach a usb drive every day to do backups, so an intermediary location would be nice. So basically the data will live on the warehouse, a nightly job will copy the data I care about to a secondary drive on my desktop and roughly once a month I'll copy the data over to a usb drive that will otherwise be stored in a fire resistant lockbox. 

Option 2 is I buy a giant usb drive and do a monthly backup of everything on the warehouse regardless of how much I care about it, while still maintaining the intermediary backups of stuff I do care about on the desktop. Since the change rate is so low, and the price difference isn't terrible this will likely be the route I go.

Based on a quick review of what's out there it looks like Borg may be a good starting point, it also takes care of the dedup so zfs won't be needed. 

### Implementation using Borg ###

#### The Borg Server ####

For now this will be my desktop. 

* Start by installing borg with pacman -S borg.
* install/configure sshd if not already done
* create/configure borg user (`useradd -m borguser && passwd borguser`)
* create folder for backups (i.e. /mnt/backups && chown borguser:borguser /mnt/backups) and create folder for repos
* create folder for each client being backed up (i.e. /mnt/backups/repos/server1.home.lab)

#### The "New" Borg Server ####

This is still on my desktop but after destroying my Arch install, I came to my senses and put it in a VM. So if someday I decide to move it to a real server I can. We're going with a boring CentOS 7 install, with a 100 gb volume for backups. Once I get the server set up I'll work out "off-site" backups of it. I'll also try to set it up in ansible. 

#### The clients ####

* copy public key to backup user on borg server
* install borg (on centos need epel-release and then borgbackup)
* make client directory on the backup server (mkdir /mnt/backup/borgbackups/repos/<client_hostname>)
* initialize repository: borg init backup@titanium.home.lab:<client_hostname>
* configure backup script and cron job (example 00 03 * * * /root/backupengine.sh)


sample backup script:
```
#!/bin/bash

REPOSITORY=backup@titanium.home.lab:engine

export BORG_PASSPHRASE='borgpassphrase'
engine-backup --mode#backup --file#backup --log#backup.log

borg create -v --stats $REPOSITORY::$(hostname)_$(date +%Y_%m_%d) /root/backup /root/backup.log

borg prune -v $REPOSITORY --keep-daily=7 --keep-weekly=4 --keep-monthly=12 --keep-yearly=3 --dry-run
```

Sample ssh config on borgserver (note I had to mangle things a bit since bookstack doesn't like long lines
```
command="cd /mnt/backups/repos/wiki.home.lab; borg serve --restrict-to-path /mnt/backups/repos/wiki.home.lab",no-port-forwarding, \
no-X11-forwarding,no-pty,no-agent-forwarding,no-user-rc ssh-rsa superlongsshkey
```

### Borg Review ###

Borg looks good, but most of the documentation appears to be written by/for someone already familiar with borg. I don't necessarily think it needs an idiot's guide, but clarification or a full run through would be nice.

Finally got a backup to happen. Borg documentation really needs a complete re-write, much of the information is poorly conveyed or conflicts with other parts of the documentation.

And, we've got the ovirt engine completely automated (in theory). Now, its just a matter of testing a restore and confirming the prune command works.

Automating with cron was straightforward. I still haven't tested a restore yet but don't foresee any issues. In summary, once you fumble through the documentation on a couple real world set ups, you'll be comfortable, but the documentation could use some polish.

### To the Cloud! ###

I've been trying to figure out an "off-site" backup solution and also got an email about automatic backups for my digital ocean VM and it got me thinking that maybe I should consider cloud backups after all. So I'm thinking I'll move to restic backups going to Backblaze B2. Pricing looks like it should be like $10/year which seems suspiciously low.

## 2021 Update ##

It's for real time to be an adult and really set up off site backups. I've got roughly 500gb of data worth backing up now. I've got 2 main options for off-site storage: Borgbase or Backblaze B2. Borgbase has the advantage of being borg native so I wouldn't have to download the entire borg repo to restore a file, but I think it will end up being 2-3x more expensive. I'm still leaning toward backblaze simply because I'd have a bit more control over the encryption. I've seen a fair number of comments stating borg's encryption is kind of iffy and I do have a few financial documents with sensitive information making good encryption a requirement.

So I think I'll add another hard drive to titanium and set up a VM to handle the backups and upload them to backblaze. Then I'll have both a local backup and the off site one.

Finally shipping borg backups over to backblaze, took about a week to upload the initial 500gb set.