# Version 2 (or 337): #

I've been trying to get this storage server working well for way too damn long. In any case I think I finally have a good idea of my requirements and what is really necessary. After running our primary storage at work off of an nfs server with just 2 drives in raid 1 and a gigabit connection I realize the fibre channel while neat is probably overkill for most of what I'll be doing. That combined with the big SSD I have in the primary KVM host means I don't really need fast storage. Now that speed is no longer a concern the only challenges remaining with the supermicro server is >2 TB drive support, lowering power usage, and decreasing noise. I hope to be able to get the large drive support and lower power usage accomplished by changing the motherboard/cpu out for one of my old desktop ones that is just collecting dust. While not the coolest solution it should get the job done. I think I'm also going to need a new backplane that can both handle SAS2 and do direct sata ports so that I don't get stuck buying a new raid card. But that would have been necessary no matter what route I went. So now that we have the improvements from the previous version out of the way we'll get into the build.

Another part of this build will be cost analysis. Have I actually saved any money doing it this way vs buying a NAS appliance? If not how much additional storage usage would I have to reach to make this worthwhile? What if I did an all new whitebox build?

## Requirements ##
* NFS server
* at least 6 TB of usable storage
* owncloud (put on a VM and use the nfs mounts?)
* FTP Server (available, probably not required to be always on)
* SELinux

## Build Sheet ##
* 2U 12 bay supermicro chassis with dual 750w PSU ($220 + $50 for sas2 backplane +$5 for non-supermicro front panel connector)
* MSI 890GXM-G65 with dual core phenom II (leftover from old desktop)
* 4 GB DDR3 RAM (leftover from old desktop)
* LSI SAS1068E ($30+$35 before getting supermicro chassis)
* mix of 1-3 TB drives (2x3TB $62.50 each, 2 TB $55, others leftover from other builds)
* Mellanox 10gb nic ($20)

Total Cost (so far): $540

## Software ##
* Centos 7 Minimal

## Testing ##

Based on the previous iterations and problems I've run into, testing hardware compatibility has become a first priority. To start with I plan on ensuring the LSI SAS card plays nicely with the motherboard. Until I get the backplane for the supermicro board I might throw everything into an old cooler master case I have. Sure i'll be limited on total drives and no hot swap but I just want to get this working for now, so I can stop hosting this on my main desktop. 

Initial test of just the sas expander card works with the mobo so I'm going to install linux and throw everything in the supermicro. CentOS 7 installed fine, now just waiting for new backplane and front panel connector to get everything in the supermicro.

Well, I got everything into the supermicro chassis with the new backplane. It will power on, but doesn't seem to be getting past POST. Not sure what it's upset about but didn't feel like troubleshooting right now. Ruled out PSU, sas card, and other drives. Only other non-standard thing currently configured is the lack of front panel connector so only a power switch is connected, but I wouldn't think the mobo would care about that. I guess we'll see when it arrives. 

IT LIVES!! Further mobo testing outside the chassis worked, so I put in some risers for the mobo in case there was some metal contact going on and now all is happy. Backplane is working great and we're in business. Last things to do hardware-wise is the front panel connector and getting some smaller fans that aren't so loud to help cool the drives. I checked the power usage and we're just over 130 watts which is less than half of what it was using originally and I don't foresee dropping it too much lower without things getting expensive. In any case I think we've got a good set up here and short of adding/replacing drives I don't foresee any additional hardware changes in the near future.

## Software ##

Now that we the hardware working and the OS is installed it's time to finish configuring the services. 

First hiccup on the software side. When checking the status of the btrfs filesystem its not seeing the 3TB drive however fdisk and dmesg show it being there. I'm going to run an update and reboot and see if it finds it after that. Still giving errors stating "devid 1 not found already", but it does mount ok if I mount with a different device. I think i'll remove the disk, wipe it, and re-add it tomorrow. It turns out this was just due to the partition on the disk and once i deleted that everything is happy. Not sure why it didn't create any issues before. 

### NFS Server ###

```
yum install nfs-utils
systemctl enable rpcbind
systemctl enable nfs-server
edit /etc/exports
mnt/warehouse/lab 192.168.1.0/24(rw)
systemctl start rpcbind
systemctl start nfs-server
```

### Samba Server ###

```
yum install samba
edit /etc/samba/smb.conf
[global] 
       server string # Samba Server Version %v 
       security # USER 
       log file # /var/log/samba/log.%m 
       max log size # 50 
       server max protocol # SMB2 
       load printers # No 
       idmap config * : backend # tdb 
       cups options # raw 
[movies] 
       path # /mnt/warehouse/movies 
       valid users # @moviegroup 
       read only # No
Create group moviegroup and add users
Create samba user smbpasswd -a <user>
systemctl enable smb.service
systemctl start smb.service
```

### Owncloud ###

```
rpm --import https://download.owncloud.org/download/repositories/9.0/CentOS_7/repodata/repomd.xml.key
wget http://download.owncloud.org/download/repositories/9.0/CentOS_7/ce:9.0.repo -O /etc/yum.repos.d/ce:9.0.repo
yum clean expire-cache
yum install owncloud mariadb-server
systemctl enable mariadb
systemctl enable httpd
systemctl start mariadb
systemctl start httpd
browse to https://server/owncloud
```

### 10gb NIC ###

Found some dirt cheap 10gb nics so i picked up a couple. Not sure how I'll use them yet as I was originally thinking of doing some iscsi but realized I don't really have the drives configured in a good way to do that. I might just configure it to handle nfs traffic and see how that goes. Also I might move one of the ssd's into the server and set it up for bcache since the 10gb link should be fast enough to utilize that. And that went much better than I expected. A quick iperf test showed it was able to hit close to line rate and I just set up the servers in a /30 and remounted the nfs using the 10gb link. 

### From the Future 5/2/16 ###

Currently it is February 2020 but I've been looking to do some updates and noticed this was missing. Looking at the anaconda file from install time it would appear I installed the latest iteration of the server back on 5/2/16. So btrfs has been running for going on 4 years now. 

### Minor Update ###

Ran into some problems with the kvm host running selinux using the nfs datastore. Also seem to be getting IO errors when using the nfs over 10gb...well actually over either of them. 

### Minor Update 6/11/16 ###

I was having some major nfs issues with writes from my desktop. I changed the scheduler on the server to deadline and on the desktop removed the atimeo and as many other non-standard settings as I could and that seemed to help but performance still seems shittier than it should be. After making similar changes on vanadium, vanadium was still having issues if using nfs as the storage for a VM disk. I changed the exports for vanadium to be async and that at least allowed me to get through an OS install. 

### Update 6/18/16 ###

I decided to kill owncloud on the server due to the project going all wonky. And since its getting hot outside and the AC is running constantly I figured I’d power down the server in general when not using it , since we’re really not doing a whole lot with it. At some point I might throw plex on there since it seems to be popular and more useful that owncloud for our typical use case. 

### Update 7/1/16 ###

I removed owncloud and installed emby. Now I can use the roku to play movies and its pretty nice. I may move emby over to  a VM but haven’t decided yet. Also once I get the important files moved I might set up an ssd as bcache. 

### Update 7/16/16 ###

I moved vanadium to using ovirt and with that nfs instead of local for the storage. I currently have the VMs on the warehouse btrfs filesystem, but will likely add the big SSD and present that as nfs as well. I realized to turn emby into a VM I should probably have it connect over the 10gb link, but since I only made that a /30 originally I'll have to reconfigure it. Also ovirt might not let me do 2 different IP spaces for each host if I stick with the direct connections. Hmmm that might put a damper on the HA. I may move warehouse over to vlan 30 now that I have my desktop configured for vlans, but I suppose I would also have to do some work on the ovirt engine and host to support that. Also if I do move emby to a VM I'll need to keep it on vlan 1 so the wifi can reach it. Also still looking for a reasonable way to get the bcache set up, since to do it cleanly it will require reformatting the btrfs volume. Also forgot to mention I put in a few quiet desktop fans to help get some air movement without sounding like an airplane.

### Update 9/4/16 ### 

thinking about trying glusterfs, also finally going to implement real backups.

### Update 1/26/17 ###

Likely going to move this documentation to a different format, but also planning on making the storage more production level. That is I need to get the engine VM off of it, possibly move the media off of it and go to a strict block device set up, also need to remove the web server hosting install media. Or ideally just move it to a hyper-converged set up and abandon the dedicated storage server all together.

### Update 6/30/17 ###

Here we go again... moving the storage server back into the desktop to save on power and noise. Going to attempt to use vlans and bind mounts to segment things so hopefully it won't really affect the desktop parts. 

# Version 1: Fibre Channel DAS (cheapest imaginable) #

To begin learning about storage and fibre channel I picked up a couple of single port 4 gb hba’s for $7 each as well as a single quad port hba ($25) for the storage server. Initially I plan on having 5 drives in zfs. Depending on how it performs and how fast i go through storage i may add a sas expander and go all out. For now I’m just going for the cheapest SAN imaginable.

http://blog.allanglesit.com/2011/08/adventures-in-zfs-configuring-fibre-channel-targets/
http://pubs.vmware.com/vsphere-50/topic/com.vmware.ICbase/PDF/vsphere-esxi-vcenter-server-50-storage-guide.pdf

The SAN currently has a 3 drive raid-z that was previously used as a fileshare and a 2 drive mirror. It was originally going to be a 5 drive raid-z but I thought I would see how the performance differed. On the mirrored drives read performance seems pretty good, while write performance is rather terrible. Also have only tested thin VMs which could be causing the terrible write performance. 

A follow up for this would be to test the performance using linux and btrfs while also re-organizing the drive layout to better handle the space available. 

Utilizing Fedora and btrfs re-tried the fibre-channel configuration but currently getting hard crashes when using. Disk is failing and IO scheduler was set to CFQ instead of deadline, will test again once drive has been replaced and btrfs volume re-balanced. 
Also installed Owncloud, nice webui, streaming works with chromecast

### DFS ###

Problem: An organization has a large windows file server that is quickly reaching an unsupportable size. A dedicated SAN is not an option due to price and customer stubbornness. 

Solution 1: The customer redesigns their app to handle files in multiple locations or limits user data. Archiving old data to a slower cloud-based storage may also be an option. This would incur development time and cost to the customer.

Solution 2: The customer purchases a NAS device. While not as good an option as a SAN, it would still allow further expansion than the current VM-based solution. Would likely be limited by a 1 gb link but might not be. Would still be a single point of failure though could be made resilient.

Solution 3: The customer uses DFS to distribute the files across multiple file servers. Depending on the current file structure and application there may be no changes other than the location the app looks at or it could require some code modifications and adjustments to the data structure. May also be a step toward using other cloud-storage options for archived data. Would still be single points of failure, but no worse than current single server configuration. There may be additional overhead from DFS redirecting to the correct fileserver.