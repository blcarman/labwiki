I considered using Backblaze B2 for backups, but I'd only be able to do a small chunk of data so I ultimately decided to get a 4 TB external hard drive since I only have about 3 TB of data I care about. To start things off I wanted to do a straight archive of the warehouse so I could then clean it up. Of course I forgot I had some VMs scattered and rsync doesn't take into account thin disks by default. So I had to do a bit of housekeeping and add the --sparse flag. With all that I now have a solid archive of the warehouse which can be achieved with the following rsync command. I added a couple excludes since they don't really matter as far as backups are concerned. Still we're down to 100GB free on the backup usb drive so might have to get another one or get a bit more creative in the future.

`mount /dev/sdi1 /mnt/backups` (most like sdi1, but doublecheck
`rsync -av /home/borguser/ /mnt/backups/borg/`
`rsync -av --progress --sparse --delete --exclude 'snaptest' --exclude 'lab' /mnt/warehouse/ /mnt/backups/`


Now, I realize I did something a bit backwards here. In organizing the warehouse I wanted to start using btrfs sub-volumes and snapshots. However something I didn't think about was that I could use btrfs-send instead of rsync for these sub-volumes. I also may not need to back up the Adrienne movies since I think she already has a copy of them but whatever.

Pro tip: always unmount (and physically detach) your external backup drive after backups.

With the initial backup done it's time to finish the housekeeping on warehouse.

First, we'll wrangle the ISOs: `find . -name *.iso` and get rid of the stupid ovirt directory structure.

Next, we'll find some duplicates with fdupes. I haven't used this tool before so the warehouse seems like a great place to test it out. `fdupes --nohidden -r /path/to/dir /path/to/dir1`. It turns out I have a lot of junk from some failed programming projects I toyed around with back in college. In all likelihood since I haven't looked at them yet I probably never will but I just can't let them go. So I'll throw that whole /mnt/warehouse/archive folder into a tarball and at least it can stay out of the way. 

Quick preview of what's ahead: I realized I have a 1 TB OS drive on warehouse so I can use that for the borg backups instead of the btrfs volume, so I really don't have to clean up the warehouse as much as I was planning on.

Cut down the number of dupes quite a bit but they were mostly small files so didn't really free up any space. I'm a bit surprised as I thought I had more crap laying around, but I guess most of it is media and then a small number of old VM images. 

So we've got the warehouse in a good state, still need to move some things around but that can happen later. Before we set up backups I need to add a vlan 30 interface so I can get it off vlan 1 finally. 

To prepare I unmounted slowwarehouse from vcenter (though it was actually only mounted on 1 host and using the .20.5 address). I left ISOs mounted since its the HA heartbeat datastore, let's hope I don't break connectivity. A couple blips and double default routes but at least I didn't have to console in.

I forgot to update the exports but luckily I rebooted the laptop to test so I didn't have to figure it out later. With that done its time for borg. And after that the last little thing to do will be to update firewalld and lock it down a bit.