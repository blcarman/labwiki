Having the lab/storage on the desktop has been working out ok but there have been a few times where I've run out of resources or selinux has caused issues. I started looking at getting a real NAS when I remembered I have a mobo/cpu ready I just need a case. And even without the existing parts I can't justify buying a NAS when I could build something either much cheaper or more powerful without the vendor lock in.

## OS ##

Fedora? known, comfortable, but big upgrades every 6 months and nothing really new to me
Centos? not the best track record lately and btrfs support may not always be there
Ubuntu? I just don't have the trust/faith in it for something like this
* Opensuse? probably the best btrfs experience
proxmox + truenas? maybe but for single host does it really offer anything over my current libvirt setup?

### OpenSUSE ###

Trying to look at this objectively. I've run into a couple frustrating issues that were probably mostly my fault. The installer tries to make it easy and assumes certain options and then allows you to change them if needed. But it's not always smart about what it accepts and what it errors on. I have a bit of a special set up (and really probably should set a native vlan on my trunks) but setting up the vlan interface in the installer took some figuring out. And I still ended up forgetting to put in a default route because I kind of expected it to be there since it was the only interface with an IP. For selecting package groups it would be nice if they provided a list of packages instead of a vague description like "useful things for doing things". But other than that the installer was straightforward and on par with others.

I decided to further embrace the SUSE and configure libvirt via yast. Of course the documentation references the gui version but apparently there is a tui as well. It asks if you want to configure a bridge and I choose yes, but it oddly errored out over my extra btrfs filesystem at that point. I'm not sure if it successfully created the network bridge and then went on to do some filesystem stuff but it still shouldn't matter if I have an unmounted filesystem.

So as much as I was hoping this would be the time I finally embrace OpenSUSE and find something to replace Fedora/RHEL it seems the search continues. That said I am going to keep running OpenSUSE here to gain more familiarity. 

I do like zypper and it seems more intelligent than either apt or yum about what things need restarting after running an update.

### Nebula ###

I was going to run all my services in VMs with nebula on the host, but as I think about it I think I'll also need to run nebula in the VMs unless I put something like haproxy on the host. With that I think it might also make sense to put nextcloud directly on the host for better performance. 

I got nebula set up fully automated with netbox and ansible, so doing it at the VM level shouldn't be a problem but I also have it running directly on the storage server for the flexibility.

### Nextcloud/Seafile ###

Performance was awful when testing on kubernetes with sqlite, like to the point it wouldn't even load the website let alone let me doing anything. So I found seafile and I think it might fit my use case better. I kind of wanted to be able share pictures with family, but I don't think any of them would actually use nextcloud even if it was available.

## NFS ##

I don't have any plans to bring up the vmware environment but am keeping that nfs config in case I do. Also going to set up some nfs exports for kubernetes dynamic provisioning. Maybe someday I'll actually get ceph working, but I've got plenty of other stuff to deal with first.

# Reliability #

Surprisingly from a hardware perspective the storage server has been rock solid for years now. The same cannot be said for the software side, that is I can't be trusted not to go messing with things. So even if it's cheaper to DIY this, it kind of feels like it might be time to buy a NAS that can only do storage stuff to prevent me from breaking it on a routine basis. So I've been looking at the QNAP TS-673A, but reviews of the QNAP software have not been great, though fortunately I should be able to install whatever OS I want if I don't like the builtin software. Synology and Truenas hardware still doesn't really excite me for how much more it costs. The other option is I still put together my own hardware and then run something like truenas. That could save me something like $500 compared to the QNAP since I do already have cpu and ram. I might even be able to drop that down to the price of a case since the motherboard I thought was bad was actually most likely just that bad graphics card I had. 

I've wasted too much time on this. Long story short even if I went a little crazy and got an Epyc 3251 system I could stay within the $900ish budget that a QNAP/Synology/Truenas system would cost which is kind of insane. I am tempted to do it that way, but at the end of the day I should be a bit more sensible and just pick up another Fractal R5 case and Corsair/Cooler Master power supply and use up the rest of the parts I still have laying around. I do plan on moving my main desktop to the new Fractal, since the Phantek case has not been as great as I wanted it to be. It's really not bad but there's no 5.25" drive bay and its not as quiet as I had hoped (to be fair part of the noise is the cpu cooler constantly changing fan speed in an obnoxious way).

So the storage server will have just storage, jellyfin, and unifi controller (and any other future "home" services). Then I'll have a lab server doing all the heavy k8s stuff, and my desktop will have small short-lived dev stuff. I guess I could technically make the r610 the lab server instead of doing bare metal k8s on it...

# Synology #

* [DS1621+](https://www.newegg.com/synology-ds1621/p/N82E16822108752) - $900
* [2x16gb RAM or similar, need to confirm it doesn't require ecc ram](https://www.newegg.com/patriot-16gb-260-pin-ddr4-so-dimm/p/0RN-002U-002G1?quicklink=true) - $105
* 6x6tb drives, having trouble finding drives on the compatibility list on ebay, so might have to break the rules here, might also start with 4 drives and possibly even re-use a couple 3tb drives, though I don't remember how many I have working, at this point I just need 10tb usable :) - $540
* 2x 500gb sn700 m.2 cache drives - $150

So it's finally time to stop fucking around with the storage server. When I moved stuff to the supermicro chassis it messed up something with the boot order and the crawl space is not fun to sit and troubleshoot in. So I either need to get a board with ipmi or something purpose-built. I want file storage to be rock solid and not break on a whim when I decide to do something silly. I can keep the supermicro chassis around and build it into a lab server or whatever. Ironically, I bet the lab ends up being super reliable after I get the NAS. 

## Setup ##

The synology has arrived. Install was simple. Not really unexpected, but it oversimplifies/dumbs down certain things while not really explaining others. For example, I went to install the Virtual Machine Manager and it wants to install it on a volume, but doesn't really explain why/what. Should I create a dedicated volume for packages? Is the volume for storing VM images? Went ahead and installed it, now it failed configuring the open vswitch but didn't actually stop the configuration so its just stuck on a spinning progress bar... closing it and re-opening seemed to fix it.

When making an iscsi lun or a volume it doesn't provide an option to disable COW, does it automatically do this? It seems COW and CRC32 checksums are "Enable data checksum for advanced data integrity" on shares.

It looks like there is no benefit to multiple volumes, so deleting the 2nd test volume and making 1 giant volume.

Going to install the samba directory server but not 100% sure if I'll use it. Should I join my desktop to the domain? kerberized nfs? I still would kind of prefer something like seafile if it didn't have to be blobs behind the scenes...

enabled ssh (and home dir service) though hopefully I don't need it. Also no option to limit to keys only so may end up disabling it again.

should probably doing more of this config as code, but hopefully most of it is set and forget

synology drive is a bit janky, had to rebuild the deb as an rpm and connect using the IP or else I'd get a really lame message about upgrading my server version. Actually I guess that means the unofficial flatpak should work after all. 

https://github.com/SynologyOpenSource/synology-csi got it working but seems like it requires admin rights for the user which is less than ideal

need to fix packer template for ubuntu to work on synology (just need to fix networking really) and create ansible playbook to deploy

The api clients look less complete than I initially expected. Looks like I might have a project there, need to create an api client for at least the virtual machine manager and hopefully also make an ansible module for it.

Why are snapshots managed in the snapshot replication manager? just a little confusing

media server/video station being separate doesn't really make sense, also video station doesn't see any of the files so I'll probably need to use jellyfin anyway

Hyper backup can do client side encryption so should be able to replace the borg/rclone combo hopefully, not sure how it does with deduplication. I must have copied the password wrong, but the encryption key file works to restore at least.


# blog posts #

* decision to move to synology
* basic storage setup/migration
* synology apps
* VMs I plan on running


## I got a Synology DS1621+, but why? ##

I had a Frankenstein storage server since around 2013. I remember starting out running OpenIndiana and attempting to do fibre channel. Around 2017 it ended up on Centos 7 trying out btrfs in raid 10. I expected to run into issues with btrfs, but that part was actually rock solid despite running a bunch of used enterprise drives of mixed sizes on a random mix of desktop and used server parts. It turns out the biggest impact on reliability was myself. I couldn't just leave the server alone, and I would often get bored mid-project so the couple hours of downtime would turn into a couple months. 

The original solution was going to be Freenas, but that felt a little too limiting. Then they announced Truenas Scale built on Linux and supporting all the things that usually get me in trouble. Also the Truenas mini are just not what I wanted from a hardware standpoint. That brought me to the Prosumer players: QNAP and Synology.

I know a lot of people using Synology, but I couldn't justify the cost for the ridiculously whimpy Intel CPUs they were using. QNAP had some compelling AMD Ryzen solutions but they were not getting glowing reviews on the software side. Finally, Synology started offering a series using the AMD Ryzen V1500B which is the sweet spot I was looking for. It should be handle the storage and the few "critical" VMs/applications I want to run.

I picked up a DS1621+, 4x6TB Western Digital Red Plus drives, and 2x500GB Western Digital SN700 cache drives. All said it cost around $1800, that amount is still a little insane to me. The previous Frankenstein build had probably cost somewhere around $1000 over its almost 10 year lifespan, but most of that was $50-100 at a time (and who knows how many hours spent trying to get the hardware to cooperate). I expect the Synology to last at least 5 years but will likely have to add/replace a couple drives along the way, so it's still going to end up more expensive. From what I've seen so far I think the extra cost will be worth it, though that doesn't mean it's perfect. It would be much nicer if it had a modern html5 interface and more complete REST APIs.

I'll hopefully write a few more of these posts further describing the migration and what all I end up doing on the DS1621+.

## Synology Configuration ##

I copied data over nfs with rsync primarily. Forgot about sparse files at first, so synology space used ended up a little higher. At some point I need to go through the old lab VM disks and truly delete the ones that don't matter.

I also tried to go all in on the Synology apps and it worked out for the most part. Their samba directory server was easy to setup and nicer than trying to do it from scratch. Their drive/office apps were also very good, though the drive client was a little painful to set up on Fedora, and gave some bogus error messages along the way. The video station/media server were confusing and didn't work at all, so I will be sticking with Jellyfin. I've built a VM to run K8S/RKE2 to handle containers since I don't really trust docker on the Synology host. Hyper backup appears to be able to replace my previous borg/rclone combination. Also I have no plans for replication, but apparently all snapshots are handled in the snapshot replication app. And last but not least I got the K8S CSI installed on the RKE2 VM for storage provisioning there. Oops look like I spoke too soon and there is a bug with the volume attacher https://github.com/SynologyOpenSource/synology-csi/issues/2. Also the documentation doesn't really have an upgrade procedure, so I guess I'll try deploying over the existing one and see what happens. It looks like simply re-running the deploy script works for updating, will see if that fixes my issue.

There were some speed bumps and hiccups along the way, but not really anything worth writing about. Most were solved with some quick googling and reading on the Synology KB.

I guess I do have a few minor complaints. The first is a few of the security settings. I enabled ssh but couldn't force it to use ssh keys. I also can't disable old versions of NFS or SMB. Second, is the lack of a remote console on VMs. It's using libvirt so it would have been nice to make the VM consoles available over spice or vnc, but that doesn't seem to be possible. But those are both fairly minor concerns and I have no regrets with the move yet.

## RKE2 Server ##

* jellyfin DONE
* nebula DONE (should probalby lock down the rules a bit more though)
* reverse proxy for warehouse DONE
* cert-manager DONE
* unifi (just keeping the plain VM for now, though would be nice to get a real cert for it)
* prometheus
* loki
* awx?
* home assistant
* grocy?
* harbor?
* kanban board?
* netbox?

when using csi, do I need to specify filesystem or block? or is something else the problem, getting this error on the pod 
```
AttachVolume.Attach failed for volume "pvc-c579b065-c0dc-4be1-b093-134159b6c12e" : Attach timeout for volume 475ab47f-4d5a-4715-9a06-7fec153490bd
```
Turns out the Synology CSI only supports up to kubernetes 1.19 which used a beta api for the volume attacher. Fortunately someone forked it and updated it to work on newer versions. 

Something new the Synology enables is a prod environment. Previously, I had a lab that happened to run a few home services. Since the lab was meant to be broken the home services were never really stable. The DS1621+ has enough cpu power to run a few VMs for the non-storage "prod" services. And now the lab can be a consumer of those services instead of the snake eating its own tail. Furthermore, this should enable me to fully run the lab from my desktop without worrying about it running 24/7. Not to get too far off track, but this would also mean the switch is the last part of rackmount hardware I have running (which I've been contemplating replacing anyway). It truly is the end of an era for me. Why didn't I set up a prod server sooner? It's taken a while for me to gather up enough things to justify it and for a long time the lab was the point and these services were just there to help simulate the real world.

So I have this prod environment now, what am I going to do with it? Here's a probably incomplete list of the things I have in mind currently:

* jellyfin DONE
* nebula
* reverse proxy for warehouse DONE
* unifi controller
* home assistant

* netbox?
* awx?
* grocy?
* prometheus
* loki
* harbor?
* cert-manager
* pihole?

I think I'll be able to run all of these in kubernetes, so will probably end up with 1 beefy VM instead of several small ones. There's also a bit of a split with only a few really being household services. 

This is where the Synology becomes interesting, though none of this really depends on the Synology. Previously I tried making my home k8s clusters more enterprise-grade, generally I had 3 controlplane nodes and a couple of workers. Since I really only have 1-2 physical machines, with limited resources, I would end up with various problems that made things not so fun. With the Synology I'm changing things so that I only have a single node k8s cluster running in a VM on the NAS. This vastly simplifies things and reduces cpu overhead. Now I can do the fun stuff with kubernetes that I wanted to instead of troubleshooting problems caused by my very special setup.

The next major win was the Synology CSI. Before I had been using an nfs provisioner which had some issues with permissions/security. Now, I can have ISCSI LUNs provisioned automatically as needed. 

Now, I am treating my Synology (and its VMs) like pets a little bit. I'm manually configuring a lot of stuff because it's going to be set and forget (hopefully). Or when I do have to touch it next hopefully I'll finish the automation then.

### Cert Manager ###

I guess I didn't do a good job of documenting the cert-manager install/config.

* straight from their install docs `kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.6.1/cert-manager.yaml`
* generate a digital ocean API key
* actually I think I ended up not documenting it because all the stuff I would have documented were just mistakes/typos I made. If you follow the cert-manager docs correctly it'll just work. It might be necessary to create the secret in the cert-manager namespace if you do a clusterissuer though.
* need to add subdomains as domains in digital ocean dns, strangely... actually I bet this was just because of my silly local dns config
* ugh forgot i made a home.bradlab.tech domain locally, had to fiddle with the cert-manager deployment and add the following `--dns01-recursive-nameservers-only --dns01-recursive-nameservers="1.1.1.1:53"`, that will cause me trouble if/when I upgrade, might be a good reason to use the helm chart instead

So yay cert-manager working, that might be a good stopping point for today. This has been super cool, but one thing I did realize is that technically I've done all this before, so while my brain thinks I'm making tons of progress, really it's just repeating progress I already made previously. Just kind of interesting how my brain decides some things are fun and others are boring. Like if I do this again I'll probably hate myself for not automating it the last time. Also been kind of fun because its the first time in quite a while that I've had music going in the background and completely tuned it out. Like I needed it to get in the zone but once I was there it no longer mattered. I wonder if I'm recovering from some burnout, but again most of this isn't new so I'm not getting stuck as much as I otherwise would be, like there have been speed bumps but no walls to block me.

### NFS Proxy ###

Next up, can I get nfs working over nebula via nginx ingress as well? Looks like I might need to add some extraargs and a configmap. This was fairly straightforward. I'm only worried about nfs 4.1 so just had to allo tcp 111 and 2049. One kind of interesting thing is my nfs client address still showed up as the default interface instead of the vpn interface making the connection. Nevermind, this appears to be starting the connection but then still connecting outside the vpn. I found the fix, need to enable connections from high ports (i.e. insecure) since nginx doesn't know anything about nfs specifically.

```
172.16.10.2:/volume1/media on /mnt/media type nfs4 (rw,relatime,vers=4.1,rsize=131072,wsize=131072,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=192.168.30.15,local_lock=none,addr=192.168.30.16)
```

https://kubernetes.github.io/ingress-nginx/user-guide/exposing-tcp-udp-services/

### Metrics and Logging ###

make syno storageclass the default 

kubectl patch storageclass synology-iscsi-storage -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'


from loki docs:
```
helm upgrade --install loki grafana/loki-stack --set fluent-bit.enabled=true,promtail.enabled=false,grafana.enabled=true,prometheus.enabled=true,prometheus.alertmanager.persistentVolume.enabled=false,prometheus.server.persistentVolume.enabled=false

helm upgrade --install loki grafana/loki-stack --set grafana.enabled=false,prometheus.enabled=true,prometheus.alertmanager.persistentVolume.enabled=false,prometheus.server.persistentVolume.enabled=true,prometheus.server.persistentVolume.size=50Gi,loki.persistence.enabled=true,loki.persistence.storageClassName=synology-iscsi-storage,loki.persistence.size=50Gi
```

Release "loki" does not exist. Installing it now.
Error: unable to build kubernetes objects from release manifest: [unable to recognize "": no matches for kind "ClusterRole" in version "rbac.authorization.k8s.io/v1beta1", unable to recognize "": no matches for kind "ClusterRoleBinding" in version "rbac.authorization.k8s.io/v1beta1"]

looks like the error above is due to the loki chart having some old dependencies, so instead of installing everything at once, we'll do loki on its own.

```
helm upgrade --install loki grafana/loki-stack --set grafana.enabled=false,prometheus.enabled=false,loki.persistence.enabled=true,loki.persistence.storageClassName=synology-iscsi-storage,loki.persistence.size=5Gi
```

hmm, I might have to make a new storageclass to provide the pvc as a block device instead of fs, so the container can manage it
```
level=error ts=2022-01-03T22:37:03.850221475Z caller=log.go:106 msg="error running loki" err="mkdir /data/loki: permission denied\nerror initialising module: table-manager\ngithub.com/cortexproject/cortex/pkg/util/modules.(*Manager).initModule\n\t/src/loki/vendor/github.com/cortexproject/cortex/pkg/util/modules/modules.go:105\ngithub.com/cortexproject/cortex/pkg/util/modules.(*Manager).InitModuleServices\n\t/src/loki/vendor/github.com/cortexproject/cortex/pkg/util/modules/modules.go:75\ngithub.com/grafana/loki/pkg/loki.(*Loki).Run\n\t/src/loki/pkg/loki/loki.go:241\nmain.main\n\t/src/loki/cmd/loki/main.go:131\nruntime.main\n\t/usr/local/go/src/runtime/proc.go:225\nruntime.goexit\n\t/usr/local/go/src/runtime/asm_amd64.s:1371"
```

tried new loki image loki.image.tag:2.4.1 but no change, not sure what's going on


```
helm upgrade --install loki grafana/loki-stack --set grafana.enabled=false,prometheus.enabled=false,loki.persistence.enabled=true,loki.persistence.storageClassName=syno-block,loki.persistence.size=50Gi
```

hmm seems like maybe the pvc/pv wasnt getting deleted when uninstalling the helm chart, so maybe that had some bad config? I guess not, even deleting and attempting to recreate with the block storageclass I created didn't make a difference. Tried one final time with the normal storageclass and still not working. I guess I'll have to dig more into the permissions after all. I had to set all the securityContext stuff to get it to run as root, but I finally got it to at least run.

```
helm upgrade --install loki grafana/loki-stack --set grafana.enabled=false,prometheus.enabled=false,loki.persistence.enabled=true,loki.persistence.storageClassName=syno-block,loki.persistence.size=50Gi,loki.securityContext.fsGroup=0,loki.securityContext.runAsGroup=0,loki.securityContext.runAsNonRoot=false,loki.securityContext.runAsUser=0
```

Soon:
* create syno pvc
* create whoami pod and mount syno pvc, inspect permissions
* repeat with securityContext setting user/group/whatever
* power on build1 vm, and/or move gitlab runner to k8s (ugh its on the ssd that did not get migrated, so probably building a new one)
