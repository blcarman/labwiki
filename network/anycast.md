# Anycast #

I've heard "anycast" thrown around a lot for dns, but haven't actually had any exposure to it. Recently, a friend mentioned it and I started trying to think of a reason to use it. My first thought was as a replacement for keepalived to enable active/active haproxy. And like the common use case of dns to enable services to only need to know about a single IP to reach the server closest to them (i.e. local log forwarder).

So the adventure begins. Surprisingly google search was failing to return anything terribly useful when looking up linux anycast so either I'm going about it the wrong way or I'm missing something. In any case these notes will hopefully act as a springboard for other people who are decent with linux and have a fairly basic understanding of routing.

Just kidding this article provides a starting point, https://www.linuxjournal.com/magazine/ipv4-anycast-linux-and-quagga. However it glances over a lot of details. It suffers from the same problem just about all these blog post style things do. It thinks you're both an expert and novice at the same time. 

A hopefully better starting point: http://routedo.com/posts/frr-ospf though it does expect a cisco device as a router... Spent more time than I would have liked fighting with virtualbox and vagrant so have not made much progress. And finally got it working with the routedo post. I don't think this is something I could convince work folks to implement as it requires a fair amount of routing knowledge and we'd have to convince the network guys to let us do some stuff with ospf on the core gear as well. 

Overall FRR is pretty neat and should I ever get crazy with routing will probably get more use, but at this point in time I think just knowing the basics and that it exists is enough.