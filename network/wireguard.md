## wireguard ##

### devices ###

* storage server...expose via kubernetes
* k8s apps: run a single worker and I wouldn't have to worry about a VIP, I don't have redundancy in a bunch of other areas so probably not worth making things difficult
* digitalocean... probably no services to expose, just useful for shipping backups and logs possibly, maybe matrix chat server, but that's public for others anyway
* laptop... need to access home from not home
* titanium 
* edgerouter? (or router vm of some sort) if I don't care about literal end to end encryption I could potentially cheat and let the edgerouter decrypt and route the traffic where it needs to go in my home network.
* phone? would be nice to have some sort of files access, but I've made it this long without it

### notes ###

* use dns to prevent any weirdness when connecting from at home
* what if I want to use an external vpn as well, like nord? probably just run it through a DO droplet
* use edgerouter or standup opnsense behind it? opnsense would be nice to keep things off the main router, but will I ever want to access more than just the lab?


maybe I should stick with nebula? https://www.reddit.com/r/networking/comments/iksyuu/overlay_network_mesh_options_nebula_wireguard/
turns out nebula has a fedora copr, so that solves the silverblue problem
if i make the nebula cert a k8s secret, can that pod bounce around to whatever node and work as an ingress IP? then i wouldn't have to worry about any VIP stuff. This feels like a good plan, only downside is it means I need to keep netbox running(or some sort of IPAM), I think that will be fine for just nebula/lab devices