I was reading recently about Mullvad integrating with tailscale. It got me thinking that I should really polish up my home vpn config.

To be honest I really don't use Nebula much since I work from home, and when I'm travelling I'm generally not doing technical stuff.

Is there a problem with letting nebula use Mullvad to talk to the lighthouse? Maybe not, but nebula traffic would still end up getting double encrypted when not at home.

Actually I should start with the non-nebula internal network. That just required putting in a route to bypass wireguard.

And same thing with the lighthouse public IP and I think it's good. That feels way easier than I expected.

DNS is another piece I was forgetting. 

Also, nebula on my phone, that will let me ditch google photos and google drive.

DNS is proving to be a problem. The nebula interface is managed outside of NetworkManager, and systemd-resolved can't be configured per interface directly. I tried adding systemd-networkd per https://github.com/slackhq/nebula/issues/318, but that broke the nebula interface. 

I could set nebula as the global dns server, that's probably the way to go, and just leave off the search suffix.