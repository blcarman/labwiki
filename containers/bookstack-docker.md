The solidnerd bookstack docker image has some problems.

`podman login -u bradmin -p $(oc whoami -t) --tls-verify=false docker-registry-default.apps.bradlab.tech`
`podman push --tls-verify=false docker-registry-default.apps.bradlab.tech/test/bookstack:0.4`

Almost working, need to rebuild image using port 8080 instead. It seems like a lot of people making plain "docker" images are doing stuff as root that doesn't play well in Openshift without making things insecure.

apache logs: https://serverfault.com/questions/711168/writing-apache2-logs-to-stdout-stderr

After what seemed like a million tweaks bookstack is finally running from openshift (indeed I'm writing this now on the container version). For such a simple application it was a challenging process. Mostly due to people doing really bad things with docker. I've also learned a ton about the deployment process in openshift and went from loading a new template and creating a new thing every time I made a change to simply doing a new deployment. I've shut down the original bookstack server and we'll use this one full time to see how it handles. Of course the database is still on the dedicated mysql server.