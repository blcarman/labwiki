This one started as trying to learn docker but turned into ansible testing mostly.

Up to now I've been a passive follower of Docker. It's always looked neat but I was worried it would end up like openstack. But I think at this point its safe to say docker will have a bit more staying power. My original thought was to try out Openshift since I like infrastructure and am more curious about how docker behaves at scale. But at the end of the day I think we've got to start small and work our way up. I've also been trying to figure out a good test strategy for ansible, so this will fit nicely into that project.

Most commonly TravisCI is used as the middle man for testing ansible with docker. And TravisCI might be a fine tool, but I just like self-hosted solutions better. I mean what happens if tomorrow TravisCI decides to change their terms or just runs out of money? And really the main reason for self-hosted is to keep proprietary information from getting out. I love open source, but that does not mean open everything. In any case I've been tinkering with Gitlab recently and it has CI functionality that will likely accomplish what I need. Jenkins may also get some investigation since we use that at work, but it seems a little overkill for just ansible testing.

I did some initial research on testing ansible both with gitlab and others. There are some decent posts about using TravisCI but outside of that most are just quick blurbs saying its possible rather than how. So with this post (or series depending on how out of control things get) I hope to actual dive into the "how" of testing ansible.

# Test Requirements #

We'll start with a fairly standard set of tests (most notably presented by Jeff Geerling on his blog).

* Ansible syntax check
* Ansible role deployment
* Idempotence check
* Desired outcome check

## Syntax Checking ##

This is the easiest part of the test since it doesn't require anything outside of ansible. Currently I have a single playbook that applies the necessary roles to the various hosts I have. This might be fine for the syntax check but might be a little heavy for the other tests.

`ansible-playbook playbooks/apply_roles.yml --syntax-check`

And that's it really, you either get the syntax problem it found or it exits happily. If you're also interested in making your code pretty you can use ansible-lint in a similar method.

## Ansible Role Deployment ##

The earlier commands checked the ansible bits, but did not check what really happens, e.g. what if a value was misspelled. To find these kind of errors we need to actually run the ansible code and see what happens. For this I initially tried using Vagrant. And for certain things it worked really well. And while vagrant can run ansible when it creates a VM its generally just a little heavier than we would want for simply testing an ansible role. What vagrant really excels at is when you are both developing something and testing it along the way. Its also nice for fairly long running local VMs. I say local because vagrant does things that aren't necessarily good in production environments. In any case, for this post its safe to say vagrant does not fit our needs and that is where docker comes in. We want the lightest possible system that still tests what we need to, in this case does the ansible playbook successfully run.

I've seen two ways for testing ansible on docker. The first is to run ansible from inside the docker container and the second is to use a docker container with ssh enabled and run ansible in the classic way. Both have their pros and cons but I like the second one better because it more closely matches a real world. I suppose a third option that might be a good compromise between the other two would be to use ansible inside the container, but still run it through ssh. As you can see this is where we start having options and the world gets a lot more interesting.

For now we'll go with the first option to keep things simple. I realize docker has generally embraced alpine linux but most of my ansible roles are still written around a classic CentOS world so that's what we'll use here. I won't go into details on getting docker running, but Fedora has a nice write up if you need it, https://developer.fedoraproject.org/tools/docker/docker-installation.html.

Here's what I am fairly certain is a "wrong" way to do this. I created the following Dockerfile. Basically it clones my ansible repo and then runs the role locally. There are some test ls commands since the git clone command wasn't putting things where I expected, but I solved that by the specifying the directory for git to clone into.

```
FROM centos:latest

RUN yum install -y ansible git
RUN git clone http://gittest1.home.lab/brad/ansible.git /root/ansible
RUN ls -lh /root/ansible
ADD files/test.yml /root/ansible/test.yml

RUN ls -lh /root/ansible && cd /root/ansible && ansible-playbook -i "localhost," -c local test.yml
```

And we still aren't completely there, systemd is not part of the container by default so those parts of the ansible role will fail. Here's an article from Red Hat about systemd in containers, https://developers.redhat.com/blog/2014/05/05/running-systemd-within-docker-container/

Conveniently while searching for the systemd docker stuff I found molecule via https://medium.com/@tklo/testing-ansible-role-of-a-systemd-based-service-using-molecule-and-docker-4b3608a10ef0. It may prove useful in the long run.

## Molecule ##

Let's take a left turn here and check out molecule, since it could very well be what I'm looking for. Looking at the molecule docs its hard to figure out exactly what/how it works. This may be a case of me simply over-complicating things but is also likely the problem that arises when someone close to the project tries to document it, that is the documentation assumes you already have some knowledge of molecule. But I'm hoping I just need to actually test molecule and everything will fall into place, so that's exactly what we're going to do.

While we're at it we might as well make a pit stop at testinfra, https://testinfra.readthedocs.io/en/latest/. Hopefully we don't just end up repeating https://zapier.com/engineering/ansible-molecule/

### Installation ###

Molecule has a good installation doc for centos and ubuntu but I had to do a couple extra tasks since I run fedora.

`dnf install python-devel redhat-rpm-config`

create a new molecule test dir
git clone into new dir
cd into your role and run `molecule init scenario --scenario-name default --role-name common` This will create a molecule folder inside your ansible role

realize you forgot to install the python docker thing and run `pip install docker-py` without this you'll get a vague error

```
TASK [Destroy molecule instance(s)] ********************************************                                         
failed: [localhost] (item=(censored due to no_log)) => {"censored": "the output has been hidden due to the fact that 'no_
log: true' was specified for this result"}                                                                                   

PLAY RECAP *********************************************************************                                         
localhost                  : ok=0    changed=0    unreachable=0    failed=1                                              


ERROR:
```
which can be made less vague by removing the no_log option from the default.yml

```
TASK [Destroy molecule instance(s)] ********************************************                                         
failed: [localhost] (item={'image': u'centos:7', 'name': u'instance'}) => {"changed": false, "failed": true, "item": {"image": "centos:7", "name": "instance"}, "msg": "Failed to import docker-py - No module named docker. Try `pip install docker-py`"}                           

PLAY RECAP *********************************************************************                                         
localhost                  : ok=0    changed=0    unreachable=0    failed=1                                              


ERROR:
```

ooo a new ridiculously vague error, again not really sure why no_log is set, removing it we get the following:

```
TASK [Create Dockerfiles from image names] *************************************
 failed: [localhost] (item={'image': u'centos:7', 'name': u'instance'}) => {"changed": false, "checksum": "52411a249832f3e29a58dc7aff3feea0eebd1920", "failed": true, "item": {"image": "centos:7", "name": "instance"}, "msg": "Aborting, target uses selinux but python bindings (libselinux-python) aren't installed!"}

 PLAY RECAP *********************************************************************
 localhost                  : ok=0    changed=0    unreachable=0    failed=1   


ERROR:
```

It's actually kind of surprising we made it this far without some selinux fun. `sudo dnf install libselinux-python`

Boom our little detour into molecule has led us to where we were before. So basically molecule handles a lot of the docker stuff for you which is great if you aren't really trying to learn docker. In any case at this point I'm more interested in getting a good ansible test set up than I am about docker, so let's keep hacking away at molecule.

Docker and systemd just really do not like each other. I got past the original systemd error, but now get "failure 1 during daemon-reload: Failed to get D-Bus connection: Operation not permitted" and a recommendation to add some selinux stuff to the dockerfile didn't seem to make a difference. So I guess we'll leave this for tomorrow and the decision of whether to give up on systemd inside of docker or take another look.

Aha! I figured out my failing. I had been adding stuff to the Dockerfile.j2 template and should have been putting it in the molecule.yml file instead. I would love to claim credit for figuring this out on my own but it was actually in https://medium.com/@tklo/testing-ansible-role-of-a-systemd-based-service-using-molecule-and-docker-4b3608a10ef0.

So now we have the basic `molecule test` command working and it applies the roles and then the final step is the yamllint. And we get a bunch of junk from molecule...that doesn't seem right. It turns out molecule only provides a .yamllint file when its creating a new role, so since we used an existing role, it didn't put in the default yamllint ruleset that applies to the molecule code. So how do we get that where we need it? That's a very good question.

The configuration documents ironically have a bit more info than the usage docs but its still not great.

## Usage ##

So now that we have molecule "working" how do we actually use it? Assuming we already have an ansible role, we just need to initialize the test scenario. The first scenario needs to be named "default", though I'm not really sure why.

`molecule init scenario --scenario-name default --role-name <role_name>`

If we are starting a new role from scratch we can simply run and it will create both the empty role folder structure and a default scenario.

`molecule init role -r <role_name>`

Now, we know this default scenario is not set up for systemd, so how do we make systemd part of the default? I'm not sure, but we'll return to that thought. The next molecule command we need is the actual test command.

`molecule test`

It doesn't get any easier than that does it. This will at a bare minimum run through the ansible role twice and finally run yamllint. This might be good enough for basic single node role testing but I'm sure we can go farther and we will. But first we need to solve this systemd thing permanently so we don't have to modify every freaking scenario we create. It doesn't appear that we can specify our own driver, at least not without adding stuff within the molecules site-packages directory which would likely get removed on upgrade. This might be the time to put my terrible python knowledge to use. The question becomes do we simply clone and customize the docker driver, or do we go all out and add support for custom drivers. Well maybe we don't actually need a whole new driver, since we are using docker after all. Looking at the create.yml file we do have some variables available to us, notably we have command and volumes which we had already used above. The final piece is do we use a docker image with systemd already installed, or do we use a different Dockerfile to start with that install it for us. There are a couple references to the Dockerfile `dockerfile: "{{ item.item.dockerfile | default(item.invocation.module_args.dest) }}"` but that doesn't seem very clean to edit. In any case let's take a break to digest this for a bit.

Side note: I took a quick glance at Test Kitchen(kitchen.ci), it didn't look bad but seemed very chef focused... I should have seen that coming.