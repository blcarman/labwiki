# Writing My First Module #

Finally, after years of using ansible I get to write my own module. I have done my own lookup plugin and extended an inventory plugin, however neither of those really used the full ansible collection framework.

## ansible-test ##

There is a huge testing framework available that is both awesome and a bit intimidating. Testing against 10+ versions of python is a bit overkill for me, but it makes sense for a public collection that won't have control over the environment their collection is used in. Otherwise, it's awesome just how much testing you get out of the box. It also looks like it could fill in some of the gaps molecule has. Basically, small integration tests for the easy stuff and then molecule tests for complicated roles.

## Documentation ##

Like most ansible documentation some of it is great, some of it is bad, and most of it makes sense after you figure out what they're talking about. Of course, some of that is my background and some just that writing good documentation is hard. A step-by-step hello world module tutorial would be nice though. 

## Fun Facts ##

* In my lookup plugin I used the display class for showing extra info when running ansible with -vvv, this is apparently not possible with a module. Instead the module just returns a json blob and most modules appear to include info in a `msg` field.

## python-cloudflare ##

This probably belongs elsewhere.

* oops I had environment variables set for terraform that caused wonkiness
* api_endpoint.post to post, otherwise implicitly does a get
* how do I know if I should use params= or data= in the post?
* exception is raised if list already exists, maybe we should check first then

## Critique of the Ansible Docs ##

"in any of the thousands of Ansible modules found in collections, you can easily write your own custom module." This is a good example of "draw the restof the owl". Can I write my own module? Yes. What does the document know about how easy it is for me to do that? Nothing. Finally found this after digging for a while https://docs.ansible.com/ansible/latest/dev_guide/developing_program_flow_modules.html#non-native-want-json-modules 

"When you write a module for local use, you can choose any programming language and follow your own rules". Sure I can follow my own rules, but the doc never really explains the interface for ansible to use my module or for my module to pass information back to ansible. 

Ultimately, could I do better? Or is this one of those things that you just have to do to figure out? I could probably fill in some additional beginner questions maybe. And I guess ultimately I always have that question of am I doing it right? So I think that is the document I'm looking for. 

They mention using a venv but really I prefer a docker container at this point.
Should we write the doc before the code?
Should we add parameters all at once or as we write code that uses them?
What's the correct way to do check mode?
How to properly format the returned object and logging?

Basically I want a getting started guide that builds confidence from the beginning, not after I've banged my head against the wall for a while.

## References ##

https://docs.ansible.com/ansible/latest/dev_guide/developing_collections_testing.html
https://docs.ansible.com/ansible/latest/dev_guide/developing_collections.html
https://docs.ansible.com/ansible/latest/dev_guide/developing_modules_general.html
https://github.com/ansible/ansible-for-rubyists

# Writing Ansible Modules: Your first or 50th #

This my guide for writing ansible modules based on my still relatively small amount of experience. None of my modules have been reviewed by or included in ansible. That is, this guide is mostly aimed at people writing ansible modules for personal or corporate use, not necessarily as someone hoping to get it accepted into an existing collection like community.general. 

To start out, I like to do my development in a docker container instead of a virtualenv. The reasons probably don't belong here, but primarily its just easier to keep it self contained and it most closely matches how I run things in production. 

I also don't care much for supporting multiple python or ansible versions, since my ansible is run in a very controlled manner. If you plan to write a module for the world to use by all means expand it to support as many versions of things as you can.

Next, I like to start with the absolute bare minimum. Make the sanity tests pass, then build it up, keep running sanity tests along the way. Unit tests are next, and finally at the very end I'll add integration tests.

argspec allows nesting, but like with ansible variables in general, the flatter the better.

For documentation, I like to include permissions required, and a link to relevant upstream documentation. It's also good to understand the docs are meant to be read via the `ansible-doc` so they can be a little hard to read in the code. Also don't feel confined to the module documentation alone. If there is additional documentation useful for development include that with the collection's docs.

I don't remember where I first found it, but for general code I try to follow the advice that functions should either do something or call other functions. This helps a lot with making testable code that is easy to follow.

Implementing check mode. I have found 2 terrible ways of doing this and 0 seemingly good ways. The first is to wrap any actions that makes changes in a `if not module.check_mode:` block. The other idea is to wrap each action in its own function and have each function handle that `if not module.check_mode`. I like the first option slightly more because it means you don't have to worry about any ansible specific stuff in your unit tests. 

That said, another "am I doing this right?" is how to pass parameters to functions. I've settled for passing in only the bits that are required for the function to make unit tests easier, but I wonder if I should be using a class or something to be more pythonic. That's one of those downsides of not having a formal background in python. When looking through other community modules there didn't seem to be a set pattern, so it probably doesn't matter or just depends on how complicated your module is. 

Module output is all handled by a json object. I like to include the resulting object (i.e. if you got back some stuff from an API call), if the object was modified, providing a copy of the original object is good too, a *msg* object that contains some human readable text about what actually happened.

Once you have the basics, you can add some flare like support for diff mode, or different levels of verbosity. But really those kinds of things are probably best done as needed. 