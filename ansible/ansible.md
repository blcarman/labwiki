# Intro #

## Why Ansible? ##

Ansible is my first experience with an automation tool, so I'm looking at it mostly compared to doing things the old-fashioned way. There are a multitude of reasons for a tool like ansible, but the main reasons I chose to start with Ansible is because its agentless, didn't require learning a new programming language, and did not require a lot of infrastructure to get started. 

## Ansible with Pets ##

Does ansible really help if every server is different? Yes! There are a number of situations where a server may need to be re-created or rolled back to a good configuration. Or perhaps a customer that didn't expect to expand all of a sudden wants a dev environment or to introduce load balancing. All of a sudden that single server is part of a group and with ansible your time is cut in half. Even different servers often have the same OS config, with ansible those can be controlled while still letting apps be custom as needed. 

What if a change needs to be done now and I don't have time to write up a playbook? It's an unfortunate fact of life that customers will want changes sooner rather than later and won't always want to test them. The great thing about ansible is it works to bring servers into the expected state rather than just push changes, so you can make a config change and then make an ansible playbook after the fact.

I'm contemplating setting up an ansible check for config files so I could put "ansible_controlled = no" in a config file if I didn't want ansible touching it. Otherwise I suppose I could have ansible keep the config files and push it out, but this seems like it will be less efficient. Primarily for apache configs where vhosts will be different across customers and they might end up with tons of custom pieces. So maybe I'll have ansible set up the framework and then keep the custom bits

# Ansible Galaxy #

Red Hat recently open sourced some part of ansible galaxy though from what I can tell the actual ansible roles part of it already was. In any case I love the community approach, but as far as the official galaxy goes it seems like there is a fair amount of competing junk. Rather than the community electing one role and all contributing to it, there are half a dozen and you can't immediately tell which one is good or bad. So I may form my own ansible galaxy but very likely won't use the existing one for any real work since I can't guarantee the quality without auditing it at which point I may as well do it myself.

Apparently the ansible-galaxy commmandline tool has some pretty awesome features, namely the ability to initialize roles or create the whole directory/file structure of a role.

# Using Ansible for Real #

I've been putting off ansible for too long. Partly because I still can't make up my mind on whether to try ansible or one of the others like salt or puppet. In any case I have too much crap to do in the lab to keep putting it off. 

1  start with installing it (after enabling epel): yum install ansible
1  add hosts to /etc/ansible/hosts
1  copy ansible ssh key to remote machine and verify connectivity with ansible ping
ansible recommends ssh-agent, look into this further...

Finally got past the initial connection part of set up and looked over the ad-hoc commands, a few are in the examples below. Now I'm kicking myself for putting this off for so long. I'm sure the playbook syntax will take a little while to get used to, but the goal will be to put everything in a playbook with the assumption that even if I only plan on doing it once, it will be there if I need it. So the couple things I have to figure out is a good way to manage inventory (can I import from a monitoring system or something?) and how to manage the playbooks, I believe ansible recommends using git, so that will probably be next on the list of things to learn. Not really sure where to store playbooks by default, I guess it shouldn't really matter as long ansible can read it, but a recommendation would be nice. The documentation also decided to randomly throw in ansible-pull when introducing playbooks, which seemed like a strange place for it. Otherwise the ansible documentation has been decent, if not a little over simplified at times.

My first playbook went together fairly well, but it was just to install a service and start it. My second one ran into quite a number of disappointing issues. It was to update a system and reboot it. However, ansible seems to have severe issues when the ssh connection is disrupted. Ansible had a blog post about it that didn't really seem to solve the problem cleanly. I guess I missed a port specification potentially, now it seems to work. But due to how common this is I would really expect ansible to have a core module to do this by now.

The farther I get with Ansible the more I realize how little I know about it. I think I might grab the "Ansible: Up and Running" book just to get an organized approach. That isn't to say the official documentation is lacking, its actually pretty solid. However, the official documentation has a very limited intro and then just acts as a reference to components. Even reading the best practices they are a bit too generalized to be useful to a newcomer. Its nice that they don't lock you into set ways, but sometimes a little too much freedom can be just as harmful. 

Finally, got a role built with a separate variable file, it took far too long to realize I had an extra space in front of the variables that was causing syntax errors. the YAML syntax is nice, but spaces seem to matter far more than they should.

I feel I'm reaching a fairly comfortable level of working knowledge with ansible and have really only 2 complaints. The first is the yaml could be a bit more forgiving and the second is that sometimes it just pauses and never gets out of a rut and doesn't tell you why it's stuck.

I've been using ansible for a few weeks or more now and I think I have a fairly good understanding of it. I ordered a book Mastering Ansible, to further ensure I'm getting the most out of it. Either way I think its safe to split ansible into 2 modes. The first is actions like running updates or deploying a quick config update. The second is maintaining desired states with roles. I've seen a few reviews where people are using ansible to push out quick things but doing actual config management with puppet, and while I haven't tested puppet yet I think its a bit mis-guided to double up on tools like this. 

These are just a couple of cool things I found that I didn't know where to put, blocks can be used to group tasks, and role dependencies can be set so you don't have to worry about doing it at the playbook level. Also the ternary jinja2 filter looks pretty sweet. 

First real negative moment with ansible, apparently blocks do no support with_items yet though a feature request has been put in back in 2015, seems kind of insane that it hasn't been done yet. 

## Configuration Management as Documentation ##

Could configuration management replace common documentation for systems? Or as has previously been contemplated, can configuration management also write the necessary documentation?

## Examples ##

### Copying Files ###

This example actually started in the Ansible documentation, but it fit nicely with a customer request. Customer has a shell script they need to run periodically on each of their servers. Ansible could be used to store a central copy of the file and deploy it out to all servers.

```
 ansible <group> -m copy -a "src=/etc/hosts dest=/tmp/hosts" 
```

Then changing the ownership to the correct user

```
 ansible <group> -m file -a "dest=/srv/foo/b.txt mode=600 owner=mdehaan group=mdehaan"
```

### Installing a Package ###

```
 ansible <group> -m yum -a "name=acme state=present" 
```

Note: python2-dnf is needed to install packages on Fedora systems

### VMWare Dynamic Inventory ###


```
source ~/ansiblepy3/bin/activate
pip install --upgrade git+https://github.com/vmware/vsphere-automation-sdk-python.git
```


## Playbooks ##

At first look the playbooks are supposed to use a friendly yaml syntax. It doesn't look terrible but it does seem to make some strange use of dashes, I guess it avoids the whole spaces vs tab problem but just seems strange. (2020 update I still love yaml)

# Best Practices #

I have yet to find a decent best practices for ansible. There are a few vague guides, and maybe best practices isn't so much what I'm looking for, but rather an SOP. I'm not a big fan of throwing everything in /etc/ansible so instead I would rather set up an ansible user and put everything in the ansible user's home folder.

* ansible user: ansmin (might be better to not have a dedicated user, that way you can tell who made changes and so there is less security concern of someone gaining access to the ansible account)
* config file location: /home/ansmin/.ansible.cfg
* roles location: /home/ansmin/roles
* playbooks location: /home/ansmin/playbooks
* inventory: /home/ansmin/inventory folder (ansible handles the inventory directory really well so its good to use even if you currently only have a small inventory that fits within 1 file)
* non-specific vars: playbooks/vars (this would be for vars that are specific to environments rather than roles, and that need to be used across inventories to prevent duplication of group_vars)
* version control: git
* monitoring/testing: how to verify ansible run did not have any adverse effects
* Roles vs Playbooks: I'm not sure where the confusion comes from but roles should be based on what a server is being used for and configuration that is expected to always be the same. A role specifically to create users makes no sense, a role that happens to create users as a requirements for the server's function makes sense. If you really want to create a "users" role, do it in a playbook not a role. Playbooks should be used for one time activities like upgrades that cannot be cleanly handled inside the role. 
* Needs to be defined: Roles file organization: when/how to split main.yml into separate files
* Group Names: start each word with capital, for example: web servers would be [WebServers]
* Use dns names for hosts
* should ansible run from a dedicated server or jump box?
* store galaxy roles separately from locally-managed roles, i.e. /home/ansmin/roles and /home/ansmin/galaxy/
* set "become: true" in ansible config. Its common enough to be worth it, and it helps prevent people just running stuff as root
* not necessarily a best practice but a common practice, in ansible config [defaults] host_key_checking = False Obviously you should understand the implications before configuring this
* How to really separate prod/dev/test inventory files? ansible recommends just applying the different inventory file when running your "apply_roles" playbook, however I feel this leaves a lot of room for error. I also feel like having to add a "prod" group inside the production inventory file is a bit redundant, but maybe it is the only way
* Tagging: do it logically, that is tag related things the same and don't go overboard, not every task needs to be tagged
* Avoid duplication in the ansible inventory, i.e. use groups of groups instead of re-defining hosts in separate groups

## My Common Role ##

When creating roles its often difficult to define what all should be included. Should the common role include package installation, user creation, and common service configuration? Or should those each be split into their own role. My original goal with the common role was to have it be applicable to ALL hosts, however this is proving difficult with things like the ovirt nodes. Part of it could be solved by setting custom package lists in the group_vars but this still leaves us with services like firewalld. Perhaps I need a basic/minimal role that is literally applicable to everything and then a common role for the 98% of servers that have those requirements. Or I guess I could do multiple "common" roles, with a common_infra, common_vm, etc. I guess it's just time to map out tasks to systems and identify the groupings.

# "Mastering" Ansible Book Review #

This book was frankly a disappointment. Based on the reviews and the fact that the author seemed well involved with the project it seemed promising. It wasn't until I got it that I realized it was not from the Sybex Mastering series of books that are generally full of so much information its not even funny. Instead it was a brief 200 pages largely taken up by screenshots. It opened with assuming you have basic knowledge of ansible including installing it, which I guess is fine considering the focus is on "mastery" instead of getting started. However, there was very little coverage of advanced topics such as best practices, real world scenarios or known caveats. The section over variable precedence included "most everything else" instead of truly going through the logic ansible follows to evaluate what variable value to use. The chapter on jinja2 templates was again lacking any real world thought and could have easily been boiled down to a paragraph recommending the reader to check out jinja2 on the internet. There were a few informative pieces, but I feel like most of them could have been summed up in a blog post rather than requiring an entire book.

An unnecessary update: The more I use ansible the more I feel like this book really missed its mark. I think there is still room for a true mastering ansible book, but this book just gets further and further from its title.

# Ansible From Beginner to Pro Book Review #

Not a bad book, but did things a little backwards, or I guess in more of an action-based way. For new ansible users this is probably a decent approach. It starts you off doing some bad practices and I think that introduces the potential for a lot of people to get stuck in that original mindset if they don't keep reading the book. Overall it was a decent starting point and I did learn a few new things like "blocks". This book was also more in line with what I was expecting from Mastering Ansible. 
