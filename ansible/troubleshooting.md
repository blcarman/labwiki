# Troubleshooting Ansible Failures #

## Information Gathering ##

* what command or awx job was run to generate the failure?
  * does it match the documented way to run the playbook?
  * if in AWX, did you run the workflow that includes syncing the project and inventories before running?
* have there been any changes in the code?
  * if yes, how were the changes tested?
  * if no, maybe you should go back and test your changes
* read the full error message
  * if multiple errors/warnings are present split them apart
  * if the error is vague/unhelpful you can try re-running the ansible in a more verbose mode (i.e. `-vvv`)
* read the code for the failed task
* keep the full output of the run (if necessary output it to a file)
* re-run to confirm not an intermittent failure
* is the target host that failed healthy?

## Investigation ##

* does the task that failed rely on variables from a previous task?
  * if yes, confirm the variables are being set correctly



* follow the code, read the code for the failed task.

* check whether the failure is consistent, does it fail against a specific host or group? does it behave the same way in other environments/clusters?
* if in AWX:
  * was the project synced prior to the run? is it still set to the master branch?
  * was the inventory synced prior to th run?


## Compendium of Errors ##

This section contains errors we've seen in the wild and how we resolved them.
