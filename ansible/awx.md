## Migrating from Plain Ansible ##

AWX is almost a drop-in replacement for normal ansible but there are potentially a few things you'll have to do with your playbooks. Primarily, hosts can be specified in a job so you might not care about including it in the playbook. You can also combine multiple jobs into a workflow so you can possibly split a playbook into multiple ones or even segment things out into different jobs so it only does what you need it to. 

## Updating AWX ##

AWX is relatively easy to update. You can just remove the awx_task and awx_web containers/images and re-run the install script. As long as you keep the containers providing the database your data including jobs and history and what not should persist. Of course it should go without saying you should back up everything if you care about it. 

Disclaimer: I'm still learning docker so this is probably not the best way to do it. Also this assumes you're on a standalone docker host.

* Locate the docker container images: `docker images -a`
* attemtp to delete the awx_task and awx_web images which will complain that they're in use by containers
* remove the containers: `docker rm 634d96f4a425` where the 634d96f4a425 is the id of the container from the previous command
* remove the images `docker rmi docker.io/ansible/awx_task`
* reinstall `ansible-playbook -i inventory install.yml`

# Ansible Tower Trial #

The install was stupid simple, just follow their documentation. But it seems like a base knowledge of ansible may be helpful before jumping into tower. Seriously if the rest of it works that good it will be the best thing ever. But is Tower needed? It looks like it has a couple neat things but probably not worth the price.

One neat thing that ansible tower may be able to do that ansible lacks is surveys. That is if you have a few variables that should be defined when the play is run you can prompt the user instead of needing them in a file. This would be great for new deployments or where the variable might change depending on the circumstance. 

Follow up January 2018. I largely forgot about tower until it finally got open-sourced as AWX. I started looking at it again when we started thinking about taking over jenkins and fully automating deploys. In any case, AWX is pretty cool, super simple to set up. Minor learning curve to figure out the gui and how the ansible bits translate into tower jargon. The real win though is job scheduling and access control. We should be able to give the devs access to only dev stuff. The next piece of the puzzle will be to figure what can integrate with tower, most notably jenkins and vrealize automation.

One potential con of tower is that the ansible jobs have to run using an account set up for tower. Before going to prod you would want to make sure audit logging in tower is set up so changes can be tracked accordingly. Another possibly annoying piece is that it doesn't count changes in the jobs dashboard, just whether the job succeeded or not. I'm not sure if this just hasn't been added yet, or if its because Red hat is pushing its insights software. 

On the plus side, this should add some consistency to the lab, I'll now be able to keep hosts up to date in ansible without having to worry about manually running the ansible constantly. I should probably move this section to its own page... In any case last comment for now, the workflow editor is having some issues, though it may be due to the fact that I undersized the VM a bit.

# Using AWX For Real #

It's 2021 and I'm finally getting to use AWX for real. Some notes as I've been doing research

* awx.awx ansible collection is awesome and will make it far easier to maintain/rebuild/test

## Inventories ##

When you create an inventory it'll look like just a static one, but after you create it you can add sources including custom dynamic inventory scripts.
