I love having a single playbook to deploy a whole platform. But as the platform grows and matures it's becoming more important to be able to run only the ansible that actually changed. Of course ansible has no concept of state, so we'll have to come up with our own method. 

Collections and tags. One possibility is to split code into collections. Then we could have a CI pipeline for each collection. That pipeline could either run its own playbook or we could use tags in our main playbook. 

What about for config/variable changes only? We could limit the playbook run to the group that was changed. If we had reliable variable names we could also calculate the tags based on the beginning of the variable name. We can also change the group files to folders and then use files inside that match the tags. 

A more complicated solution would be loading vars from yaml files and calculating the diffs.

## Collections and Tags ##

The first thing we need is to bump the version in our requirements.yml when we update a collection. 

1. get new version number from collection's galaxy.yml
2. clone other repo with requirements.yml
3. update requirements.yml
4. open PR... or can we skip the PR?

hmm the second PR is going to make it harder to launch the awx workflow with the correct tag. Let's treat them separately.

Side tangent I should get az cli working on my machine so I can open my own PRs from the cli. 

Pipeline to trigger awx job with tags based on changes in requirements.yml


gitversion with commit-msg hook? gitversion seems way to focused on nuget. As much as I hate reinventing the wheel I think this is a good example of times it might just be better to write my own script instead of trying to force something else into working for my usecase.

I'm also not necessarily interested in forcing my usage upon others. 

Is this a good use of a pre-push hook? I think that will be better than per commit, since I really only care about the version getting bumped when I'm ready to open a PR.

So basically I'm thinking of a script that will:
1. determine which collections have been modified
2. check branch name and commit messages for version change info
3. determine updated version number based on branch name and commit messages
4. update version number if it will be greater than what it's already set to (this might need some finesse, that is if I manually update the version I don't necessarily want the script undoing that... unless the script would bump the version higher than I did).

Not directly related to this but it would also be nice to run tests/build before doing the push, since a lot of times I rush through before validating.

Now that I've started doing this I realize a while ago I had started a very similar thing, at that time I wanted to trigger only the collection builds that had changes. Instead we went with a pipeline per collection which allowed us to use AzDO's path filters. Now I kind of wish I had saved some of that work (or maybe I did and just don't remember where). 

One of these days I should commit every step of the way to show how many iterations I go through

```shell
#!pwsh

$ANSIBLE_NAMESPACE_DIR="ansible_collections/lab"

# determine which collections have been modified
$BRANCH=$(git symbolic-ref -q HEAD)

# alternatively we could get all files with git diff master...$BRANCH --name-only
# get the changed dirs, limit to just ansible collecitons, strip off the percent changed, collection name is reliably the 3rd directory, remove duplicates due to collections having multiple directories
$CHANGED_COLLECTIONS=$(git diff master...$BRANCH --dirstat=files,0 | grep ${ANSIBLE_NAMESPACE_DIR} | awk '{print $2}' | awk -F / '{print $3}') | get-unique
# $CHANGED_COLLECTIONS = []

# for each changed_dir in changed_dirs get the collection name

# $CHANGED_COLLECTIONS = $CHANGED_DIRS | foreach-object {}
#$COLLECTIONS = $CHANGED_COLLECTIONS | get-unique

write-host $CHANGED_COLLECTIONS
# foreach ($COLLECTION in $CHANGED_COLLECTIONS)
# {
#   write-host $COLLECTION
#   # get only the 3rd dir
#   if 

#   # add to list if not already there
# }
```

actually looks like we might be able to just do a "git diff master"

ugh fucking linter did a number on galaxy.yml when i manually changed the version, to make testing easier i should commit those changes to master and then figure the version stuff out.

Should I squash commits before pushing/opening a PR?

versio looks like it has the features I want, might be worth doing as part of CI/CD after all.

if we move our requirements.yml into the ansible repo versio could bump it at the same time...i think, though that might still be better left to renovate.

So, versio does look like what I would consider production grade, but should that prevent me from doing my ~half-assed~ minimal powershell script?

need to turn my script into a cmdlet so I can use write-debug.