netshoot research

# networks

to make for easier troubleshooting with one off `docker run` containers, make the overlay network attachable
`docker network create -d overlay --attachable perf-test2`

currently internal/dmz are not attachable so we will need to deploy/maintain troubleshooting container as a service

# notes

* docker exec must be run on node with the container being exec'd into
* whoami does not appear to have a shell


* iperf done
docker service create --name perf-test-1 --constraint node.role!=manager --network perf-test nicolaka/netshoot iperf -s -p 9999
docker service create --name perf-test-2 --constraint node.role!=manager --network perf-test nicolaka/netshoot iperf -c perf-test-1 -p 9999


`ss -s` for summary of port usage

# routing #

how do things get from traefik to a container on a different host?

# docker socket #

how can we monitor traefik's access to the docker socket? traefik binds the docker socket, could use socat in middle or strace but both seem relatively messy

`curl --unix-socket /var/run/docker.sock http://localhost/events`

`docker events` command also useful

# tcpdump #

how can we get useful tcpdump data from outside and inside the swarm?

docker_gwbridge connects the docker daemons

nothing shows up docker_gwbridge when doing a tcpdump as root, how/what does it do?

# traefik tracing #

can we enable this safely for everything? if not how do we enable it when we need it?

traefik ports published ingress mode

# manager consensus #

how do managers become ready/know what services are running?

# separate control and data networks? #

from docker docs https://docs.docker.com/network/overlay/ "When you initialize or join the swarm, specify --advertise-addr and --datapath-addr separately. You must do this for each node joining the swarm."

# TODO #

* observe packet arriving into traefik (leaving client, arriving at host, arriving in traefik container)
* determine route/path packets take between manager and workers (i.e. is it direct or is it possible you go through multiple nodes before hitting the destination)
* observe packet path from traefik to container on separate host (leaving traefik container, entering/leaving traefik host, entering service host, entering destination container)
* tool to make request to external LB, docker manager/traefik, and from within container network directly
* tool to collect docker events from socket (are these also logged or possible to log?) and other diag info
* observe docker management packets between managers and workers
* sar? vmstat? other classic tools that might be helpful?
* can we get creative by setting different timeouts for various things so we know if a timeout occurs at say 28 seconds its caused by x thing
* how do we know one of the replicas for a service didn't die/move in the middle of a request? or does that behave differently than a timeout?
* do we need to run netshoot on every node?

# nsenter #

allows tcpdump of overlay network on a node, gets around some of the limits of non-attachable overlay networks, after doing nsenter can we curl and other stuff?

`docker run -it --rm -v /var/run/docker/netns:/var/run/docker/netns --privileged=true nicolaka/netshoot`

# Alternative Networking #

## Weave Net ##

* legacy version doesn't segment networks
* must specify cidr in docker run command as an environment variable to isolate
* supports encryption
* prometheus metrics

## Calico ##

* swarm version appears no longer supported
* requires etcd

## Flannel ##

* requires etcd
* doesn't appear to work in swarm mode

## macvlan ##

### Pros ###

* possibly better performance than ingress/overlay
* makes it easier to expose containers to the network without something like traefik
* shouldn't cause port conflict on hosts (how would things that bind *:80 behave?)

### Cons ###

* containers exposed to actual network (probably not a great idea for most things)
* require real IPs on the network
* can't use with swarm mode and default IPAM driver, https://github.com/moby/moby/issues/37089 (i.e. would likely need infoblox IPAM driver which requires additional infoblox license)

### Configuration ###

* `docker network create -d macvlan --subnet=192.168.30.0/24 --ip-range=192.168.30.160/28 --gateway=192.168.30.1 -o parent=ens192 --scope swarm --attachable macnet` however this appears to give the containers address in the 172.22.0/16 range...don't seem to be any errors in the logs but must be conflicting with something since I re-used the host interface/range maybe? Also tried using ens192.30 as sub-interface, also tried ipvlan with 192.168.31.0/24 instead, but no change

Jun 05 14:28:18 dock1.lab.bradlab.tech dockerd[22094]: time="2020-06-05T14:28:18.985125410-05:00" level=debug msg="form data: {\"Attachable\":true,\"CheckDuplicate\":true,\"ConfigFrom\":null,\"ConfigOnly\":false,\"Driver\":\"macvlan\",\"EnableIPv6\":false,\"IPAM\":{\"Config\":[{\"Gateway\":\"192.168.30.1\",\"IPRange\":\"192.168.30.160/28\",\"Subnet\":\"192.168.30.0/24\"}],\"Driver\":\"default\",\"Options\":{}},\"Ingress\":false,\"Internal\":false,\"Labels\":{},\"Name\":\"macnet\",\"Options\":{\"parent\":\"ens192\"},\"Scope\":\"swarm\"}"

# Testing #

* configure network
* deploy stack consisting of traefik and whoami or guacamole containers on network
* apache benchmark or siege test x concurrent y times
* netshoot iperf test same docker host, different docker hosts, and different esxi hosts
* mtu normal (1500) and jumbo (9000)



docker network create -d macvlan --subnet=192.168.34.0/24 --ip-range=192.168.34.160/28 --gateway=192.168.34.1 -o parent=eth1 --scope swarm --attachable macnet


docker network create -d macvlan --subnet=192.168.30.0/24 --ip-range=192.168.30.160/28 --gateway=192.168.30.1 -o parent=ens192 --attachable macnet

Finally got network setup as local and spun up container, but no connectivity from outside world...arp sort of worked but no ping or http through it,

need to enable promiscuous mode and forged transmits on the vmware portgroup, https://github.com/jpetazzo/pipework

good overview https://thenewstack.io/container-networking-breakdown-explanation-analysis/