Another lab wiki!

I've been keeping notes pretty much since I started the lab back in 2013ish. At this point I've changed document formats so many times I've lost some of the history and metadata. In other words just about everything here is outdated and/or incomplete. I started this git based wiki to replace bookstack. I really liked bookstack's layout but wanted something I didn't need a web browser to use. It also seems like this will be the most easily archived and I can use a number of different frontends for the web portion if I want, currently that's wikijs.

I have a few different objectives with this wiki:

* keep track of what I've done
* work on my writing skills
* share things with others who might be interested

## Things I'm Currently Working On ##

* my first kubernetes operator
* stock tracking/trading application
* something cool that I haven't figured out yet for my new Nvidia Jetson Nano