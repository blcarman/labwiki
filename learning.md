# My Learning Process #

My learning process roughly breaks down to:

* read a bunch
* play in a sandbox
* bash head against wall
* jot down a poor fraction of the notes I should
* read some more
* get distracted for a day or month or forever
* come back with reduced scope
* keep reducing scope
