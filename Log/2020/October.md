# October #

It's been a productive month. Surprising considering it started off in a bit of a slump. I probably need to chill a bit so I don't end up burning myself out again. Drinking tea in the morning seems to be contributing to more alertness during the day too.

## 31 ##

* moved libvirt VMs to ssd, hard drives stopped being so clunky
* re-arranged/cleaned up wiki to make it public
* configuring traefik on the vps, something is causing it not to read labels correctly (or at all?), too tired to figure out tonight

## 30 ##

* Euler problem 4 solved

## 29 ##

* jellyfin refresh people task ran again, deleted task this time, seriously don't understand what its doing that uses so much disk writing

## 28 ##

* more silverblue tweaks, running pretty good now
  * installed material-shell extension via rpm-ostree
  * install virt-viewer via rpm-ostree
  * tried to remove packaged firefox since flatpak version seems better, but couldn't
  * gnome still has some limits (why can't I set apps to run at login, maybe need to test https://help.gnome.org/admin/system-admin-guide/stable/autostart-applications.html.en) but not terrible
* TODO: Brian found wikijs and it looks pretty neat, https://docs.requarks.io/install/docker (git sync plugin addresses limits that bookstack had)

## 27 ##

* installing Fedora 33 silverblue on laptop, mostly because unitedrpms did some bad things with dependencies so now i can't remove gstreamer1-plugins-bad-free without also removing the core of KDE
  * only rpm had to install is zsh (maybe could have skipped)
  * vs code flatpak doesn't completely suck if doing remote ssh dev, instaled fira code/hack fonts to home dir, enabled ligatures
  * had to disable "natural" scrolling
  * TODO: slack nebula, curious if this will just work, forgot to backup certs so may need to start over unless I had previously saved them and forgot about it
  * still not sure how I feel about gnome, need to try out material shell
  * install firefox flatpak for video support

## 26 ##

* completed Project Euler problem 3

## 25 ##

* found r/algotrading, maybe a project for a jetson nano? what kind of return could I expect?
* completed Project Euler problems 1 and 2

## 24 ##

* got the samba shares running on titanium. Temporarily put samba in permissive mode while I figure out how to create a policy for it without using audit2allow since things are not showing up there the way I would expect. Had to label /mnt/warehouse to fix the search thing (I think). Final policy below.

```
module samba_container 1.0;

require {
        type container_file_t;
        type smbd_t;
        class file { create open read setattr unlink write };
        class dir { add_name remove_name write };
}

#============= smbd_t ==============

#!!!! This avc can be allowed using the boolean 'samba_export_all_rw'
allow smbd_t container_file_t:dir { add_name remove_name write };

#!!!! This avc can be allowed using one of the these booleans:
#     samba_export_all_ro, samba_export_all_rw
allow smbd_t container_file_t:file { create open read setattr unlink write };
```

## 23 ##

* TODO: convert blog to a container without googling "pelican container"

## 22 ##

* restored gitlab vm
* built cloud gitlab runner vm

## 20 ##

* vmware workstation 16 released, runs on fedora 32 now at least, but nothing really new otherwise
* started building out centos 8 swarm
* building silverblue 33 and manjaro sandbox desktop VMs again, will probably not keep manjaro around, its updater is really bad (granted I did start from a 6+ month old iso)

## 15 ##

* podman/jellyfin selinux fix `chcon -R system_u:object_r:container_file_t:s0 /mnt/warehouse/pods`, also had to update `/mnt/warehouse/media/movies`, added service as well, at some point I should probably consider adding resource limits
* packer el8 template mostly working, need to add yum upgrade/ansible config

## 14 ##

* tested packer kvm image, issue with dhclient, had to add `ldconfig` to provisioner
* potential provisioning script
~~~
cp el7_template/el7_template /mnt/warehouse/libvirt/vm_name.qcow2
virt-install \ 
  --name vm_name \ 
  --memory 2048 \ 
  --vcpus 2 \ 
  --disk /mnt/warehouse/libvirt/vm_name.qcow2 \ 
  --import \ 
  --network=bridge:br30
  --os-variant centos7 
virt-customize -a /mnt/warehouse/libvirt/vm_name.qcow2 --hostname vm_name
~~~

## 13 ##

* packer kvm build finally succeeded, just need to test it out for real now
* re-arranged a bunch of the /mnt/warehouse/lab stuff (snapshotted first)

## 11 ##

* kvm packer templates
  * installed hashicorp repo

## 7 ##

* KDE activities are still more of a pain than their worth, I had one for normal stuff and one for work and after a reboot I got flipped around and appears to be no persistence, switching is also incredibly clunky. Maybe I'll look at a tiling window manager next. 

## 5 ##

I didn't do much on the computer this weekend, I almost did yesterday but decided against it. But I think holding off set me up a bit better today, like I was so tired of not doing stuff that it felt good to be productive again. Obviously I shouldn't just let myself slack off during the week, but I do need to make sure I give myself some downtime even if it doesn't feel like I need it.

## 2 ##

* set up internet gateway for garage door opener, works pretty good