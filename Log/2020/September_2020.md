# September #

Another month sneaks up out of nowhere.

## TODO ##

* scripts to standardize/speed up my work

## 28 ##

* TIL powershell history is kind of terrible, get-history only does current session. But up arrow goes back indefinitely. Turns out PSReadLine does its own history thing and stores it in `(Get-PSReadLineOption).HistorySavePath`, not really sure why get-history doesn't just read from that file...

## 27 ##

* btrfs balance finished

## 26 ##

* moved packer repo to gitlab.com

## 25 ##

* trying out virt-viewer from the laptop as a guacamole replacement, copy/paste is seamless but mouse/visual responsiveness still kind of disappointing. It works great locally though.

## 21 ##

* started reading up on powershell modules
  * really not much guidance/framework from MS

## 19 ##

* tried messing with flatpak permissions(I don't use audio on discord so wanted to remove its ability to), and it seems flatpak doesn't suppor the --user flag with permissions so I'm fucked. It really feels like flatpak/snap weren't solving a problem for the users, but for the package maintainers so they could push things back to the upstream project to manage. 

## 18 ##

* replaced 1.5 TB drive in warehouse with a 4tb sas... well tried to, the new drive was bad, so I had to swap it out as well. I did the original as a btrfs add and brtfs delete and it was kind of strange that btrfs emptied the old tiny drive without seeming to find a new drive to put it on. Also these HGST drives run hot, not sure if that's normal or not, but might try to find something else next time.
* also put the 500gb ssd in titanium, so maybe I can hold of buying the 1TB nvme for a bit longer

## 16 ##

* lowered win10 guest cpu usage: https://www.reddit.com/r/VFIO/comments/80p1q7/high_kvmqemu_cpu_utilization_when_windows_10/
* also moved win10 vm to virtio nic after seeing weird usage in the task manager
* convert vmdk to qcow2 `qemu-img convert -f vmdk -O qcow2 vmware/test_warehouse/test_warehouse.vmdk vmware/test_warehouse/test_warehouse.qcow2` (for vcenter choose the tiny vmdk not the flat one)
  * also have to boot rescue cd and rebuild initramfs... (change kernel version as needed)
```
mount --bind /proc /mnt/sysimage/proc
mount --bind /dev /mnt/sysimage/dev
mount --bind /sys /mnt/sysimage/sys
chroot /mnt/sysimage
dracut -f /boot/initramfs-2.6.32-358.el6.x86_64.img 2.6.32-358.el6.x86_64
```

## 15 ##

* finally got all the screws and connectors to be able to mount the warehouse in titanium, probably should have considered the noise/heat before doing it but we'll give it a few days. Surprisingly once everything was connected it did just work.
* guacamole on titanium
```
podman run --name guacd -d -p 4822:4822/tcp guacamole/guacd
podman run --rm guacamole/guacamole /opt/guacamole/bin/initdb.sh --postgres > initdb.sql
podman run --name guacamole -d -p 8080:8080/tcp guacamole/guacamole -e GUACD_HOSTNAME=192.168.50.15 -e 
```

## 9 ##

* AIO lab inventory
rancher/k8s cluster (3x8gb)
graylog 4gb
postgres 1gb
mysql 1gb
win10/win10-clone (2x4gb)


## 8 ##

* It's 2020 is a rack-mounted lab still a good idea?
  * the dell r610 is getting old, needs more RAM or replaced all together
  * 3560g, I don't do any networking now and this is so old as to not be useful on that side anyway
  * supermicro chassis is power hungry, nothing else fits well
  * crawlspace is annoying to work in
  * can fit 128gb RAM in a cheap amd desktop now
  * fuck it we're moving the storage to titanium
* My Coolermaster Xornet died a slow painful death (thought it was a bad kernel update, then a bad usb controller, before narrowing it down to the mouse). It started out being ok after I unplugged it for a minute, but each time after it would die sooner until it wouldn't even work at all

## 7 ##

* warehouse upgrades
  * removed katello repos from warehouse, added elrepo and did a yum upgrade
  * fixed podman with https://gbenson.net/?p=697 (note don't do the /var/run/containers one as that is already on tmpfs)
  * `loginctl enable-linger 1005` (poduser) based on debug output from podman command
  * jellyfin now running again
  * hardware upgrades didn't go so well, new system refusing to boot, I removed the extra offset in the chassis, maybe that was a bad idea.

## 5 ##

* tried jupyter notebook in vs code and did not go well. Jupyter lab in browser is kind of neat but kind of clunky, that is it loses a lot of screen real estate with the app inside an app thing.

I'm going through another one of those phases where I just kind of want to throw everything out and start over. I know I always go on about continuous improvement but sometimes it just feels good to clear off the desk and start from a blank slate. But I'm starting to wonder if its more about the fact that the initial part is always the easiest gain. I think this is also why I never seem to lift weight more than 6-8 months in a row. That first month you feel like you're making huge jumps, then you start to feel like you're actually accomplishing something, and then the next phase you realize you're actually going to have to work to keep making progress. You might even have to do things outside of simply lifting, like changing your diet. So we end up chasing the easy part of the curve even though realistically its the less rewarding part overall. What I really want to do is jump to the next curve instead of reliving the last one, but new stuff is scary.

## 1 ##

* vmware workstation does not work with kernel 5.8, trying out win10 on libvirt, seems to be working fine, just needed to install the spice client(https://www.spice-space.org/download.html) in windows to get copy/paste working. vpn works too, so I think vmware workstation is completely out of the picture now.
* fixed my brave and virtualbox repos, enabled brave sync between titanium and laptop
