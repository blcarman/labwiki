# August #

I spend way too much time on reddit. Obviously I need some time to turn off the brain, but I also need to get better about doing useful non-work computer stuff again. Maybe the Oreilly Online thing would be worth getting again, it's been hard finding good articles to read (and stupid repetitive reddit posts are getting old). Kubernetes seems too far off to start thinking about but I really probably should.

I haven't been tracking it here but I've also been working on the truck as a non-technical project. It's right on the edge of being worth it but overall has been a fun change of pace. So if I sell it and get a new(er) truck what will the next non-tech project be?

## Ideas ##

* git activity log: have a pipeline that updates a page on the blog or wiki with every git commit that is pushed to a master branch. Also publish to the matrix chat
* libvirt esxi vm (will it perform better than vmware workstation nested esxi)

## 25 ##

* installed cockpit on titanium (had to open port in firewalld and nebula, need to figure out how to do it in just nebula)

For the past year or 2 I've been mounting my nfs shares to directories inside my user's home. After recently deleting some stuff unintentionally from my home directory and today trying to use an iso to create a libvirt machine I realize maybe I should move those nfs mounts.

* moved ISOs and movies to /mnt/ on titanium
* cockpit vm management somewhat limited, won't allow esxi vm because it can't guess the guest os, vnc console seems to have trouble connecting reliably, can't set cpu count
* enable nested virt on titanium, https://docs.fedoraproject.org/en-US/quick-docs/using-nested-virtualization-in-kvm/
* esxi vm on libvirt seems to be working, but vcenter won't connect to it. vcenter gets the ssl fingerprint so it seems some connectivity is there, but then vcenter claims its unreachable.
* installed kwalletmanager5 to be able to disable the wallet subsystem, why does it randomly start prompting for a password that I never even set...

## 19 ##

* started looking into CI doing git commits and automated changelogs
https://docs.microsoft.com/en-us/azure/devops/pipelines/scripts/git-commands?view=azure-devops&tabs=yaml
https://docs.gitlab.com/ee/user/project/deploy_keys/
* find what commits are on your branch but not master `git log origin/master..origin/not_ready --oneline --no-merges`
* set `export PAGER="less -X -F" to override oh-my-zsh` silly pager default

## 8 ##

* netbox broken, so can't use it to allocate IPs for rancher... edgerouter actually has pretty slick way to set dhcp leases to reservations
* TODO: add rke and kubectl to ansible workstation role
* TODO: add cluster.yml to git somewhere, configure ssh user (can't be root on centos apparently)

```
➜  ~ rke up
INFO[0000] Running RKE version: v1.1.4
INFO[0000] Initiating Kubernetes cluster
INFO[0000] [certificates] GenerateServingCertificate is disabled, checking if there are unused kubelet certificates
INFO[0000] [certificates] Generating Kubernetes API server certificates
INFO[0000] [certificates] Generating admin certificates and kubeconfig
INFO[0000] [certificates] Generating kube-etcd-192-168-30-103 certificate and key
INFO[0000] [certificates] Generating kube-etcd-192-168-30-117 certificate and key
INFO[0000] [certificates] Generating kube-etcd-192-168-30-118 certificate and key
INFO[0000] Successfully Deployed state file at [./cluster.rkestate]
INFO[0000] Building Kubernetes cluster
INFO[0000] [dialer] Setup tunnel for host [192.168.30.117]
INFO[0000] [dialer] Setup tunnel for host [192.168.30.103]
INFO[0000] [dialer] Setup tunnel for host [192.168.30.118]
WARN[0001] Failed to set up SSH tunneling for host [192.168.30.103]: Can't retrieve Docker Info: error during connect: Get http://%2Fvar%2Frun%2Fdocker.sock/v1.24/info: Unable to access the Docker socket (/var/run/docker.sock). Please check if the configured user can execute `docker ps` on the node, and if the SSH server version is at least version 6.7 or higher. If you are using RedHat/CentOS, you can't use the user `root`. Please refer to the documentation for more instructions. Error: ssh: rejected: administratively prohibited (open failed)
WARN[0001] Failed to set up SSH tunneling for host [192.168.30.118]: Can't retrieve Docker Info: error during connect: Get http://%2Fvar%2Frun%2Fdocker.sock/v1.24/info: Unable to access the Docker socket (/var/run/docker.sock). Please check if the configured user can execute `docker ps` on the node, and if the SSH server version is at least version 6.7 or higher. If you are using RedHat/CentOS, you can't use the user `root`. Please refer to the documentation for more instructions. Error: ssh: rejected: administratively prohibited (open failed)
WARN[0001] Failed to set up SSH tunneling for host [192.168.30.117]: Can't retrieve Docker Info: error during connect: Get http://%2Fvar%2Frun%2Fdocker.sock/v1.24/info: Unable to access the Docker socket (/var/run/docker.sock). Please check if the configured user can execute `docker ps` on the node, and if the SSH server version is at least version 6.7 or higher. If you are using RedHat/CentOS, you can't use the user `root`. Please refer to the documentation for more instructions. Error: ssh: rejected: administratively prohibited (open failed)
WARN[0001] Removing host [192.168.30.103] from node lists
WARN[0001] Removing host [192.168.30.118] from node lists
WARN[0001] Removing host [192.168.30.117] from node lists
FATA[0001] Cluster must have at least one etcd plane host: failed to connect to the following etcd host(s) [192.168.30.103]
```
* rancher install through the following error,
```
➜  ~ helm install rancher rancher-latest/rancher --namespace cattle-system --set hostname=rancher.lab.bradlab.tech
Error: Internal error occurred: failed calling webhook "webhook.cert-manager.io": Post https://cert-manager-webhook.cert-manager.svc:443/mutate?timeout=30s: context deadline exceeded
```


## 6 ##

* new motto: "Code is a liability": from the nocode framework https://github.com/kelseyhightower/nocode

## 5 ##

* reading docs to set up new rancher cluster, need to static ip/dhcp reservations

## 4 ##

* https://docs.fluentd.org/plugin-helper-overview/api-plugin-helper-record_accessor#syntax
* terraformed 3 new rancher nodes con1-3
* docker logging to graylog on new nodes

## 3 ##

* fluent bit cool stuff
    * recently added a docker events input (but doesn't seem to support filtering yet)
    * can filter systemd input by multiple systemd keys (i.e. maybe able to drop _level=info?)
    * supports fluentd forward protocol
    * use grep filter to drop NetworkDB

```
[FILTER]
Name grep
Match host.docker.service
Exclude MESSAGE NetworkDB
```
* and I broke some part of the fluentbit config on dock1, its not forwarding any docker daemon logs now...