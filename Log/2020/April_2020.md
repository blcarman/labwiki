
## A New Plan ##

I'm going to try a new method of 3 goals/todos per month. I hope this will keep the list of squirrels to a reasonable number and more realistically track what I've been doing. This month will hopefully also get back away from doing work work in the home lab.

I would also like to make the wiki public, but I want it to be usable. Essentially I want to capture my experiences so others can see my process for figuring things out. This is of course something I've been trying to do for a while, but maybe now is the time to pick it up (unless the weather is too nice and I just spend the month biking).

A question for the month. Do I really dig into chef to understand it or do I cut my losses? Would digging in make a difference in using it?

## TODO ##

* terraform workspaces in good state (plan should always work)
* (almost done) chef in good state (figure out a good workflow that isn't too heavy, but keeps things realistic)
* (punted) track chef client failures (will get picked up by log insight agent, just need to install and config alerts)

### Stretch Goals ###

* (DONE) track chef client failures (log insight?)
* (punted) consider moving titanium to manjaro xfce and/or get theme config and run xfce on fedora or other distro
* (punted) consolidate repos into src directory where appropriate, consider adding bash script to clone all the repos for a new system
* (punted) terraform rancher
* (punted) terraform run by gitlab or azure devops
* (punted) https://bendews.com/posts/implement-dns-over-https/
* (punted) script to do daily git commit/push of notes?
* (punted) packer windows 10 template
* https://github.com/lingtjien/Grid-Tiling-Kwin
* matrix/riot bots (rss reader, grafana alerts, others?)

### Unplanned ###

* (DONE) upgrade vcenter
* upgrade esxi hosts to 7 if possible
* graylog
* grafana
* (DONE) nginx geo-ip blocking https://www.digitalocean.com/community/questions/block-some-countries-from-visiting-my-website

## Final Notes ##

Well, the 3 goals thing didn't really go as expected though it was an overall productive month. Maybe I should put a kanban board up after all. I do need some more stuff to run in rancher...

## 30 ##

* got ruby to read yaml for config and connect to postgres. Either I was in the zone or I'm finally starting to understand ruby a little bit, but seemed to go much better than it did just a few days ago.

## 29 ##

* not really a lab thing, but finally used actual ruby code to augment some chef stuff (well chefspec to be exact)

## 25 ##

* started ruby app and connected to postgresql db

## 22 ##

* noticed graylog server is upset about timestamp format from gelf nginx logs (wants float instead of string), log insight forwarder also giving bad timestamps

## 21 ##

* fixed up nginx logging
* enable geo-ip blocking on nginx
* nginx content pack refuses to install, what should "entity list" be set to?

## 20 ##

* digital ocean nginx logs being sent to graylog via fluent bit

## 19 ##

* downloaded graylog ova but doesn't seem compatible with vcenter... installed the old fashioned way on centos 8 instead

## 18 ##

* grafana running on rancher
* added grafana user to mysql1 and created dashboard showing 10 most recent updates

```
select entity_id, created_at from activities limit 10;

select activities.created_at, activities.entity_id, books.name from activities left join books on activities.book_id = books.id;

select activities.created_at, activities.entity_id, books.name, pages.name from activities left join books on activities.book_id = books.id left join pages on activities.entity_id = pages.id;

select activities.created_at, books.name, pages.name from activities left join books on activities.book_id = books.id left join pages on activities.entity_id = pages.id;

select activities.created_at, books.name, pages.name from activities left join books on activities.book_id = books.id left join pages on activities.entity_id = pages.id order by activities.created_at DESC limit 10;
```

## 15 ##

* spent too long trying to figure out why tags weren't working only to find I was using `tag:` instead of `tags:` in the chef search

## 14 ##

* test kitchen can use dummy node json files (not sure if this feature is new but I can finally test chef searches)

## 13 ##

* trying to read http://ruby-doc.com/docs/ProgrammingRuby/
  ** instance variables (@thing) are variables associated with that specific "instance" of an object and only usable from within the class, https://www.rubyguides.com/2019/07/ruby-instance-variables/
  ** got to "Standard Types", book has dropped some of its "cuteness" and is now fairly concise/reasonable, or I've just gotten used to it
  ** how does `def [](key)` work? like how does it know key is actually between `[]` when called?

## 11 ##

* cross check rear brake cable completed
* hass.io running on pi, used dd to write image to sd card

## 9 ##

* upgraded vcsa to 7.0, unfortunately doesn't seem to include the kubernetes part, and couldn't find anything indicating whether vmug would get that version or not

## 5 ##

* more of a work thing, but AOS install via powershell script working in home lab now. Note: windows_package sourcing from fileshare doesn't work when chef runs via the scheduled task, but does with my user, why doesn't it honor the remote file attributes? how did it work on aos3, it only had issues with visual studio...oh wait maybe the file was already in the cache? just kidding now aos1 is also erroring out there

## 4 ##

* windows log insight agent config, each event log needs its own section, even though log insight gives the hint as comma separated list
* installed keyboard tray in standing desk. I re-used the one I had by cutting it down a bit, but it's still not quite right. It hangs down a bit too far and doesn't retract in enough. But I'm not sure the official one with its spacers would be any better. For standing I suspect it will be good though. Otherwise I foresee me frequently bashing my knees in though hand placement on keyboard does seem good. Basically the keyboard is able to just about rest on my legs now which I think is supposed to be the ideal placement. We'll try it out for a couple of weeks and go from there. I bet the official one with it's spacers would let the tray be more horizontal and thus more under the desk which would be nice for space and writing without having to swivel the keyboard out of the way.
* set dns suffixes on dc1 so it behaves like dhcp clients (i.e able to resolve short names for lab.bradlab.tech)
* all (ish) windows VMs chef converging and logging to log insight (also added log insight to linux base cookbook)
* log insight dashboard for chef runs
* aos1 is giving me the "source does not exist" error now as well...

## 3 ##

By accident it seems I have decided to delve further into chef. At work someone pushed up some code that did not match the test conditions. Turns out we had other test failures that further confused the matter. So while trying to understand his code and the tests I started looking at the surrounding code and just cringed. I don't want to call it bad code, but I can't say its good code either. I've realized I'm a bit of a perfectionist when it comes to code, even if I don't know how to achieve that perfection myself. I also believe in leaving things the same or better than I found them. Long story short, I don't see this code base disappearing any time soon so I should probably learn more about chef to ensure I don't make it any worse than it already is. And while a lot of my criticism for chef has centered around its behavior with windows, this will further decide if chef is truly evil or if Windows is the real one at fault.

Also a great catalyst for getting out of a slump is having someone ask you about something and/or someone submit bad code.

* installed nginx on warehouse to serve random software files
* add log insight to windows base chef cookbook, still need to tweak the logs it collects