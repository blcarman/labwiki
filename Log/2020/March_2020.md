# March #

## TODO ##

* (DONE) manjaro vnc(vmware workstation vnc performance is bad) and ssh access (should this go on vcenter?)
* (DONE) fix jellyfin on warehouse
* (punt) terraform workspaces in good state (plan should always work)
* (half done) chef in good state (figure out a good workflow that isn't too heavy, but keeps things realistic)
* (punt) track chef client failures (log insight?)
* (punt) consider moving titanium to manjaro xfce and/or get theme config and run xfce on fedora or other distro
* (punt) consolidate repos into src directory where appropriate, consider adding bash script to clone all the repos for a new system
* (punt) terraform rancher
* (punt) terraform run by gitlab or azure devops
* (punt) https://bendews.com/posts/implement-dns-over-https/
* (punt) script to do daily git commit/push of notes?
* (punt) packer windows 10 template
* (half done)ansible-ize the warehouse

## 29 ##

* fixed jellyfin on the storage server by deleting the pause.pid file, not sure why it was throwing the xfs formatting error in the service logs, also deleted some old emby images

## 28 ##

* lots of work towards ansiblizing the storage server

## 27 ##

* btrfs balance failed, possibly due to new kernel and old btrfs-progs, considering installing fedora server to resolve and also fix the podman xfs formatting issue

## 25 ##

* created chef cookbook for build server that installs terraform, had to knife delete bld1 and re-bootstrap because it had a policyfile set
* replaced 1 TB drive in warehouse with 4 TB drive to increase capacity

## 23 ##

* reinstalling windows due to eval expiring, vmware workstation's easy install doesn't play well with the enterprise eval
* finally fixed vmware workstation update issue, vmware-installer was missing since I had tried to fix the remote console a while back, per Arch linux wiki I was able to extract the bundle and copy the vmware-installer into its correct location after which upgrade actually worked correctly.

## 21 ##

* enable terraform autocomplete for zsh `terraform -install-autocomplete`
* mirror useful gitlab repos to azure devops (ansible, packer)

## 18 ##

* manjaro disable stupid beep when logging out, `echo "blacklist pcspkr" > /etc/modprobe.d/blacklist.conf`
* new project idea: No Bullshit chef resources. I'm tired of chef being opinionated about the wrong things. I get that older versions of windows might not have powershell or all the new stuff but that still doesn't mean chef should just shittily wrap some windows command and decide how they think it should be handled. Or why do I have to read the source to understand what chef is trying to do? If I was a better person I might try to open some PRs to fix up the docs. 


## 15 ##

* ansible workstation role on fedora looking good now, added some stuff for powerline and zsh though it's probably not complete. I think I might drop the dotfiles repo in favor of just putting a dotfiles tag on the ansible role to keep things consolidated.
* ansible runs on titanium and scandium
* vmware workstation is trying to update to 15.5.2 but it looks like it's just a couple security fix and not adding support for new kernels.
* testing out sublime for the wiki editor, code doesn't handle multiple windows very well, or at least launching them is kind of awkward. Still trying to figure out how to nicely handle multiple workspaces in code

## 13 ##

* remote vs code server had lock file that it was really upset about existing, not sure why it didn't prompt to override it or at least tell you to manually delete it
* starting to split workstation ansible into distro specific and non-specific stuff, but getting `[WARNING]: Unable to parse /home/brad/remote/technical/ansible_private/inventories/workstations/inventory as an inventory` when trying to validate existing against titanium (update 3/14 turns out this was because I had the vmware inventory plugin enabled in ansible config, apparently this overrode the default inventory plugins so adding them alongside the vmware plugin fixed it)

## 11 ##

* updates on manjaro VM, disabled unneeded autostart services

## 1 ##

Trying to start this month off strong. I'm thinking of splitting terraform into infrastructure, platform test, platform prod (and maybe application) workspaces. It might require more data blocks in the platform modules but it prevents duplication of resources. The goal (not necessarily for March) will be to get all VMs into terraform except a short-lived dev folder. At the same time I hope to start upgrading things to EL8 and utilizing chef more. The AX project at work has made me realize as much as I want to I can't rely on work resources for learning/testing. There is also potential for more show vs tell when trying to introduce new ways of doing things. Also with all the struggles I've had getting AX to a testable state, I've realized I need to get the basics under control so I can actually do stuff. I don't want to lose track of learning golang, but infrastructure is where I'm needed it seems (and what I really enjoy for that matter). 