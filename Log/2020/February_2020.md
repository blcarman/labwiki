# February #

## TODO ##

* terraform rancher
* terraform run by gitlab or azure devops
* https://bendews.com/posts/implement-dns-over-https/

## 1 ##

* add build number to packer templates
* upgrade VMs
* start looking at terraforming rancher

## 2 ##

* installed procs on titanium, https://github.com/dalance/procs, found via cronweekly newsletter
* determined plasmashell to be what manages kde's taskbar (and probably other things), when it disappears `killall plasmashell` then `plasmashell &`. Supposedly `invalid metadata` logs are just useless debug logs because plugins don't actually have to provide metadata, I find this argument stupid but at least it's not cause for concern.
* reverted kde icon theme back to breeze dark on titanium, really considering abandoning kde
* upgrade warehouse, forgot mainline kernel doesn't get along with the realtek nic for whatever reason, also doesn't seem to like btrfs...
* installed Fira Code font on scandium and titanium, still like hack best but also like ligatures so I'm torn
* actually script powering on and off all VMs in lab vcenter

## 8 ## 

* powerline config on titanium (tmux is no longer hideous)
* create dotfiles repo and add tmux, powerline configs
* create bitwarden account, very positive so far

## 13 ##

* clone bento repo and build windows-2016 vagrant box

## 14 ##

* `dnf remove vagrant-libvirt` since its version of fog-core conflicted with `vagrant plugin install winrm` and `vagrant plugin install winrm-elevated`
* installed bitwarden client on titanium
* vmware-host-modules getting confused about version, need to `make clean`
* vagrant and windows is not a good combination
* upgrad chef-server to 13.1.13 and chef-workstation on titanium to 0.15.18
* lots of manual config on bld1 to get it to still fail on knife upload (error below in after making it server admin)
```
ERROR: You authenticated successfully to https://chefserv.lab.bradlab.tech/organizations/lab as builduser but you are not authorized for this action.
Response:  'builduser' not associated with organization 'lab'
```
* azure pipeline now deploying chef code! `chef-server-ctl org-user-add lab builduser --admin` did the trick

## 16 ##

* chef provisioning AD domain working

## 20 ##

* got 12 tb usb drive for backups, now to re-think my storage set up

## 22 ##

* update gitlab server, install git 2 from rackspace repo to fix gitlab-runner error
* work on windows 2019 packer image

## 23 ##

* warehouse kernel 5 network issues might be related to this, https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=11287b693d03830010356339e4ceddf47dee34fa, and https://bugs.centos.org/view.php?id=16413
```
rmmod r8169
modprobe realtek
modprobe r8169
systemctl restart network.service
```
* did some awk magic in the power off vm scripts, based on https://www.unix.com/shell-programming-and-scripting/106208-move-line-end-file.html, VM power script fully tested and functional

## 24 ##

* switching windows packer to e1000e was not enough, had to sneak into console and install vmware tools before waiting for IP timed out... and chef license failed, so upgraded packer which wasn't compatible with old vsphere-iso, so upgraded that before finding out its actually been brought into packer itself as an official builder which is super awesome. And it still failed because it tried to install vmware tools but didn't actually download the iso...(though that's bento project's fault not packer) (late update packer has examples for windows w/ pvscsi driver and vmware tools install in unattend file)

## 25 ##

* checked out the new Manjaro linux XFCE release, it's actually pretty slick for the most part though they've deviated from arch more than I would like. Really curious if I can just steal the style and run xfce on fedora

## 26 ##

* windows packer template finally builds... well it failed due to timeout so I had to bump that to 2 hours but it at least didn't destroy itself at the end this time. Still needs some tweaking and starting to wonder if forking the bento project was really worth it.

## 27 ##

* Manjaro is growing on me, seems very responsive, at first I just wanted to copy the theme to fedora but now I'm thinking I might try it out for real.
* added sysprep to server 2019 packer and confirmed SIDs are unique on each clone now. There is probably some more work that could be done but is at least MVP now.

## 28 ##

* Manjaro remove module laod for vboxsf/vboxguest, kind of sad its not smart enough to automatically sort it, also they put in a file for each linux release which seems kind of excessive
* heavy chef work, long story short, even without berks chef will due dependency resolution on cookbooks and only use the latest version that has all dependencies satisfied (I forgot to install/upload a community cookbook)
* TODO: figure out why powershell ISE exists on 2019 even with feature removed and optional capability showing disabled, its possible bento cut corners or maybe it's just seriously baked into the core OS

## 29 ##

* I just want something to work, vmware remote console won't run on titanium, trying to install newer version and I got a zlib error that was "fixed" by deleting the old vmware-installer directory, but now it doesn't error out but it doesn't work either. Was able to resize the display in the console to get the stupid sql installer to fit. Also using vmware workstation to connect didn't seem to help much, that's probably what broke vmrc in the first place
* re-armed dc, did it over winrm and it worked but never cleanly exited which was kind of annoying
* changed server2019 template to use pvscsi and vmxnet3 and bumped video memory from 4 to 8 mb for higher resolution console
* technically finished this at 1am so should go in March, but finally got an AX AOS installed in the home lab