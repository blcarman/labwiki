
## Opening Notes ##

I think I need to change up the lab log. Mostly I want more kanban so I don't have to keep re-arranging tasks in the log. Also I think if I clean up my commit messages I could potentially add another piece to the lab app that could nicely format things and automatically add to the log. I've got kind of an unofficial goal to hit 1000 lines of ruby. I think historically if I get around 100 lines I start looking for already existing tools and usually find something that fits what I need. But I really want to go from being someone who just modifies an existing thing to someone who actually creates a thing (even though its really using the same puzzle pieces really).

## TODO ##

* DONE ansible ruby chef inventory script
* kubernetes the hard way (new Ben suggested)
* https://www.katacoda.com/ worth looking at (new Ben suggested)
* https://jumpcloud.com/ (Esten recommended)


* backup warehouse/titanium
* start using github more
* chef nebula client config including cert generation
* chef fluent bit config
* chef/ruby namespacing, is there a technical reason for `node['cookbook']['attr']` style?
* ruby script to export bookstack articles from db and put in git wiki
* manage gitlab repos with terraform

* check out https://github.com/nicolaka/netshoot

* custom/modified fedora toolbox images, could possibly replace workstation ansible role
* terraform workspaces in good state (plan should always work)
* (almost done) chef in good state (figure out a good workflow that isn't too heavy, but keeps things realistic)
* (punted) track chef client failures (will get picked up by log insight agent, just need to install and config alerts)
* idea from Ben vagrant can use vsphere( see https://www.idkrtm.com/how-to-use-vagrant-with-vmware-vcenter/). This might get around my issues with running it in a fedora toolbox, and even better remove my need for virtualbox completely. Might be worth putting an esxi vm on titanium to still take advantage of the resources
* split gitlab runner to its own VM and/or give gitlab more memory, possibly also split postgres out

### Stretch Goals ###

* (punted) consider moving titanium to manjaro xfce and/or get theme config and run xfce on fedora or other distro
* (punted) consolidate repos into src directory where appropriate, consider adding bash script to clone all the repos for a new system
* (punted) terraform rancher
* (punted) terraform run by gitlab or azure devops
* (punted) https://bendews.com/posts/implement-dns-over-https/
* (punted) script to do daily git commit/push of notes?
* (punted) packer windows 10 template
* https://github.com/lingtjien/Grid-Tiling-Kwin
* matrix/riot bots (rss reader, grafana alerts, others?)

### Unplanned ###

* upgrade esxi hosts to 7 if possible
* graylog
* grafana

## Final Notes ##

## 25 ##

* traefik stack deployed to test swarm
* titanium uptime over 7 days (don't want to jinx but seems like new gpu has fixed the stability issues that plagued me for so long)
* guac stack file starting point: https://digitalmccullough.com/posts/setting-up-apache-guacamole-with-docker-stack.html

## 24 ##

* fixed up guac ansible role so it can be fully deployed via terraform/ansible
* installed vmware python sdk in fedora toolbox for ansible vmware inventory, nicer than dealing with venvs

## 23 ##

* finished changed to windns terraform provider, more work that could be done, but I think at least the part I set out to fix is done
* rebuilding terraform VMs to use versioned templates so they don't rebuild every time the template changes

## 22 ##

* terraform-windns-provider fixed read powershell slighly (removed try/catch that was masking errors), read method is not completely and will not detect changes made outside of terraform, should the read be calling SetID on success?

## 21 ##

* finally started updating the terraform windns provider
  * refactored create is working, but read still doesn't realize records were deleted outside of terraform, wonder if pwsh is not returning an error since the command itself probably succeeds
* provisioned windows jump box for testing terraform windns provider

## 18 ##

* rx 570 and fans arrived. removed the `pci=noaer` at boot, put pcie speed to auto in bios, displays turn off correctly, will see how it behaves overnight
* figured out the ansible packer issue, it was the classic vmware plugin issue, I need to standardize branches better, updated packer to use ansible master branch instead of development
* found some weird files on the gitlab runner starting with -v (probably had a typo at some point that caused weird behavior) in any case learned you can do -- on commands to signal the end of options to avoid issues with it thinking the file was another option
* packer builds with CI build number in template name finally succeeding, now I can convince terraform to stop complaining about template changes

## 17 ##

* consolidated code into ~/src on titanium
* updated el7 packer template
* katello failing to sync elrepo, thought about upgrading to 2.0 but that would require several intermediate updates so just powering it off for now

## 16 ##

* more postgres reading and chefing. Perhaps this has been a good example for TDD or its just the fact that I'm starting to understand chefspec and was able to start from scratch with a cookbook, but it has been a nicer experience than trying to bolt chefspec on to existing cookbooks. It's also kept myself focused on small iterative changes. So even if it adds a bit of extra time I can see the benefit and as I get more comfortable with chefspec I can definitely see that time decreasing.

## 15 ##

* started postgresql_wrap cookbook with chefspec tests
    * might be worth disabling transparent huge pages and enabling 1gb huge pages (need to look more into how 1gb actually work out of curiosity) https://www.percona.com/blog/2018/12/20/benchmark-postgresql-with-linux-hugepages/
    * also look at pgbench

## 12 ##

* removed colord on titanium (trying things at random for the display issues)

## 11 ##

netshoot research

# networks

to make for easier troubleshooting with one off `docker run` containers, make the overlay network attachable
`docker network create -d overlay --attachable perf-test2`

currently internal/dmz are not attachable so we will need to deploy/maintain troubleshooting container as a service

# notes

* docker exec must be run on node with the container being exec'd into
* whoami does not appear to have a shell


* iperf done
docker service create --name perf-test-1 --constraint node.role!=manager --network perf-test nicolaka/netshoot iperf -s -p 9999
docker service create --name perf-test-2 --constraint node.role!=manager --network perf-test nicolaka/netshoot iperf -c perf-test-1 -p 9999


## 10 ##

* upgraded bios on titanium and tinkered with display settings, flipped hdmi cable around and it doesn't work at all, set pcie to 2.0 with old card and no more aer errors, also no errors with new card if setting `pcie_aspm=off`, turned off global c states in bios (I think that's what fixed the idle cpu crashing before...), ugh mouse point flipped out after only a few minutes on old gpu
* virtualbox not updated for fedora 32, existing install from 31 seems to be ok, but new install not possible, might be time to fully move to libvirt, set VAGRANT_DEFAULT_PROVIDER to make test kitchen/vagrant use it unless otherwise specified (not bento boxes don't have libvirt versions)

## 9 ##

* docker iperf tests show interesting results, both docker nodes on the r610 gets 2.5Gb/s, hp 1.3Gb/s, across hosts 900Mb/s (also disabled firewald and selinux for now), both containers on same node/worker 4.3Gb/s

## 8 ##

* upgraded laptop to fedora 32, keeping kde for now, going to try to do most dev in toolbox containers

## 7 ##

* docker-ce does not play well with centos 8 and is in fact unsupported
* centos 7 docker swarm created (need to create a ruby/chef script to init swarm and upload token to netbox? unless a token already exist at which point it should join the existing swarm) `docker swarm join --token SWMTKN-1-0tih0qoww60pod3ilf85lc4e0l8ij9hb30wg047df984nvomd0-ei244126g55yvbn22z42xfdhm 192.168.30.107:2377`
* having trouble running things on the worker might be firewalld config

## 6 ##

* imported keepass into bitwarden finally

## 5 ##

* powered down log insight
* extended disk on graylog, thought I caught it soon enough but apparently not, so elasticsearch was read only mode, had to run `curl -X PUT "localhost:9200/_all/_settings" -H 'Content-Type: application/json' -d'{ "index.blocks.read_only_allow_delete" : null } }'` based on https://stackoverflow.com/questions/34911181/how-to-undo-setting-elasticsearch-index-to-readonly
* still seeing bad timestamp errors from nginx logs on graylog, tried fiddling around with parser timekey, no luck. Might have finally got it, changed timestamp to time and format to $msec, actually that just caused it not recognize the timestamp field and let the server add it's own. Think I finally got it for real, fluent bit nginx parser expected $time_local and key time. That was way harder than it needed to be, but I guess that's what I get for trying to just use some random nginx thing from the marketplace.


## 4 ##

Observability. The ruby script for ansible went together fairly easily, need to keep that momentum going...but what to do next? Ruby/chef to get certs for nebula for new clients

## 2 ##

Ansible inventory using ruby/chef is looking good. Only 60 lines and able to get environments, roles, tags and the attributes that matter. Could possibly reduce further by using a method instead of repeating each type on its own. I've done a couple bad things around the cli argument parsing since we know ansible will only do it a certain way, and I haven't added caching yet so I guess it's not quite as feature complete as the one from the internet. Ruby still doesn't feel natural to me but I am at least starting to understand it more.

## 1 ##

### Friday Fun Project ###

https://github.com/AutomationWithAnsible/ansible-dynamic-inventory-chef/blob/master/chef_inventory.py, 200+ lines of python to generate an ansible inventory from chef. It's ok, but missing some things that might be nice. Also it just seems longer than it needs to be. So I thought I'd give it a try myself.

* bash script simple using knife node list to create an inventory... apparently ansible expects json output so this is more work than expected
* ruby script to generate a more complete inventory/groups
* finally populate group vars as well

Started working on a ruby script to generate an ansible inventory from chef. Using the chef-api gem because it looked smaller than the full chef gem. However, it doesn't use the same config style as knife so maybe it's not any better. In any case one of the severely lacking points is it's method of ssl verification. For self-signed certs it expects you to provide the cert and key for it to verify rather than simply verifying the cert is the one you think it is. So I could either modify the chef-api code a bit to allow providing a ca cert, or just add the ca cert to my local trust store. Since it probably should have been added there in the first place I went ahead and copied it to `/etc/pki/ca-trust/source/anchors` and ran `update-ca-trust` (on my fedora workstation). In any case I can now successfully connect to the chef server and get a list of nodes.