## 30 ##

* upgraded nebula to 1.3, ironically my CA cert expired today as well, so I guess I've been running nebula for a whole year now. I also need to see if I can run the lighthouse as a container, though I guess I'd still need it on the host for ssh access so maybe that doesn't really make sense

## 28 ##

* blog now running on the k3s server (manually applied kubernetes manifest though)

## 26 ##

* blog container building on gitlab CI now

## 23 ##

* powered on graylog server and upgraded to 4.0 and elastseach 7.10. It looks like the advanced search features I was hoping to get are part of the enterprise version

## 22 ##

* installed xubuntu 20.10 on the laptop, probably should have went with kubuntu. Updated the workstation ansible a bit as well, now its in ok shape, need to make it more portable, i.e. feel comfortable running it on any system I need to work from. How to install vs code extensions from cli/config? Also typing/mousepad sensitivity seems off in xfce (i.e cursor getting moved around a lot on me and its super annoying), there also seems to be a fair amount of crashing apps especially after updates/config changes (i.e. font viewer crashed after installing hack font)

## 15 ##

* moved more bookstack docs over to wiki
* trying out tiling on kde https://github.com/kwin-scripts/kwin-tiling

## 14 ##

* unitedrpms version of the pulseaudio-module-bluetooth package seems to have super broke stuff, so unitedrpms is no longer trusted

## 11 ##

* wrote script to dump bookstack pages from mysql db to prep for finally moving to the git wiki

## 7 ##

* k3s on digital ocean centos 8 droplet not working, not sure whether to troubleshoot or just move to ubuntu. ubuntu went up just fine so I guess we'll go with that.
* dug more into the band Baptists and their record label has some bands worth looking at https://southernlord.com/bands/ 
* traefik on digital ocean deployed, wikidb secret created, wiki up, but looks like i'm missing some config for tls

## 6 ##

* k3s dns errors were caused by firewalld being enabled
* traefik nodePort working, but it doesn't seem to be using any values I set in the helmchart definition
* traefik service doesn't seem to get updated when the helmchart def does, had to delete the service and the batch job for it to be redeployed with the updated config

## 5 ##

* peer ip already present possible to produce when disconnect/reconnect nic, but swarm seems to recover? maybe need to generate intermittent network timeouts between random nodes in the swarm
* built 12 node swarm for testing, ansible role for basic docker install

## 4 ##

* updated packer el7 template because it was having boot issues but still not working, moving upgrade to yum and adding a reboot but now appears stuck on reboot task...maybe packer didn't like the reboot

## 3 ##

* disabled traefik on k3s (just add --disable traefik to systemd unit, will probably get reverted on upgrade)

## 2 ##

* traefik included with k3s is only version 1.7, spent a long time trying to figure out why my 2.x stuff wasn't working