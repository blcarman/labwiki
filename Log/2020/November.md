## 21 ##

* finished reading through kubernetes the hard way. There is less to it than I expected and basically no real explanations. It will probably be worth running through but not what I was expecting.

## 20 ##

* started kubernetes the hard way

## 18 ##

* let's encrypt with traefik is smooth enough will probably not worry about having a long term test server

## 17 ##

* got docker/traefik set up on linode test node and even got let's encrypt certs working. Centos 8 needs firewalld set to iptables instead of nftables. 

## 16 ##

* trying out linode because I got a $100 credit. Pro: api key permissions can be limited, cons: no firewall outside of host. 1gb = 818 MB vs 991 MB on digital ocean

## 11 ##

* https://codesignal.com/developers/
* dockerizing the matrix server, need an extra container just to serve the 2 .well-known files, seems a bit excessive (alternatively could set up the default port but this would require special traefik config as well)

## 10 ##

* still having wireless issues on the laptop, might have to stop using silverblue to see if latest firmware fixes it, but don't have high hopes

## 9 ##

* fluentd documentation is not the clearest
  * what can go into a directive section?
* tried out alacritty, don't really see the point of having a "faster" terminal, also no tabs and mouse support kind of lacking, might help reinforce good tmux usage though
* bat looks kind of neat, realize I lost my page env variable setting as well
* TODO: add -Z (for selinux contexts) option to exa
  
## 7 ##

* monthly backups
* fluent python is probably a little on the advanced side, but I am learning some things.

## 6 ##

* got powerline set up again (lost most of the config in the silly home deleting snafu). I like powerline but its clearly not one of those things I really missed when it wasn't there.

## 5 ##

* do I find solutions or make solutions? I finally have a reason to build a python flask app

## 2 ##

* upgraded titanium to fedora 33

## 1 ##

* why can't I modify `/etc/hosts` in Fedora Silverblue?
* traefik/wiki stack files progress, had some issues with silent rollbacks and things not being at the right levels.
* solved problem 5 of project euler