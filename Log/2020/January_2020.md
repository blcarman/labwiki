# January 2020 #

## TODO ##

* nebula deployment
* TODO: create systemd service for matrix pod, create service account to run as, set up postgres backups, maybe add config to ansible?
* TODO: netboxcli use config file for url and token
* TODO: start building out `netbocli add` functionality

## 29 ##

* `netboxli add` working (not that I logged it here, but been working on that for the past 3ish nights)

## 25 ##

* more tmux tweaking, making it automatic on ssh to titanium, instead of working on go like I had planned
* `netboxcli get` sort of working

## 23 ##

* started exploring spf13/cobra and spf13/viper, very cool

## 22 ##

* clean up netboxcli code a bit now that it can retrieve a thing via the api

## 18 ##

* go successfully make api call to netbox with auth token

## 17 ##

* installed random vmware drivers to fix workstation on 5.4, https://communities.vmware.com/thread/623768 and https://github.com/mkubecek/vmware-host-modules.git
* set up matrix server and connected to Brian's new server, need to write a blog post about it as there are a lot of gaps in the matrix federation docs
* deployed netbox on rancher via random helm chart, http://netbox.apps.bradlab.tech/

## 16 ##

* re-installed dns blacklist after upgrading edgerouter a few days ago.

## 15 ##

* a little more work on the nebula ansible
* hdmi port started working randomly again, either Salem bumped it, or running less somehow magically fixed it.

## 14 ##

* hack night, a little work on the nebula ansible
* terraform multi workspace demo working

## 13 ##

* FUCK hdmi, raising the desk and appears I either broke another cable or the connector in the graphics card... at least 3 of these now... ironically the displayport that was actually stuck in and about ready to rip something apart is still working just fine.
* moving home terraform to terraform cloud and creating workspace(s)
    * It wasn't exactly documented but it appears I could move my existing state to a workspace by simply creating a `terraform.tfstate.d/lab` directory and moving the default `terraform.tfstate` into it.
    * then moved to state to terraform cloud


## 12 ##

* Jellyfin replaced emby (fully open and refactored to use .net core instead of mono)
`/usr/bin/podman run --name jellyfin --volume /home/poduser/jellyfin_config:/config --volume /mnt/warehouse/movies:/movies:ro --volume /mnt/warehouse/shows:/shows:ro --publish 8097:8096 --publish 8921:8920 jellyfin/jellyfin`
* anycast with ospf working in vagrant boxes

## 11 ##

* titanium: upgrade virtualbox 6.0 to 6.1 due to changes in kernel 5.4, https://www.virtualbox.org/ticket/18945?cversion=0&cnum_hist=16 which in turn led to vagrant being broken and requiring edits to file since new version hasn't been released yet, https://github.com/oracle/vagrant-boxes/issues/178 

## 9 ##

* vagrant boxes for anycast set up

## 8 ##

* upgrade edgerouter from 2.0.4 to 2.0.8

## 3 ##

* nebula installed on titanium, scandium and digitalocean vps