### 12/29/18 ###

* good rancher cluster installed
   * emby app deployed but not fully tested
   * bookstack working well
   * gitlab in progress
   * vmware storage provisioner good

### 12/28/18 ###

* restored lab after LAG mess
* nfs moved to 10Gb on esxi hosts
* added a couple more home.lab records in AD
* changed lab log format to reverse chronological and 1 page per month
* bookstack running on rancher using the chart provided by helm

### 12/27/18 ###

* broke lab with LAG stuff

### 12/26/18 ###

* upgraded warehouse to kernel 4.19 (and registered with katello)