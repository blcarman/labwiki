### 2/20/19 ##

* removed gpu from desktop, pcie errors appear to have gone away

### 2/7/19 ###

* installed packer on titanium, had to remove the system "packer" with `dnf remove cracklib-dicts`

### 2/2/19 ###

* re-enable AD realm in foreman-proxy (seems to have reverted during update)
* registered mysql1 with katello and ran updates

### 2/1/19 ###

* updated katello from 3.9 to 3.10
* cleaned up git working area on workstation