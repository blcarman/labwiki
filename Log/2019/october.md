# October #

## 26 ##

* guac ansible role is "done" or at least enough so for my use case, should probably add a readme

## 25 ##

* fixed up guacamole/nginx ansible role a bit, still more work to be done, but almost complete.
  * added self signed cert creation
  * merged master back into development
  * generalized nginx proxy config a bit, still kind of guac specific though

## 22 ##

* power outage, turns out I didn't save the rackswitch config after changing the warehouse's port
* moved rackswitch to vlan 30 for mgmt, turned off http/https server

## 19 ##

* deleted infoblox VM (cleaned up terraform state with `terraform state list | grep infoblox | xargs terraform state rm`)
* upgraded terraform to 0.12
* add cloud-init to el8 template (guest customization still failing though)
* terraform building el7 vm and using dhcp is working

### Tiers ###

I'm tired of the lab having so much work in progress and not knowing where I left things, and/or wasting time automating stuff that may not need it. So I'm going to split the lab into tiers, full write-up in Projects/tiers.md.

### Workstation Monitors ###

I flipped my left monitor to vertical a bit ago but I think its going back to normal now. Maybe if I didn't have the ultrawide it would have worked better.

## 18 ##

* fixed emby playback issue, apparently permissions on /mnt/warehouse/movies had gotten mucked up, changed to 775

## 16 ##

changed mtu to 9000 on titanium and moved win10 virtualbox to bridged adapter to see if I could rdp to it while on vpn.

## 9ish ##

upgraded warehouse to 7.7 and kernel 5.3 (finally got it to default boot to the mainline kernel), seems like the built-in nic is failing so should probably start planning a hardware upgrade. I also upgraded NSX around this time to see if they finished moving the edge stuff to html5, but no luck there. 

## 2 ##

### Packer EL8 Updates ###

Working through a few minor glitches in updating the EL7 packer build to EL8. `--nobase` is no longer accepted as an option, `ntp` package appears to be gone, floppy kernel module is gone, NetworkManager is necessary but not actually part of core..., sshd didn't install, apparently doing the `--nocore` and then adding `@Core --nodefaults` doesn't work anymore. And finally had some ansible location issues when doing it locally so now testing with gitlab CI after making adjustments to only run the tasks when changes to their respective directories happen. I wonder if I would need to add the gitlab ci file itself to the changes field as well. Of course most of this is in the git commit messages. Looks like things are mostly working now, but I also found that apparently the ansible has been failing for a while but it doesn't actually cause packer to fail so might have to look into that more. Or more accurately ansible has not been failing but simply not matching hosts and so not doing anything. Finally, the EL7 template is building fully and correctly. However it looks like I'll need to tweak my common ansible role to work with EL8, probably should have tested that before trying to do the packer part. 