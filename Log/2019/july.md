## July 24 ##

* titanium fixed (see bookstack for details), TLDR gigabyte B450 c-states disabled

## July 7 ##

* moving lab log to gitlab wiki
* removed AD from vsphere identity sources
* ad01 is failing to demote so destroying entire domain (there's no reason to keep it)
* happened a few days ago but edgerouter mostly configured
* start using vs code remote on laptop so all dev work happens on titanium
* terraform successful in building VM and allocating IP
* minor updates to the packer template
* dns2 (pihole) built