# December #

## 31 ##

* renewed VMUG Advantage for 2 years (nearly cancelled but the workstation license made it worth it)
* thycotic API work stalled due to not having a "business" email

## 27 ##

I'm doing a poor job of tracking lab work again, and having trouble staying on track. Not that I really need to stay on track, but its difficult to visualize progress. Not sure whether I want to use a kanban board or just put it in the wiki notes, but I need to task things out a bit better and/or start doing better commit messages. Maybe while learning something I should even go as far as making a blog post for each commit.

## 26 ##

* more work on terraform thycotic provider

## 21 ##

* Thycotic Terraform Provider
    * added empty go.mod file which seems to have sorted out my go get issues
    * commented out terraform provider stuff while working on rest api client parts
    * able to get auth token with go
    * not able to get the auth token response parsed from json into struct, not sure what the problem is at this point