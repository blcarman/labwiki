### 6/24/19 ###

* built new AD server on 2019 (AD01 license expired), transferred FSMO roles

### 6/22/19 ###

* broke rancher completely 
* temporarily using bookstack VM
* infoblox back in action for the terraform abilities (had to reset and restore)

### 6/15/19 ###

* chef
    * server installed
    * chef-workstation configured on titanium
    * git repo create on home gitlab

### 6/14/19 ###

* replaced laptop battery
* decommissioned infoblox (removed from terraform as well)
* built chefserv VM
* decommissioned spacewalk
* ~~TODO: fix packer to not use spacewalk~~

### 6/12/19 ###

* installed sophos xg firewall vm
    * crashed overnight
    * no dns blacklist but does web content checking (https requires agent)
    * better than UTM so far