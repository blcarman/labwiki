# November #

## Plan ##

* ~movie emby to rootless podman on storage server (pre-req set up test storage server)~
* replace storage drives and move 3 TB drives to titanium for backups/replication
* esxi VM on titanium
* go dev environment
* swap 3560G with SG300? would this save power?
* ~Azure CI Pipelines~

## 2 ##

* emby moved to podman container service on warehouse

## 3 ##

* guacamole terraform complete

## 5 ##

* downloaded Fedora Silverblue to try again, considering moving dev work to a dedicated VM, is there anything I need a gui for/anything that can't be handled inside vs code/terminal?

## 6 ##

* took btrfs snapshot of warehouse, need to explore sub-volumes to make backups/snapshots easier

## 7 ##

* installing Fedora Silverblue on vcenter (probably should have started here rather than the laptop...)
* enabled experimental terraform 0.12 support in vs code (bring up command panel with ctrl+shift+p and terraform: enable, this was not at all clear from the helper text)

## 8 ##

* fixed bug in ansible/guac user-mapping.xml
* instaled tigervnc on fed silverblue according to https://www.cyberciti.biz/faq/install-and-configure-tigervnc-server-on-ubuntu-18-04/
    * need to make xstartup executable and still not quite working

## 9 ##

* switched silverblue vnc to use vino instead, enable via Settings -> Sharing (and reboot), and now it's working with guacamole. Note: vnc resolution is dependent on vmware console resolution to get 1920x1080, had to full screen it then close the console window

## 11 ##

* started home automation research

## 14 ##

* added vagrants to git finally

## 15 ##

* cleaned up laptop and titanium

## 16 ##

* fixed up terraform and deployed azure build agent

## 17 ##

* azure pipeline working

## 27 ##

* add perl to el8 packer template for guest customization (cloud-init caused problems, appears it requires more config)
* terraform consul el8 VM...almost. need to fix up chef packages to work with el8

## 28 ##

* getting 500 errors when trying to push new base policyfile. It turns out chef server still had the old AD server for its dns, which caused it to not be able to reach itself to do some stuff

## 29 ##

* went through a couple of the Consul getting started modules, so far it's pretty neat