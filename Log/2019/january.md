### 1/27/19 ###

* minor update to katello sync script

### 1/21/19 ###

* updated officeswitch
* vcenter:
  * created patch and extension baselines and test group, attached to labcluster
  * remediated lab cluster
* created auto-update GPO for AD01/domain

### 1/18/19 ###

* firewall backup script

### 1/16/19 ###

* added cron job to katello to promote content view automatically

### 1/15/19 ###

* removed memory reservation from nsx VM
* PAM auth on spacewalk fully figured out
* shrunk ranch3 resources

### 1/2/19 ###

* gitlab (cloud native) running on rancher

### 1/1/19 ###

* fixed docker exec in rancher
* synced repos
* replaced a 1TB drive with a 4TB sas drive in warehouse