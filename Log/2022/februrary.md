## 26 ##

* moved windows 10 vm to syno, wasn't able to run windows 11 on the syno due to some hardware incompatibility bs.

## 6 ##

* re-trying rancher install, deleted hopefully more namespaces this time around. Still seems broken but going to let it sit for a while just in case

## 5 ##

* installing rancher on rke1 (or at least trying to)... first time failed and now the bootstrap secret either never gets set or doesn't work...
  ```bash
  helm install rancher rancher-latest/rancher \
  --namespace cattle-system \
  --set hostname=rancher.k8s.home.bradlab.tech \
  --set replicas=1 \
  --set ingress.tls.source=rancher-tls \
  --set ingress.extraAnnotations\.'cert-manager\.io/cluster-issuer'=prod-issuer \
  --set bootstrapPassword=Pa55word
  ```
  note: need to edit the ingress and remove the default cert issuer
* added 100gb to rke1624803



- name: CATTLE_BOOTSTRAP_PASSWORD
          valueFrom:
            secretKeyRef:
              key: bootstrapPassword
              name: bootstrap-secret
