## 30 ##

* tiny bit of kodekloud k8s studying
* messed around with the cursed usb dac some more, still no luck. Tried blacklisting kernel modules, booting into a fedora 34 livecd. This crappy dac totally isn't worth the effort but at this point I just really want to know why it works on the laptop but not the desktop, in any case it does seem to be hardware related, since I haven't even been able to pass through the usb to a VM. (update 3/5/22 tried from live cd and it worked...so I guess i'm reinstalling the os on titanium)

## 20 ##

* moved the desk in the office out from the wall, so I could get the speakers closer together. Also dropped the crossover down considerably, will probably keep tweaking it a bit more, but getting close to optimal.

## 18 ##

* moved REL Strata III sub up to the office. I think it'll be better utilized there. It didn't take too long to get set up but will probably make a few more tweaks in the next few days.

## 17 ##

* built out new gitlab-runner on ubuntu

## 16 ##

* got rid of armoire in the weight room, people who picked it up were not at all prepared, so we ended up helping them carry it and driving it to their house. They were nice and made us tea though.

## 15 ##

* built speaker stand for the classix out of a 1x12 and some scrap 2x4s

## 9 ##

* added tls to warehouse ingress
* imported unifi controller vm, will move to k8s later

## 4 ##

* screensharing causes desktop to crash on wayland, so moved back to X11.
