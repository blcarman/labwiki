## 30 ##

* started deploying smallstep ca to test lab cluster (need to set up storageclass)

## 15 ##

* created user/certificate on test cluster. It took a second to put the pieces together since its based around certs and rolebindings rather than just "create user" and assign user to role. The kodekloud way was helpful but doing it myself end to end definitely filled in some gaps that I missed in the labs.

## 10 ##

* upgraded laptop to fedora 36 including toolbox.