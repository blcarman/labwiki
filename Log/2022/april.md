Here we are at the end of the month and it feels like things are finally getting back on track. I've got a test k8s cluster I can rebuild on demand. We're down to just the one puppy who is still a bit crazy but at least mostly sleeping through the night now. Ironically, that's allowed me to start staying up late again.

## 29 ##

* created playbook for destroying VMs
* with all the dhcp troubles maybe I should go back to run netbox for ipam. It also seems like setting static mappings at the system level is messing with some of the ones set at the dhcp server level.
* finally got the testranch controlplane running again, also added ability to upgrade rke2

## 26 ##

* `/etc/machine-id` existing in the template caused all the VMs to pick up the same dhcp ip, but looking in the packer definition that file should get removed. I messed around with the user-data for the packer installation and set the dhcp-identifier there to mac and that seems to have fixed it.
* got remote virt-manager working, https://discussion.fedoraproject.org/t/how-can-i-add-myself-to-the-libvirt-group-in-fedora-silverblue/1412

## 25 ##

* tried rebuilding the test cluster but all the VMs seem to be getting the same dhcp ip... looks like they have different macs so not sure what's going on there... turns out this was once again the duplicate machine-id thing. I guess I must have just used static IPs instead of fixing before.

## 24 ##

* vm building on titanium silverblue is configured and working. I think that is one of the final pieces to getting things back to the way they were before silverblue.
* also did did it a couple weeks, but stopped using usb dac, and am now using the yamaha a-s501 dac via optical cable. I can't honestly tell a difference and it sounds amazing.
* completed (well except for staining/poly) one oak speaker stand, turned out pretty good, though obviously not perfect. Table saw was a huge help, but I could still probably benefit from a cross cut sled. Also turned out one of the boards was bowed a bit, I probably should have went back and got a new one but I rolled with it. In any case I think I like these over buying some expensive cheap looking stands. It might all be in my head, but I think it sounds better than the super wobbly stand I had before. Now we just have to see how long it takes me to do the other one.

## 23 ##

* get vm deployment working on titanium again. Packer build was getting stuck until I removed the network config. Added br30 bridge via nmtui (probably need to add to code), looks like i also need to do a virsh net-define ( think I previously manually added it in virt-manager)

```
ansible-galaxy collection install community.network
sudo dnf install -y dnf-plugins-core
sudo dnf config-manager --add-repo https://rpm.releases.hashicorp.com/fedora/hashicorp.repo
sudo dnf -y install packer

rpm-ostree install guestfs-tools
```

## 8 ##

* nebula installed on molybdenum(framework laptop) via copr repo. Looks like the service surived an update too so that turned out surprisingly easy.

## 6 ##

* got noctua cpu fan. seems to a good improvement, though can still hear it when it spins up, might need to adjust the fan speed in the bios to let the cpu run a little hotter, or maybe go the opposite and increase the minimum speed of the fan so it doesn't have to burst as much. Constant noise would be less annoying than the short increases.