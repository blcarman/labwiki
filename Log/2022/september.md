## 18 ##

* dvd transcoding
DVDS=$(find . -name "*.mkv" | grep -v "eaDir")  
Roku 2160p60 4K HEVC Surround

for dvd in $DVDS; do flatpak run --command=HandBrakeCLI fr.handbrake.ghb - "Roku 2160p60 4K HEVC Surround" -i $dvd -o encoded/$dvd; done

handbrake does not care if output file already exists
