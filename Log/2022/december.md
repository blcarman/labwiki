## 27 ##

* add removal of old kernels and unused dependencies to unattended-upgrades on rke1 server

## 11 ##

* rke2 auto upgrades working, set it to channel so it should keep things on the latest stable version. I think this would be nice for at least rke2 at work.
* going with ubuntu unattended-upgrades for the OS side, the system-ugrade-controller seemed a little over complicated for OS stuff, that is it would need version pins stored in the secret

## 10 ##

* got rancher working again (turned out I could just reset the admin password)
* ugh syno csi got messed up during the whole ubiquiti switch fiasco, deleted loki and jellyfin, but afterward realized I could have probably just updated the syno client config secret and it would have reconnected
* upgraded/pinned jellyfin to 10.8.8
