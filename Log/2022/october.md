## 29 ##

* installed vs code ansible extension on titanium, but since I'm using the flatpak/silverblue I don't have the ansible commands it wants on the host. I guess I have the option of either installing ansible on the host or running vs code in a toolbox. Somehow, silverblue gives us more options but makes them all feel wrong. I remembered flatpak vs code's terminal does have pip3 so I can install ansible that way. Nevermind that doesn't seem to be persistent and gives weird errors when trying to run ansible anyway.
* stumbled across some ansible recommendations https://github.com/redhat-cop/automation-good-practices. It's a nice centralized docs for a lot of the practices I've pieced together over the years

## 28 ##

* removed ip from vlan30 interface and that fixed the VMs on br30, think I just need to put the IP directly on the br30 if I want one on the host and the VMs

## 26 ##

* br30 on titanium doesn't seem to be working any more, might have messed up configuration when I was troubleshooting the unifi switch

## 23 ##

* unifi switch working. Was able to move the uplink to another port then change the management network to trust and then switching it back to the original port with the trust vlan being native. The APs also do not like the management network being changed to trust, so I guess I'll just leave that alone on those. And finally done, powered down the Cisco 3560g and it's so quiet now. Also looks like the Cisco 3560g was using 100 watts, compared to like 20 for the new USW-24-POE.

## 22 ##

* had to reset the unifi switch a couple more times. Got vlan 30 working by marking it as native on the uplink port, but that broke the connection from unifi console to the switch itself. I wonder if I can get it back by just moving the uplink to another port. I hope this switch lasts for a long time, because its the last unifi device I want to deal with for a while.

## 21 ##

* started trying to migrat to the new unifi switch, thought it was working but then the AP clients were flapping and storage server(vlan 30) stopped working
