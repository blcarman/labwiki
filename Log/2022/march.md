Most of February was consumed by puppies and work. Not that I really did much with the puppies, but they did interrupt our sleep and life a bit. Hopefully, things will get back on track this month. Work at least seems to be settling back to a sort of normal state, trying to not let myself go back into workaholic tendencies. And here we are at the end of the month and it was again consumed mostly by puppies, but this month they got to stay home and generally interrupt my days.

## 31 ##

* turns out fedora silverblue's firefox still has some major issues with reddit and was causing ridiculous bandwidth usage. Good and bad firefox itself no longer crashes, so its not super noticeable that it's causing problems, until you get a usage warning from your ISP. Running firefox from a toolbx container seemed to be ok, but not sure how to do that without running it from the commandline. I ended up using the flatpak and overriding the flatpak settings to use the normal profile location in case I do ever want to switch back `flatpak override --user --filesystem=~/.mozilla org.mozilla.firefox`. For some reason firefox still created a new profile and I had to set the default back to my original one, but after that things are looking good. (update 4/5/22 looks like the flatpak does have the same issue)

## 5 ##

* tested usb dac on titanium from a live fedora cd and it magically worked... so now I guess it's time to reinstall, will probably move to kinoite while I'm at it. Good thing I moved the windows VM to the syno last week.
