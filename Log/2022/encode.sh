#!/bin/bash
IFS=$'\n'

cd /mnt/media/movies/dvd
DVDS=$(find . -name "*.mkv" | grep -v "eaDir")  

for dvd in $DVDS
do
  if [ -f "encoded/$dvd" ]
  then
    echo "$dvd already encoded, skipping"
  else
    echo "encoding $dvd"
    flatpak run --command=HandBrakeCLI fr.handbrake.ghb - "Roku 2160p60 4K HEVC Surround" -i $dvd -o encoded/$dvd
  fi
done
