I'm sure this is duplicated from somewhere else, but oh well. To make it better moving the ansible and blog stuff to gitlab issue boards

* app for tracking daily work, that is 3 things to accomplish, perhaps also create timers for a few broad categories (i.e. wasted time, working time, unplanned work?)
* ansible variable resolver
    * grep a role for all variables referenced, basically what's between double curly braces `grep -ro {{.*}}` and before a
    * first(ish) attempt: `grep -hoPr '(?<={{)[A-Za-z0-9_. ]*' | sed 's/^ *//g' | sed 's/[ ]*$//g' | sort --unique`, spent roughly 2-3 hours while watching movie
* stock tracker/analyzer (moving average), positive/negative in the news, sources to query?
* automatic updates for everything
* A linux command every week
* A python script every week (Project Euler)
* Performance tuning/troubleshooting
    * open file limits (how to tell if they've been reached?)
    * sar ( I always forget about this one)
    * abrt ( not really performance related, but what can this do for troubleshooting)
* not the first time i've had the idea, but a noir style book about a normal IT guy
* terraform the homelab... like all of it
    * dns updates via terraform or dhcp/firewall?
* lab status page on blog (thinking like a x days without issue type of thing)
* ipv6 in lab
* webui for scripts (i.e. button push to run a backup script or reposync)
* stackstorm, might be kind of cool: https://docs.stackstorm.com/start.html

* ansible module for requesting certs from windows/AD cert authority
* k8s operator for nebula, should create cert secret and set up sidecar

* implement moonlander style keyboard in framework laptop (maybe create a custom keyboard driver?)
* cron job to do daily git commits "<date> - automatic cleanup because I can't be trusted to commit this myself"
* desktop phrases
    * everything goes in the fucking journal

* funkwhale? not sure about this one, looks like it could be cool, but the website does not do a good job showing what it is
* kasm
* sidero metal
* app shutdown scripts
    * friday 4pm auto shutdown slack and teams
    * kill webmail tab...