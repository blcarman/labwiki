## 27 ##

* titanium: moved boot partition over to new wd black drive, only took 3 or 4 times to follow the fedora grub instructions correctly
* titanium: pvmove of root from samsung 960 evo to wd black went at roughly 1100MB/s

## 25 ##

* configured ubuntu laptop to automatically unlock gnome keyring at login with kde/sddm, https://en.opensuse.org/GNOME_Keyring

## 24 ##

* pushed monitors back a good 6 inches, not sure why/when they got pushed so far into the middle of the desk, but I was getting really annoyed at how small my desk seemed to be.

## 23 ##

* k3s HA cluster working, need to write up a blog post

## 22 ##

* packer ubuntu template still has issues with network interfaces, apparently something now creates a machine-id that was not getting reset when deploying new templates, this machine-id is used in the hash that systemd-networkd provides as the client id in dhcp requests causing all the servers to get the same IP, https://vmware.github.io/photon/assets/files/html/3.0/photon_admin/clearing-the-machine-id-of-a-cloned-instance-for-dhcp.html

## 21 ##

* enabled hardware offloading of nat and vlan on the edgerouter, didn't do any performance testing before/after though
* found out edgerouter does support bgp in the cli, so will be looking into that with kubernetes to avoid needing haproxy/keepalived
* finished reading kubernetes in action
* updated edgerouter (accidentally clicked the update button because I thought it would give me some release notes/ask for confirmation)

## 20 ##

* installed updates on warehouse
* seafile server via podman-compose running on warehouse, does appear to store files as blobs instead of the original file, not sure how I feel about that
* tweaked nebula on titanium/warehouse to support 8800 mtu and allow seafile, should probably do some performance tests
* warehouse had to allow port 8080 through firewalld even though going over the nebula network, need to investigate that more, but at least I have transport encryption without actually doing https... right?

## 19 ##

* packer ubuntu vagrant box mostly working, spent way too long fighting with late commands and configuring the vagrant ssh key

## 8 ##

* virt-sparsify will shrink ubuntu packer image down to near the expected size, should add this step to packer post-provisioning, only saved 200mb on centos 7 so probably not necessary there. Also for some reason host ssh key didn't get generated on the last packer install, pretty sure it had worked before...

## 7 ##

* think I might have gotten the ssh timeout issues sorted for ubuntu packer image, now I just need to get the size down to something reasonable. Inside the VM it thinks only about 5.5 gb its used (including a 3gb swap file) but the qcow2 is 10gb not sure what's going on there. Looks like this is a losing battle, might be able to do bad things deleting firmware, but would only want to do that for vagrant images. Looks like it should end up around 2 gb after I remove the swap file.


## 6 ##

* finally got playbooks running in awx, I was getting an inventory parsing error because I was including an ansible.cfg in my repo that didn't have the json inventory enabled. Ansible still has not gotten any better about its errors
* changing gears, I think I have ipv6 working now, though there is still some stuff I'm not 100% sure about

## 4 ##

* started work on ubuntu autoinstall packer image, seems to be decent so far, will be curious to see if I can leverage a more unified setup between ubuntu and centos with cloud-init
* installed ansible awx on testk3s. It has the classic red hat webui now, but otherwise doesn't seem to have changed much in the past few years since I last looked at it

## 1 ##

* git repo cleanup, commit any outstanding stuff, move repos that are not mine to ~/external_src to speed up vs code