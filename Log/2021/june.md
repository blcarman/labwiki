## 27 ##

* ran updates on titanium a few days ago and that was apparently a bad idea. systemd-resolved now needs restarting after booting because it and networkmanager can't fucking get along anymore. KDE will randomly not do autostart stuff, but doesn't throw an error either. The one good thing I got is I can change my bluetooth headset profile from the little sound taskbar icon, so now it's only 3 clicks instead of 20 (but I still shouldn't have to do that in the first fucking place). I can no longer logout of kde either, also screen won't wake up if I leave it on the login screen.

## 25 ##

* blog topic: avoiding the runaway train style of coding. That is commit often even if it might be bad code. Too frequently I try to iterate through something and about halfway through I get it mostly working but then end up completely re-doing it because it wasn't working 100% only to make things worse, having that partial working commit to roll back to would make it easier to get back to a known state without having to redo all the original work it took to get there in the first place
 
## 21 ##

* figured out the borg stuff, had some typos in my ssh command, and also apparently spaces matter, needed to remove the space after the command and before the "restricts"...wasted way too much time on that

## 20 ##

* some notes about world readable home directories
  * ansible does not seem to use adduser so `/etc/login.defs` is ignored during user creation but `UMASK 027` still needs to be set here so pam-umask picks up the change for all directories the user creates
  * shell specific umasks appear to be settable in /etc/profile but /etc/login.defs seems to be the best as long as pam-umask is in use.
  * need to add to packer template and/or ansible common role
  * fun fact, df -h run as a user will not show mountpoints for other users with this set, but also doesn't throw a permission denied
  * this borg guide leaves a lot of gaps, once I figure it out I should submit a PR to make it more complete, https://borgbackup.readthedocs.io/en/stable/deployment/central-backup-server.html?highlight=ansible#machines. Almost feels intentional to push users to borgmatic/borgbase
* need to remove vagrant stuff from default packer template
* trying to adopt a commit often and small philosophy to avoid the branch messes I keep creating with multiple half finished things
* also trying to use ansible-doc command instead of google for faster ansible module info

## 17 ##

* ubuntu template missing... rebuild with packer running into issues, had typo in network section, but now machine ssh keys didn't get created and doesn't seem like user authorized keys got configured either
* built borg VM but will likely need to rebuild after fully fixing packer template

## 4 ##

* moving /tmp to tmpfs on scandium laptop, I thought it was the default all over the place but apparently just arch and fedora. It gets weirder, it was enabled on my testvps VM running 21.10 that I did an interactive install with, but its not enabled on the 20.04 packer template. Not sure if it was set by 21.10 or something else enable it...
```
sudo cp -v /usr/share/systemd/tmp.mount /etc/systemd/system/ 
sudo systemctl enable tmp.mount
```

## 1 ##

* hit rearm limit `slmgr /rearm` on my win10 vm, need to create a packer template
```
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
comment out admin group from %PROGRAMDATA%/ssh/sshd_config

restart ssh server

```