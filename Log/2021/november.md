## 13 ##

* nebula pod working on nginx ingress ports without adding a sidecar by adding `hostNetwork: true`. This was more just an experiment, since I'll still want it to be a sidecar to ensure it ends up on the same host as nginx. Hostnetwork true also fixed the connection issues with titanium, I guess it made it realize its on a local network. So maybe I do keep it separate from nginx and either make nginx a daemonset or otherwise make them run on the same host.

## 12 ##

* nebula pod is working but might need to drop the local subnets part since it's not able to ping titanium or vice versa. Also updating the configmap does not trigger an update to the deployment/pod. Removing local subnet part of config did not make a difference for ping. I did end up using a few sources from the internet to help a little bit. For the deployment config: https://github.com/buildsociety/nebula.

## 9 ##

* powered on build1, ran updates, installed latest gitlab-runner, changed podman to use crun, built new blog image and deployed it
* disabled shared runners on ansible project, it's build succeeds now too
* set up git repo and CI job for nebula image

## 8 ##

* netbox running on devrke2 cluster, I think for lab stuff I prefer using ansible over flux, its just a little more comfortable and gives immediate feedback

## 3 ##

* manually force installing the community.kubernetes collection fixed the errors from yesterday, guessing an old version is getting bundled with ansible even when pulling latest from pypi
* dev postgres deployed

## 2 ##

* fedora 35 upgrade day. Titanium had no missing packages or conflicts this time around. Laptop was already on the 35 release branch so nothing needed to be done there.
* fedora 35 has "old" version of ansible that does not play nice with the k8s modules, think I actually hit this bug a while back as well https://github.com/ansible/ansible/issues/72769. Still seeing issue after upgrading ansible and openshift client.
