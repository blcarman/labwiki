## 30 ##

* successfully deployed loki to prodranch via fluxcd, need to fix the tls ingress though, also set up fluent-bit daemonset and added loki source to grafana

## 29 ##

* deployed rancher monitoring to prodranch, note make sure to stay on the 9.4 release, there's a weird 14.5 one that doesn't seem to work

## 28 ##

* added 50 gb disk, increased ram to 16gb and cpu to 3 vcpu on prodrke nodes
* add digitalocean-dns secret to prodranch for certs
* from cert-manager logs, no ingresses set up with that so not sure why its throwing warnings `W0729 03:46:47.720614 1 warnings.go:67] networking.k8s.io/v1beta1 Ingress is deprecated in v1.19+, unavailable in v1.22+; use networking.k8s.io/v1 Ingress`
* digitalocean token thing is not working, maybe i have format wrong... k8s will encode to base64 when you create the secret, you don't need to do it beforehand. This feels like an instance of poor documentation, but maybe I'm just a noob, in any case cert issuer is now working and netbox has a shiny legit cert

## 27 ##

* limit journald disk usage: https://wiki.archlinux.org/title/Systemd/Journal#Journal_size_limit
    * set prodrke1 to 500M limit, was previously at 1.5gb, should still double size of / on the servers

## 26 ##

* removed test flux from prodranch, and updated to multi-cluster config and bootstrapped test and prod ranch
* noticed disk pressure on prodrke servers, removed lxd snap (should consider ripping out of packer template as well)

## 25 ##

* starting to hate having to ctrl+shift all the shortcuts in kitty terminal, need to either change config or find something different, seriously why is ctrl+shift even a thing, its scrolling sensitivity also seems oddly special...

## 24 ##

* tested bitnami postgresql helm chart on its own and db password worked as expected...still don't see what funny business netbox chart is doing though

## 23 ##

* netbox moved to prodranch
    * installed netbox via helm on scandium, re-used existing backup config, but applied manually
    * still can't figure out what's up with the netbox postgres helm chart, went in and reset the netbox user password to what is in the secret
    * manually added ingress
    * db restore succeeded but missing things like devices... just kidding they showed up after a bit, must be calculated and stored in redis
    * TODO: add postgres backups to borg
* I should probably put a worker node on titanium, since its got more cpu at this point

## 22 ##

* prodranch kube-karp was stuck in a weird state, `[WARNING] Bad digest - md2=[b21229d6...] md=[8e79dcf1...] - Check vhid, password and virtual IP address`, had to delete 2 out of the 3 pods for it to sort itself out. Downside of using a small project like this is there isn't much documentation on issues. Looks like there was a PR started to add a healthcheck feature but looks like it got abandoned. I wonder if I could run a keepalived daemonset with a different VIP to see if it behaves similarly. Since all the VMs live on the same physical host it's hard to point the finger at something like the network

## 21 ##

* installed argocd on prodranch cluster
* updated the matrix chat server

## 16 ##

* starting the netbox migration, apparently I never put any retention policies on the postgres backups, good thing each one is only 400kb
* netbox helm chart giving issues with postgres password again... might be time to abandon it. It looks like the bitnami postgresql chart says you have to provide the passwords every time you make a change or upgrade, is this an actual helm limitation or just a poorly written chart? 

## 15 ##

* configured nfs provisioner on prodranch

## 14 ##

* rke2 and rancher ansible fully working, "prod" cluster deployed, still need to do the following:
    * ~configure storage class~
    * ~migrate netbox from testk3s~
    * jellyfin
    * grocy?
    * ~flux/argo~
    * ~loki/prometheus~

## 12 ##

* got new usb microphone that works well
* limited discord flatpak permissions
```
flatpak override --user --nodevice=all com.discordapp.Discord
flatpak override --user --nosocket=pulseaudio com.discordapp.Discord       
flatpak info --show-permissions com.discordapp.Discord
```
starting config:
```
[Context]
shared=network;ipc;
sockets=x11;pulseaudio;
devices=all;
filesystems=xdg-download;xdg-pictures:ro;xdg-videos:ro;

[Session Bus Policy]
org.freedesktop.portal.Fcitx=talk
org.freedesktop.Notifications=talk
org.kde.StatusNotifierWatcher=talk
com.canonical.AppMenu.Registrar=talk
com.canonical.indicator.application=talk
com.canonical.Unity.LauncherEntry=talk

[Environment]
XDG_CURRENT_DESKTOP=ubuntu:GNOME
```
end result:
```
[Context]
shared=network;ipc;
sockets=x11;
filesystems=xdg-download;xdg-pictures:ro;xdg-videos:ro;

[Session Bus Policy]
org.freedesktop.portal.Fcitx=talk
org.freedesktop.Notifications=talk
org.kde.StatusNotifierWatcher=talk
com.canonical.AppMenu.Registrar=talk
com.canonical.indicator.application=talk
com.canonical.Unity.LauncherEntry=talk

[Environment]
XDG_CURRENT_DESKTOP=ubuntu:GNOME
```


## 3 ##

* installed crunchydata postgres operator on testrke cluster
* it appears my fleet config for testrke got wiped after the upgrade (or maybe I never had it configured in the first place?), might be time to rebuild the cluster

## 2 ##

* another `dnf upgrade` on titanium appears to have fixed the systemd-resolved issue but I didn't see any actual package changes that would have indicated that
* borgserver was failing to boot because I had the backup disk set by uuid... but the uuid was attached to the btrfs filesystem I wiped so it disppeared after I rebooted
* started uploading borg backups to backblaze b2, looks like it's going to take a week to upload the approximately 500gbz, getting 700KB/s