## 31 ##

* dynamic nfs provisioning working on testk3s

## 30 ##

* did OS updates on warehouse

## 28 ##

* netbox postgres db backups set up
* ansible-lint is being a pain, won't just look at all files in a folder

## 24 ##

* nextcloud helm chart working but performance is terrible, might be the sqlite db but its a totally empty server and it can't even run? seafile might be a better option, only does file stuff but that's really all I need.

## 22 ##

* I think the nebula ansible is now really fully working. I got the ssh forwarding for the ansible.posix.synchronize working, and had to sort out permissions since become makes it try to ssh as root. Considering setting up an admin user instead of my normal user but will deal with that later.

## 21 ##

* updating the netbox helm resource will cause it to regenerate a db password but postgres won't actually change the pw so netbox loses access to the db... seems odd that I'm the only moron that has come across this. I was able to fix it with the help of https://docs.bitnami.com/aws/infrastructure/postgresql/administration/change-reset-password/ 
* ansible netbox IP allocation working
* tried atom again with the platformio terminal, it doesn't work with zsh, so atom is still a no-go.
* nebula ansible now fully working, other than a couple issues with the netbox provider it ended up a lot easier than I expected.
  ** just kidding apparently I should have tested with a client that wasn't the CA itself. I tried setting up ssh agent forwarding but didn't seem to make a difference, also did change to become false so it wouldn't ssh as root but have a feeling that might end up with file permissions issues after I get ssh working. In any case I think I'm done for the night. Oh wait is it trying to go from scandium to the warehouse for the ssh connection...eh even if it was ssh forwarding should work the same. Futhermore I think ansible's output `[scandium.lab.bradlab.tech -> warehouse]` is just being silly with the arrow. 
```
fatal: [scandium.lab.bradlab.tech -> warehouse]: FAILED! => {"changed": false, "cmd": "/usr/bin/rsync --delay-updates -F --compress --a
rchive --rsh=/usr/bin/ssh -S none -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null --out-format=<<CHANGED>>%i %n%L /mnt/ware
house/lab/ca/nebula/scandium.crt scandium.lab.bradlab.tech:/home/brad/", "msg": "Warning: Permanently added 'scandium.lab.bradlab.tech,
192.168.50.84' (ECDSA) to the list of known hosts.\r\nPermission denied, please try again.\r\nPermission denied, please try again.\r\nb
rad@scandium.lab.bradlab.tech: Permission denied (publickey,password).\r\nrsync: connection unexpectedly closed (0 bytes received so fa
r) [sender]\nrsync error: unexplained error (code 255) at io.c(228) [sender=3.2.3]\n", "rc": 255}
```
looks like I need to tell ansible to use ssh agent forwarding maybe and then tell the synchronize module to use the ansible ssh config, https://www.calazan.com/using-ssh-agent-forwarding-with-ansible/. For real though setting this aside until tomorrow.


## 20 ##

* put new 2x32gb ram in desktop and the 4x16gb in the storage server. Storage server doesn't like hardware changes...and my ansible put in a bad fstab, so future self should always confirm a reboot works before doing hardware changes in the future. Also thought of new name for the storage server. the NAS And Lab server or NASAL server, not actually going to call it that but maybe I will put it in a joke in the blog.

## 19 ##

* jellyfin configured on the new storage server, still had to go through the initial wizard but maybe I can track down the config and add it to ansible as well.
* flashed moonlander keyboard with first set of customizations

## 18 ##

* ran CIS ansible on testvps during study group, took snapshot first, need to see if there were any ill effects
* plugged in the old Cisco SG300 so I could have a wired connection for the desktop and storage server, couldn't remember the port mappings but was able to guess well enough, will have to break out a console cable and config if I decide to use long term.
* updated storage server ansible for opensuse, config is 99% working. Need to figure out a network error in podman jellyfin, and need to set passwords for samba users.

## 16 ##

* opensuse installed on the "new" storage server, was kind of a pain but mostly due to me not being familiar with the installer. It left an existing partition on and setting up a vlan network was downright painful. 
* moonlander is still going kind of slow but getting better.

## 15 ##

* moved titanium to the new case
* trying to get used to the moonlander keyboard, going to take some time. Part of the frustration is just the unmarked keys but I realize its kind of the nature of the beast. Not sure if I should go all in and force myself to use it for everything or do a couple hours a day or something. Also wish the trainer would tell me which fingers to use. It did say to use index finger for a thumb key which seemed odd. I think I keep trying to find keys based on their old locations instead of the ortho but getting better. hard to angry type as well. L

## 13 ##

* finally thought of a good problem to use sed/awk to solve. I needed to update a version in a yaml file. Of course there are multiple versions so its not as simple as just finding "version" and replacing it. Thinking about it more it seemed better to find something that could parse the yaml and edit the specific key I was looking for. So I ended up using yq instead and now I'm back to wondering if I'll ever do more than basic sed find/replace commands. 
* netbox and traefik running on testk3s via helm charts... and netbox redeployed with persistent storage for the db

## 12 ##

* ~Late Night Devops~(apparently there is already a discord server using that name) blog posts: ideas that sound insane during the day (e.g. running virtualbox from inside a docker container) but really aren't so bad

## 10 ##

* add kubectl install to ansible and run on titanium

## 6 ##

* attempted to automate install of gitlab runners but apparently they haven't implemented an api for getting a new runner registration token, this is a deal breaker for recommending gitlab for anything more than personal projects. There appear to be some feature requests for it that have been open for a while so its surprising they haven't addressed it.

## 5 ##

* created testk3s vm (el8) for more scientific approach to k3s, will set up a gitlab runner and force changes to better track what I'm doing

## 4 ##

* vagrant appears to have broke when I started using the hashicorp repo, had to mask it and reinstall fedora version with vagrant-libvirt package as well

## 3 ##

* standard dnf upgrade caused issues with pulseaudio again, had to run `rm -rf .config/pulse` and reboot
* also managed to use all RAM and fill swap on desktop, maybe I should go back to dedicated lab hardware?