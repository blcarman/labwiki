## 30 ##

* moved mobo standoffs around on the supermicro, not sure it will actually make a difference though.

## 29 ##

* I think i can move the mobo standoffs around on the supermicro chassis (apparently there are screws on the underside of the case for some of them), so I think I might need to give that another try for the storage server.

## 28 ##

* netbox restoration
  * install kubegres
  * deploy pg instance
  * restore netbox db
  * create k8s secret
  * deploy netbox using external db

## 25 ##

* added 120mm front fan to titanium desktop, also replaced thermal paste with the black tube that came with the cooler (not sure which one I used before), seems like it might be handling it better, but still never quite got to steady state when running `stress-ng --matrix 0 --matrix-size 64 --tz  -t 600`, but didn't get above +70C either. So not sure if it really made a difference, but definitely not worse. A little internet reading says my temps are good. I was mostly just annoyed at the amount of cycling the cpu fan was doing, but maybe if that gets too annoying I need to just replace it with something quieter.
* add some specific colors to the moonlander keyboard for hints to help use layers better
* looks like I can add a nebula sidecar to the existing nginx ingress helm chart with the `extraContainers` option.

## 24 ##

* layer switching keys in bottom corner of moonlander is probably a good part of why I don't use them, middle pinky seems like a good place to put them if i can get used to the change

## 23 ##

* set up wireguard vpn on the laptop, stupid easy to import config into networkmanager

## 22 ##

* still need to ansible the kubeconfig copy, in any case manually copied it over, messed with tmux copying to use xclip since otherwise it would include some extra junk (think it was copying ALL the tmux buffers to the clipboard, not just the latest)
* when creating awx instance its important to specify the namespace, operator by default only watches the namespace you originally set
* need to set nfs storage as default storageclass
* awx ingress still working after full cluster reboot
* awx collection docs https://docs.ansible.com/ansible/latest/collections/awx/awx/index.html#plugins-in-awx-awx (not sure why there isn't a link to this from galaxy)
* successfully created awx organization via ansible
* recommend we do a single awx with credential/isntance group per datacenter
* firefox on laptop was running great until I opened element... could the gnome keyring be what's really causing problems? killing the process for gnome-keyring and xdg-portal didn't make a difference...a few reboots later and definitely seems related to element/vs code, both unlock the gnome keyring and start /usr/libexec/xdg-desktop-portal-gnome. Trying `sudo rpm-ostree override remove xdg-desktop-portal-gnome` to see if it makes a difference. I think that might have done it, also possibly got rid of the annoying gnome keyring unlock prompt... just kidding firefox crashed again. Going to also remove gnome-keyring, tbh it's a bit disappointing that it's baked in so thoroughly to begin with on the kinoite image. Also disabling screen reader since it seems related to `/usr/libexec/at-spi2-registryd` which is currently running with `--use-gnome-session`. `at-spi2-registryd` still started with firefox, and firefox crashed shortly after launching vs code.

```
Oct 22 21:39:27 molybdenum systemd[2938]: Started Firefox(flatpak) - Web Browser.
Oct 22 21:39:27 molybdenum systemd[2938]: Started app-flatpak-org.mozilla.firefox-7591.scope.
Oct 22 21:39:27 molybdenum systemd[2938]: Started dbus-:1.2-org.freedesktop.portal.IBus@7.service.
Oct 22 21:39:27 molybdenum ibus-portal[7666]: Not connected to the ibus bus
Oct 22 21:39:27 molybdenum systemd[2938]: dbus-:1.2-org.freedesktop.portal.IBus@7.service: Main process exited, code=exited, status=1/FAILURE
Oct 22 21:39:27 molybdenum systemd[2938]: dbus-:1.2-org.freedesktop.portal.IBus@7.service: Failed with result 'exit-code'.
Oct 22 21:39:58 molybdenum systemd[1]: Starting Cleanup of Temporary Directories...
Oct 22 21:39:58 molybdenum systemd-tmpfiles[7896]: /usr/lib/tmpfiles.d/rpm-ostree-1-autovar.conf:32: Duplicate line for path "/var/lib/dbus", ignoring.
Oct 22 21:39:58 molybdenum systemd-tmpfiles[7896]: /usr/lib/tmpfiles.d/rpm-ostree-1-autovar.conf:112: Duplicate line for path "/var/cache/man", ignoring.
Oct 22 21:39:58 molybdenum systemd-tmpfiles[7896]: /usr/lib/tmpfiles.d/rpm-ostree-1-autovar.conf:131: Duplicate line for path "/var/spool/cups/tmp", ignoring.
Oct 22 21:39:58 molybdenum systemd-tmpfiles[7896]: /usr/lib/tmpfiles.d/tmp.conf:12: Duplicate line for path "/var/tmp", ignoring.
Oct 22 21:39:58 molybdenum systemd-tmpfiles[7896]: /usr/lib/tmpfiles.d/var.conf:14: Duplicate line for path "/var/log", ignoring.
Oct 22 21:39:58 molybdenum systemd-tmpfiles[7896]: /usr/lib/tmpfiles.d/var.conf:19: Duplicate line for path "/var/cache", ignoring.
Oct 22 21:39:58 molybdenum systemd-tmpfiles[7896]: /usr/lib/tmpfiles.d/var.conf:21: Duplicate line for path "/var/lib", ignoring.
Oct 22 21:39:58 molybdenum systemd-tmpfiles[7896]: /usr/lib/tmpfiles.d/var.conf:23: Duplicate line for path "/var/spool", ignoring.
Oct 22 21:39:58 molybdenum systemd[1]: systemd-tmpfiles-clean.service: Deactivated successfully.
Oct 22 21:39:58 molybdenum systemd[1]: Finished Cleanup of Temporary Directories.
Oct 22 21:39:58 molybdenum audit[1]: SERVICE_START pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=systemd-tmpfiles-clean comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
Oct 22 21:39:58 molybdenum audit[1]: SERVICE_STOP pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=systemd-tmpfiles-clean comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
Oct 22 21:40:05 molybdenum systemd[2938]: app-flatpak-org.mozilla.firefox-7591.scope: Consumed 4.605s CPU time.
```

firefox on its own runs fine, but crashes roughly 1 minute after launching vs code

non-flatpak firefox does not seem to crash after vs code is launched, also it no longer throws the openh264 plugin crashing error on reddit. So maybe we go back to just using normal system firefox? vs code and element are also flatpaks, is there a conflict between the flatpaks? I thought each flatpak would be sandboxed on its own but maybe not. Going to remove firefox flatpak and delete all firefox data before fully configuring system firefox.

## 21 ##

* rebooted cluster again and ingresses broke in similar way as before...
* building new dev cluster, re-worked createVM playbook to build multiple VMs at once, but might need to run it serially to avoid overloading computer. Also seems like the edgerouter didn't properly track the dhcp leases it handed out so I can't ssh by hostname to 3 of the 4 new VMs.
    * need to add whoami service to test ingress after rebooting entire cluster... whoami ingresss working... rebooting cluster... and the ingress still works yay. So I think that means I mess something up when I do the daemonset, but I'm not quite sure what that is yet.

## 19 ##

* powered on test k8s cluster...
    * nginx ingresses all giving 404 errors...not sure why but leader election was failing and the fix here worked(basically just give it access to update the configmap) https://github.com/kubernetes/ingress-nginx/issues/6658
    * new error `E1020 01:27:34.764620       7 server.go:82] "failed to process webhook request" err="rejecting admission review because the request does not contain an Ingress resource but networking.k8s.io/v1beta1, Kind=Ingress with name rancher in namespace cattle-system"`... looks like this might be related to me mucking with nginx versions? https://github.com/kubernetes/minikube/issues/11121. Tried `k delete validatingwebhookconfigurations.admissionregistration.k8s.io rke2-ingress-nginx-admission` but did not seem to make a difference. I need to figure out the correct nginx version or maybe just unpin it and rebuild the cluster. If/when I rebuild I'll need to make a playbook to handle building all the VMs and installing rke2 with one command. Manually added an ingressclass for fun, but no difference. Upgraded nginx image from 0.41.2 to 0.49.3 and things are working now. Not sure if the upgrade fixed it or maybe I just needed to restart the nginx pods.

## 16 ##

* powered down test k8s cluster before going camping

## 15 ##

* disabled kde wallet on molybdenum (kept getting prompted to open it but nothing was getting stored it in anyway)
* did another rpm-ostree update but firefox is still crashy
* deployed kubegres to testranch cluster
  * will not dynamically generate passwords...
  * need to override the init script to do stuff like create a db, would be nice if this could be done with env vars or annotations or something

## 13 ##

* forgot I need to make disks bigger on k8s nodes
* running into another app that doesn't like the nfs provisioner, the awx postgres also wants to chown its stuff `chown: changing ownership of '/var/lib/postgresql/data/pgdata': Operation not permitted`, so I guess I need to find a different storage provisioner (or disable root squash, but that doesn't sound like a good idea)

## 12 ##

* tore down old crappy garage shelves and put up first set of new shelves.

## 11 ##

* well that was weird... testcontrol1 was running out of memory so I added more and rebooted it, but when it came backup rke2 was completely gone, no systemd unit, no /var/lib/rancher. For fun I'm going to try just re-running the ansible but pretty sure that will break the whole cluster
* rancher helm install is failing due to nginx ingress error.. `Error: Internal error occurred: failed calling webhook "validate.nginx.ingress.kubernetes.io": an error on the server ("") has prevented the request from succeeding`. Rolling back nginx version to see if it makes a difference, helm is no longer throwing an error at least and rancher webui is working!
* installed rancher monitoring (grafana persistence still not working). Looks like I will need to make some adjustments to deal with the taints
* started building nebula container image, was curious if I could do it from scratch but looks like it does have some dynamically linked libraries, so I then tried alpine, but that was missing something and throwing a not useful error, so jumped to ubuntu 21.10 and looks like that worked. Now I just need to get it running on kubernetes. 

## 10 ##

* rke2 cluster running again, keepalived working but have not tested failover yet. Going to add a worker, then rebuild the whole thing with taints in place again, ended up just uninstalling rke2 on the nodes, and re-running the bootstrap. I also need to move rancher to its own role... or maybe deploy with flux
* made a few more tweaks ot the workstation ansible for the laptop
* DONE: buy the freaking garage shelves, I'm clearly not going to redo the existing shelves so I need to just buy some that I can put together without worrying about it not being perfect. (also adjustable will be nice when I inevitably get something that doesn't fit nicely)

## 8 ##

* building a new testranch cluster, manually assigned VIP/dns in edgerouter because netbox isn't currently running. I forgot the helm controller has issues running with the criticaladdons taint, so kube-karp doesn't want to deploy, should probably move to kube-vip with a normal manifest if possible. At least one node is working though yay.

## 7 ##

* gave up on trying to get firefox to work on fedora, will try again in a month or 2 I guess

## 5 ##

* framework laptop it's 99% configured, still need to remove default firefox and get wireguard configured.
* wrote a blog post about laptop

## 1 ##

* firefox flatpak has much lower cpu usage than default silverblue version, not sure why they even package their own still
