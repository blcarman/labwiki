## 30 ##

* framework laptop arrived. I got it put together and installed the Fedora Kinoite beta. So far I'm loving it (well both the laptop and kinoite really). I liked my Dell XPS 13 when I originally got it, but it really didn't age well, and it also helped that I was coming from a cheap chromebook. This framework laptop is in a league of its own, and I wouldn't be suprised if we start seeing others try to copy them now. Everything about the laptop is well thought out and the quality is top notch. I've always had trouble finding laptops that fit my usecase, but this one matches it exactly. I wasn't sure how I would feel about the screen ratio but it actually works way better than the normal widescreen that was always too wide for 1 window but not wide neough for 2.  So now this also has me rethinking the whole docking station thing and whether I should use this for my main system or keep my desktop. 

## 28 ##

* I know I said I was abandoning the prodrke cluster, but I did find this article which showed how to get rid of the stuck namespace, https://craignewtondev.medium.com/how-to-fix-kubernetes-namespace-deleting-stuck-in-terminating-state-5ed75792647e. That cleaned up a couple errors but not all of them unfortunately, I think I'm at the point where deleting things is not really helping. I suspect my calico config got messed up when I reset the cluster but I'm not 100% sure. This is the error I'm still seeing on a number of containers `Failed to create pod sandbox: rpc error: code = Unknown desc = failed to setup network for sandbox "4b38d7c2ea3ba34cfa9020b26c067c1007e572f5af9d023444174ac60db5122a": error getting ClusterInformation: connection is unauthorized: Unauthorized`. Trying to change back to canal didn't just work so not going to dig into that further. Time to start planning the next iteration of the cluster.
* started playing with kubeadm
* TODO: remove ufw from ubuntu template (but maybe it doesn't really matter)

## 27 ##

* running through the broken prodrke cluster one more time before I abandon completely
helm-install-kube-karp-b84l2 error
```
Warning  FailedCreatePodSandBox  6s                     kubelet  Failed to create pod sandbox: rpc error: code = Unknown desc = failed to setup network for sandbox "459f7c0791c12d3a2ca807d72784de68bf24383265d6723b48b1c6980dbc4f7a": error getting ClusterInformation: connection is unauthorized: Unauthorized
```
calico-crd helm install pod logs
```
+ [[ true != \t\r\u\e ]]
+ [[ '' == \1 ]]
+ [[ '' == \v\2 ]]
+ [[ -n '' ]]
+ shopt -s nullglob
+ helm_content_decode
+ set -e
+ ENC_CHART_PATH=/chart/rke2-calico-crd.tgz.base64
+ CHART_PATH=/tmp/rke2-calico-crd.tgz
+ [[ ! -f /chart/rke2-calico-crd.tgz.base64 ]]
+ base64 -d /chart/rke2-calico-crd.tgz.base64
+ CHART=/tmp/rke2-calico-crd.tgz
+ set +e
+ [[ install != \d\e\l\e\t\e ]]
+ helm_repo_init
+ grep -q -e 'https\?://'
+ [[ helm_v3 == \h\e\l\m\_\v\3 ]]
+ [[ /tmp/rke2-calico-crd.tgz == stable/* ]]
+ [[ -n '' ]]
+ helm_update install --set-string global.clusterCIDR=10.42.0.0/16 --set-string global.clusterDNS=10.43.0.10 --set-string global.clusterDomain=cluster.local --set-string global.rke2DataDir=/var/lib/rancher/rke2 --set-string global.serviceCIDR=10.43.0.0/16 --set-string global.systemDefaultRegistry=
+ [[ helm_v3 == \h\e\l\m\_\v\3 ]]
++ helm_v3 ls --all -f '^rke2-calico-crd$' --namespace kube-system --output json
++ tr '[:upper:]' '[:lower:]'
++ jq -r '"\(.[0].app_version),\(.[0].status)"'
+ LINE=,deployed
+ IFS=,
+ read -r INSTALLED_VERSION STATUS _
+ VALUES=
+ [[ install = \d\e\l\e\t\e ]]
+ [[ '' =~ ^(|null)$ ]]
+ [[ deployed =~ ^(|null)$ ]]
+ [[ deployed =~ ^(pending-install|pending-upgrade|pending-rollback)$ ]]
+ [[ deployed == \d\e\p\l\o\y\e\d ]]
+ echo Already installed rke2-calico-crd, upgrading
Already installed rke2-calico-crd, upgrading
+ shift 1
+ helm_v3 upgrade --set-string global.clusterCIDR=10.42.0.0/16 --set-string global.clusterDNS=10.43.0.10 --set-string global.clusterDomain=cluster.local --set-string global.rke2DataDir=/var/lib/rancher/rke2 --set-string global.serviceCIDR=10.43.0.0/16 --set-string global.systemDefaultRegistry= rke2-calico-crd /tmp/rke2-calico-crd.tgz
Error: UPGRADE FAILED: create: failed to create: Internal error occurred: failed calling webhook "rancher.cattle.io": Post "https://rancher-webhook.cattle-system.svc:443/v1/webhook/mutation?timeout=10s": service "rancher-webhook" not found
+ exit
```
Finally found the bastard webhook, I guess by now I should realize I need to look at custom resources. https://www.gitmemory.com/issue/rancher/rancher/33988/904233191 is not the same problem but it did lead me to the mutating webhook. I went ahead and deleted it and now I'm getting a new and exciting error for calico
```
Error: failed to create resource: serviceaccounts "tigera-operator" is forbidden: unable to create new content in namespace tigera-operator because it is being terminated
```
So I think it's safe to say my cluster is still FUBAR but I think I've learned enough from it to safely move on with deleting it and starting fresh. 

## 24 ##

* still getting tls errors with the r610, looks like might be a cert-manager issue, basically cluster doesn't seem to want to trust the client cert from the r610. Thought maybe something wasn't running right because of the taints, but even after removing them getting unauthorized errors trying to run some containers on the controlplane. So thinking I might need to change my IP management to using dhcp reservations so I don't need to rely on netbox. Also don't think I'll need netbox if I get netmaker working. Digging further looks like some containers aren't starting because the calico container isn't running because the job is failing... seems to be failing because it can't send a webhook to rancher `Error: UPGRADE FAILED: create: failed to create: Internal error occurred: failed calling webhook "rancher.cattle.io": Post "https://rancher-webhook.cattle-system.svc:443/v1/webhook/mutation?timeout=10s": dial tcp 10.43.205.30:443: connect: connection refused`. Deleting cattle-system namespaces since everything is already broken, maybe I should have looked around for the webhook service first
* ripped out netbox from vm provisioning to rely on dhcp

## 23 ##

* ran cluster reset on prodrke1, I think the missing r610 etcd was causing issues with quorum but not sure why. I think we're back in business, kube-karp vip showed it was on reset node, but was giving connection refused until all 3 nodes were running again. prodrke3 is still not doing well, maybe it lost its taint and was trying to run EVERYTHING...taint seemed to help. Now, getting stuck trying to get the r610 agent to join the cluster, keeps complaining about it already existing and suggests using `--with-node-id` flag but that flag seems to have gotten removed at some point or something. Changing the hostname and trying again and still did not work, powering it off for the night, at least controlplane seems stable now.

## 21 ##

* got the prodrke cluster somewhat fixed up but rancher itself is still down, not sure I should keep messing with it vs setting up a new cluster. Forgot I need netbox to provision IPs so I guess I'll be fixing it. I noticed the r610 was running etcd and other things that shouldn't be on the agents, didn't seem to stop running them when I changed the labels so I deleted r610 from cluster and uninstalled rke2. It would be nice if the cluster didn't keep losing its shit every couple minutes, maybe I do have network issues somewhere. I guess I'll leave it overnight again, and see what happens.

## 20 ##

* got taints working on prodranch cluster, had to add some tolerations to the nginx daemonset that I had missed last time. Currently set "CriticalAddonsOnly=true:NoExecute" on prodrke1-3, but may change it to "PreferNoSchedule" so I don't have to worry about the r610 as much.
* making a little progress on the prometheus etcd monitoring, apparently rke2 has a flag for `etcd-expose-metrics` but still getting(or this might be a new one actually, but looks like it's happening for multiple nodes, where I've only exposed metrics on prodrke3)  `err="Post \"http://pushprox-kube-etcd-proxy.cattle-monitoring-system.svc:8080/poll\": dial tcp: i/o timeout"`, maybe I need to not use calico? also prodrke3 seems to drop to notready now... Found the bug, looks like an upgrade is in order https://github.com/rancher/rancher/issues/33052. Upgraded the cluster by replacing binary because install script was failing, accidentally restarted r610 as server instead of agent, going to let it think about what it's done and check on it tomorrow. Will definitely want to add a thing to disable the server unit on all agent nodes.

## 19 ##

* re-deployed monitoring to prodranch, still not sure what's going on with etcd, but seems like maybe I'm using etcdctl wrong and that the cluster is actually mostly ok. Logs do still show some errors about it being too slow, but seems to be less of a problem after rebooting everything, kubectl is also super fast again. Enabling persistence with grafana was failing, looks like it's trying to do a chown which probably doesn't play well with the way nfs is set up. Hmmm, prometheus is having a lot of trouble reaching various targets, will need to dig into that more. Thinking about standing up a single node cluster to see if it makes a difference.
* built testrke10 vm, need to tweak the rke2 ansible a tiny bit more to not overlap with the existing testranch cluster

## 18 ##

* prodranch performance issues seem to be just happening when connecting from outside the cluster, at least kubectl on prodrke1 seems to not have the roundtrip errors. actually execed into etcd pod from prodrke1 and etcd cluster is all sorts of not happy

## 14 ##

* titanium kde desktop kept having windows get stuck in transparent mode, and/or staying as a transparent shadow after being closed. Changed compositor to opengl 3.1 (from 2.0) to see if it makes a difference

## 13 ##

* got grpc tls working with traefik on kube3 (had to re-enable GRPC_SSL in netmaker). Now the question is do I leave the grpc port on 80? I feel like it being on port 443 is what led to traefik having issues with it originally but not sure I want to go through another round of testing just to confirm.
* prodranch cluster is moving excruciatingly slow, not sure why. Powered on the r610 but not much of a difference, rebooting prodrke1 to see if things will move around, seems to be doing a bit better now


## 12 ##

* kube3 netmaker: created test network but getting an error from netclient, thinking i might need something to automatically force https
```
2021/09/12 14:42:29 Error installing:  rpc error: code = Unavailable desc = Bad Gateway: HTTP status code 502; transport: received the unexpected content-type "text/plain; charset=utf-8"
2021/09/12 14:42:29 Failed to authenticate: rpc error: code = Unauthenticated desc = Something went wrong with Auto Login: rpc error: code = Unavailable desc = Bad Gateway: HTTP status code 502; transport: received the unexpected content-type "text/plain; charset=utf-8"
```
* set SQL_CONN on netmaker because it seems to only discover rqlite on the first run...thought it made a difference, but after deleting namespace and having flux redeploy I'm back to the no host error... deleting the pod a couple times seems to sort it out, but seems like maybe it just gets lucky that rqlite starts first sometimes
* changing grpc to non ssl didn't seem to work, might only be set on initial run?
* boom finally got it with non-ssl, annotation for h2c does need to be on the service, still not sure why that didn't work for tls though. I wonder if I can have traefik do tls and do the normal no ssl to the backend... does not look like that will work, it gives the same error as above... helps to re-enable hte GRPC_SSL option

## 11 ##

* changed kube3 over to prod let's encrypt
* deployed netmaker to kube3, had an issue with one of the pvc's being readwritemany which isn't supported by the k3s local provisioner, but switching to readwriteonce appears to have worked, will try to deploy a network/client tomorrow

## 9 ##

* confirmed kube3 traefik working with a whoami deployment, did seem to take a few minutes to figure itself out, using staging let's encrypt so still need to flip over to prod one

## 8 ##

* kube3 traefik configured for let's encrypt staging... still need to test, also configured manually due to autodeploying manifest provided with k3s

## 6 ##

* updating the plan, going to build a new k3s droplet (new version does traefik v2 by default). Flux requests 350m cpu by default, which might not seem like much but on a 1 core system is quite a lot. I'll have to see what controllers I end up using to see if I can cut that down. With just k3s and flux I'm at 868m cpu
* traefik documentation is lacking, how to get let's encrypt wired up with standard k8s ingress? The internet has mostly decided on cert-manager but I was trying to keep things lightweight

## 5 ##

* kube2 resized to 2cpu/2gb ram and flux bootstrap completed, flux deployed cert-manager, but noticed rke2-metrics pod is not running due to resource constraints, seems like I might have to go back to k3s/traefik if I want to keep things cheap, might have to give up flux as well...

## 4 ##

* upgraded rancher to v2.6.0 from v2.5.9 (really was as easy as specifying the new image tag version in the helm values)
* changed oil in the truck
* built kube2 droplet, installed rke2, added firewall rule for home IP for all tcp ports, attempted flux bootstrap...1 cpu is not enough

## 3 ##

The goals:
  * replace nebula with netmaker
  * migrate chat server to k8s
  * replace k3s droplet with rke2

The plan:
  * upgrade home rancher and/or rebuild (should it be single node?), make r610 its own cluster as well?
  * build new rke2 droplet
  * add rke2 droplet to flux
  * create cert-manager namespace and secret for digitalocean dns validation
  * deploy cert-manager to rke2 droplet
  * deploy netmaker server to rke2 droplet
  * add new rke2 droplet to rancher
  * migrate blog and wiki to rke2 droplet
  * decommmission k3s droplet
  * migrate chat server to rke2 droplet
  * ship important logs over to loki
  * possibly set up prometheus monitoring
  * write a blog post
