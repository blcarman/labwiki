## 31 ##

* nebula ca cert expired, generated one that's good for 10 years instead of 1. I can't believe I've been using nebula for 2 years.
* nebula working on rke2 and verified that it works with ingresses as hoped
* added `setopt HIST_IGNORE_SPACE` to my .zshrc, neat thing is it will keep it when it's the last command but after that it'll disappear forever
* cert-manager working

## 30 ##

* jellyfin working, had to install a forked/patched version of the synology csi 

## 29 ##

* jellyfin almost working

## 28 ##

* have the synology mostly configured. Also got the k8s csi driver working. Started backups to backblaze but those will take a chunk of time to complete. Only thing left is cleaning up some permissions and making sure its ready to hand over access to the wife.

## 11 ##

* warehouse at leasts gets passed BIOS now, so it seems like before there indeed was a standoff in a bad location. However, now opensuse will only boot 1 of its old kernels in recovery mode, not sure what's up since I verified things were working after the last update. Running a new update didn't make a difference either. So it might be time to replace opensuse.

## 10 ##

* found more info on toolbox containers, so forked the project and going to see if I can make some custom ones

## 9 ##

* shutdown the warehouse with plans to move it back into the supermicro chassis but I lost the screws for the hard drive trays... and found them
