## 27 ##

* ansible VM provisioning basically done, able to provision IPs and DNS, and create the VM
* titanium was having weird dns resolution issues, not sure if actually fixed or just took long enough that it started working, in any case resolved is about the worst experience I could imagine for dns

## 26 ##

* more libvirt network nonsense
  * changing nic pci bus/slot to match packer template doesn't appear to be a good idea
  * finally just did some virt-customize stuff in ansible playbook, could be better but at least it works
* git tip of the day: `git fetch --prune` to clean up old branches that have been deleted/merged, also consider settings `git config --global fetch.prune true`
* cleaned up ansible branches, will potentially delete old unused roles/code

## 25 ##

* vim thing of the day: to delete all characters up to and including the next x character `dfx` to delete up to but not including x use `dtx`
* this might be the easiest way to extend my terrible deploy_vm.sh script instead of having to use some random terraform provider, https://sumit-ghosh.com/articles/create-vm-using-libvirt-cloud-images-cloud-init/

## 24 ##

* finally fixed the stupid konsole terminal profile weirdness by deleting ~/.local/share/konsole and customizing the default profile instead
* set up ssh on win10 for proxyjump, it's awesome
* add rke2 to the todo list, might be a better option than k3s for normal deployments
  * ansible basics done (just need to make it wait for init)
  * we don't need no stinking haproxy https://github.com/immanuelfodor/kube-karp
* why does my packer image still have a machine-id? re-running packer just in case I didn't build after adding the removal
  * useful for mounting and investigating qcow2 images: https://www.jamescoyle.net/how-to/1818-access-a-qcow2-virtual-disk-image-from-the-host
  * it's totally not getting cleared...wth
  * tried cat'ing the file in the provisioner but the output doesn't make it to packer level, guess I could do it in ansible

## 23 ##

* new cpu cooler for titanium arrived
  before temp 42-50
  ```
  k10temp-pci-00c3
  Adapter: PCI adapter
  Tctl:         +42.8°C  
  Tdie:         +42.8°C  
  Tccd1:        +42.2°C
  ```
  stress (did not run long enough to find steady state but its worrisome how quickly it jumped)
  ```
  k10temp-pci-00c3
  Adapter: PCI adapter
  Tctl:         +74.5°C  
  Tdie:         +74.5°C  
  Tccd1:        +73.5°C
  ```
  after temps look better, still around 66 when running stress but drop down to 30ish under normal use, also looked like the temps might have been high with stock cooler due to not putting enough thermal paste on. In any case I'm sure beefy cooler will be much quieter as well. Desktop is also now silent under normal operation
* still fighting with dns resolution on warehouse
* probably should not have doubled up changes but also ran updates on titanium

## 22 ##

* finally got the network on the warehouse figured out, got rid of wicked and using systemd-networkd instead. Stole most of the config from my old workstation config, nice to know some things don't change I guess. resolved dns is acting weird though, status shows dns server configured but not its not resolving.

## 21 ##

* fixed warehouse, turned out removing packages was not the issue, for some reason the btrfs mount was failing due to the 3 TB drives being attached to the hba which only wanted to support 2 TB drives... not sure how it worked previously.
* and I broke the warehouse again... tried adding an openvswitch bridge attached to vlan30 and lost access. Originally used opensuse's wicked documentation but from the OVS docs: ```When you add eth0 to the OVS bridge, any IP addresses that might have been assigned to eth0 stop working. IP address assigned to eth0 should be migrated to a different interface before adding eth0 to the OVS bridge. This is the reason for the separate management connection via eth1.``` So another strike against opensuse. Reboot restored connectivity, guess I'll stick to native bridges for now.

## 18 ##

* stubbornly going through trial and error to get the Moguri Mod installed for FFIX on steam linux, a few random snippets on the internet with partial instructions, maybe its just not meant to be

## 15 ##

* grafana loki installed on testk3s, will likely end up replacing graylog fully

## 13 ##

* rebooted warehouse after the "dist-upgrade" and it didn't come back... maybe I shouldn't have messed with packages

## 12 ##

* removed nscd on warehouse due to nsswitch.conf not found errors and doesn't seem like its necessary for anything
* removed and locked packagekit and gnome-online-accounts and nscd on warehouse

## 11 ##

* opensuse fluentbit package doesn't include systemd unit, puts things in /usr/etc/ instead of just /etc