## 31 ##

* sorted out configuring multiple k8s clusters in .kube/config
* was going to run keycloak instead of okta, but its installation instructions are surprisingly archaic. I was really expecting a helm chart.
* rancher okta saml config working, had the usual saml weirdness and some strange 404s from rancher until I refreshed a couple times, but overall not too bad of a process, still need to work out the authorization part though
* fixed flatpak app's not showing up in kde on ubuntu, appears to be due to zsh, fedora seems to have some extra zsh profile scripts to do the flatpak stuff
* finally finished the bookstack migration, well at least the lab part of it, need to figure out what to do with the other random stuff in there.

## 30 ##

* k8s smb driver working
* moved warehouse to the crawlspace

## 29 ##

* pipewire is kind of a disappointment so far. I thought maybe I could finally get my bluetooth headset to always use the headset profile, but no matter what I tried it wouldn't work. Putting a bluez-monitor.conf in either my home or /etc/pipewire had no effect, trying old pulseaudio configs didn't work either. The documentation is still lacking severely. I wanted to be excited about this considering the hype but I guess I just missed the target audience. I'm sure its a great project for developers and that there are benefits I haven't noticed. It also seems if Fedora was going to set it up in a way that deviated from the project's sparse documentation they should have provided their own documentation on how to configure it. In any case I'm admitting defeat and buying a USB headset.
* had to replace my mouse yet again due to it constantly treating single clicks as doubles, but at least this time the CoolerMaster MM720 was available, supposedly it has optical switches to prevent the double click issue from happening, but we'll see how it holds up.

## 15 ##

* replaced laptop screen on the xps 13 (scandium), was fairly easy, but did lose a connector for one of the wifi antenna, signal still seems ok, but getting complaints in dmesg and battery charging light keeps flashing like its upset about something. Also accidentally broke a corner off but it seems to be ok without it. I had broke a corner on the original screen, so I went ahead and also upgraded to the 4k touchscreen. It's a nice upgrade but I don't think it's a must have.

## 13 ##

* convert flask server request thing to ansible module? 

## 10 ##

* titanium: configured nfs to connect over nebula, so I no longer have to worry about nextcloud/seafile. Would still like to get one of them set up at some point

## 9 ##

* upgraded titanium to fedora 34, monitors no longer come back on if session idles...https://bbs.archlinux.org/viewtopic.php?id=263124 looks like discord (and possibly spotify) don't like wayland and/or there's a kernel bug, x11 working good