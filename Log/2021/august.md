## 28 ##

* moved cpu/ram from hp back into dell r610 and installed ubuntu 20.10, will use it to provide some extra lab power as needed. Deciding to embrace the future and put rke2 on the bare metal and use kubevirt for VMs later if needed.
* gave prometheus more memory due to some oomkiller errors, also tried "fixing" the prometheus targets that show as down, but think I just made it worse, also maybe they're expected to show as down? though that seems odd
* still not having much luck getting netmaker to work, might end up abandoning it. The join always seems to just hang, though at least now it's getting to the point where it knows the network range. 

## 24 ##

* had to shut down the lab and network to move it all out of the crawlspace for insulaton installation. Now, thinking about how I want to put it all back in.

## 21 ##

* tried openhab, it was pingable for a bit, but then nothing and reboot didn't make a difference either so going to retry home assistant.
* homeassistant re-installed, z-wave add-on now working and light switch added

## 20 ##

* exploring netmaker as an alternative to nebula for my mesh vpn. It uses wireguard and looks well documented.

## 19 ##

* confirmed edgerouter syslogs are flowing into loki!
* powered on the home assistant pi and upgraded the os and core, but now my z-wave integration seems stuck on an old version. I guess I'll let it think about stuff overnight and try tomorrow, but might be looking at openhab instead.

## 18 ##

* confirmed via fluentbit logs that syslog is able to get through the lb to a fluentbit pod. However, does not seem to make it to loki, oops had a typo in the output...

## 17 ##

* turned on kubernetes labels in fluentbit and thought it fixed my out of order errors but maybe not. Like it seems to be ok for a short periord of time and then only 1 pod is good. I also found the loki fluentbit chart had been including a label map, not seeing a good one to do the same with the fluent chart, but will figure something out. Like most community provided things, its become clear that helm charts are nice until they aren't at which point you spend more time trying to understand someone else's use case instead of just making yours work. Manually setting labels to be similar to the original grafana fluentbit chart might have fixed things, or at least appears to have for the 2 nodes still running on warehouse. The one moved to titanium is stuck in a crashloopbackoff, not sure why. Updating and rebooting prodrke3(titanium) to see if it resolves it. Looks like that might have fixed it... well everything is "running" now, but loki logs still showing out of order stuff occassionally And now I'm getting a ton of these from fluentbit `[2021/08/17 23:52:11] [ warn] [output:loki:loki.0] could not translate record accessor ` I fixed that by removing the app/release labels that don't exist on everything. I'm still seeing some out of order errors but they don't really make sense to me. They seem to be from like 30 minutes ago, like fluentbit got confused about its position in the file and doesn't know how to get past it.
* made some syslog changes, but don't think edge is sending enough logs to tell if its really working
* finally installed z-wave light switch I got last year. Putting it in the office because the light in there was super bright otherwise. Also using some flood bulbs that were too small for the recessed lighting we have downstairs and not sure how much I really like them, seems they go either too dark or too bright, but will give it a couple of days.

## 12 ##

* figured out flux namespacing issue: kustomization files do not need to be namespaced but when they are that will override other namespaces set down the stack. 
* moved things to a logging namespace, and changed to fluent chart repo, now logs are sort of working but getting a bunch of out of order errors and seem to have lost the labels. also fluentbit containers never seem to be reaching healthy state.

## 11 ##

* started work on fluentbit syslog listener, think I should just add it to the existing daemonset and slap a metallb lb on it, might want to flip over to the official helm chart when I do that though. Think I have a good config but might need to rip out the existing resources before I commit it.

## 10 ##

* finally got my loki query working `{namespace="flux-system"} | regexp "([^{]+)(?P<log>.*)" | line_format "{{.log}}" | json | __error__ != "JSONParserErr"`, I need to clean up the flux namespace so it's not picking up non-flux logs, then I wouldn't have to exclude json parsing errors, will dealing with counting tomorrow

## 9 ##

* fixed metallb flux thing, apparently do not need to explicitly create the namespace when using kustomize
* loki query still not parsing json `{namespace="flux-system"} | regexp "(?P<log>{.*)" | json`

## 5 ##

* added metallb to flux, but getting weird namespace error
```
2021-08-06T06:57:42.756Z error Kustomization/infrastructure.flux-system - Reconciliation failed after 6.876922885s, next try in 10m0s kustomize build failed: namespace transformation produces ID conflict: [{&#34;apiVersion&#34;:&#34;v1&#34;,&#34;kind&#34;:&#34;Namespace&#34;,&#34;metadata&#34;:{&#34;annotations&#34;:{&#34;config.kubernetes.io/previousNames&#34;:&#34;loki,loki&#34;,&#34;config.kubernetes.io/previousNamespaces&#34;:&#34;_non_namespaceable_,_non_namespaceable_&#34;},&#34;name&#34;:&#34;loki&#34;}}{nsfx:false,beh:unspecified} {&#34;apiVersion&#34;:&#34;v1&#34;,&#34;kind&#34;:&#34;Namespace&#34;,&#34;metadata&#34;:{&#34;annotations&#34;:{&#34;config.kubernetes.io/previousNames&#34;:&#34;metallb-system,default&#34;,&#34;config.kubernetes.io/previousNamespaces&#34;:&#34;_non_namespaceable_,_non_namespaceable_&#34;},&#34;labels&#34;:{&#34;app&#34;:&#34;metallb&#34;},&#34;name&#34;:&#34;loki&#34;}}{nsfx:false,beh:unspecified}]
```

## 4 ##

* cert-manager namespace thing sorted, had to move to a ClusterIssuer and move the digital ocean secret to the cert-manager namespace
* TODO: add metal lb so i can set up fluentbit syslog receiver

## 3 ##

* loki tls ingress failing to get cert, I think because ingress is in loki namespace and cert-issuer is in default

## 2 ##

* configured flux prometheus monitoring and set up flux dashboards (need to make persistent)
