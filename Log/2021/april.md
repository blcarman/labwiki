## 27 ##

* updated laptop to ubuntu 21.04 (removed most of the xfce stuff, should probably reinstall pure kubuntu at some point)

## 25 ##

* power outage, so ran OS updates on warehouse and titanium after it came back
* ansible k8s is failing in a bunch of annoying ways
  * can't specify kubeconfig without getting an invalid error despite kubectl working
  * setting env var for kubeconfig at the task got further but now getting a 404 default backend error, despite kubectl working on the host, wtf

## 18 ##

* vs code settings appear to have drifted from what's in ansible, also need to add kitty.conf to ansible

## 17 ##

* started working on server provisioner flask app

## 15 ##

* tried out Ubuntu MaaS, not terrible but not exciting either.
I really want to find a lightweight way to provision VMs and bare metal servers in a standard way. I figured there had to be some project out there that could pxeboot a machine and provide it a custom cloud-init config and then maybe run some ansible to finish off the OS config. But so far I've come up empty. I really don't want to get dragged back into old school infra management with things like spacewalk or foreman, so perhaps it's time to face the fact that there just isn't a good way to accomplish what I want. And really the only challenging part is IP allocation, maybe I could use a generic config if there was a way to have it pick up a pre-allocated static IP. Could I write a small flask/webapp that will provide a different cloud-config based on the source IP? That is machine pxeboots with a cloud-config of http://mycloudinit/config.yml and the webapp takes that client IP, determines its mac, allocates a static IP to it and sticks that in the config.yml that it responds with? That might actually be fun to try. 


## 12 ##

* deployed prodrke VMs, ran apt upgrades on testrke nodes, vip did not come back up, looks like not enough cpu available to spin up 3rd instance of kube-karp but should be able to do VIP with just 2 out of 3 otherwise what's the point? maybe I restarted the nodes too closely together after os updates, 3 kube-karps running again fixed the vip, will need to test it out more thoroughly
* even with nocow the btrfs spinning disks is excruciatingly slow for VM storage, need to move to SSD

## 10 ##

* started udemy course for CKA
* manual helm install kube-karp on testrke, working good, still need to fix it up for the helm crd

## 5 ##

* tmux/system clipboard works with kitty terminal, konsole doesn't support OSC 52 escape keys. This post, https://ianthehenry.com/posts/tmux-psa/, got me digging into tmux again. With a little bit of effort/config I think it'll pay off. Also note I did have to install kitty on the ssh target as well so it had the terminfo for tmux

## 3 ##

* truck: broke off wheel stud while attempting to swap out winter tire for summer ones... is it time to give up on doing any automotive stuff?