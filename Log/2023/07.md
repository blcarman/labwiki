---
title: July 2023
description: Log for July
published: true
date: 2023-07-01
tags: log
editor: markdown
---

## Plan ##

Maybe for real get mastodon and/or lemmy/kbin running.

### 13 ###

* updated firmware on framework laptop, super easy with fwupdmgr

### 7 ###

* disabled kde discover on the desktop, since I think it was causing some crashing shortly after login.

### 5 ###

* got a local install of lemmy working

### 4 ###

* joined mastodon.social

### 3 ###

* upgraded synology csi controller to try to fix security context permission issues

### 1 ###

* update synology to 7.2