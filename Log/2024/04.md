## 28 ##

* yolo upgrading ubuntu on rke1

## 27 ##

* upgraded laptop to fedora 40, too soon to tell for sure but might have fixed my docking issue. Otherwise the kde 6 upgrade was surprisingly smooth and no noticeable changes. I'm sure a ton went into it that is not visible to the user and I think that just makes the accomplishment even greater. I remember the update from 3 to 4 being a nightmare and again from 4 to 5 was a bit rough. 

## 21 ##

* started return for the mushkin ram that didn't work. Actually dropped off at UPS on the 26th.

## 20 ##

* tried going to record store day, but line was ridiculously long, and fairly certain at least a few people going for the same albums I was. Then they started smoking and that was the end of it. So now I'm back home streaming the album I wanted.

## 19 ##

* finished putting carbon fork on the cross check, or more accurately finished tweaking the brakes. I still need to take it for a test ride. Brief test ride and it seems the front wheel is a little bit loose but I don't think it's anything related to the fork.

## 18 ##

* power tuning on laptop, disabled nmi_watchdog, blacklisted amd watchdog module, set some dirty writeback setting

## 16 ##

* Day 1 of using laptop full time. Docking station is a tiny bit glitchy, had to login on laptop then it would be ok. Also had to switch back to X11 for slack screensharing. The day was a lot of call time and by the end the laptop was really cooking. That said there were no noticeable performance issues, so I suppose it could be considered a success. 

## 15 ##

* dug docking station out of basement and hooked it up. It's right on the edge of usb capacity but it looks like it's going to work just fine for what I need.
* I threw away some 2012 era windows/vmware books I had. I probably need to throw some others out, but at least I got started.
 
## 11 ##

When I started trying to do the k8s operator thing there was an example that basically just created a deployment. It was probably a decent example but it just didn't interest me. But now I might have something to work with. I could create a slimmed down postgres operator. Basically just need to manage a statefulset for the postgres. And then the upgrades. I think I have 2 options, compare the version tag to the previous revision or add a separate field to signal an upgrade. To do the upgrade then it would be scale down the stateful set, create a job to do a backup and pg_upgrade, then update the image and scale up the statefulset. 

## 10 ##

* new thinkphone arrived and set up. Tons of updates, setup was pretty smooth. It feels like a nice upgrade. wifi hotspot surprisingly worked with net10, but still planning on switching to mint.

## 9 ##

* yolo upgrade kubegres by just running the install using the latest version.
* tried to also upgrade the postgres version on the stocker-db but while it got stuck in crashloopbackoff. I thought kubegres was supposed to handle the db upgrade but maybe that's only if it is configured with multiple replicas? documentation is lacking in this regard. I tried bumping to multiple replica first but didn't make a difference. I thought maybe it would resolve itself but it seems stuck on "stepId: Attempting to fix a stuck Pod by recreating it". Maybe I should dig into the project and see if I'm missing something. Looks like they do have a feature request still open for it, https://github.com/reactive-tech/kubegres/issues/7. 

## 8 ##

* laptop upgrade complete, Crucial RAM worked with no issues.