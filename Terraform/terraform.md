## Module Strategy ##

I was originally creating modules based on services, so like a docker swarm would get a module but I'm now thinking I need another module layer in between. So I'll have a virtual_machine module (or several) and then just call that from the swarm module. This should result in significantly less code duplication.

## Interesting Behavior ##

* changing VM template cloned from will trigger new VM
* adding chef provisioner will not trigger change