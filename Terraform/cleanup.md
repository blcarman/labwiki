# Cleaning up My Terraform #

I've been experimenting a lot with terraform in the lab and so it's in a bit of a bad state. Most of this is due to starting things and not finishing them. So it's time take another look at it and organize the workspaces better so that I can experiment more easily without breaking the good parts. 

## Workspaces ## 

* Infrastructure: vcenter, network and other stuff that is shared by the upper workspaces
* Platform Test: first run at things, should match prod as close as possible but basically a proving ground
* Platform Prod: stuff that actually matters in the lab
* Application: might not add it right away, but things like jellyfin and other apps might go in here
* Dev: anything goes, I should be able to destroy and re-create this at any point and use it for all the experimental stuff without impacting other stuff

## CI ##

I would love to have terraform run by CI, but as I've found with chef sometimes that's a burden more than a help. We'll see how things go, maybe I'll at least have CI run plans. 