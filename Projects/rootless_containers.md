It seemed like a good idea at the time:

* https://developers.redhat.com/blog/2018/11/29/managing-containerized-system-services-with-podman/
* https://www.redhat.com/en/blog/preview-running-containers-without-root-rhel-76: though apparently I missed 7.7 being released so this shouldn't realy be necessary any more.
* https://www.scrivano.org/2018/10/12/rootless-podman-from-upstream-on-centos-7/

## Emby ##

Jellyfin is a forked version of Emby since they kind of closed things down, but the ideas here are all still relevant

Emby is the major thing I would like to run in its own special container, but I feel like they fall into that group of folks that don't actually do things right. Or maybe the rootless podman isn't quite ready yet.

```
podman run --volume /home/poduser/emby_config:/config --volume /mnt/data/movies:/movies:ro --publish 8096:8096 --publish 8920:8920 emby/embyserver:latest --env UID=1000 --env GID=1000
Error: container create failed: cannot specify gid= mount options for unmapped gid in rootless containers
: internal libpod error
```

Or this might just be that I need to upgrade to 7.7. Perhaps I'll test it out in a vagrant VM. Apparently this just needed the upgrade to 7.7. 

I should probably put this in source as well but here it is, based off the one from here, https://developers.redhat.com/blog/2018/11/29/managing-containerized-system-services-with-podman/

```ini
[Unit]
Description= Emby podman service
After=network.target

[Service]
User=poduser
Group=poduser
Type=simple
TimeoutStartSec=5m
ExecStartPre=-/usr/bin/podman rm "emby-service"

ExecStart=/usr/bin/podman run --name emby-service --volume /home/poduser/emby_config:/config --volume /mnt/warehouse/movies:/movies:ro --volume /mnt/warehouse/shows:/shows:ro --publish 8096:8096 --publish 8920:8920 emby/embyserver:latest

ExecReload=-/usr/bin/podman stop "emby-service"
ExecReload=-/usr/bin/podman rm "emby-service"
ExecStop=-/usr/bin/podman stop "emby-service"
Restart=always
RestartSec=30

[Install]
WantedBy=multi-user.target
```

So now I have emby running in a podman container on warehouse which means the lab is back to being a lab. 
