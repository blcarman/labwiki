AWS

permissions
  should we let humans manage anything? if we do, audit all human actions
  should identity federation be configured in its own account? in the management account
  does gov cloud need a dedicated non-gov cloud account paired to it?

when i set up okta at home, my okta account email matches my aws root account and i think that is muddying the waters a bit. that is i have 2 different users acting as one.

## Terraform ##

Oh terraform, we have such a love/hate relationship. Will I be able to make sane use of you this time? 

AFT is a hard no. Hopefully we can just create accounts the normal way and add them to OUs managed by control tower? Actually CT is a no as well. I shouldn't have doubted my original decision, but I suppose I'm glad I gave it a more thorough look. Ultimately, it it doesn't feel complete. Account customizations still have to be defined, the controls are confusing and don't have the templates like pure config does. 

Bootstrapping an AWS Organization. I've done too many mouse clicks in my personal account, I think I need to create a new one.

Naming is hard, in the past we've called things "envs". Workspaces is probably the most correct, but does also have a meaning within terraform that we don't necessarily use. Maybe configs? 

configs/
  aws/
    organization/
    devops/
    environments/
      dev/
        account
        platform
      test/
        account
        platform
      prod/
        account
        platform
  cloudflare/
    account/
    environments/
      dev/
        platform
        mail
      test/
      prod/

configs/
  aws/
    organization/
    devops/
    accounts/
      managed/
        dev/
        test/
        prod/
      unmanaged/
        somePOC
        anotherSandbox
    environments/
      dev/
      test/
      prod/
  cloudflare/
    account/
    environments/
      dev/
        platform
        mail
      test/
      prod/
modules/
  aws/
    managed_account
    unmanaged_account
    platform
  cloudflare/
    


config/
  organizations/
    aws/
    cloudflare/
  environments/
    dev/
      aws_account
      aws_platform
      cloudflare_platform
      cloudflare_mail
    test/
    prod/

Either something we missed before or something added since then is the remote state datasource. Previously we tried to cram everything into the same state. Like you terraform a whole world, not a single thing on it. But this turned into spagghetti and was nearly impossible to manage. The reason we wanted the single state was so you could take an object you created and reference it somewhere else. With remote state datasource, we should still be able to reference other things, but can still run independently. We will still have to be careful we don't create circular dependencies. Like cloudflare should only layer on top of aws. AWS would never depend on something from the cloudflare side.

A good example of the start of some spagghetti. In theory we go org -> account -> platform -> app, but off to the side we'll also have the devops account with ecr. The platform will need access to ecr. For the purposes of bootstrapping we would need to apply the devops terraform first, but after the initial setup it should be possible to run them independently. Unless there's a way to grant all accounts in the org access to ecr?

Permission sets apply to an account but are configured in the org identity center.

AWS Okta SAML Config

1. Create Organizations in commercial(commercial might already be done) and gov cloud
2. Create AD groups
  * okta tile access groups for commercial and gov AWS
  * org admin group
  * individual account admin group(s)
  * individual account readonly group(s)
  * AD service accounts
    * nonprod/prod, per environment, or per AWS account? or re-use the same ones we use for other stuff GTSPlatformOperations-Services-PROD?

Someday dreams:
* enterprise-deployment could template out the initial terraform files when we create the new AD users/groups/emails for creating an aws account
* AzDO OIDC connector to match the way hubs does their terraform authentication. https://aws.amazon.com/blogs/devops/use-openid-connect-with-aws-toolkit-for-azure-devops-to-perform-aws-codedeploy-deployments/

Questions

* codename for govcloud?
* account naming scheme, simply environment and "-gov" for those, email adress GTS-AWSAccount-<env> GTS-AWSGovAccount-<env>? or GTS-AWSAccount-<purpose> for things like IOT/marketing. For example in AWS it would be SIT and the root email would be GTS-AWSAccount-SIT@protolabs.com.
* AD group naming, I think to start we need Admin and Readonly for each AWS account. Then at the Org level: Admin, Readonly,and billing. What's the standard naming convention?
* we will want to lock down the account root users and other admin accounts in thycotic. I'm thinking checkout policies and forced password rotation on every use. Not necessarily something we have to do right away, but I think this will be useful for a lot of other secrets once we figure it out.
