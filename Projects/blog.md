# 2020 Update #

Kind of fun to reread this now that the blog has been running for 4ish years. Of course now I'm looking at doing it on kubernetes but I've come so far since the activities listed below, now most of them are just second nature or not even a thing any more. Not sure why I didn't put anything about vhosts in here since I've had it set up with them for a long long time now. 

# Intro #

I think I'm finally going to create a blog. Domains aren't as expensive as I expected and I should be able to put it on Digital Ocean droplet for $5/month. I'm going to avoid advertising as much as possible but might try the Patreon thing. In any case this post will cover the build of the blog server and testing. This will include everything I had to do to get the blog running considering my current lab set up, so it may go beyond some needs but may also skip over some things. Later on I'll go back and fill in the details on building the lab to get it the point its in. 

### Blog Design and Goals ###

I plan on creating 2 separate blogs, though both will be closely related. One will be more professional and polished and something I could put on a resume and the other will be a bit more uncensored and include more in depth write ups including mistakes made along the way or rants regarding the technology. Eventually I hope to combine these further with a wiki or similar repository unless I can tag the blog in such a way that makes it usable as an index as well. I would also like to put realistic time estimates though that will take some effort since a lot of times I multi-task and/or procrastinate so accurate time tracking is difficult. Consider adding book reviews or currently reading snippets? and holy crap, looking at the ghost themes has made me realize everyone has a flipping blog, which is cool, but one thing I want to avoid doing with mine is pretending its unique or original. Sure the posts will be but the idea that I'm the only systems engineer that has made a blog won't be.

### DMZ Configuration ###

1  Add DMZ nic to pfsense firewall and configure basic rules to allow outbound traffic and block traffic to other internal subnets. Also created rules to allow foreman to provision hosts on the DMZ network
1  Add DMZ subnet to foreman server
1  Attempted to deploy host and realized I forgot to set up dhcp on the DMZ subnet and did not give access to the installation source. I'll be moving the installation media and/or re-deploying foreman to be a full katello install with the installation media built in.
1  I also took care of a firewall upgrade while I was messing around with it since it had been a little while since the last one
1  Created an alias for the foreman ports and the foreman host and allowed access to it and the installation media.
1  Re-enabled the cancelled build for ghost.home.lab in newforeman.home.lab
1  Apparently forgot to enable the dhcp server for the dmz interface so boot was still failing and now the host build is making progress...I spoke too soon, now seeing "transient problem" when trying to download the kickstart file.
1  Tried adjusting permissions on /var/lib/tftboot but didn't have the desired effect
1  re-read error and noticed it was trying to resolve newforeman.home.lab but I didn't have a dns server specified in the dhcp settings. So I updated the dhcp settings and firewall rules but it still isn't resolving. Packet capture shows it hitting the firewall and exiting the servers vlan as expected but no return traffic from the dns server...
1  not that I expected it to make a difference but I also tried adding a reverse dns zone for the dmz network on the dns server in the freeipa console, but no luck.
1  Another long shot, but peculiar configuration is the firewall set up as a dns resolver on the lan and server interfaces but not on the dmz, adding the dmz did not make a difference and I'm not using the firewall for dns resolution so I went ahead and disabled it all together.
1  I think I've found the problem. The dmz interface on the firewall is configured as a virtio adapter instead of e1000, I believe this can cause issues with attempted hardware offloading. Unfortunately I have to reboot the firewall to update it. Sure enough that was the problem, but apparently it is less broken than I thought, I only had to disable hardware checksum offloading and then re-configure the interface (I faked a reconfig of the interface by simply editing it and making checking and unchecking the enabled box and then clicking save).
1  Making progress now, but apparently I didn't have http allowed on the local firewall for warehouse which is being used to host the source temporarily
1  Now we're seeing some package install errors likely due to the packages not being included in the installation media by default, will continue troubleshooting tomorrow.
1  Not directly related to this project, but added the libvirt compute resource to foreman to provision a backup server on my workstation. I ended up handling the workstation libvirt as bare metal in foreman
1  Updated the install media to use the CentOS 7 everything iso and now deployments are working

### Server Configuration ###

1  Did the standard ansible config (only put in lab servers group since not using apache for the web portion), will likely break web servers into apache and nginx groups
1  Followed installation guide from ghost, just used nodejs from epel and nginx for the proxy. I did run into an issue with selinux and had to run "setsebool -P httpd_can_network_connect 1"
1  Looks like it uses markdown for the format which is fairly similar to the wiki format and should be pretty easy to use
1  Still searching for a theme, but liking this one to a degree, https://github.com/curiositry/mnml-ghost-theme, though I might change it up a bit. Besides the red title color, I'm also not a huge fan of the horizontal menu bar. https://github.com/boh717/beautiful-ghost might be a decent option

### Production Blog Build ###

I didn't really think about how I was going to run the 2 separate blogs, but I guess I figured I would set up different VMs for each. Well, it crossed my mind that with my ridiculously low number of expected readers I could probably fit them both on the same server and be able to stay in the free AWS tier for a while.

https://www.digitalocean.com/community/tutorials/how-to-serve-multiple-ghost-blogs-on-one-vps-using-nginx-server-blocks