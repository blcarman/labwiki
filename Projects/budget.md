# Fuck Budgeting #

I have yet to find a good budgeting method and/or software. Either I'm going crazy counting pennies, having to decide shit every month or its so expensive that it falls under one of those things that you cut out of your budget to save money(YNAB I'm looking at you). I'm fairly conservative to begin with so I really don't need a "strict" budget, I just want something that will help me track my spending and coordinate savings and other yearly goals. 

Firefly III comes close to being a decent tool for what I want...until it does something ass backwards like not making piggy banks capable of tracking transactions. The budgets reset every damn month and then it constantly acts like you're a terrible person because you spent the same money on stuff you did the previous 1000 months in a row. I thought maybe I could contribute to it as well, but of course it's written in PHP so that's not happening.

So it would seem I have 2 options. I go back to my lazy spreadsheet where I kept track of things on a monthly basis and was able to track a single thing to save for or I create my own webapp that does what I want it to. I've been looking to actually create something instead of simply using someone else's tool for a long time.

## Goals ##

* piggy banks or virtual accounts
* tags (which are also categories)
* reports (spending based on tag, income vs expense)

## Requirements ##

* containerized (https://hub.docker.com/_/golang/)
* golang rest api backend
* javascript frontend?


### Getting Started ###

1. go webapp running in container

That is the initial step to do before I get too far into dreaming the feature set and decide the project is too big for me.

Getting a simple web server working was no sweat, but then I got a bit sidetracked before making it run in a container. Long story short I spent way too much time trying to figure out what type to use for storing dollar amounts. The one consensus I found was to absolutely not use floats. Some suggested using ints and just doing things as cents but I felt like that would be a bit clunky. Eventually I just decided to go with shopspring's decimal package. It's probably overkill as well as undermaintained but should be good enough to get me started.

With that out of the way I switched focus back to containerizing it. I had a couple issues with podman that appear to due to upgrades, but simply deleting the `~/.config/containers/libpod.conf` fixed that. And now we have the start of a new web app. 

2. Test Framework

Next on the list while we're still in the early stages is testing. I suppose had I done things correctly I would have started there, but it wasn't in my initial thoughts. Testing the http server is proving to be more annoying than expected. `go test` doesn't seem to launch the app but run the functions you tell it to. To get around this it appears that I can move the http server part into its own function/object that is called in both the app and test framework. 

I'm sure part of it is the comfort level, but I think this experiment has reinforced that I am better suited to the infrastructure side of things. Maybe I'll look back on this in a few years when I've figured out what I'm trying to do and chuckle at myself for thinking it was so hard. I suppose in a way I am cramming a lot of new stuff into this poor attempt at a budgeting app.

Update 12/18/19: Maybe someday I'll write a webapp and actually do that side of things, but really I just want to do infrastructure. That said I do want to be able to do more than just piece already existing things together so I still do need to learn go, it's just feeling like maybe the bugetor app was not the thing to start with. Also in talking to Ben at the bar throwing in testing at the learning phase is probably not worth the trouble.