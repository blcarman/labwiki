As much as I love gitlab it's probably more practical to start learning Azure Devops.

## Summary of Steps ##

* created Lab project, currently set to private
* add ssh public keys
* create labwiki repo... it would appear since gitlab embeds the wiki repo it doesn't have the normal options to sync, so just going to move it.
* Add wiki to azure devops pointed at labwiki repo, looks like I can have multiple wikis too. Also supports using a specific folder in a repo as well so it might make sense to consolidate repos at some point. 

## Pipeline Agent ##

I've been cleaning up my workstations and noticed some firewall backups that should probably be getting done in a more standard way, so thought I'd try out a pipeline agent for it.

Following this doc: https://docs.microsoft.com/en-us/azure/devops/pipelines/agents/v2-linux?view=azure-devops

* access token also given access to deployment groups (not sure that it'll be necessary though)
* has you download agent as if its specific to the pool, but appears to be generic.

Left turn into terraform land to build the VM for the agent (will do the chef/ansible part of that later)
and found a bit of an annoying dependency thing. I do not have a "build" tag in vsphere yet so even though terraform will create that when I run it since it doesn't already exist the run is failing. I think this is finally a case for where output from a module is useful, so rather than pulling the data from vmware I will get it from my vcenter module...hopefully. Sure enough that worked and let me remove a couple data sources from the VM module, still a few more to clean up now. 

### Unattended install ###

```
useradd -m build_user
sudo mkdir /opt/azbuild
sudo chown build_user:build_user /opt/azbuild
sudo -iu build_user
wget https://vstsagentpackage.azureedge.net/agent/2.160.1/vsts-agent-linux-x64-2.160.1.tar.gz
cd /opt/azbuild
tar xvzf vsts-agent-linux-x64-2.160.1.tar.gz
./config.sh --unattended --url https://dev.azure.com/blcarman --auth pat --token <your_token> --pool default --agent lab01 --acceptTeeEula
also need to install the systemd service
sudo ./svc.sh install
update systemd service to run as build_user
systemctl daemon-reload
start service
```
possibly of interest: configure cgroup to limit resource usage of pipeline jobs

set capabilities

Now we have a build agent ready to use.

## First Pipeline ##

Because I'm paranoid I also went ahead and disabled anonymous badge access in Project Settings -> Pipeline -> Settings

Originally I was going to set up some backup stuff, but now I'm thinking chef might be the better initial target. And then maybe I'll build out some docker diagnostic containers.

Install latest version of azure-cli (>2.0.49) and devops extension
`az extension add --name azure-devops`
create full access token and load into environment variable `AZURE_DEVOPS_EXT_PAT`, this replaces having to explicitly login

`az pipelines create --name "chef-lab.CI"` choose starter template, unfortunately it will try to use a web browser for an editor... but you can create a new branch for it at least.

To start I edited to use my local agent pool instead of MS hosted one. This requires authorizing the agent pool to use this release yaml (or allow to do all releases).
install git on build server...ugh its too old as well...
`Min required git version is '2.0', your git ('/bin/git') version is '1.8.3'` so might be upgrading it to EL8 tomorrow

using IUS repo (don't feel so bad now that not a "competitor"), 
`yum install https://repo.ius.io/ius-release-el7.rpm`
`yum remove git`
`yum install git222`

Queued new build and now its working!

So now that we know the agent is working it's time to set up something more complicated... but not too complicated. So looking at chef we've got some linting we can do and then we can publish it out to the server. And maybe I want to only do the publish when its on master. For some reason or other it seems I can't simply do something like "when: master"...instead we have to do something like `condition: eq(variables['Build.SourceBranch'], 'refs/heads/master'))`. So that's still not terrible but just slightly annoying which I feel like is the whole goal of MS right now. It's not bad enough to pick something else but its also not good enough to make you want to use it. 

### Demands and Capabilities ###

Obviously I want my chef stuff to run on something that has chef. Now I also plan on configuring these servers with chef so I could really just skip the whole demands/capabilities thing but I'm trying to embrace this and the what if I have a pool of mixed agents. Well, I don't see a good way to set capabilities using the az cli though surely it has to be possible, or even better just do it from the agent at install time.

Also at this time I installed chef-workstation on the build server.

### Branches Builds ###

If I change the pipeline yaml in a branch does it use the one from my branch or the one from master? It looks like it always uses the one from master which I suppose it would have to to prevent unauthorized changes being made. 

A detour into chef land to fix the stuff found by cookstyle/foodcritic.