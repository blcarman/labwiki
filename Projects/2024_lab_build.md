It's been a while since I've done much lab stuff so I've forgotten a lot of the details.

Since my synology got corrupted I need to rebuild things. I'm going to attempt to document the process from start to finish.

## VM Template ##

packer repo is up to date.

I ran it from outside toolbox on titanium.

`packer build --only=qemu ubuntu20_04/ubuntu.json`

Then copied the output file over to the lab share. After that I had to add `.gcow2` to the filename for Synology to see it for importing.

Then simply create a new VM from the imported image.

Next is the configuration. I think the ansible to do rke2 is still mostly accurate, just need to bump the version probably.

On titanium 

```
toolbox enter custombox-0.2
cd src/ansible
ansible-playbook -i ../ansible_private/inventories/lab --diff -v --limit "rke1" playbooks/rke2_bootstrap.yml
```

Oops apparently I did some inventory cleanup that might have been too aggressive.

And ssh seems broken on rke1... or it's just froze, trying a reboot. No change after reboot. It's picking up an IP so it has at least some level of connectivity but the console is not responsive and it's not at a login prompt, so something still seems to be wrong.

changing vm networks to the default and back to trust seems to have got it working but I'm not sure why. Going to do a dist-upgrade and then try the ansible.

I had kept the hostname but forgot about the static map on the edgerouter, so that's what was causing that weirdness. 

Some ansible code and variable mishaps of course, but rke2 is installed. 



Now what?

cert-manager
nebula
jellyfin
warehouse proxy ingress


synology storage provisioner
upgrade controller
flux?
unifi controller... or I pick up a cloud gateway ultra
home assistant?

## Kubernetes Apps ##

I should figure out my kubeconfig situation. 

### cert-manager ###

Looks like my cert-manager config is also out of date. It's still using digital ocean for the dns.

I did create the namespace and api token secret manually.

Going to use rke2's helm integration/auto deploying manifests for now... that just got stuck in a loop of failure, didn't seeem to be installing the CRDs even though the option was set.

cleaned up cert-manager mess and going with the static way.

```bash
root@rke1:~# /var/lib/rancher/rke2/bin/kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.15.3/cert-manager.yaml
namespace/cert-manager created
customresourcedefinition.apiextensions.k8s.io/certificaterequests.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/certificates.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/challenges.acme.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/clusterissuers.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/issuers.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/orders.acme.cert-manager.io created
serviceaccount/cert-manager-cainjector created
serviceaccount/cert-manager created
serviceaccount/cert-manager-webhook created
clusterrole.rbac.authorization.k8s.io/cert-manager-cainjector unchanged
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-issuers unchanged
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-clusterissuers unchanged
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-certificates unchanged
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-orders unchanged
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-challenges unchanged
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-ingress-shim unchanged
clusterrole.rbac.authorization.k8s.io/cert-manager-cluster-view unchanged
clusterrole.rbac.authorization.k8s.io/cert-manager-view unchanged
clusterrole.rbac.authorization.k8s.io/cert-manager-edit unchanged
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-approve:cert-manager-io unchanged
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-certificatesigningrequests unchanged
clusterrole.rbac.authorization.k8s.io/cert-manager-webhook:subjectaccessreviews unchanged
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-cainjector unchanged
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-issuers unchanged
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-clusterissuers unchanged
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-certificates unchanged
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-orders unchanged
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-challenges unchanged
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-ingress-shim unchanged
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-approve:cert-manager-io unchanged
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-certificatesigningrequests unchanged
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-webhook:subjectaccessreviews configured
role.rbac.authorization.k8s.io/cert-manager-cainjector:leaderelection unchanged
role.rbac.authorization.k8s.io/cert-manager:leaderelection unchanged
role.rbac.authorization.k8s.io/cert-manager-webhook:dynamic-serving created
rolebinding.rbac.authorization.k8s.io/cert-manager-cainjector:leaderelection unchanged
rolebinding.rbac.authorization.k8s.io/cert-manager:leaderelection configured
rolebinding.rbac.authorization.k8s.io/cert-manager-webhook:dynamic-serving created
service/cert-manager created
service/cert-manager-webhook created
deployment.apps/cert-manager-cainjector created
deployment.apps/cert-manager created
deployment.apps/cert-manager-webhook created
mutatingwebhookconfiguration.admissionregistration.k8s.io/cert-manager-webhook configured
validatingwebhookconfiguration.admissionregistration.k8s.io/cert-manager-webhook configured
```

### Nebula ###

Nebula config went in fine.

I need to figure out some better k8s secret management, then I'll be able to commit these yamls in git.

Also did os updates just for fun.

### Warehouse Ingress ###

tcp services config causing a bit of trouble, will it find the configmap automatically? It doesn't seem like it but also using extraArgs wasn't working either. I'll have to try again tomorrow.

figured out the issue, I thought it wanted a string but it wanted key/value pairs.

still doesn't seem to be fully working.

disabled and masked rpcbind service to avoid conflicts on port 111.

I don't see 80/443 in ss which seems weird too.

Side quest to get the main ingress working. I was seeing cert not found errors in the nginx logs.

Turns out I had a few issues on the cert-manager side.

The cloudflare secret needed to be in the same namespace as the issuer, not cert-manager. Previously I was using a cluster issuer, now just an issuer in the default namespace. So I had to update the ingress annotations as well.

Then dns resolution was still trying to go through the synology. So I removed that option on the edgerouter. I suppose I will need to restore some kind of dns server. It would be nice if I could do wildcard in /etc/hosts

Certificate has been sorted but still having issues with nfs.

Set hostNetwork true and now at least see the ports listening.

Oh I bet nebula is blocking it. Yep, apparently I have my groups mixed up. I really don't have enough different groups on nebula to worry, so just switched it to allow all.

Actually since I really only have 1 k8s server to worry about I can just resolve that whole domain in the edgerouter like so

`address=/k8s.home.bradlab.tech/172.16.10.2`

I will need to update cert-manager to use cloudflare as it's dns resolver but that's no problem.

I should probably just put that record on cloudflare and not orange-cloud it. It's not like it's accessible anyway and that would feed further into more stuff in the cloud.

### Jellyfin ###



## Automatic Updates ##


## Closing Notes ##

This page was called a lab build, but really this is more my production home server. Lab things should happen elsewhere.

