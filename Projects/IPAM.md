I got tired of Infoblox's 60 day license limit so I decommissioned it. I had the idea that I could simply create a variable in either the terraform or ansible code with a list of dns and IP mappings. However, this proved to be a bit more complicated than expected since a simple list would make it hard to pass values for the entries a module might care about. It also wouldn't handle changing counts of resources. So the lab needs a "real" IPAM solution which brings us back to Netbox. 

It looks like Netbox has matured a bit since the last time I tried it. There's also a community terraform module, https://github.com/limberger/terraform-provider-netbox, though it's not quite complete. This does however present the opportunity to contribute and learn more about Go/terraform. 

And finally, there's now a paradox of how to deploy netbox via terraform if I then want to use it from within terraform. Do I need to separate the netbox build into a separate infrastructure environment?

Looking back at my notes it looks like most of the problems I had with dhcp were openshift and my own failings so maybe I could get away with dhcp after all and just set up reservations where needed. 