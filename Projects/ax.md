# The Annual Windows Project #

It seems like about once a year I get pulled into a Windows project. This time around I'm getting some experience with Microsoft Dynamics AX. I've been able to limit my exposure to Windows over the past several months and it's been nice but now I need to get back into it. The first thing I attempted to do was spin up some VMs with vagrant. This has proven to be annoying. Here is a dumping ground of notes to help organize my thoughts and frustrations.

* Bento project: has configs for windows but doesn't publish the boxes... half-assed documentation, tells you how to build them with packer but not actually use them with vagrant
* Windows is fat, I see a lot of random failures and a lot of them were probably due to the bento box or vagrant only giving the VM 1 cpu and 1 gb RAM by default
* vagrant is terribly impatient and will decide to just randomly fail when windows takes its sweet time on things
* unrelated but my workspace is a mess making it difficult to decide where VMs and configs should live, that is do I do things with vagrant or terraform and chef?
* this is a project I am just going to have to stubborn my way through

* started domain controller chef (windows_ad community cookbook looks solid), amazing how much easier this is now that I have some experience, if done well I might even favor it over ansible in certain cases

`knife bootstrap localhost -N test -p 2222 -U vagrant -P vagrant --ssh-verify-host-key never --sudo`

`knife bootstrap 192.168.30.151 -o winrm -N dc1.test.bradlab.tech -x Administrator -P Pa55word`
note: had to put in a dummy entry for the validation_key in my .chef/knife.rb per "validatorless bootstrap" doc

how to add/use community cookbooks? perhaps `knife supermarket install windows_ad`, this works but will commit the community cookbook to your chef-repo.

edgerouter conditional dns forwarding `set service dns forwarding options server=/test.bradlab.tech/192.168.30.151`

did some work on base role to get windows clients to join domain, turns out I did not sysprep my 2019 template VM... maybe I should have gotten packer working after all (though vagrant boxes might have same issue)
