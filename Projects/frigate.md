# Frigate: Camera Software #

```bash
podman run -d \
  --name frigate \
  --restart=unless-stopped \
  --mount type=tmpfs,target=/tmp/cache,tmpfs-size=1000000000 \
  --shm-size=64m \
  -v /home/brad/frigate/storage:/media/frigate \
  -v /home/brad/frigate/config.yml:/config/config.yml \
  -e FRIGATE_RTSP_PASSWORD='password' \
  -p 5000:5000 \
  -p 8554:8554 \
  -p 8555:8555/tcp \
  -p 8555:8555/udp \
  ghcr.io/blakeblackshear/frigate:stable
```

config 

```yaml
mqtt:
  enabled: False

cameras:
  basement: # <------ Name the camera
    ffmpeg:
      inputs:
        - path: rtsp://admin:blah@192.168.50.69:554/cam/realmonitor?channel=1&subtype=1 # <----- The stream you want to use for detection
          roles:
            - detect
    detect:
      enabled: False # <---- disable detection until you have a working camera feed
      width: 1920 # <---- update for your camera's resolution
      height: 1080
```

wifi camera is on wrong network, though I really probably need to set up a camera/iot vlan and drop the internet access

for the jetson nano

frigate-tensorrt-jp4

```bash
docker run -d \
  --name frigate \
  --restart=unless-stopped \
  --mount type=tmpfs,target=/tmp/cache,tmpfs-size=1000000000 \
  --shm-size=64m \
  --runtime nvidia
  -v /home/brad/frigate/storage:/media/frigate \
  -v /home/brad/frigate/config.yml:/config/config.yml \
  -e FRIGATE_RTSP_PASSWORD='password' \
  -p 5000:5000 \
  -p 8554:8554 \
  -p 8555:8555/tcp \
  -p 8555:8555/udp \
  ghcr.io/blakeblackshear/frigate:frigate-tensorrt-jp4
```

```yaml
mqtt:
  enabled: False

ffmpeg:
  hwaccel_args: preset-jetson-h264

cameras:
  basement: # <------ Name the camera
    ffmpeg:
      inputs:
        - path: rtsp://admin:blah@192.168.50.69:554/cam/realmonitor?channel=1&subtype=1 # <----- The stream you want to use for detection
          roles:
            - detect
    detect:
      enabled: False # <---- disable detection until you have a working camera feed
      width: 1920 # <---- update for your camera's resolution
      height: 1080

objects:
  track:
    - person
    - cat
    - dog
```

At some point my jetson nano died. I'm going to deploy frigate to k8s vm for now. Long term I'll add my old framework board to the cluster with a coral tpu. There also seems to be a helm chart now so that's helpful.

It looks like I also moved the wifi camera to the iot network at some point, but it's not as locked down as I thought. 

```bash
cd ~/src/labinfra/deployments/frigate
helm repo add blakeblackshear https://blakeblackshear.github.io/blakeshome-charts/
helm upgrade --install frigate blakeblackshear/frigate -f frigate-values.yml
```

disabled tls on the frigate side since the ingress controller will handle that, https://github.com/blakeblackshear/frigate/discussions/12043

initial admin password can be found in logs.

and my frigate is working yay.

~io and network does seem a bit high~ compared to the usual nothing I have going on and actually turns out the disk activity is coming from elsewhere... Hmmm iotop inside the k8s vm didn't seem too busy, but shutting it down does definitely stop the io, 

Got my old amd 2200g/x370 system running, minimized ubuntu 24.04. Troubleshooting is a pain on it, and the minimized install still seemed kind of large. I also ordered a coral usb. For now testing with cpu and not great results, going to leave it running overnight to see if the dogs/cats get picked up. Camera timestamps do get seen as motion. 

In the debug section you can add motion masks by drawing the polygon, but it's kind of clunky getting it into the config since its readonly in k8s. There is a copy coordinates button if you look closely.

Also confirmed person detection working now.

I removed the amcrest logo in the settings on the camera itself.

This is turning into a cool project. If the coral works out I'll probably do the plus subscription just to support it.

With a tiny bit of fiddling I got the coral working. I had to remove the model config I had put in for the openvino cpu detector. Before I did that I was getting a weird error when it would try to load the tpu.

```
ValueError: Model provided has model identifier 'l ve', should be 'TFL3'
```

inference speed dropped to 10ms from 30. I didn't measure cpu very well but I can only assume it dropped that quite a bit as wel.