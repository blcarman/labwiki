I don't know why I didn't start documenting this sooner. Last year I started going for more of a bee lawn. I've been planting clover and some of it's growing. The back is actually doing pretty good, there's some clover and violets. I want to get some wild strawberry as well. The front yard is more of a disaster, I think the soil is just too poor and too much sun. There is some clover growing but it's almost yellow instead of a healthy green. I'm thinking it might be time to call some professionals that do native/bee lawns. We previously had some landscapers out to for other stuff and I was never really impressed. Old school I guess is the term I would use. 

Actually the front yard really isn't doing as bad as I thought. There are still some bare patches, but a good amount of clover has actually taken hold. I'm going to keep trying to put more down. There was also some <i forget the name> trying to crowd out the clover. I'm pulling it where the clover is already, but mostly leaving it in other places since it is at least green.

Under the pine trees I have some stinging nettle growing. It's still not great but better than the invasive thistle. 

https://minnehahafallslandscape.com/services/bee-lawn/


There are at least 3 or 4 oak seedlings that popped up in the back yard. A couple are in good spots, but one I might have to move. I'll need to cage them before the deer find them. It looks like I could order some for $10 but that's a bit less fun. 

The blue spruce trees still seem to be getting the fungus even after cutting back a lot of branches last year. I'm not really sure what to do with them. I tried planting some clover in that area but not having a ton of luck. I might need to wait til fall and just overseed then. 

I got a big 5 pound bag of bee lawn seed mix so late August I'll try to plant that. I did some more reading and bee lawn mixes seem to do better being seeded in november so they get the freeze/thaw cycle. So for now I'm just trying to do some prep work, the stilt grass suddenly took over the backyard so trying to clear that out now.

Seeded heavily (used most of the 5 pound bag) around 11/23/24 and now just have to wait and see what happens in the spring.