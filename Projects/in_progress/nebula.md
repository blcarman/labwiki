## Tasks ##

* configure CA
* install lighthouse
* install client
* automate client provisioning

### Planning ###

I found an ansible role on [Ansible Galaxy](https://github.com/Trozz/ansible-nebula) but it expects the certificates to be generated/in place before running. At first I expected the CA to be on the lighthouse but it should actually be separate. I suppose it'll end up living on titanium though maybe I'll set up a VM for it. The certs also require the IP for the node, so I'll have to set up IPAM. On the plus side I'll finally have a reason to set up Netbox. (Side note: netblox has a community terraform provider but still might be worth making my own).

Instead of dealing with Netbox from the start, I'll just manually IP things and get a feel of whether or not Nebula is even worth pursuing further.

### Attempt #1 ###

Manually configured on titanium and digitalocean VM and titanium. Easy to configure. Will probably not use the ansible galaxy role I set up since its just doing a few things and it wants to restart the service every time it runs...(if I'm feeling adventurous maybe I'll throw up a PR and see if the guy accepts it). I think I can also put the cert generation as a local task in the playbook, leaving IP assignment the only question. 

So I got titanium, scandium, and the digitalocean VM set up. Completely unscientific scp test from scandium(wifi) to titanium got about 6.7 MB/s over nebula and about 7.3 MB/s on internal network, really not much of a difference considering no tuning yet. 

Not that it was really a question, but I also confirmed Nebula will keep traffic local if both nodes are on the same non-nebula subnet. Things also worked nicely when I tested in Omaha. Also while testing the local bit the firewall also got tested, though I didn't really dig into how/what it does for the actual underlying firewall. 

The initial tests have all been good so I think it's time to automate it and roll it out everywhere. Once I have it everywhere I can start securing things like nfs and ssh.

Can nebula set mtu based on local network?

### Full Deployment ###

~Should I do this with ansible or chef? I could probably use the chef practice, but it is simple enough that ansible would probably be ok too.~ I ended up going with ansible and it was indeed fairly easy. If I do go with ansible I need to have CI start running it so things stay in a good state. If I do ansible I could potentially cheat the IPAM a bit with host vars as well if I really wanted to. I'm also thinking of installing it on the edgerouter. 

## 2021 Update ##

I kind of forgot about this one for a while. I had to create a new CA because I lost the original and the certs expire after a year anyway. 

### Netbox ###

I deployed netbox and wrote some ansible to use it to allocate the IPs, as well as generate/deploy the certs. 

### Lighthouse ###

I originally deployed the lighthouse as a normal service on the host, but since I'm moving the VPS to k3s I was thinking about moving the lighthouse to a pod as well. I also have some services on the home k8s cluster that could benefit from a nebula sidecar. 
