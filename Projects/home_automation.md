Finally looking at home automation. There are actually a lot more self-hosted options than I expected. Here's a quick overview of my initial research.

* https://www.universal-devices.com: Ed recommmended this one, he's had good luck with it, but java server and client is kind of gross
* OpenHAB: OSS, shiny looking but bad reviews, possibly multiple fragmented UIs, nodejs and java? based
* Home Assistant: OSS, generally favorable revies, python based, yaml configs
* Node-red: OSS, up and coming, lot of people moving from Home Assistant to this

I think I'm going to start with Home Assistant, as it looks solid and it should be fairly easy to use the same hardware across any of the OSS solutions. 

4 years later in 2023, I have home assistant running for probably a couple years now and still only have a single light switch and a couple temperature sensors configured.