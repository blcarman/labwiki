## Secrets ## 

Thycotic server installed relatively smoothly. Did have to reboot for it to realize it had installed the pre-requisites and had to disable the enhanced IE crap for it to download sql express.

Get a token:
```bash
curl -k -d "username=bradmin&password=Pa55word&grant_type=password" -H "Content-Type: application/x-www-form-urlencoded" -X POST https://192.168.30.131/secretserver/oauth2/token
```

API Documentation:
```
https://secret/SecretServer/Documents/restapi/
```

for using token after its granted, appears to be better to set up a full client (ironically the way I was doing it originally), so client can be set in the providerconf function and re-used elsewhere

https://stackoverflow.com/questions/51452148/how-can-i-make-a-request-with-a-bearer-token-in-go

vs code seems to occasionally get confused as to the state of the file and complains about things it shouldn't, re-launching seems to fix.

Yet another left turn, perhaps instead of headers i should be using the cookie jar, does the token automatically get added to the cookie jar when requested?

Finally making progress only to find out the actual API is disabled until I apply a license and I need a "company" email address to get a temp one. I suppose I'll have to ask the boss if we can get a dev license at work.

Also worth noting the path for the api is `/api/v1/`