Best golang tutorial I have found https://spf13.com/presentation/building-an-awesome-cli-app-in-go-oscon/, it actually goes through setting up your environment and the Cobra CLI thing looks super useful/neat as well. 

`go get` does not make sense to me. Why wouldn't it naturally get things as part of the build process? And even running go get manually just causes it to bitch about stuff not being local. Go touts itself as batteries included but they conveniently left out any sort of manual. Like I have to go through their tutorial for idiots instead of them just writing a fucking blog post on how to download dependencies. This is why python wins, just pip install your requirements file and you're done. So we'll tackle go and its damn modules or not modules whatever the fuck tomorrow.

## Update 1/19/20 ##

I have a problem with Go. Maybe others have the same problem, but for now we'll assume it's just me. The problem itself is rather basic, the solution less so. I feel like there is a duality with Go. The documentation either treats you as if you know absolutely nothing or absolutely everything. A common answer I see when searching on stackoverflow and similar is to just make a custom wrapper function or just re-implement something common yourself because... I don't know why. But really I guess my problem with Go is that it's not python. Go is not as easy as "import thing" and "do thing" and that frustrates the hell out of me when I see so many people acting like it is the future. 

Maybe I'll get over it and laugh about how stupid I was when I first tried to learn it. I really hope that happens. Or maybe more accurately I need to understand the Go way similar to how things can be "pythonic". 

I suppose one of the differences with Go is that rather than spending time finding a library that already exists, you spend that time writing your own that fits what you need. I still feel like this creates a lot of similar but not quite the same code in the world, but maybe that's ok. 

## Update Late 1/19/20 ##

I've come to terms with the fact that I will likely have to learn a lot more core concepts of both Go and programming in general to really grok it. I'm sure this will carry over to a multitude of other areas so should be a good win. I also hope that once I get a better understanding of things I'll be able to rely less on existing projects and stackoverflow. In any case I am not letting Go defeat me. I will make a working terraform provider. 

## Notes ##

* defer: use this to do a thing when the function containing it returns, useful for cleaning up even if an error occurs
* _ use to eat error object when you don't care about it
* exported names (functions, structs) start with uppercase, only exported things can be used when imported by another package
* trailing commas: go loves them
* go is very opinionated, refuses to build if you don't use a variable, likes error objects, picky about comments

## 1/23/20 ##

I am coming to terms with the fact that go is not python, and with that realizing that I do like it. So here's where stuff stands now

bash/ansible: config management, simple one-off scripting
python: slightly more complex scripting, maybe a flask web frontend
go: more complex "apps", i.e. cli tools, terraform providers
ruby: just don't (unless it's in the form of chef)

## 1/29/20 ##

After roughly 3 days of fighting with getting a POST request I have learned a few more things. Go's http client will follow a 301, I had actually read about this early on while looking up setting headers but didn't really think about it since it wasn't relevant at the time. I also found out 301 can change the POST to a GET which would then give me a 200. After spending way too much time thinking there was something wrong with my code I finally looked at the server logs and found the 301 which was caused by me not having a trailing "/" on the url. I thought I had tried that at some point and got code panics but when I tried it again now it worked, so who knows. I've said it in the past and it's still true, I need to get better about tracking actual changes while troubleshooting these things. Maybe I could use commit squashing to worry less about committing bad code? Also tests might help a bit, but it's difficult to write tests when you're still learning the language itself. In any case the journey continues and now I think it's ready for the next step of incorporating into a terraform provider. I will say netbox has been a good project to work with as it has a good API and documentation. 

Besides the terraform provider I also need to decide how in depth I want to go with this. There are already cli and terraform providers that exist for netbox, but I might still be able to learn some things from it. For fun I could also make it run in a container, and add some tests as well. Finally I'll need to formalize my notes here into a blog post. Another note, perhaps I should not have given up on the go tour thing, it bored the hell out of me and I like jumping into things, but every time I do it that way it feels like I bash my head against a wall until it breaks (hopefully the wall and not my head, but sometimes its hard to tell which). I do get a lot out of learning the hard way though or at least the added effort makes it feel that way, so it's very likely how I will continue doing things in the future.

And another thing finally just making certain variables global made things so much easier. No more worrying about passing pointers or what not. Maybe on the terraform side that would be worse, but in this cli app where the thing is so critical I think it makes sense. I'm sure at some point I'll read a blog that tells me I'm evil for doing this but whatever.

## A Terraform Provider (2/2/20) ##

I've never been good about repetitive tasks (except in regards to lifting weights). So rather than add a bunch of commands I won't ever use to my netboxcli project, I'm going to make the jump to the terraform provider. That said there are several commands I will need to add to netboxcli for the terraform provider to do everything I want it to as well. I want to at least get a POC terraform provider working just to make sure the cli bit I've written will indeed work the way I expect. 

* data source: be able to use a DCIM site as a data source
* resource: be able to add a DCIM site as a resource
* bonus: gitlab CI builds and publishes artifact

## 3/10/23 ##

I basically dropped go after the last 2020 update above. Now, I'm giving it another try because I really want to make a kubernetes operator. I'm at the point where writing ansible roles/playbooks is second nature, and most of the low hanging fruit has been handled. At this point I'm looking at operators as a way to provide a better experience than having a bunch of separate CI/CD pipelines. I also hope it will help me gain a better understanding of the kubernetes internals, not necessarily the code itself, but how the various components work together.

I started by trying to follow the kubebuilder tutorial and just couldn't really follow the bouning ball. Next I tried reading `Learn Go with Pocket-Sized Projects`. It flowed well, but being early access was still lacking some completeness. I wasn't able to get the second example working, and while I'm sure I could have figured it out, it just didn't feel worth it. So I switched over to `Go in Action` which is also early access but is the 2nd edition so hopefully more complete. I also noticed this gives me the ability to keep going over the same basics again and again while feeling like I'm making progress. So I need to start writing things down and keep myself moving forward.

I've written it elsewhere, but it's definitely becoming apparent I should have wrote the go code to do the thing I needed and then put that into a k8s operator instead of trying to jump straight into the operator. It might also turn out that writing ansible modules in python and then using the ansible kubernetes operator framework is the better option. In any case I'm trying to embrace the idea that learning go is more for the fun of it than having an actual problem to solve with it.

And turns out the `Go in Action` MEAP only has 4 chapters...flipping over to the original version.