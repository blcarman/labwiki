## Matrix Migration to k8s ##

It's time to start thinking about how I'm going to move the matrix server to k8s. I think using flux with a basic manifest is the way to go, will probably use the Bitnami postgres helm chart as well since I'm already using that elsewhere. I'm also torn between continuing to use traefik or moving to nginx + cert-manager for ingress. These all seem like kind of boring problems at this point which I guess is why I've waited so long to actually do it. 

1. get plain test matrix server running at home on prodranch
1. test database import from existing server ( is anything stored outside the db? should I prune any data while I'm at it? i.e. only bring over 6 months worth of history)
1. plan real move (can 2 synapse servers talk to the same db?)
1. do the real move

## Flux ##

```
wget https://github.com/fluxcd/flux2/releases/download/v0.16.1/flux_0.16.1_linux_amd64.tar.gz
cd ~/bin
tar xvf ../flux_0.16.1_linux_amd64.tar.gz
rehash
# make sure gitlab token is exported export GITLAB_TOKEN=<my-token>
flux bootstrap gitlab \
  --owner=<organization> \
  --repository=<repo-name> \
  --token-auth
```

Oops also started this in my blog repo, moving it over here for now.

Title: Migrating My Chat Server to kubernetes
Date: 2022-10-16
Category: lab
Tags: kubernetes, postgresql, chat

This has been on my todo list for a couple years now. I have been putting it off somewhat because people actually use it, but also I didn't have a good idea for how handling the ".well-known" page for federation. It turns out there is now (or I just missed it previously) an option to enable serving the well-known page from the matrix server instead of relying on a static page served by nginx separately. With that complication out of the way, this should be a fair bit easier.

## The Plan ##

* ingress for chat service with Let's encrypt TLS
* deployment for matrix synapse pod
* configmap for matrix homeserver.yaml
* local directory volume for chat server data
* postgresql database deployed via bitnami helm chart

## Backups ##

Of course I need to test out the plan as well as actually do the migration, so we need to backup everything (to my actual users, don't worry I totally have routine offsite backups happening). 

* backup all the chat data (note this includes the homeserver.yaml, we'll exclude that in the restore to prevent confusion with the configmap)

  ```bash
  tar czvf chat.tar.gz /opt/chat
  ```

* backup the database

  ```bash
  sudo -iu postgres
  pg_dumpall -f 12/backups/all_10_16_22.sql
  ```

* copy those files to an offsite location

## Testing ##

Now, I'm a bit torn here. I can either just start setting things up in the "prod" instance or I can test things out in the lab first. 

## Detour ##

My code is all over the place again. I'm thinking of splitting code for "prod" off on it's own. 

Creat an archive tag on ansible repo and delete cruft

## Deployment ##

Going to take advantage of K3S's auto-deploying manifests.

## Minimum Effort ##

9/18/23 still haven't finished this project. I think it's time to do the minimum. 

New k3s vm on same vpc as existing chat server, so I can move matrix and then the db. Though I'll probably move the stuff from the other k3s first and then get sidetracked again :). 

The vpc was the magic bit I needed. That and just ditching the whole idea of making it repeatable. So roughly here's what I did.

1. stand up new ubuntu 22.04 vm in same vpc as existing chat vm
    * enabling backups at vm level so I don't have to worry about k8s jobs (although it looks like the bitnami postgres can also setup backup jobs)
2. install k3s
3. move blog to make sure everything working
4. create pvc for chat data and copied over as is ( this allowed me to skip secrets and configmaps for the chat pod )
4. move chat pod while still using existing db
    * slight issue getting let's encrypt cert, probably because I didn't move dns soon enough, so just using self-signed with cloudflare in front for now
5. unrelated but moved nebula pod as well so I could get rid of kube1
6. moved the database, this was the biggest challenge and had to be re-done several times
    * bitnami helm chart created the db with wrong collation for synapse
    * I changed the db username but that caused warnings on the restore so went back to original
    * db restore didn't work quite right when helm chart created the db with the correct collation. All the tables got created but there was no data in them

## Conclusion ##

So maybe I didn't learn anything new out of moving things this way, but I am back down to a single vps with up-to-date software again. I might make some improvements down the road like using flux for updates, but I'm not rushing it.

Some other cool ideas that came out of this:
* create a channel and bot for maintenance notifications
* monitoring both my server health and the federation

It might have taken 3+ years, but we got here.