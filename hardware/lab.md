It's time to think about lab hardware again. I really haven't done anything major since 2017ish but every year I think about just rebuilding the ultimate lab from the ground up. After seeing the price tag I usually end up with a couple minor upgrades/tweaks to get through the next year. I've also been moving away from datacenter to consumer grade as my job has moved to the software side. I've been doing an all in one storage + lab system for a couple months but it's 4 core cpu (no hyperthreading) is really getting stressed with just 2 VMs running k8s. So time to see if I buy a new big cpu for it or if I try to get a little more out of my old Dell R610.

## 2021 Requirements ##

* run multiple 1-5 node k8s clusters

* is the dell r610 worth running? move the cpu/ram from the hp back to the dell since HA is less of a concern. proxmox or bare metal ubuntu? idrac/ups to remotely power on/off

* get better power estimates

* flatten network? encrypted mesh for security?
