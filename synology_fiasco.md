synology fiasco

uninstall some stuff

syno when recommends removing volume and then restoring from backup but that seems excessive and im not sure if removing the volume deletes the data

if i have to remove anyway i might as well try to repair

enable telnet 

create recovery user(my normal user has a home on the affected volume)

do some of this https://community.synology.com/enu/forum/1/post/156958

synosystemctl stop pkg-iscsi

root@warehouse:~# btrfs check /dev/mapper/cachedev_0 
Syno caseless feature on.
Checking filesystem on /dev/mapper/cachedev_0
UUID: 3bbace46-34ac-43c1-80a4-0fd7a8cb73a1
checking extents
leaf parent key incorrect 12454806437888
bad block 12454806437888
Errors found in extent allocation tree or chunk allocation
Syno block group cache is sync
checking free space tree
checking fs roots
checking csums
checking root refs
found 11708491481232 bytes used err is 0
total csum bytes: 8778672868
total tree bytes: 10702946304
total fs tree bytes: 16384
total extent tree bytes: 899497984
btree space waste bytes: 736537896
file data blocks allocated: 0
 referenced 0
root@warehouse:~# btrfs check --repair /dev/mapper/cachedev_0 
enabling repair mode
couldn't open RDWR because of unsupported option features (3).
Couldn't open file system

https://gist.github.com/bruvv/d9edd4ad6d5548b724d44896abfd9f3f

root@warehouse:~# btrfs check --clear-space-cache v2 /dev/mapper/cachedev_0 
Syno caseless feature on.
Clear free space cache v2
free space cache v2 cleared


root@warehouse:~# btrfs check --repair /dev/mapper/cachedev_0 
enabling repair mode
Syno caseless feature on.
Checking filesystem on /dev/mapper/cachedev_0
UUID: 3bbace46-34ac-43c1-80a4-0fd7a8cb73a1
checking extents
leaf parent key incorrect 12454806437888
bad block 12454806437888
Errors found in extent allocation tree or chunk allocation
Fixed 0 roots.
block group cache tree generation not match actul 668348271, expect 668348272
checking free space cache
checking fs roots
checking csums
checking root refs
found 11708462268559 bytes used err is 0
total csum bytes: 8778672868
total tree bytes: 10673717248
total fs tree bytes: 16384
total extent tree bytes: 899497984
btree space waste bytes: 730390471
file data blocks allocated: 0
 referenced 0


well repair did not seem to fix anything. Backing stuff up to usb now. Annoyingly hyper backup and other apps were installed on the same volume. 

So now this has me rethinking the idea of a single volume. It was nice so everything got caching. But now I'm wondering

* small app volume
* VMs and iscsi with caching
* important file shares
* don't care if lost file shares

20 tb usb drives are expensive, I'm right at the awkward 11TB, but if i split volumes I could probably do simple backups to a 10tb and a 4 tb usb

hyperbackup does not store things as their original files, so any restores through that will have to be through hyperbackup again. Only about 650gb, I think most of the local stuff is movies

backed up most things. Now I'm tempted to put the drives in my desktop and try to repair btrfs there. 

I dropped the ssd cache hoping I could turn into a volume, but synology doesn't like that they aren't synology brand drives. Looks like I can update the drive database to allow it, still disappointing they couldn't just let me bypass in the UI.

One final try doing a `btrfs rescue chunk-recover /dev/mapper/cachedev0`


root@warehouse:~# btrfs rescue chunk-recover /dev/mapper/cachedev_0 
Scanning: DONE in dev0                          
Check chunks successfully with no orphans
Chunk tree recovered successfully

ooo success... maybe

running another check now

hmmm looks the same

root@warehouse:~# btrfs check /dev/mapper/cachedev_0 
Syno caseless feature on.
Checking filesystem on /dev/mapper/cachedev_0
UUID: 3bbace46-34ac-43c1-80a4-0fd7a8cb73a1
checking extents
leaf parent key incorrect 12454806437888
bad block 12454806437888
Errors found in extent allocation tree or chunk allocation
Syno block group cache is sync
checking free space cache
checking fs roots

able to create an nvme1 volume on the cli without updating drive list

synostgpool --create -l raid1 /dev/nvme0n1 /dev/nvme1n1

now to see if i can move my iscsi luns over... i'm not terribly hopeful but we'll see.

can't ls /volume1 after reboot even though it's mounted. Guess it's time to just remove and start over.

definitely doing multiple volumes this time around.

and volume fails to remove... ffs.

I reinstalled hyper backup to see what I can restore and it's throwing an error about insufficent privilege on the destination despite having full read/write to the bucket. Apparently it defaulted to the wrong bucket, not really synology's fault here I guess. Doesn't seem like I can select the volume to restore to.

I don't think I will ever buy another synology or recommend them after this. 

more logs of interest but probably not useful 

[Sat Aug 10 19:30:08 2024] parent transid verify failed on 9531367112704 wanted 668348275 found 664456792
[Sat Aug 10 19:30:08 2024] BTRFS error (device dm-3): BTRFS: dm-3 failed to repair parent transid verify failure on 9531367112704, mirror = 2

tring an mdadm check as one final thing before i nuke the whole syno

echo check > /sys/block/md2/md/sync_action

No difference and doesn't seem like that found anything either.

Finally got the remove to work.


...and here we are again 11/26/24. At least this time it was just the VM volume. SSDs still appear to be healthy so not sure if its a disk issue or maybe ram issue.

tried running memtest and seemed to maybe work once but now just getting connect timeouts

ok memtest results pulled out of /var/log/messages and failure

Memtest failed! err=6


this "starts" the unmount but doesn't actually do it

sudo synostgvolume --unmount -p volume2

wth is using postgres on volume2?

swapped memory with 16gb stick from my old framework laptop, memtest seems to be taking a loooooot longer so I'm hoping that means its working.

stopped office and photos and bunch of stuff then stopped pgsql

not sure how it got to volume2 but moved the postgres to volume1, updated symlink https://www.reddit.com/r/synology/comments/14t0c37/move_postgres_db_to_another_volume/


stopped s2s and synologand

umount finally worked

btrfs check cleared space whatever

then repair

```
Well this shouldn't happen, extent record overlaps but is metadata? [70733758464, 16384]
Aborted (core dumped)
```

