# Lemmy #

Looks like a few people have gotten lemmy going on kubernetes. https://github.com/LemmyNet/lemmy/issues/2969. They all seem to run an nginx pod and I'm really curious if that could just be done as an ingress, but maybe that's the wrong thing to worry about.

Ultimately, I want to create a bot that will scrape posts and create spotify playlists.

starting out with this helm chart, https://gitlab.com/ananace/charts/-/tree/master/charts/lemmy but had to modify it somewhat.

of course getting db auth errors, this is getting old. Got the db sorted by setting the password

however now having trouble logging in as admin user. It did auto-generate the password so maybe something borked there too?

Setting the admin password in the helm values seemed to work. Searching federated stuff will return nothing initially, kind of wish it gave some sort of indication of that. I don't seem to be able to grab communities from kbin.social though.

The drama around reddit mostly blew over, so I basically abandoned lemmy. If I was still back in college I probably would have loved it and stuck with it, but now I just want some memes and music recommendations.