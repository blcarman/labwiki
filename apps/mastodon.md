It's time to ditch reddit. Lemmy and kbin are still a little too new to decide which to use. So brings me back to mastodon. Of course I'm not the first person to do this and there are a ton of guides already written. But most of them seem to assume you already know some stuff about mastodon. 

So here are my notes that will attempt to fill in gaps while inevitably leaving others as I get familiar with things.

Of course I want to deploy it on kubernetes because any other platform is just silly. I realize for others they might not have k8s set up, but for me it's a must. I already have cert-manager configured, kubegres, dns, and so on. 

I found the [helm chart](https://github.com/mastodon/chart). And of course they do the usual, "just fill in your values" and have some hints. But then they have a bunch of secrets without any instructions on what they should be set to. I tried to let it auto-generate most of the secrets, but there are some it refused to. Of course I didn't keep that error message but maybe I'll track it down for historical purposes.

```bash
brad  ~  external_src  chart  helm install --namespace default testmastodon ./ -f values.yaml
Error: INSTALLATION FAILED: execution error at (mastodon/templates/secrets.yaml:30:24): vapid.private_key is required
```

Side tangent, of course I need a domain, but I always feel too much pressure to have a cleverly named one. 

Next hurdle was email, I've been avoiding setting up my own email anything for so long. I just need to send emails but it ended up being its own mess. 

So it's not completely undocumented, I guess. For example, the mastodon docs say

```
VAPID_PRIVATE_KEY
Generate with rake mastodon:webpush:generate_vapid_key. Changing it will break push notifications.
```

I should have realized I could probably just grab the mastodon image and run that command with it. Let's try that now. Hmmm

```bash
podman run ghcr.io/mastodon/mastodon rake mastodon:webpush:generate_vapid_key
WARN[0000] Error validating CNI config file /var/home/brad/.config/cni/net.d/87-podman.conflist: [failed to find plugin "bridge" in path [/usr/local/libexec/cni /usr/libexec/cni /usr/local/lib/cni /usr/lib/cni /opt/cni/bin] failed to find plugin "portmap" in path [/usr/local/libexec/cni /usr/libexec/cni /usr/local/lib/cni /usr/lib/cni /opt/cni/bin] failed to find plugin "firewall" in path [/usr/local/libexec/cni /usr/libexec/cni /usr/local/lib/cni /usr/lib/cni /opt/cni/bin] failed to find plugin "tuning" in path [/usr/local/libexec/cni /usr/libexec/cni /usr/local/lib/cni /usr/lib/cni /opt/cni/bin]]
Trying to pull ghcr.io/mastodon/mastodon:latest...
Getting image source signatures
Copying blob 6a1e857febe4 done
Copying blob b25f1bb722e1 done
Copying blob b76e155def5b done
Copying blob 025c56f98b67 done
Copying blob 64161be65588 done
Copying blob d59a1322f524 done
Copying blob c0458fc87e8f done
Copying blob 5828cbbabc36 done
Copying blob d857205084a9 done
Copying blob 4f4fb700ef54 skipped: already exists
Copying blob e603ef4163fa done
Copying config 335cd83c51 done
Writing manifest to image destination
Storing signatures
rake aborted!
Gem::LoadError: You have already activated rake 13.0.3, but your Gemfile requires rake 13.0.6. Prepending `bundle exec` to your command may solve this.
<internal:/opt/ruby/lib/ruby/3.0.0/rubygems/core_ext/kernel_require.rb>:85:in `require'
<internal:/opt/ruby/lib/ruby/3.0.0/rubygems/core_ext/kernel_require.rb>:85:in `require'
/opt/mastodon/config/boot.rb:8:in `<top (required)>'
/opt/mastodon/config/application.rb:1:in `require_relative'
/opt/mastodon/config/application.rb:1:in `<top (required)>'
<internal:/opt/ruby/lib/ruby/3.0.0/rubygems/core_ext/kernel_require.rb>:85:in `require'
<internal:/opt/ruby/lib/ruby/3.0.0/rubygems/core_ext/kernel_require.rb>:85:in `require'
/opt/mastodon/Rakefile:4:in `<top (required)>'
(See full trace by running task with --trace)
```

Doing what it says and prepending `bundle exec` appears to work.

```bash
brad@titanium [brad@titanium]~% podman run ghcr.io/mastodon/mastodon bundle exec rake mastodon:webpush:generate_vapid_key
WARN[0000] Error validating CNI config file /var/home/brad/.config/cni/net.d/87-podman.conflist: [failed to find plugin "bridge" in path [/usr/local/libexec/cni /usr/libexec/cni /usr/local/lib/cni /usr/lib/cni /opt/cni/bin] failed to find plugin "portmap" in path [/usr/local/libexec/cni /usr/libexec/cni /usr/local/lib/cni /usr/lib/cni /opt/cni/bin] failed to find plugin "firewall" in path [/usr/local/libexec/cni /usr/libexec/cni /usr/local/lib/cni /usr/lib/cni /opt/cni/bin] failed to find plugin "tuning" in path [/usr/local/libexec/cni /usr/libexec/cni /usr/local/lib/cni /usr/lib/cni /opt/cni/bin]]
VAPID_PRIVATE_KEY=blehbleh
VAPID_PUBLIC_KEY=blahblahblah
```

I've seen some people claim they run their mastodon on a raspberry pi, and I'm sure it will calm down a bit, but this is a lot of containers. And causing some issues, will leave it run overnight to see if it eventually sorts itself out.

Ultimately, might be better off running it outside the synology. 

This is probably not good...

```bash
kube-system                 kube-scheduler-rke1.lab.bradlab.tech                    0/1     CrashLoopBackOff       34 (2m35s ago)   200d
```

...20 hours later...

bunch of containers still in error/crashloopbackoff, we'll start with the postgres one since you can't do much without a db

```bash
mkdir: cannot create directory ‘/bitnami/postgresql/data’: Permission denied
```

I love bitnami stuff, but I think I should have let kubegres deploy this postgres instance.

helm uninstall did not remove the pvc's which might be causing some redis errors

```
 brad  ~  k logs testmastodon-redis-master-0
1:C 01 Jul 2023 02:10:46.527 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
1:C 01 Jul 2023 02:10:46.527 # Redis version=6.2.7, bits=64, commit=00000000, modified=0, pid=1, just started
1:C 01 Jul 2023 02:10:46.527 # Configuration loaded
1:M 01 Jul 2023 02:10:46.528 * monotonic clock: POSIX clock_gettime
1:M 01 Jul 2023 02:10:46.548 # Can't open the append-only file: Permission denied
```

looks like java is just eating cpu like nothing... i think that must be elasticsearch which is kind of ironic considering there's not any data to even search yet. Doing a helm uninstall again, this time deleting the redis pv's as well and disabling elasticsearch for the moment.
 
That did not fix the redis permission errors.

Getting permission denied errors on the "assets-precompile" job/pod as well

```bash
Errno::EACCES: Permission denied @ dir_s_mkdir - /opt/mastodon/public/assets/doorkeeper
```

So now the question is, is my rke2 cluster badly configured or is the helm chart? Probably a bit of both, but I don't have issues with things like kubegres. Maybe I will try deploying on talos, but I think I have a few things to work out there first.

I think I found the source of my permissions issues. Synology CSI was reporting it could do security context stuff that it wasn't actually doing. https://github.com/SynologyOpenSource/synology-csi/issues/30

new issue, storage is getting provisioned but not attached/mounted

E0704 03:32:38.259895 1 reflector.go:156] k8s.io/client-go/informers/factory.go:135: Failed to list *v1beta1.VolumeAttachment: the server could not find the requested resource 

found this github issue, followed it but bumped csi-attacher to latest version 4.3.0 instead of referenced 2.3.0 https://github.com/SynologyOpenSource/synology-csi/issues/18

oops had to change the registry too registry.k8s.io/sig-storage/csi-attacher:v4.3.0

And now, I think it's looking better, redis finally started successfully.

All the pods start but the sidekiq and mastodon-web keep restarting, so uninstalled again until I can look more. Might have to try the embedded db as I'm guessing the external one is not getting initialized.

Set redis to "standalone", re-enabled the included postgres. I think we're at the same state as yesterday now, sidekiq and web containers start but then fail pretty quickly. Seems to be db related, but these logs are complete shit.

```log
.137864535Z from /opt/mastodon/vendor/bundle/ruby/3.0.0/gems/activerecord-6.1.7/lib/active_record/attributes.rb:250:in `load_schema!' 2023-07-04T21:53:20.137868035Z from /opt/mastodon/vendor/bundle/ruby/3.0.0/gems/activerecord-6.1.7/lib/active_record/model_schema.rb:539:in `block in load_schema' 2023-07-04T21:53:20.137871415Z from /opt/mastodon/vendor/bundle/ruby/3.0.0/gems/activerecord-6.1.7/lib/active_record/model_schema.rb:536:in `synchronize' 2023-07-04T21:53:20.137874775Z from /opt/mastodon/vendor/bundle/ruby/3.0.0/gems/activerecord-6.1.7/lib/active_record/model_schema.rb:536:in `load_schema' 2023-07-04T21:53:20.137954775Z from /opt/mastodon/vendor/bundle/ruby/3.0.0/gems/activerecord-6.1.7/lib/active_record/model_schema.rb:402:in `attribute_types' 2023-07-04T21:53:20.138046095Z from /opt/mastodon/vendor/bundle/ruby/3.0.0/gems/activerecord-6.1.7/lib/active_record/model_schema.rb:428:in `type_for_attribute' 2023-07-04T21:53:20.138067635Z from /opt/mastodon/vendor/bundle/ruby/3.0.0/gems/activerecord-6.1.7/lib/active_record/type_caster/map.rb:16:in `type_for_attribute' 2023-07-04T21:53:20.138071455Z from /opt/mastodon/vendor/bundle/ruby/3.0.0/gems/activerecord-6.1.7/lib/arel/table.rb:108:in `type_for_attribute' 2023-07-04T21:53:20.138074805Z from /opt/mastodon/vendor/bundle/ruby/3.0.0/gems/activerecord-6.1.7/lib/active_record/table_metadata.rb:18:in `type' 2023-07-04T21:53:20.138078155Z from /opt/mastodon/vendor/bundle/ruby/3.0.0/gems/activerecord-6.1.7/lib/active_record/relation/predicate_builder.rb:63:in `build' 2023-07-04T21:53:20.138081565Z from /opt/mastodon/vendor/bundle/ruby/3.0.0/gems/activerecord-6.1.7/lib/active_record/relation/predicate_builder.rb:58:in `[]' 2023-07-04T21:53:20.138085185Z from /opt/mastodon/vendor/bundle/ruby/3.0.0/gems/activerecord-6.1.7/lib/active_record/relation/predicate_builder.rb:131:in `block in expand_from_hash' 2023-07-04T21:53:20.138088555Z from /opt/mastodon/vendor/bundle/ruby/3.0.0/gems/activerecord-6.1.7/lib/active_record/relation/predicate_builder.rb:84:in
```

yeah postgres auth failures

```
2023-07-04T21:55:41.469692971Z 2023-07-04 21:55:41.469 GMT [1576] FATAL: password authentication failed for user "postgres" 2023-07-04T21:55:41.469739531Z 2023-07-04 21:55:41.469 GMT [1576] DETAIL: C
onnection matched pg_hba.conf line 1: "host all all 0.0.0.0/0 md5" 2023-07-04T21:55:49.975504664Z 2023-07-04 21:55:49.975 GMT [1591] FATAL: password authentication failed for user "postgres" 2023-07-04T21:55:49.975569873Z 2023-07-04 21:55:49.975 GMT [1591] DETAIL: Connection matched pg_hba.conf line 1: "host all all 0.0.0.0/0 md5"
```

I think having the username set to postgres was messing it up, here we go again...


```log
2023-07-04 22:06:16.160 GMT [106] DETAIL:  Role "masuser" does not exist.
        Connection matched pg_hba.conf line 1: "host     all             all             0.0.0.0/0               md5"
```

so much for that idea, this is really starting to feel like it's not worth the trouble. So obviously this wasn't about learning stuff, or at least that wasn't the primary reason, but it's just always kind of the same thing. I didn't learn anything new just adjusted config until it worked.

And on that note I joined mastodon.social. They have a patreon so I can throw $1/month at them.