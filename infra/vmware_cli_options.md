## govc/govmomi ##

After finding govc, powercli is as good as dead to me. govc isn't quite complete but all the every day stuff is there and its way easier to get along with than powercli. In fact, I might cut off my usage of the webui just to see where it's lacking and to hopefully make scripting stuff more natural.

One slightly annoying thing in govc is that they aren't consistent with subcommands, i.e there is folder.create and import.ovf. Maybe there is a reason but I think it would be nice if they always did verb.noun.

## Pyvmomi ##

Pyvmomi is great when doing python scripts, but kind of like powercli it might disappear if I take some time to learn go instead. 

## PowerCLI on Linux ##

Yes, you read that correctly. I had recently been thinking what insanity should I try next, as if driving myself insane with the Openshift cluster commands wasn't enough. Just kidding, I really was just thinking that the vcenter gui is a bit of the wrong kind of lazy. I also love the pyvmomi stuff, but python doesn't really work as a shell (at least not that I've found yet). So a quick google search turns up that PowerCLI is now available on linux and to make it even better its officially supported. 

First we need [powershell](https://docs.microsoft.com/en-us/powershell/scripting/setup/installing-powershell-core-on-linux?view=powershell-6#fedora), even crazier MS actually provides a yum repo like they seriously want people to use powershell on linux.

Then we just install PowerCLI from the Powershell gallery thing with `Install-Module -Name VMware.PowerCLI -Scope CurrentUser`. Well, technically speaking you have to run `pwsh` first. 

We're almost ready, but there's one more thing we need to do. Disable certificate checking, since we all know you aren't using a valid cert on your vcenter server.

`Set-PowerCLIConfiguration -InvalidCertificateAction ignore`

Now, you have all the PowerCLI awesomeness you could ever dream of.

Add a new disk:
`New-HardDisk -VM $VM -StorageFormat Thin -CapacityGB 100 -Datastore newISOs -Confirm`

get VMs and their resource pools: `get-vm | Format-Table -Property Name,ResourcePool -AutoSize`

get VMs in a resoure pool `Get-ResourcePool -Name infra | get-vm`

Creating a new VM is a little clunky and it doesn't want to let me specify a portgroup

From the examples:
```
 C:\PS>$myResourcePool = Get-ResourcePool -Name MyResourcePool1
    $myTemplate = Get-Template -Name WindowsTemplate
    $mySpecification = Get-OSCustomizationSpec -Name WindowsSpec
    New-VM -Name MyVM2 -Template $myTemplate -ResourcePool $myResourcePool -OSCustomizationSpec $mySpecification

    Creates a virtual machine from the specified template and applies the specified customization specification.
```

Also if using KDE's konsole you might need to adjust the color scheme a bit. Set the "intense color" for "color 1" to something different from the background. I chose a nice light blue that seemed to go well with the solarized dark, I may also need to adjust others as the variables still seem a little dark. Also change intense color 3 to something lighter, it corresponds to the variables.