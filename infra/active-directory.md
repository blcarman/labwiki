Here are the basics for setting up a domain. The goal is to provide a concise document of non-default "defaults", in other words the annoying things that should be part of the standard configuration but are conveniently left out. 

### Creating the Domain ###

```
Import-Module ADDSDeployment
Install-ADDSForest `
-CreateDnsDelegation:$false `
-DatabasePath "C:\Windows\NTDS" `
-DomainMode "WinThreshold" `
-DomainName "lab.example.com" `
-DomainNetbiosName "LAB" `
-ForestMode "WinThreshold" `
-InstallDns:$true `
-LogPath "C:\Windows\NTDS" `
-NoRebootOnCompletion:$false `
-SysvolPath "C:\Windows\SYSVOL" `
-Force:$true
```

### NTP Configuration ###

ensure the domain controllers can get accurate time and are set to act as reliable sources for the rest of the domain.

### FSMO Roles ###

https://techcommunity.microsoft.com/t5/ITOps-Talk-Blog/How-to-Migrate-Windows-Server-2008-R2-FSMO-roles-to-Windows/ba-p/538377

### Demoting a DC ###

Even though the FSMO roles transferred fine ad01 is refusing to demote since it thinks it can't transfer some piece. So I'm tired of dealing with it and the whole domain is going to be destroyed. I'll set up freeipa if I really want LDAP in the future.