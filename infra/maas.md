## MaaS ##

Canonical/Ubuntu's version of the Foreman. It seems to only handle getting a machine to a base install. Also seems a bit clunky, nothing really earth shattering. Doesn't really seem to bring a whole lot to the table, but is fairly lightweight compared to ManageIQ and Foreman. It's actually a little reminisicent of old school spacewalk. 

## Matchbox ##

https://github.com/poseidon/matchbox seems closer to what I want but appears too limited to CoreOS. It appears dnsmasq can also do tftp, perhaps something custom could be built fairly easily, https://github.com/poseidon/dnsmasq.