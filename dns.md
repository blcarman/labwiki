## A Brief History ##

I've hopped around a fair number of dns solutions in the lab: bind, freeipa, AD, infoblox, and pihole. After a friend showed me pihole I've gotten kind of spoiled. But it's also been kind of annoying that I have to run an extra dns server to do the blacklisting. And now I have the edgerouter which has an unofficial dns blacklist thing available as well. 

Also with AD not being worth the trouble I need to move my local dns records somewhere else, i.e. the edgerouter. I don't really want to put unsupported stuff on the edgerouter yet so another pihole server is probably the way to go.

## Research ##

I wanted to set up the edgerouter such that it would use the pi.hole for dns and 1.1.1.1 if the pi-hole fails. At first I thought `strict-order` would accomplish this, but it would appear that only checks at start up and is essentially broken. Further reading, http://lists.thekelleys.org.uk/pipermail/dnsmasq-discuss/2009q3/003295.html.


## Brittanic thing ##

https://community.ui.com/questions/DNS-Adblocking-and-Blacklisting-dnsmasq-Configuration-Integration-Package-v1-1-7-5/eb05f1b2-5316-4a80-8221-5e8b02575da4?page=1

It's free so I can't really complain but really not impressed. It seems like I should fairly easily be able to put a script together to do better than this. Maybe it is working after all though I might need to add some more lists. I might fork the project and play around with it a bit. And here I am a ~month or 2~ year or 2 later and still using it without any issues so far so I guess its not so bad. 