03-21-2023

I've been using Fedora Kinoite on my desktop for just over a year now. Do I like it? I don't hate it, but honestly it still needs some polish. For the laptop and other thin client type of things it's pretty great. For my main machine it does protect me from myself a bit. However, I never know if I'm doing things the right way. The toolbox containers are useful and I do a lot in them, but they're still super easy to accidentally destroy and their tied to a fedora version so not easy to upgrade. I've added some packages as overlays in the rpm-ostree thing, but that seems like the worst option, especially for packages not provided by Fedora. And finally, flatpaks are cool, but things like vs code fail when they expect things that don't exist in the flatpak sandbox.

So here I am contemplating what I should do. I have a few different options:

* turn my desktop into a thin client (or just docking station the laptop), and put a dev machine in the basement that I remote into for the heavy stuff. 
* reinstall normal fedora workstation on my desktop
* dev vm running on desktop

## Hardware ##

In a truly surprising turn of events I actually haven't done much more than buying a quieter cpu fan in the past couple years. I have been getting the itch to build a mini system, but so far I've held back.

That's even harder now that I see Framework is releasing amd mainboards and they have a little standalone case from coolermaster.

It got hot this week(04-12-23) and now I'm once again wondering if maybe a thin client in the office would be the better approach. 

Part of me also wants to migrate to just my laptop, that might actually help in a few ways. For one I think it would be cool if I was able to get a 5g hotspot or maybe starlink and work from a remote campground. It also might help me get back to the habit of leaving it in my office. We watch a lot of crappy tv while I only half pay attention to it and some boring crap on my laptop. If I don't have it readily available it might push me to walk away from the tv easier.