I've been using VS Code's dev containers feature at work for a couple months now. I'm now starting to do it with home projects as well. Once I gave up on the podman part, it's been pretty nice.

I used to do a mega workspace with ALL my projects. This wasn't so bad when things were similarly focused. But now that means tons of extensions, and other dependencies I don't actually want on my host anymore. So now my python project will get a tuned python dev container, and my go project can have its own go container, and neither will complain about missind dependencies for the other.

But wait, my python project needs a database, how does that work? Oh looks like I can use docker-compose, not surprisingly but less exciting than I was hoping for.

ssh agent forwarding for git is supposed to work, but doesn't seem to be.