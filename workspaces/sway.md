My first time experimenting with a tiling WM. Documentation is basically nonexistent, I guess they expect you to use/know i3 before switching to sway. 

Opensuse has a little chameleon guy at the top where a classic menu would be but he doesn't seem to do anything if clicked on?

seems to have the same problem as tmux, some of the default keyboard shortcuts just seem insane. 

this seems like a better way to go https://github.com/kwin-scripts/kwin-tiling more similar to the material shell extension for gnome without having to actually deal with gnome