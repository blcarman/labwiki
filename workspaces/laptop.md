I got a new laptop so trying some new stuff, most notably Fedora Kinoite (KDE version of silverblue). I didn't really have any complaints with ubuntu from the last few months but it wasn't really offering anything exciting either. I also went ahead and enabled encryption finally.

```bash
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub im.riot.Riot~
toolbox create
ssh-keygen
sudo rpm-ostree install virt-install virt-manager virt-viewer
ssh-copy-id titanium
sudo flatpak override --socket=wayland org.mozilla.firefox
sudo flatpak override --env=GDK_BACKEND=wayland org.mozilla.firefox
```

Switching firefox over to wayland backend has totally made the fonts look like shit and its super crashy. Resetting the flatpak overrides didn't help and deleting ~/.var/app/org.mozilla.firefox didn't either. 

set brightness in KDE power settings, so it will hopefully persist across reboots (I like my brightness at like 10%, default is like 50%)

Need to decide if I'm going to use the kde wallet, it's once again started failing to unlock at login and I get prompted when launching programs that really have no business using it, and that haven't appeared to even save anything in the wallet.

```
flatpak install --user com.spotify.Client
flatpak install --user com.discordapp.Discord
```

had to manually set spotify icon and still not showing in the kde panel
in toolbox for installing ansible (annoyingly the ansible docs don't mention any pre-requisites besides pip):
```
sudo dnf install python-pip gcc libffi-devel
pip install ansible --user
```
mkdir ~/src
git clone ansible and ansible_private repos
passwordless sudo

had to manually set zsh at both host and in toolbox, despite ansible telling me all was ok

firefox flatpak randomly uninstalled itself, reinstalled but its still crashing... disabled secureboot and that might have fixed it, seems like some xorg thing was getting blocked maybe... just kidding it crashed again... deleted the flatpak firefox data and the .mozilla directory and did another rpm-ostree upgrade. Now firefox is working.

Only question remaining is how to get wireguard/netmaker working. I think wireguard could be configured via networkmanager, so maybe it just needs to be an external client? it looks like external clients will route everything through the netmaker server, this is likely a deal breaker as 98% of traffic is local. I might need to set up my own wireguard mesh on a few test VMs before I decide what to go with, it might end up better rolling my own for as small of a setup as I'd have.

also copied ssh key to win10new user .ssh/authorized_keys and work VM, need to set up browser with socks proxy and local windows VM

added dark reader extension to firefox

...and it turns out I didn't actually fix the firefox flatpak issue, I just accidentally started using the system firefox without realizing it until I uninstalled it. But it also seems to only affect my user, what did I do? And deleting the .mozilla folders and rebooting seems to have fixed it again...wtf... and it's broke again. giving up on firefox flatpak and I guess I'll go old school and add the rpm fusion or whatever repo to make packaged firefox work. tried rpmfusion, and separately the included openh264 stuff and both cause the plugin to crash when browsing reddit. tried firefox in a toolbox and same crashing. tried to install brave with rpm-ostree but it was failing on some keyring post install part, so I installed in toolbox and that seems to run ok, now I just need to figure out how to make a desktop icon for it.



sudo rpm-ostree install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
