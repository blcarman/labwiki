Previously I would have thrown this all together as a workstation. However, one of the problems I've had in the past is figuring out how to separate tasks on my workstation so that I can experiment without breaking any of my daily needs. And that's when I realized I should separate the hardware from the software and also segment the software. In doing this it should make it easier to maintain and experiment as needed, as well as consolidating things so I'm not duplicating things across systems.

I've realized I have roughly 2 basic activities: work and play.

### Work ###

This is by far the most heavily used and abused activity. This is where I get my work down and where things get broken. 

* deploy VMs/containers easily/quickly
* Ansible and python work area
* access to the lab

### Play ###

* music/movies
* web browsing
* banking/shopping
* occasional gaming
* writing

# Fedora 27 FTW #

Forget the other stuff, I've been on the hunt for the perfect working environment for years. And now that Fedora 27 has been released I think its time to revisit things and create the ultimate work environment to get the clunkiness out of the way and get shit done. No more of this "well I have to hop over to this VM to do that" or "oh I forgot I was making changes to git on that other computer" etc. I want a one stop shop to do anything and everything I need without causing problems if I break it. 

First and foremost, the ansible workstation role is happening. Its the best way to be able to return things to normal should I happen to break them and it will also save time when I set up my work VM the same way.

## My work VM: Holy productivity batman ##

I'm generally not a fan of "X things to do after installing linux" articles but I still end up reading them and mocking most of the things and considering one or two. With the latest fedora release I moved my work VM from xfce to kde and so decided to just install new instead of upgrading. Obviously this is what I did and by no means a recommendation, do whatever you want. At some point I plan to also move this into an ansible role, so this lays the groundwork for enabling that.

### Virtualbox set up ###

* set up VM with at least 32 MB of video memory to support 2560x1440
* I use 2 vCPU, 8 GB RAM and a 20 or 30 GB disk
* standard install of [Fedora 27 kde spin](https://spins.fedoraproject.org/kde/)
* virtualbox guest additions was supposed to be included automatically but copy/paste wasn't working even after enabling bidirectional clipboard
  * `dnf install gcc kernel-devel kernel-headers dkms make bzip2 perl`
  * run guest additions installer and boom copy/paste works (though it complained about some crap during the install)
* created virtualbox share folder on windows host and set to automount
  * add user to vboxsf group

It's only a matter of time before Oracle ruins virtualbox like they have everything else, so here's some info on VMWare Workstation and hyper-v. If I'm feeling adventurous maybe I'll compile a non-oracle version at some point.

### VMWare Workstation ###

Get it with VMUG Advantage so cost isn't really an issue. It's missing seamless mode for linux guests but otherwise is good.

### Hyper-V ###

Progress has been made with hyper-v and linux guests but its still lacking severely. I can't even begin to dream of a seamless mode as it doesn't even dynamically resize the display. There is "enhanced" session support via xrdp that gives you shared clipboard and decent graphics performance, but still just not quite there.

### Fancy KDE is fancy ###

* enable kde unstable copr for plasma 5.11.3 (no longer needed)
  * `dnf copr enable mkyral/kf5-unstable`
  * `dnf copr enable mkyral/plasma-unstable`
  * `dnf upgrade`
* let's make things a little more pretty (this is by no means exhaustive, but just makes it a little nicer without adding 3rd party themes and what not)
  * set the look and feel to breeze dark
  * set window decorations to plastik
  * set gnome application settings to Adwaita-dark for gtk2 and check box to prefer dark gtk3 theme
  * set gnome applicastion settings to use Sans Serif font
* install plasma-nm-fortisslvpn (the primary reason I wanted plasma 5.11)
* disable automatic screen locking and "screen energy saving" in Power Management since its not really needed in a VM
* add hack font
  * http://sourcefoundry.org/hack/
  * in kde add it to the personal fonts
  * adjust applications as needed (atom, konsole)
* set solarized dark theme in konsole and atom
* create konsole profile
  * set solarized color scheme
  * set unlimited scrollback
  * random note: 12 font in konsole is bigger than 14 in atom(pretty close to 16)...
* remove "software updates" from the task manager widget (it's annoying and doesn't honor the dnf config), while your at it remove the dnfdragora crap too. Dragora might be ok but I guess I just don't understand the point of a graphical package manager. 
* set time in lower right hand corner to also show date

### Just Kidding, Back to XFCE ###

In troubleshooting some issues on the desktop I noticed KDE is just spamming logs with junk. I've also found it difficult to configure kde via ansible since everything assumes you'd use the gui. My work VM is also still on Fedora 27, so I thought I'd stand up a new one using the xfce spin. I didn't want to just upgrade since I think there is some weirdness leftover from when I was using the unstable kde COPR repos. While doing this I decided to use my ansible workstation role that I've been working on. I need to add some toggles for things like the nfs filesystems and amd drivers, but otherwise it's in mostly good shape. I was bit hard by a dnf cache bug in ansible where it kept saying some dependency was obsoleted by something else. After trying to narrow down the package that was causing it in the list I realized it wasn't a package but the stupid cache bug and removed the option to update the cache for the task.

XFCE does appear to have one annoyance, when I resize my vmware workstation window it no longer properly maximizes windows. Also right clicking in tmux does weird stuff.

For samba shares need to install gvfs-smb.

### Install and configure applications ###

* install atom: download rpm from [atom.io](atom.io)
* minor but changed firefox's default search engine to duckduckgo 
* install other applications: `dnf install libreoffice vim git sshfs keepass ansible`
  * todo: fix terrible libreoffice theme (seems like its trying to use the light color but dark icon set), even better get it to just use qt natively
  * stick to the classic keepass for now. keepassxc oddly doesn't seem to be as security focused and hasn't been audited yet
* install flash for firefox
  * `dnf install http://linuxdownload.adobe.com/adobe-release/adobe-release-x86_64-1.0-1.noarch.rpm`
  * `dnf install flash-plugin`
* customize atom
  * install autocomple-ansible language-ansible linter-ansible-linting linter-ansible-syntax split-diff atom-jinja linter-ui-default markdown-writer file-types atom-python-virtualenv
  * set atom autocomplete to use tab instead of tab/enter (seriously who thought enter was a good idea for tab complete)
* tmux install and customize (see my ansible for the correct way to do stupid ssh agent forwarding)
 
### Python Virtualenvs ###

for python 3
* `python3 -m venv mypy`
* `python3 -m venv ansible25`

For python 2
* `dnf install python2-virtualenv`
* `virtualenv -p python2.7 mypy2`

### Work stuff ###

* configure anyconnect vpn
  * only complaint so far is the network configuration window does not launch in an appropriate size which hides the add/remove buttons from view
* messed with firefox bookmarks and bookmarks toolbar
  * it would be cool if I could add a custom bookmarks toolbar without having to mangle the default one
* move over ssh keys and other files from original vm
  * add `ssh-add` to ~/.profile (didn't work, added to ~/.bash_profile which did work)
* I don't really want to, but install hipchat client
  * enable super secret hipchat yum repo (thanks http://jsn-techtips.blogspot.com/2016/05/install-hipchat-4-in-fedora.html)
   ```
  [atlassian-hipchat4]
  name=Atlassian Hipchat4
  baseurl=https://atlassian.artifactoryonline.com/atlassian/hipchat-yum-client/
  enabled=1
  gpgcheck=0
  ```
* `dnf install hipchat4`

* did what I was trying to avoid, and just kind of tinkered with firefox a bit
  * briefly broke the new tab button location (seemed to be ok after restarting)
  * set theme to dark
  * set density to compact
* set up sshfs directories, since I use a vpn not sure I want to do automounting (git directories are slow with atom over sshfs, to workaround mount a dir lower than the git repo)
* bashrc and ssh configs
* firefox work bookmarks
* vs code workspaces

With this we're getting pretty close to perfect and only a few of the things we've done are really system level things that could be built into the distro. So while everyone is always talking about how polished distros are we can see that out of the box things really are set up well and its just some personal preferences and a couple packages that need to be installed for an ideal experience. And now that we have a definitive list we can slap this into ansible and save some time in the future. Also this only covers 80% of the use of my work laptop, I stil have windows running on the host for some of those pesky windows-only apps like webex and jabber. And because corporate requires it... In any case I'm trying to get away from doing development and testing on my own equipment so I need to set up things like vagrant on my windows VM to start fully utilizing it to do stuff.

### Powershell Stuff ###

http://book1.home.lab/books/lab/page/powercli-on-linux

## My work laptop: Get out of my way Windows and let me do my job ##

## Titanium: My Personal Desktop ##

Similar basis to the work VM 

```
sudo dnf install gstreamer1-libav gstreamer1-vaapi gstreamer1-plugins-{good,good-extras,ugly} -y
sudo dnf config-manager --set-enabled fedora-cisco-openh264
sudo dnf install gstreamer1-plugin-openh264 mozilla-openh264
```

### Riot IM ###

```
cd /etc/yum.repos.d/
sudo curl -O https://raw.githubusercontent.com/taw00/riot-rpm/master/riot-messaging-client.fedora.repo
sudo dnf install -y riot --refresh
```

from [this guy](https://github.com/taw00/riot-rpm)

### Cruft Removal ###

PackageKit can fuck off since it just lies and the little kde widget doesn't have a never check option thingy.

`dnf remove plasma-pk* PackageKit`

### Text Editors ###

I had largely settled on Atom but its been flaky lately so the search has started again. Here's an April 2018 rundown of things. 

#### Atom ####

I started using atom in 2017ish and initially loved it. However, lately its been kind of losing its charm. At some point the ansible autocomplete and linting stopped working. And on my desktop it doesn't correclty open the previous project. It's also lacking in awareness outside itself; that is it doesnt know about thinks like a python venv or an ansible config in a non-standard directory. So its time to fix it or find something new. The other downside to atom is the ridiculously poor sshfs performance. 

I finally looked into the linting problem and it turns out I needed to install linter-ui-default, so atom might be back in the running. I also needed to install the file-types plugin and associate the ansible types. During this it became apparent I had originally done more to configure atom than I initially realized. 

linter-write-good may or may not be helpful. It didn't seem to be working until I tried with one of the examples. 

So after spending some time configuring plugins it would appear atom is back on top, though with the same amount of effort VS Code will likely be a contendor. Really the only problem I have with atom is the poor sshfs performance, but I think that might be resolvable by disabling some of the git bits. Indeed, if mounting the sshfs directory in a sub-directory of the git repo atom performs much better, confirming the performance issue is with whatever atom is doing with git.

Atom loses some points for a weird bug where the background has small square around text that is a shade off from the real background color. Still not completely fixed as of 1.32.1.

#### VS Code ####

VS Code almost beats atom, but at the end of the day it does some annoying things that make it intolerable. Namely, the way it highlights words and the weird vertical line thing. Also the code summary thing on the right is just plain useless. I bet these can all be adjusted but I would like to not have to change every setting from the defaults just to get something usable.

Code might be edging further ahead. It seems to handle json templates that have everything quoted better than atom. It also does not seem to be affected by the weird atom background bug. Not sure when it started (noticed it 11/4/18) but tabbing isn't working right and it's not auto-closing quotes like it should be. It might be because of my extensions but even setting closing quotes to always and tab to 2 spaces it's not doing it. Atom still hasn't gotten it's shit together on the weird background thing so I guess we'll put up with Code. Further review shows its just happening on the workstation role, so it must not being picking up something correctly in there.

Supposedly, VS Code should be able auto-detect python venvs to use as your interpreter but that didn't seem to work for me, so I had to manually add the python path as outlined in a [Fedora blog post](https://fedoramagazine.org/vscode-python-howto/)

#### Ninja IDE ####

Still in the early phases of testing but will likely move at least my python stuff over to this. Perhaps I've been expecting too much from atom. Unfortunately I don't think this will work very well for non-python stuff but we'll see. 

#### Vim ####

For the longest time I wanted to like vim because its the poweruser choice. I still use it when a gui isn't available or I just need to make a quick change but I never really drank the kool-aid. I had trouble getting basic syntax highlighting working and it just never felt right.

### Flatpak ###

Maybe its because I don't use GNOME, but getting flatpak working is difficult. The "just download and install" instructions on the website are insulting in that they don't tell you how to actually install it. I think it assumes your browser will know what to do with the file which is a big assumption. In short, as of October 2018 its not even close to being usable.

### Virtualbox ###

I was using libvirt for virtualization on the desktop but the majority of vagrant boxes are for virtualbox so I'm switching to that.

* install virtualbox repo: `wget http://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo`
* install pre-requisites: `dnf install kernel-devel kernel-headers dkms`
* install virtualbox: `dnf install VirtualBox-5.2`
* add user to vboxusers group

### Firefox DRM / Google Music ###

Not sure how I managed to not document this so many times, but in order for firefox to use html5 for google music it needs some crazy combination of things including ffmpeg. I've updated the workstation ansible role to include this from unitedrpm repo, so as long as I use the ansible everything should be good, but considering this seems to happen every time I upgrade or install I doubt it will be the last time I forget how to do it.

### Hardware ###

I should have started documenting this sooner but I've been dealing with ryzen issues for a while and replacing all the hardware expect case/psu hasn't fixed it. Today I found a comment on a random slashdot article with a new fix, adding this kernel parameter: `rcu_nocbs=0-11`. I'm putting it in place today (6/18/18) and will see if it makes a difference. To be more specific I added that to `/etc/default/grub` and ran `grub2-mkconfig -o /boot/grub2/grub.cfg`. This did not change the gpu pcie error messages so I also went ahead and added `pci=noaer` to the kernel parameters just to reduce the noise a bit. Update 6/21/18: Crashed last night, so back to the drawing board). I might look at the C6 stuff now though I know for sure neither mobo I've tried has an option for it in the bios.

#### Audio Issues ####

Fedora had some crackling audio issues. Part of the fix was to uninstall the speech-dispatcher but if I remember this also helped. I grabbed this bit from some old notes in turtl. These [articles](https://www.ghacks.net/2017/11/04/5-things-to-do-after-a-fresh-install-of-gnulinux/) are usually crap, but for some reason I still read them. I tried the pulseaudio bit but I can't tell a difference so probably not worth worrying about, I think a lot of people just want to feel superior. 

```
resample-method = speex-float-10
default-sample-format = s24le
default-sample-rate = 96000
```

After this is done, save and exit the configuration file, and then we need to restart PulseAudio:

```
pulseaudio -k
pulseaudio --start
```