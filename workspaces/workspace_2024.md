# Workspace 2024 #

I started a blog post as well, but wanted something that can be more complete. I am getting tired of switching between desktop and laptop. My desktop has been on wifi for long enough now that I don't really have concerns with network performance. I do want to upgrade the laptop to amd 7040 series but every time I'm actually in the mood to spend money it's sold out.

## Dev Containers ##

Last year I tried vs code dev containers using podman and it sucked. I then spun up an ubuntu VM running docker which did work better, but I want to revisit the dev containers to see if anything has changed.

Spoiler alert: it still sucks. 

Maybe I should give up on the flatpak version of vs code. I tried vscodium on the host, but the dev containers extension is different and the documentation was difficult to follow.

Getting closer with the host version. It's at least talking to podman but running into some permission issues.

Finally got it thanks to this medium article, https://medium.com/@guillem.riera/making-visual-studio-code-devcontainer-work-properly-on-rootless-podman-8d9ddc368b30 and setting "--security-opt=label=disable" in the runargs.

That was a bit more effort than I was expecting but hopefully it was worth it.

I did end up changing remote container settings back to using the "docker" command, so I can use it with my ubuntu machine that is using docker. 