Microsoft terminal does not handle mouse input so no tmux mouse support either...

## Current Goals ##

* script out dotfiles config to support same thing everywhere

A change in attitude. For a long time I did not believe in customizing my shell. I ssh'd around enough that it never seemed worth the trouble. That changed when I moved to a devops role where automation handled pushing things out and I really didn't do much on individual servers. These minor gains start to feel worth it, and for the most part should transfer as needed. The next big thing I'm looking at is an ergonomic keyboard, not because I'm necessarily worried about my hands, but because I think the extra keys and customizability could be useful, that is I really hate having to do things like ctrl + shift or ctrl + a for tmux. I think it would be much nicer to have single keys for this. I have thought about remapping my caps lock for this as well. I have also realized I'm a terrible typer when it comes to symbols, It's pretty clear that I spent most of my early typing writing literature rather than coding. So whether or not I get an ergo keyboard I should spend some time on my typing. 

But now that I've started using zsh I kind of want it everywhere (and mostly its just nicer handling of history). 

### Ugh windows ###

still need a minimally viable solution for work

ssh agent
vs code
port forwarding for web browser

thought alacritty might provide a common terminal experience but it doesn't run on libvirt guests with remote spice (and even doing local the performance was so bad I couldn't even log into windows)

### Everything is a git repo ###

Maybe this doesn't exactly belong here, but since I'm already over here. Basically everything that is not a secret or data needs to be in a git repo. Here are some of the reasons

* backups 
* portability
* version control
* helps ensure sensitive parts aren't hardcoded

This also brings up a problem with basically every modern desktop environment. None of them provide good APIs for modifying the config outside of a GUI. 

Firefox doesn't have a good way of installing extensions by cli, so we just use its sync feature

generate or restore ssh keys

xfce screen brightness is way too bright on the lowest setting

kubuntu's kde default theme is actually pretty nice

enable 2 finger horizontal scrolling in kde as well

ugh configured nebula manually again, need to ansibleize

noticed packer supports inspec, maybe I need to set that up on my workstation ansible as well since I continuously run into things that aren't used daily breaking without realizing it. I could write some inspec tests that run weekly or after every `ansible` and `dnf upgrade`