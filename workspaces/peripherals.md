I've been perfecting my desktop set up for ages and realize that I should write up something regarding the stuff that I connect to my computer in addition to the computer itself. 

May 2021 updates, show I've got a mostly stable setup finally. Of course I'll keep tweaking it, but it's a good set up. Perhaps the only change I have left is to move the bed out of the office and get a nice reading chair.

### Keyboard ###

I switched to an Ergodox Moonlander in January 2021. It has a huge learning curve but the customization is nice. I still need to do more with layers. The laptop keyboard is starting to get annoying since I keep expecting z to also be ctrl. I stuck with Cherry MX Browns but maybe I should try some others at some point. I do run into positioning problems with the sides being independent, I often bump one side and have trouble getting them back to where I want them. I should make a bracket to align them.

I've kicked around several Coolermaster keyboards with Cherry MX Browns, the main difference between them is no numpad, condensed, or full size. I hated the no numpad, can deal with the condensed, but still like the full size the best. They all seem to have slightly different keycaps and while they're all on browns the full size definitely seems nicer to type on. The condensed also had some tacky led backlighting that I was never a fan of. In any case I just cleaned off the full size after it had sat unused since I started working from home. Needless to say glad I finally cleaned it up and plugged it back in, though I'm sure AJ will think otherwise once she hears the clackiness. Someday I'll make the jump to buckling springs but then I'd really have to soundproof the office.

### Mouse ###

I've been moving through mice more frequently than I would like. I just had to get a CoolerMaster MM720 in May 2021 because the MM520 I bought 6 months ago started double clicking. The MM520 wasn't quite what I wanted anyway. The MM720 uses the same form as the old Xornet that I liked. I do wish the MM720 was heavier but the weightless feel seems to be the new hotness.

I tried the Madcatz R.A.T. but it was just too big and heavy and didn't really fit me well, plus it was crazy expensive and wireless so it always needed new batteries. I followed it up with a Coolermaster Xornet which has been awesome though I did break the scroll wheel on one of them. It was also fairly cheap which was nice. 

### Monitors ###

I'm currently (as of April 2017) rocking dual 27 inch 2560x1440 Viewsonic monitors. The wider screen probably wasn't worth it, as now I have a lot of poorly used real estate. I was originally eyeing the Dell 34 ultrawide and am glad I didn't go with that now. These monitors are solid and the bezels are unobtrusive even if they could be a bit smaller.

If I do it again I would probably go for a single ultrawide or dual normal ones. Also while fine for office use I would probably step up the quality a tiny bit in the future to avoid the light bleed and bezels.

### Speakers ###

I upgraded to Edifier R1280T in May 2019. I've been happy with them though I should raise them up over the monitors one of these days.

I seriously need a new set of speakers. I've had this Logitech 2.1 system for a while now and while it's not terrible it also isn't great. If I bump the audio cable going into the computer it loses its shit and if I bump the cables in general 1 or both speakers will cut out. The sub also isn't that impressive. In the big kid house I'll probably build in something a little more awesome. 

### Desk ###

Like 6 months ago (October 2019) I switched over to an Uplift standing desk (though to be honest I hardly ever use it as a standing desk). It's the 30x60 inch model with the bamboo top. If I had to do it again I probably would not have got the curved inset. I also attached a 3m keyboard tray, but cheaped out and didn't get the spacers for it so I had to cut the slider and it doesn't retract all the way under the desk. It works for also being able to write on the desk, but it made less of a difference than I expected. 

### Office Chair ###

Now that I'm WFH full time the DX Racer I got in April 2017 is slightly less optimal but it's been doing ok. I did get some rollerblade wheels for it that have been awesome on the vinyl flooring.