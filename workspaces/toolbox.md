Title: Toolbox... err Toolbx
Date: 2023-11-23
Category: workspace
Tags: Fedora, podman, containers

Fedora 39 came out recently and a simple rebase upgraded my system nicely. But my toolbox containers are still stuck on 36. I've made a few attempts in the past to make a custom toolbox image, but usually hit some speed bump and abandoned it before finishing. So here we are again, hoping maybe the project has matured enough that this will be easier. Also, with some questions.

## How do I customize a toolbox image? ##

This actually ended up pretty easy this time around. I just added on my packages to the fedora-toolbox image.

## Am I using toolbox correctly? ##

Maybe in a few places but for the most part I don't think so. But the other problem is I'm not sure the silverblue or toolbx people really know either. Really, I should probably just install the few cli things I need in the base os and use the toolbox for temporary testing only.

I had originally leaned hard into the idea that the host os should be as vanilla as possible. Everything else was either a flatpak or in the toolbox. The problem is then I couldn't even use vim. So I started loosening that definition, and now I'm stuck wondering if I even need toolbox. 

What about the tmux/exa/powerline/whatever. Git seems to be part of the base os, which again seems counterintuitive.

This used to be fun for me. Now it just feels like a chore. It's really just a bunch of package installs, so wasn't that bad once I got started. I'm just locally building the image at the moment, it's probably a little fat for putting on gitlab.

## Are there things I should move over to the base OS? ##

Moving from rdesktop in toolbox to krdc in flatpak.

## Conclusion ##

I think I've cleared up most of my complaints. I'll maintain a custom version so I can upgrade more easily. I'll spin up dedicated containers for various python/go/whatever projects.