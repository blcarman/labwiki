## Windows on Synology ##

Sticking with Windows 10 because apparently windows 11 won't run on the DS1621+

1. plain install
2. install ssh server (https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse)
```
Add-WindowsCapability -Online -Name OpenSSH.Client~~~~0.0.1.0

# Install the OpenSSH Server
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
```
3. put ssh keys in c:\Programdata\ssh\administrators_authorized_keys (make sure no .txt gets put on the end of the file). Fix permissions by disabling inheritance and removing "authenticated users" access.
4. enable rdp
5. install vpn client
