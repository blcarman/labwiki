This page will outline the current desktop configuration and general reasoning. Currently using OpenSUSE Tumbleweed with KDE Plasma but may change at some point.

### Network Configuration ###

By far my largest complaint of OpenSUSE so far is YAST and the wicked network service. It is unreliable, unpredictable and does not provide feedback. For that reason I'm moving the desktop configuration to systemd-networkd. I doubt I will ever see networkd in a production environment, but I am unlikely to see suse/wicked in one either, and networkd is distro-neutral so long as systemd is used which for better or worse is a given at this point. Until I get Ansible(or some other tool for config management) the wiki will be the place it goes. After a couple false starts networkd came together nicely. It looks like vlan 1 isn't getting tagged since its the default vlan on the switch which was probably causing the weirdness with wicked, but either way networkd is the way to go. 

```
systemctl enable systemd-networkd
systemctl disable network
systemctl disable NetworkManager
systemctl enable systemd-resolved
ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
if needed disable network manager gui applets

brad@titanium:~> ls -lh /etc/systemd/network
total 28K
-rw-r--r-- 1 root root 30 Jul 16 23:47 br1.netdev
-rw-r--r-- 1 root root 52 Jul 16 23:46 br1.network
-rw-r--r-- 1 root root 31 Jul 17 00:10 br50.netdev
-rw-r--r-- 1 root root 57 Jul 16 23:49 br50.network
-rw-r--r-- 1 root root 45 Jul 17 00:03 enp5s0.50.netdev
-rw-r--r-- 1 root root 43 Jul 16 23:43 enp5s0.50.network
-rw-r--r-- 1 root root 90 Jul 17 00:25 enp5s0.network

brad@titanium:~> cat /etc/systemd/network/*

Updated slightly

#br30.netdev
[NetDev]
Name=br30
Kind=bridge

#br50.netdev
[NetDev]
Name=br50
Kind=bridge

#br50.network
[Match]
Name=br50

[Network]
DHCP=v4

[DHCP]
UseDNS=yes

#enp5s0.50.netdev
[NetDev]
Name=vlan50
Kind=vlan

[VLAN]
Id=50

#enp5s0.30.netdev
[NetDev]
Name=vlan30
Kind=vlan

[VLAN]
Id=30

#enp5s0.30.network
[Match]
Name=vlan30

[Network]
Bridge=br30

#enp5s0.50.network
[Match]
Name=vlan50

[Network]
Bridge=br50

#enp5s0.network
[Match]
Name = enp5s0

[Link]
MTUBytes=9000

[Network]
VLAN=vlan50
VLAN=vlan30
Address=192.168.1.15/24

```

### Ideal Workstation Notes ###

The Ideal Workstation
I’ve spent more time than i care to admit configuring my various workstations and then reconfiguring them when they get upgraded or borked in an update. As such I’ve decided to write this doc to contain the steps I take when configuring and how best to streamline things in the case of rebuilding or adding an additional workstation into the mix. This will include both software and hardware components. 

Software Requirements:

* VMWare view client support
* OS: Linux (windows is not usable without basic tools such as nmap, dd, etc)
* Virtualization support: KVM or virtualbox to run a windows VM needed for the terrible windows only software
* tmux/screen (screen needed for serial connections, tmux fancier/newer version for terminal multiplexing)
* vim, iperf, ncdu
* password manager: keepass(alternative option?)
*  the usual software: chrome, firefox, libreoffice, calibre
Hardware Requirements:

* dual monitors (ability to rotate, slim bezel)
* mechanical keyboard (coolermaster cm storm tk)
* mouse: coolermaster xornet mouse that was pretty cheap beats the hell out of the office r.a.t.
* desk: http://www.roguesupplyco.com/rogue-bolt-together-desk?icid=slider
Overview

* OS: Fedora Workstation (using GNOME but mostly due to none of the choices being really great)
* KVM: windows 10 (powershell is getting better)



optional applications

* solarwinds (or other) wake on lan utility (not worth worrying about but nice to have)
* librecad (or other) basic cad program

non-ideal: chromebook, missing various keyboard keys, no vmware view app

### OpenSUSE Tumbleweed trial ###

Using kde desktop

Change hostname
Install google chrome, virt-manager, krdc(is krdc any good?)
Optional: lshw

Fucking kwallet, generate gpg key, then configure, why the fuck can’t kwallet launch kgpg?
Chrome seems to have some issues


ssh-keygen
ssh-copy-id user@destination

Virt-manager spice display performance seems better, not sure if due to version of virt-manager or something else

Install hack font -> kde font manager hung when installing “system” fonts

Enabled opengl 3.1 for compositor

suspend/resume work out of the box (add suspend button to favorites)

Firefox install noscript and stumbleupon

Install virtualization tools from system settings rather than package manager

One thing i’m not impressed with is all the subvolumes for mysql/mariadb and a bunch of others that aren’t generally installed on desktops. Why wouldn’t these be created when the program is installed instead?

Vmware view bundle installed without issue, it did say it failed on a couple of the checks but things are working

Add packman repo for handbrake, add vlc repo for libdvdcss

Also noticed there is a taboo option for packages which sets it to never install

Idrac console just worked

First real “argh” moment, gtk3 theme just decided to shit all over itself one day

Minor modifications including but not limited to: installed diamond plasma theme 

2nd argh moment: kvm permissions seem really screwy out of the box

KRDC also acting funny when minimizing from full screen, not sure if due to theme or some other update. It seemed to work for a little while but now is broken again, seeking alternatives

Also somehow a bunch of gnome crap got installed, had to remove it and block a few of the gnome packages, will continue to block stuff as needed. Half tempted to go back to gentoo or arch just to slim down the junkware that gets brought in but it’s not excessive yet…

Opensuse is slowly losing its charm, I’ve had issues with several 1-click install packages provided by opensuse, which i realize aren’t part of the repositories or tested, but what’s the point of having them if they don’t work. I might jump back to Fedora with the 24 relea


ppose I've become a bit biased toward Fedora being its closely tied to Red Hat but I couldn't imagine running Opensuse on a server. 

I'm finally abandoning opensuse and going back to arch. It was a tough choice but what it ultimately comes down to is I have enough VMs running centos and fedora that I won't lose track of those projects even with arch on my primary desktop. And arch stays clean and up to date enough to not worry about falling behind otherwise. 

### Returning to Arch ###

I don't remember when exactly I jumped ship from Arch but its probably been 2 or 3 years. Well, after trying out Fedora and Opensuse and a few others along the way, I've been less than impressed. They just miss the boat on usability and common sense. Fedora can be forgiven a little bit for trying to be fast moving without rolling, but Opensuse takes a lazy approach of "good enough for government work" and tries to shove its own poorly designed/maintained tools down your throat and its just not pleasant. So enough of a rant, welcome back arch. 

#### Installation ####

Installation went surprisingly smooth. Only hiccup I encountered was forgetting to add lvm2 to the mkinitcpio hooks. There is no longer the text-based installer, now you simply configure partitions, chroot, install and go. Overall, I think ditching the text installer was the right choice, it seemed to always take a few times to get it right whereas by running the commands yourself you're able to fix mistakes without restarting the installer. 

#### Post-Installation ####

Obviously with the base install of Arch there is some additional configuration needed. Primarily this means install a desktop environment and the various other applications you need. Really this isn't much different from other distros other than that Arch doesn't pretend to have things pre-configured. Sure other distros might drop you into a DE by default or have a livecd but really if you're going to be using the system as a daily driver you can spend the extra little bit it takes to install a DE, and in doing so you get options. Don't like a certain display manager, try one of the other half dozen available. Here is an overview of steps I took post-installation.

1  pacman -S xorg-server xfce4 xfce4-whiskermenu-plugin lightdm light-locker vim alsa-utils libreoffice-fresh openssh xfce4-goodies wget qemu libvirt virt-manager firewalld keepass filezilla nmap handbrake libdvdcss
1  systemctl enable lightdm
1  install gpu drivers, running old ati card so pacman -S xf86-video-ati
1  configure lock screen, for some reason arch wiki config did not work, had to edit the xflock4 file and add dm-tool lock to the list after gnome-screensaver. Per http://unix.stackexchange.com/questions/101806/why-doesnt-my-screen-lock-in-xfce. Still wasn't locking automatically, fiddled with config, finally changed the screen locker config in the autostart apps to usr/bin/light-locker --lock-on-suspend --lock-after-screensaver=2. Not sure if there is an easy to way to script those settings.
1  systemctl enable libvirtd and add user to libvirt group
1  configure firewalld, systemctl enable firewalld, firewall-cmd --zone=internal --add-interface=br1 --permanent,  firewall-cmd --zone=internal --add-interface=br50 --permanent
I chuckled a bit when I realized openssh wasn't installed as part of the base. Arch doesn't mess around.

I was feeling lazy/generous and installed all the xfce4-goodies metapackage, need to go through later and weed out the ones I don't want (if there are any)

#### AUR Packages ####

* VMWare view client just works
* chromium pepper flash
* pyroom actually works
### XFCE: The Only DE Worth Considering ###

I always wanted to like KDE and its power user feel and perhaps had KDE 3.5 stuck around (or the forked project hadn't been stupid and decide to do new things) I would still be using KDE. But the recent iterations have become buggy and spread thin. At first it looks really pretty but once you start using it you realize there are just too many bugs and all the features that seemed cool aren't really usable in the current state. GNOME has gone the opposite route by removing any bit of free thinking a user could have, it does make it a bit more stable and I guess expected but I don't want to use the exact same thing as every one else. I want to be able to apply small tweaks that fit me while not requiring a fork of the whole project.

So that leaves us with XFCE. For too long I put XFCE behind the others either because GNOME is the general default or because I wanted to give KDE another chance. Well, that is finally over. XFCE has become what KDE 3.5 should have been. They're moving forward but at a pace that isn't breakneck, so the code is stable but you still get new features. Thunar is still the best file manager I've found especially when you combine it with the plugins available. The whisker menu brings an update to the old school menu without being obnoxious. And they figured out how to make a post-it note app that doesn't suck. I fought with KDE for too long trying to get a notes app to work the way it should have and here XFCE has this gem hidden. Those along with the XFCE attitude of get out of the user's way and let them function makes it nearly perfect. 

#### First (and only?) Complication with XFCE ####

Screen locking and blanking is being a pain in the ass. Got it sorted, but there is a weird number of places in xfce that refer to screen locking with no real reason for them. 

#### XFCE on Fedora 29 is Crap ####

I'm seriously disappointed with XFCE this go around. It still looks like shit by default, I can't resize my VM window without it fucking with the maximum size of the windows (the desktop itself still knows about the change), the screenshot tool just fucks up my entire session. So after I get done being on call I'll likely be switching the work VM back over to KDE. 

### Software Tweaks ###

#### Vim ####

* Get rid of annoying dark blue comments: add set background=dark to .vimrc or run :set background=dark inside the current vim session to set it temporarily.
#### RDP Clients ####

I'm still trying to find a not-crappy RDP client for linux. I recently tried freerdp and couldn't even get it to connect and found the documentation lacking. I was also using the chrome remote desktop plugin, but for some reason it stopped working on Arch chromium and I don't have the patience to troubleshoot something that seems like chrome just randomly disabled.

#### NFS Mounts ####

### Standardizing the Config ###

In my recent adventures with ansible I've realized it just might be possible to create a playbook for the desktop configuration. I think at first I'll tackle it for arch, but would like to add Fedora and possibly others as well. I might also try to make one for windows. I figure the more my configuration stays the same across systems the more efficient I'll be. 

### And We're Back to Fedora ###

After attempting to join my arch box to my freeipa domain and instead allowing passwordless authentication on the cli while blocking any sort of gui access I decided to jump back to Fedora, though this time with the XFCE spin. I think this will also be an easier platform to tackle the ansible role as well since it's a bit more enterprise-friendly. It's a bit sad and I really do love Arch, but at this point I don't necessarily care about customizability as much things working in a standard way. I also decided I had gone long enough without IPA and while I could have configured arch manually it just didn't seem right. That said I still don't think Fedora 25 was particularly good compared to 23 or 24, I've had more trouble with it than my whole experience with Fedora prior to it. 

Fucking PolicyKit or Polkit now I guess, in any case either it or the xfce polkit agent isn't playing nice with IPA users. Freeipa had a doc to workaround it but it didn't seem to work and the polkit project has fucking terrible documentation if you could even call it that. I guess you could say it provides security by not letting you do fucking anything, but that seems like a piss poor policy to me. 

I got around the stupid polkit thing finally by adding my ipa user to the libvirt group which seems rather stupid. Still hitting polkit crap with the network manager gui, will probably move back over to systemd-networkd at some point perhaps.

remove fprintd because its not used

Seriously Fedora, NetworkManager is a requirement (well I was able to remove it but then when I upgraded it got dragged back in). I realize disk space doesn't really matter but I seriously hate that things can't just exist on their own and I still think its just laziness that these dependencies exist. Also worth noting after NetworkManager gets re-installed it will set itself to enabled and start immediately. 

#### The Fedora'ing Continues ####

A new position at work has led to me being able to use a Fedora VM for the fun stuff. And now I'm kind of wishing I had gotten my workstation ansible role done when I first moved back to Fedora, but oh well, we'll get it done now. I'm also beginning to rethink my past assumption that I shouldn't try to customize my shell environment much because I can't depend on it when in unfamiliar servers. I still don't want to track a bunch of crap to new servers, but I think its fair to set up some nice stuff where I'm going to be doing the actual work. So now the question is do I push that stuff out via ansible or do I set it up in git and symlink stuff as needed. I guess the real answer will likely be a bit of both. 

XFCE terminal customization: set font to hack, enable copy on selection (doesn't work with remote tmux), enable unlimited scrollback (do remote tmux sessions mess with this?)

tmux.conf setw -g mode-mouse on

created tssh function in .bashrc to start tmux when connecting via ssh, but one downside is if i detach the tmux session it terminates my ssh session

VMWware horizon client update required an old version of libffi, libffi.so.5 to be exact so I linked it to the current libffo.so.6 and hopefully it'll work.

## The Search for KDE ##

I keep reading good things about KDE and decided maybe it was time to start looking at finding a decent kde experience. I tried bluestar because it was supposed to have a unique layout that would make you wonder why it wasn't the default. Well the reason why its not the default is because it kind of sucks and besides that the theme was hideous. Add to that a bloated set of software and you have to question why they even based it on Arch in the first place. Clearly KISS was not a design consideration for these guys.

Next on the list I suppose is the Fedora beta, since I can't wait long enough for real fedora to come out. Really, what I want to do is create a linux distro that is simply an ansible role. Then it would just be a matter of customizing jinja2 config files to whatever you wanted it to be. No more package maintenance and re-inventing the wheel every time someone got pissed and wanted to do something slightly differently.

### Sabayon KDE ###

Testing Sabayon in a VM on the desktop. I'm curious if a local VM console window will perform well enough to be used as a daily driver, and thus removing the requirement for gpu passthrough. Initial impression of Sabayon shows promise. It did have an issue finding a network in vlan 50, but seems fine on 30 so might be the host at fault, to further that br30 is defined in virt manager but br50 is not, so very likely the issue is somewhere in that. It did have gnome-bluetooth installed but was easily removed, will clean out the other gnome crap when done upgrading. Unfortunately it looks like there is not an ansible module for the sabayon package manager which will either be a dealbreaker or be a potential development project.

Well, host desktop decided to have a bit of a freakout mid-update and now things are wonky, so might move this to ovirt to see if its more stable. Unfortunately the windows virt-viewer isn't working so it makes it a bit more annoying to work with. Actually decided to re-try updates from the cli to see if that would fix whatever the gui is upset about now. I suppose its better than gentoo, but its slow and reminds me of windows, just seems like they've added a lot of overhead to the package manager. I guess I am running on a spinning disk so maybe that is slowing it down a bit too. And updates crapped out at about 200/500...

### Fedora 26 KDE Spin ###

Currently playing around with this and so far it looks decent. It looks like a mostly default configuration but at the same time everything seems to be working and nothing terribly annoying up front, so I'm fairly excited.

#### Activities ####

I attempted to set up different activities for work and non-work stuff. However, I ran into issues with atom where it would launch a new instance on the same activity as the original instance which largely defeats the purpose of separating things into their own activities.

## Ansible-defined Distro ##

So many distros these days are superficial, just a particular set of applications or default appearance. Rather than wasting resources on spinning off a whole distro as something new I think a better approach would be to customize existing distros with ansible. No more overhead and people wouldn't have to rely on you for maintenance.

Well, I have the start of an ansible workstation role for Fedora. I thought it was getting pretty good but I recently used it against a new work VM and its not as great as I thought. I need to tweak the packages a bit, python venv creation failed, atom has issues, powercli had issues, the list goes on. So next up I need to up my gitlab CI game and have it really deploy on a fresh VM to ensure things are working.