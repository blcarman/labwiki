Every once in a while I start looking at e-ink tablets. It would be cool to have something for whiteboarding at work, as well making work notes digital. And mostly just make it easier to read tech books. Unfortunately none of the devices do what I want.

remarkable/supernote have no web browser and almost seem to be actively against adding them. They also don't use android so no apps there.

boox appears to be poor from a privacy/security standpoint and also seems to have some gpl compliance issues.

kindle scribe appears fairly limited as well and trying to get out of the amazon ecosystem anyway.

I really don't have much of a need for whiteboarding and if I did I think a wacom tablet might be the better way to handle that. Thinking on it more, do I not have a need or have I just been pretending that I don't? The only part of the wacom tablet I'm not sure about is that I wouldn't be able to see the drawing on the tablet like I could with the supernote. 

lenovo smart paper doesn't look good. Expensive and doesn't have good software.

Somehow I forgot I get 12 books as part of my Manning online subscription. So having a web browser won't be as critical as I originally thought.

I'm leaning toward a supernote, which at $300 doesn't sound terrible, but the stylus and folio are extra, so its actually like $450 just to have everything in white. And this is where the cheap bastard part of me kicks in and says, do we really need this? I do like paper and fountain pens. And some of my notes really shouldn't be searchable. 

I think the pinenote, https://pine64.com/product/pinenote-developer-edition/, is the dream device, but still seems to be in an alpha state and out of stock anyways.

Who am I kidding, a tablet isn't going to consolidate my notes, it's just going to add one or more notebooks. Maybe that's ok, but I don't think I should consider it a replacement to my existing journals.

If I had my journal alongside my kindle, would I be able to take notes as I read? As long as it's not in the dark. Picked up a Galen Leather cover for my notebook and kindle.

Kobo just released a couple new color devices that look interesting, particularly the Libra Colour. Apparently I've had my kindle paperwhite since 2014 and it was actually released in 2013. So might be a good excuse for an upgrade. Also worth noting the Libra is definitely not going to be a notebook replacement. That's ok, I think it will work decently well at highlighting and annotations. Sadly no direct Readwise integration either. Also while the device itself is reasonably priced the stylus does seem expensive. And I went ahead and ordered it. If nothing else this will help me get further away from Amazon.

I'm still finishing a book on my old kindle so haven't fully switched to the kobo. For reading I think it'll be great, I have been reading Scott Pilgrim and the colors are amazing. I've tried out the note taking a couple times and was not terribly impressed. I think it'll be nice for jotting short reminders or notes in books, but not much more. Maybe it will get better as I get used to it.