My storage server has had a long history and gone through several iterations over the years. I've gotten it to the point of being mostly ignorable but it's still just been a bit of a Frankenstein build. With the direction I've taken at work over the past year, I've decided to re-align the lab to match where I see myself going in the future. That is I'm getting away from low level infrastructure into the application infrastructure. So now I don't really want to tinker with the lab itself but create a solid platform on which I can experiment with various VM/container level technologies and applications. 

The storage server has been fairly stable over the last year or so, but that doesn't mean its great. Performance isn't awesome, ovirt regular sees latency hit 7 seconds, and its not necessarily utilized correctly (it was running libvirt and httpd for a while...). However, rather than limiting myself by what was done/used previously I want to take a fresh look at what I need from a storage server. 

### Requirements ###

* Media: 3 TB (currently on warehouse, need to be re-organized a bit and duplicates removed)
* VM Images: 500 GB (currently scattered on warehouse and titanium)
* User files: 100 GB (currenly on kerberized nfs VM secfs.home.lab)
* iscsi/nfs (10 Gbps network ideally)
* backups (monthly? more frequently for important/changing data)
* redundancy? (currently btrfs raid 10 for most stuff)
* test system(i.e. dummy VM to confirm changes won't break things)
* IPMI preferred
* PCIE 3.0 or M.2 support
* ECC memory

### Solution ###

Obviously there are a few different ways to do this. I could move the media and user files to a dedicated synology NAS separating it from the lab configuration completely. I could do a vsan set up for the VM images and remove the need for a dedicated storage server. The final option is to build a classic all in one NAS. This final option is the one I'm going to be pursuing.

For the most part I've always run my storage server with either a minimalist linux or openindiana install and let VMs handle the application stuff like emby and kerberized nfs. But after breaking my virtual infrastructure recently I realized that might not be the best idea or even necessary. This is where FreeNAS comes in, it should support all the storage stuff I want/need while also being able to handle the media server and user shares.

Sanity Check: Is FreeNAS really the solution? I won't simply be trading btrfs for zfs, so it's unclear if any of my actual problems will be resolved. Yes, I would gain some organization but I can fairly easily do the same thing on the existing server. I need a set of tests to run to know how things are performing. Is the 7 second latency spikes actual storage latency or something caused by nfs? Of note, the latency shows up on both the btrfs and xfs partitions so its more likely an nfs/os/network issue than something specific to btrfs.

I think I've come to my senses and am going to stay with my current configuration. I do plan on moving the VMs off the btrfs partition, but have not decided whether to completely get rid of it. I may explore snapshot-based backups for both the btrfs side as well as the VM iamges. 

##### Network Configuration #####

* Management interface? (and IPMI) on vlan 2
* iscsi via 10gb interfaces on "vlan 20"
* emby/smb on DMZ? 
* nfs via trust?
* NIC teaming for non-iscsi

##### Storage #####

* 2x 3TB, 2x 1.5TB 2x 1TB, 1x 2TB(spare?)
* 1 large zpool, datasets for media, user files, vdev? for VMDKs
* M.2 for zil (optional)

##### Emby #####

##### NFS #####

* v4?
* kerberos?

##### iSCSI #####

* 

#### Hardware ####

This is where things get a bit complicated, but I guess a homelab isn't necessarily a typical set up.

#### Maintenance Procedures ####

* Monthly updates? (or just keep it stable

### The Old Setup ###

#### Was there anything wrong with BTRFS? ####

Not necessarily. Without red hat support its unlikely I'd ever see it in a production setting. That said the same is true of zfs. 

* Is anything really gained by going to FreeNAS?

Primarily consolidation of all my storage stuff. Single server for emby, user shares, and VMDKs

* Am I actually solving a problem or just migrating it?

Making nfs not dependent on either lab or desktop. Emby will also be taken out of the lab. This of course means we're considering the warehouse to be a production server.