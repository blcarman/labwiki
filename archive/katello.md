Katello extends the functionality of Foreman essentially providing full life cycle management vs just provisioning. Along with this functionality comes complexity. There are several different systems combined into one and they don't necessarily mesh completely. Based on recent changes it looks like Katello and Foreman are at least getting closer together and may even be combined but pulp is still a bit on its own. 

# The Pros #

*  Able to manage Red Hat content (i.e. no need to buy Satellite)
*  Able to email patch reports potentially
*  Able to control patches via grouping such as dev, prod, test
*  Scale out possible through Capsules
# The Cons #

*  Tightly integrated with Puppet
*  Requires agent to be installed on client servers
*  Good documentation, but no real start to finish walkthrough
*  Ongoing integration with Foreman, i.e. foreman has "hosts" and katello has "content hosts"
*  Doesn't play well with RH Developer subscription (limits to 1 host total)
*  Editing existing host caused issues with some deployment bits that shouldn't matter after deployment, I guess i should reconcile them to allow rebuilding a broken host
*  no way to import puppet modules directly in web ui, have to do it in commandline and then import
*  katello likes to override default settings for puppet module, so if you only want to set one item you have to go and uncheck all the others which kind of ruins the efficiency
*  not sure this counts for a bug or not, but I am having issues with removing a disk from a vm created in vcenter due to some bootorder option that doesn't seem to be done correctly, need to test further before understanding the impact of this in a production setting
*  Another weird thing, when cloning a host it seems the network interfaces cannot be named the same as the original host, not sure where this limit comes from
*  Custom Local repositories does not seem well supported, per RH(https://access.redhat.com/solutions/9892) you should create a repo using the createrepo command and then sync it with Katello as you would a repo from Red Hat or elsewhere, but since we already have pulp included with Katello, it would make more sense to add functionality to create a custom pulp repo and upload packages within the gui. 
## Getting Started ##

The installation is straightforward and very similiar to foreman, well it basically is the foreman installer with a katello option. But the documentation is decidedly lacking on what to do after the installation or how to plan a deployment. 

### Its Installed, Now What? ###

1  Create an Organization and Location
1  Add compute resource (see foreman wiki for details)
1  hmm, issues with domain creation, home.lab already in use but not listed...https://access.redhat.com/solutions/1165743
1  Create a Lifecycle Environment, for example dev, test, prod
1  add provisioning and partition templates and other foreman implementation steps
1  configure tftp smart proxy, set ":enabled: https" in tftp.yml config, https://theforeman.org/manuals/1.13/index.html#4.3.9TFTP
1  yum install tftp-server? syslinux, make sure tftpboot is writable by foreman-proxy, and that installation media is avaialable
1  set katello server as the tftp proxy for the necessary subnets: https://access.redhat.com/discussions/2085933
1  Add a Red Hat subscription
### Planning your Deployment ###

### Migrating from another System ###

## Bugs ##

* when creating new host fails once for not defining content view, but then goes through on 2nd time, update not a bug, content view is required if lifecycle environment is set
* after installation still tries to pxeboot and install...
## Lab Implementation ##

I'm documenting this better since the first installation had issues with pulp and even when it works happily its a complex set up.

1. install centos 7 minimal, auto-partitioned 50 gb drive + 30 gb drive for pulp

2. disable selinux

3.

```

yum -y localinstall http://fedorapeople.org/groups/katello/releases/yum/3.3/katello/el7/x86_64/katello-repos-latest.rpm
yum -y localinstall http://yum.theforeman.org/releases/1.14/el7/x86_64/foreman-release.rpm
yum -y localinstall https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm # will install with Puppet 4
#yum -y localinstall http://yum.puppetlabs.com/puppetlabs-release-el-7.noarch.rpm # use this instead if you prefer Puppet 3
yum -y localinstall http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum -y install foreman-release-scl

```

oops accidentally used the 3.3 docs which aren't ready yet.

```

yum -y localinstall http://fedorapeople.org/groups/katello/releases/yum/3.2/katello/el7/x86_64/katello-repos-latest.rpm
yum -y localinstall http://yum.theforeman.org/releases/1.13/el7/x86_64/foreman-release.rpm
yum -y localinstall https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm # will install with Puppet 4
#yum -y localinstall http://yum.puppetlabs.com/puppetlabs-release-el-7.noarch.rpm # use this instead if you prefer Puppet 3
yum -y localinstall http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum -y install foreman-release-scl

```

And starting over again... This time I'm just going to put everything on a single giant partition just to see if things will at least work. Also did 12 GB RAM and 4 cpu. 

...centos installer decided to make /home half the disk, awesome...

foreman-installer --scenario katello --foreman-proxy-tftp

re-run due to puppet user not existing, apparently this bug is fixed for 3.3 but not applied to the 3.2 branch

and it worked

yum install foreman-ovirt foreman-vmware

survived a reboot woohoo

apparently tftp proxy wasn't configured even with the option specified, enabled in settings.d and installed syslinux-tftp

created organization and location

trimmed extra templates from org

created global parameter for ansible ssh key instead of putting in kickstart template... caused major issues with generating kickstart file ue to missing remote_execution plugin.

also installed tftp-server and enabled tftp service and chown foreman-proxy /var/lib/tftpboot

deploy mostly worked finally, but failed to set ovirt to boot the vm from disk and also left the pxeconfig for the mac

didn't test but did enable template.yml, not sure it will make a difference though

After more tweaks finally have this working, turns out just needed to create /var/lib/tftpboot/grub and /var/lib/tftpboot/grub2 and chown to foreman-proxy, seems this was part of the fail of the tftp smart proxy not getting fully set up, also not sure if the tftp service is strictly needed since it wasn't running on the old foreman server I had, but will work on that later.

created lifecycle environments, it seems there is some duplication here between a lifecycle environment and lifecycle environment path but it appears they are actually the same thing.

Now I think we're ready for some repos. Just going to do RHEL 7 for now

Log into red hat portal, go to subscriptions -> Subscription Asset Manager Organizations and create a new manifest. Attach a subscription and download the manifest. On Katello import the manifest into the red hat subscriptions section.

added just rhel 7.3 to repos and forced sync, created weekly sync plan

ssh'd in and noticed some errors from cron, had to change ownership to tomcat for a couple of candlepin logs

created a content view, added repos, and published to test

apparently should have synced 7server instead of 7.3

had to add satellite tools repo and adjust the subscription manager snippet to use auto-attach

It feels like we're almost done, but now I'm running into some weird issues where yum isn't seeing packages but I can see them in the katello repository. I'm wondering if this is related to me starting with the 7.3 repo instead of 7server. I'm downloading the debug cert to look around now and might try setting up centos respositories.

possible issues due to repos being set to download packages on demand instead of during sync

Hmm, on demand sync seemed to cause some performance issues, seeing crazy high wait times on the VM and on the host the load was up around 60, going to try moving the vm disk to fastwarehouse, also stopped the httpd service on warehouse since I believe those can be hosted from katello as well. I might need to take a serious look at storage though.

I think we might be back on track. It seems when katello slammed the cpu it also caused some issues with the host itself, or they became apparent at the same time. In any case upgraded the physical hosts and moved katello to the ssd. Now we'll see if I break things with the sync again.

Learning more and more. Had to resume and cancel a bunch of sub-tasks to restart the sync operation. I tried resuming the sub-tasks and letting them run but they didn't seem to do anything and didn't seem to fail on their own either.

Sync was successful finally, and I can actually see packages but yum install on local system still doesn't think they exist...

I think I might have figured out the problem but I'm still not sure and it still doesn't make sense when looking at the directory listing, but basically since the client was just installed its at version 7server which doesn't have any packages specifically at that level of the repo, and some of the packages that it points to are not in 7.2, so now I'm syncing 7.0 and 7.1 and we'll see if that brings in the package version we're looking for.

I'm kind of starting to wonder if katello/satellite is worth it, I guess satellite might be a little better just for the fact that you'd be able to call red hat any time it broke, but it still seems like this product is a long a way from being ready, I think its also unfortunate that there are multiple upstream projects being combined as they all do things a bit differently and sometimes having issues co-existing.

Still having one issue after another, tempted to rebuild the katello server on fedora, but not sure if it would just make matters worse.

looking through doc, i should have ran --foreman-proxy-tftp=true to enable the tftp functions... also --foreman-admin-password=Pa55word since I keep having to do this.

### New Work lab Deployment ###

* Starting over in the work lab since I have more bandwidth freedom there. Using 100 GB disk to start and not separating /var from / partition
* going with 4 cores and 12 GB RAM, might even bump the RAM a bit later on

1. Install CentOS 7 and update, set up dns record
1. yum -y install http://fedorapeople.org/groups/katello/releases/yum/3.2/katello/el7/x86_64/katello-repos-latest.rpm
1. yum -y install http://yum.theforeman.org/releases/1.13/el7/x86_64/foreman-release.rpm
1. yum -y install https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm
1. yum -y install epel-release
1. yum -y install foreman-release-scl
1. yum -y install katello foreman-ovirt foreman-vmware
1. foreman-installer --scenario katello --foreman-proxy-tftp=true --foreman-admin-password=Pa55word
1.  re-run installer due to the no puppet user bug
1.  Created organization, then location, then domain, and finally subnet
1.  Created Lifecycle environments Test and Production
1.  Created new manifest file and uploaded (note originally uploaded to default org instead of lab org)
1.  Edited admin user so default org and location are the lab ones
1.  Enabled RHEL 7 repos for "7server", satellite tools, and kickstart repo for 7.3
1.  In the Products area I updated the repos to download immediately instead of on demand
1.  Began syncing the repositories one at a time manually
1.  Created content view and activation key

    Wiki went a little wonky so I lost track of some of the steps

1.  Published content view
1.  Set up vmware compute resource, takes just the hostname without any http:// junk
1.  Set up host group
1.  configured dhcp server for pxe booting
1.  pxe boot failed, so re-synced the kickstart repo since it originally wasn't able to download a few packages, this led to a new error during the pxe boot, so now re-syncing the main 7server repo since it also had a few packages missing

Had to do some more fiddling to get the kickstart image to boot, seems there may be a bug in the 7.3 version so I dropped back to 7.2. This worked once... then i tried to do some stuff to the content views and now systems will "register" but not actually subscribe. So going to re-add 7.3 kickstart back into things, and if that doesn't work I'll move 7.2 back to its own content view like it was originally.

It would seem katello is seeing the individual developer license as good for only a single system at one time.

Since we've realized Red Hat Developer isn't going to work I started moving things over to centos. Deployment works and registration works however for some reason the centos boxes see the red hat subscription even if registered with the centos key and it isn't automatically subscribing to centos. It also isn't clearing out the default centos repos so its favoring those over the katello ones. And finally for whatever reason it doesn't want to install the puppet-agent. I suppose I'll finish removing the red hat junk and then write up some provisioning templates to take care of the others but the puppet piece just seems odd.

I screwed up a bit and removed the red hat manifest before disabling the repos, going to attempt to run /etc/cron.weekly/katello-remove-orphans manually to see if that will take care of it. Currently sitting at about 65 GB used space which should go down considerably without the red hat junk. Otherwise will re-import manifest and then attempt clean up.

Got centos mostly working but now it seems registration is kind of wonky and doesn't see the centos id on the client but it does have the repos enabled so that begs the question of could I have enabled the red hat repos even though the system didn't think it was subscribed...

Still working on getting centos hosts to remove the public repos...also did some tweaks to get puppet working since for some reason the logic was picking the wrong paths...apparently forgot to change a couple names on the templates and the general kickstart template appears to do the exact same thing as the finish template so not sure why they are separate.

And I think its finally safe to say this is ready for everyday use. I tested pushing package installs to a client and it worked. Now I'll have to dive into puppet and decide if that is the way to go or to implement ansible integration.

An interesting question has arisen, what is the best way to deploy systems that will not live on the same subnet as the katello host? I suppose it won't stress the firewall too much to send traffic through it, but it would be cool if katello could provision a VM on a dedicated network after which it would move it to the correct network. I guess one way to get around this would be to deploy the VM with 2 NICs since there is an option for provisioning in the nic settings of katello.