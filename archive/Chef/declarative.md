# Making Chef Declarative #

As I learn more Chef and begin to see its use in the real world I've found some things about it that are good and bad. Chef has the idea of environments and policy groups that you should be able to put "environment" specific attributes into, but Chef also being loosely wrapped ruby allows you to do those things directly in recipes with terrible case statements. 