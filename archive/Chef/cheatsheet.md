# Bootstrapping #

example windows bootstrap: `knife bootstrap 192.168.30.115 -o winrm -N aos1.test.bradlab.tech -x Administrator -P Pa55word -r 'role['base']' -E lab`

# Roles vs Cookbook #

One con of roles is changes are not versioned across environments. Is this really a problem though or just a made up concern? That is attributes that need to be rolled out per environment could possibly be set in the environment json.

The benefits:
* 1 PR for attribute modifications
* decouple cookbooks for easier testing
* fewer attribute locations to look at
* less duplicated code/cookbooks without any real code

Cons
* roles are meant to be static and cannot use the node object (i.e. normal chef attributes defined elsewhere). If using a monorepo style role cookbooks may be less of a pain, but with berkshelf it adds a lot of toil. 

## Editing Node Attributes ##

`knife node edit <nodename>` only works with normal attributes (the command will say it updates default/override but those changes appear to be overridden by a chef run making it pointless) https://discourse.chef.io/t/editing-nodes-with-knife-node-edit-node-name-doesnt-save/2152