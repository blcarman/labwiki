My initial opinion of chef was not good. And I still generally like ansible better. However, I am starting to realize that my initial view was poorly influenced by a bad implementation. That said I still find it a bit overly complicated in areas and the built-in resources don't always make sane decisions (especially on the windows side). The community cookbooks are generally shoddy, but the same goes for ansible.

I like to keep things simple and feel like if you really need to drop into pure ruby to do system configuration you probably did something wrong. But I'll also admit I didn't know ruby before learning chef, so maybe with that knowledge I would have found it more comfortable to use. Containers have also made custom app configuration a non-issue for the most part. I also prefer ansible jinja2 templating and loosely wrapping config files vs chef's attempt to completely mask config files behind attributes. It generally becomes clunky as you have to figure out what attribute maps to what real config item for the app.

So long story short, after nearly a year of using chef I don't completely hate it anymore.

## Community Cookbooks ##

With any community collection of things that have no real vetting process it's inevitable that some of the things will be bad. So I can't really fault chef for this, but there are a lot of bad community cookbooks. I see 2 main patterns that I consider "bad".

The first is making custom resources for things that already have standard resource. This fluentbit community cookbook, https://supermarket.chef.io/cookbooks/fluentbit, is a good example of this. It has a `fluentbit_conf` resource which simply expects an inline config. It doesn't parse attributes to build a config or anything other than mask the location of the file on the target system. So you still have to understand how to build a fluent bit configuration but won't know where it puts it without digging into the cookbook's source. How is this better than simply using a template resource?

The second bad behavior is poorly obfuscating configuration with custom resources and/or attributes.

On the flip side there are good community cookbooks that strike a nice balance between doing the heavy lifting for you while still making it reasonable to customize the usage to fit your needs. The postgresql, https://supermarket.chef.io/cookbooks/postgresql, does this well. It has custom resources for the installation but allows you to specify things like source.

## Cross Cookbook Relationships ##

There is a not uncommon pattern of tools that may have different configurations based on the type of server their installed on. For example an nginx server and a docker server will have different logs. You could configure the basic logging tool in its own coobkook and then add a recipe to configure docker specific logging in your docker cookbook, but with the separation it's easy to duplicate similar code without realizing it. I think it may be more useful to use attributes to toggle the specific things you want while keeping all the code located in the single logging cookbook.

## Failure can be good ##

Sometimes it's better for a converge to fail than do something unintentional while appearing to be successful.

## Documentation ##

Stumbling around the docs I found some stuff about chef-repo and the following command just kind of thrown at the bottom. One of the big failures I've seen in the chef docs is not actually telling you what a command does. Sure you can see it "generates" something, but is that on the server or locally? It appears these generate commands essentially just create files/directories locally and also create a git repo. Also it appears the following command does not like nfs, or more specifically does not like nfs and SELinux together. So I guess lab-chef will just have to hang out locally. 

`chef generate repo REPO_NAME`