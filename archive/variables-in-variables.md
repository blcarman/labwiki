There have been very few things I've found could not be done in ansible. There are also a few things that just can't be done well. Most of the time if ansible does not handle it well there is probably a better way to do it.

My favorite example of this is user management. Anyone in there right mind would simply set up some kind of directory server if they had to manage more than a handful of users with different permissions. But some times the real world stomps on your dreams of doing things the "right" way and you're stuck using tools that aren't necessarily right for the job. 

I've tried out 2 or 3 methods for user management with ansible and so far I've kind of hated all of them, because it either requires repeating things that shouldn't need to be or otherwise complicating things that shouldn't be. So let's take a look at what I consider a fairly standard environment. We have a set of dev, test, and prod servers and a set of dev, QA, and engineering users. The fun part generally comes when you want the dev users to have sudo privileges in dev/test but not prod. And the finally caveat is you have multiple ansible inventories, one for each dev,test, and prod, so you can't just put a list of users in the group_vars/all without duplicating it for each inventory. But that does solve a user's groups being different across each environment, so maybe the duplication is ok. 

Another option might be to create groups on the servers and only add the dev group to wheel on the dev servers. However this breaks down when you add in that not all the devs need access to all the servers.

And so my latest idea for solving the problem is to create a variable file of all users and use the inventory group_vars to define the subset of users and their level of access. If you don't want to worry about a special variable file you could always put them in the all group_vars like mentioned above, it just kind of depends on how much you hate duplication. In any case the nice thing here is that its generic enough to work in a fair number of situations and you could possibly extend the access control in a more granular fashion without things getting out of control. 

Now, in reality I would consider users to be a "common" thing that all servers would have and not something a server is doing. However, as I said above this is really something that should be done with a directory server. So for this I'm going to set up a proof of concept users role instead of polluting my common role with junk that I won't actually use.

`ansible-galaxy init users` because I'm lazy.



One of my earlier attempts, was really kind of messy


```
# - include: manage_users.yml
#   when: users is defined
#   with_items:
#     - "{{ users }}"
#   loop_control:
#     loop_var: outer_item
#   tags:
#    - user
```
manage_users.yml
```
---
- name: add or remove users with sudo access
  user:
    name: "{{ outer_item.username }}"
    groups: wheel
    shell: "{{ outer_item.shell | default(shell) }}"
    state: "{{ outer_item.status }}"
  when: outer_item.employee_role == "engineer"
- name: add or remove users without sudo access
  user:
    name: "{{ outer_item.username }}"
    shell: "{{ outer_item.shell | default(shell) }}"
    state: "{{ outer_item.status }}"
  when: outer_item.employee_role != "engineer"
- name: add ssh key to users
  authorized_key:
    user: "{{ outer_item.username }}"
    key: "{{ outer_item.key }}"
    manage_dir: yes
    state: "{{ outer_item.status }}"
  when: outer_item.status == "present"
- name: add or remove ssh key to root
  authorized_key:
    user: root
    key: "{{ outer_item.key }}"
    manage_dir: yes
    state: "{{ outer_item.status }}"
  when: outer_item.employee_role == "engineer"
  tags:
    - root
```

not quite the same, but another attempt at user management from when I was using a dedicated ansible user
```
---
 - hosts: all
   remote_user: root
   tasks:
   - name: create ansmin user
     user: 
       name: ansmin
       groups: wheel
       shell: /bin/bash
   - name: copy ssh key
     authorized_key:
        key: "{{ lookup('file', '/home/ansmin/.ssh/id_rsa.pub') }}"
        manage_dir: yes
        user: ansmin
#   - name: update sudoers file
#     lineinfile:
#        destfile: /etc/sudoers
#        line: 'ansmin    ALL=NOPASSWD: ALL'
#        state: absent
   - name: update sudoers file
     lineinfile: 
        destfile: /etc/sudoers
        line: 'ansmin    ALL = (root) NOPASSWD: ALL'
        state: present
# add step to remove ssh key from root
```