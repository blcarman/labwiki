At this point I'm pretty much sold on Ansible, however there are a few tools that seem to favor Puppet, so I figured I'd test it out and see how it compares to Ansible.

## First Impressions ##

### Pros ###

* continuous check in keeps things in desired state
* gui integration into katello/foreman
### Cons ###

* agent-based
* not as good for pushing changes across an environment (maybe there is a cli way to push updates)
* no built in modules, puppet forge has some that are supported, but does not appear to have as many as ansible
## Implementation ##