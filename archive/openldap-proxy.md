Here we go again with one of those projects that I expected to take 2 hours and instead took far longer. I have a fairly decent background in the "fancy" side of LDAP with both Active Directory and more recently FreeIPA. But recently I got brought into an interesting solution to a fairly common problem. That is using an openldap proxy to handle requests to multiple domains rather than using a domain trust. I won't get into the "why" of this solution, but mostly its to bypass the AD guys when they refuse to set up a trust while also having the added security benefit of not giving direct access to your domain controllers to everything. 


Now, 


`[root@slap1 ~]# ldapsearch -H ldap://localhost -b "dc=home,dc=lab" -x -v -D "uid=opsldap,cn=users,cn=accounts,dc=home,dc=lab" -W`


can we use the openldap proxy to add things like ssh keys to an AD schema?