### Lab Tasks ###

* syslog (https://www.digitalocean.com/community/tutorials/how-to-install-elasticsearch-logstash-and-kibana-4-on-centos-7)
* ansible/puppet/wds, wpkg/sccm (software deployment)
* CA? (windows, integrate with AD, furthermore build a trust between freeipa and AD)
* powershell DSC
* windows DFS
* linux hardening checklists and scripts to perform and verify
* http://www.mediawiki.org/wiki/Extension:Wikilog
* nmtui/nmcli automate vm template deploy
* create a script that will create a .bak for a file in the same directory (bash)
* cruft mover/archiver, script to clean up old files from home/desktop (powershell and bash)
* multicast/network testing (with iperf?)
* dns forwarding? between windows and linux (winhome.lab and home.lab)
* RADIUS server (compare windows vs linux)
* phpmyadmin (on centos)
* Citrix Xenapp
* dummynet network testing: http://info.iet.unipi.it/~luigi/dummynet/ (seems to only work with 32 bit windows despite coming with a 64 bit binary)
* lab in a script (create a script to automate deployment of a small lab environment, i.e. on a hyper-v server, deploy a domain controller, and domain member with basic configuration, such as time zone, powershell policies, and test users)
* http://www.tecmint.com/create-luns-using-lvm-in-iscsi-target/
* http://blog.delouw.ch/2015/03/14/using-ipa-to-provide-automount-maps-for-nfsv4-home-directories/
* http://backdrift.org/lacp-configure-network-bonding-linux
* Might be cool, gluster, https://www.howtoforge.com/tutorial/high-availability-storage-with-glusterfs-on-centos-7/
* Ansible playbooks: join domain, updates?
* guarantee resources or set preferences to certain processes(e.g. keep engine.home.lab running instead of letting chromium eat all the RAM)
* less a task and more a curiosity, would it be possible to have a cisco-style bash such that it would complete as long as it was complete without tabbing through, and/or how to get nice tab completion that fedora has on red hat
* define a set of tests that need to pass to consider a server to be working and script that such that a monitoring system can use it. Even better, have a change management system that can apply and roll back a change if the server does not pass the test after a change is applied.
* Standard Network Diagram, separate layer 2 and 3, (possibly add an application level)
* netdata, https://hackernoon.com/the-spectacles-of-a-web-server-log-file-90c995e9e986#.zcw49npet
* convert observium to libreNMS
* send logs to graylog (http://www.rsyslog.com/doc/v8-stable/configuration/modules/imfile.html)
* move ansible role config files to jinja2 templates in role folder
### Completed Lab Tasks ###

* blog with ghost
* Git, learn and self-host
* Monitoring, most likely via observium (http://www.observium.org/docs/install_rhel7/)
* FreeIPA server, DNS
* oVirt/KVM hypervisor
* NFS server
* pulp/foreman/katello (package management and server management) (mostly done)
* Owncloud (forked to nextcloud, deployed but then removed, didn't fit needs)