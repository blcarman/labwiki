### Ansible Smart Proxy ###

The foreman manual does not currently realize it supports ansible as a smart proxy and the documentation available is in its own ansible foreman section.

Unless otherwise noted, the following steps take place on the ansible server

1. yum install https://yum.theforeman.org/releases/1.13/el7/x86_64/foreman-release.rpm (there is reference to a specific foreman-plugins repo, but from the installed packages it looks like the foreman repo is needed as well)
1. yum install centos-release-scl (needed for ruby dependencies)
1. yum install rubygem-smart_proxy_ansible (hmm, this version might not actually be coming from the foreman repos
1. edit /etc/foreman-proxy/settings.d/ansible.yml and update with ":ansible_working_dir: '/etc/ansible'" (or with your ansible directory if its different)
1. start and enable the foreman-proxy service
1. on the foreman server, yum install tfm-rubygem-foreman_ansible
1. in the foreman gui add the ansible host as a smart proxy, http://ansible.home.lab:8000
1. in the foreman gui import the ansible roles by going to configure->ansible roles
1. add ansile to trusted_puppetmaster_hosts in Administer > Settings, Puppet tab. Note format should be "[ ansible.home.lab ]"
Foreman unable to push ansible roles, claims all the capable proxies are not responding, even though it was able to import roles.

### Templates ###

Foreman comes with a fair number of default templates, but there is room for improvement/customization

* create a standard partition table
* add ansible ssh key to authorized keys in kickstart template
* possibly remove puppet/salt kickstart items if not using them
* added timezone parameter(possibly move to ansible)
### Upkeep and Maintenance ###

Foreman is fairly simple to manage. Routine patching and timely removal of decommissioned hosts should keep things clean. There may be minor updates needed for OS updates, but for the most part foreman will simply have the new host download the latest packages from the repos. 

### ERB Syntax ###

&& lazy and operator, basically will check from left to right until one of the results is false

### FreeIPA Integration ###

1.  Install freeipa-client and ipa-admintools on foreman host
1.  join domain (ipa-client-install --mkhomedir)
1.  create foreman proxy user: foreman-prepare-realm bradmin realm-proxy
1.  enable realm smart proxy on foreman host (change principle according to domain i.e. realm-proxy@HOME.LAB)
1.  follow remaining foreman documentation
1.  reboot (because i still don't understand how all the foreman pieces integrate)
1.  in the foreman gui, infrastructure-> smart proxies, refresh the services for the foreman host (this might be all that's needed rather than a full reboot)
1.  Add Realm in Foreman
* Side note: freeipa supports host groups which might have some cool stuff to look at later, and foreman can have it automatically go to a group in freeipa.
### Caveats ###

*  Foreman does not like deleting hosts outside of it. If a VM is removed outside of foreman it needs to be disassociated from foreman
### Appendix ###

Standard Linux Configuration