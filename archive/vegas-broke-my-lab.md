I powered down my lab in preparation for a trip to Vegas. When I returned nothing came back up as expected. This is why its important to have configuration management and low system uptime. Even with the issues I encountered the lab is in a pretty good place, and several of the issues were simple things like not saving a config. To alleviate some of the issues I encountered I'm going to attempt to fully patch the lab monthly-ish.

Here's an overview of the issues, what I did to troubleshoot, and what did to prevent them from happening again.

## Network Issues ##

To start things off I powered up the storage server and then the esxi hosts. Both booted up fine, and I logged into the esxi host with my vcenter and firewall VMs and powered them on. However I couldn't hit either of those over the network after they booted. ESXi was complaining about the port group not existing so I thought it had gotten confused by the vcenter server not being powered on (which was probably accurate, but not the underlying issue). The actual issue was that I had messed up some port channel settings on my switch and didn't save the configuration (or actually finish troubleshooting) when I had seen the problem during the initial distributed portgroup set up. Once the port-channel configuration was corrected traffic started flowing as expected, I still need to do some testing to see if I can handle individual ports on the switch/host failing.

## Storage Issues ##

With vCenter now working I was ready to power on the rest of the VMs. However the VMs on the iSCSI storage were all showing inaccessible as was the datastore itself. After logging into the storage server I was able to find a number of issues that were easily fixed. The first issue was that the iSCSI port wasn't allowed in FirewallD since I had previously turned it off. With a simple `firewall-cmd --add-service=iscsi-target` and `firewall-cmd --add-service=iscsi-target --permanent` that issue was resolved. Pro tip to find out what services are available in firewalld you can run `firewall-cmd --get-services`.

With that done it was time to look at the targetd configuration. I was expecting it to not have been saved but it turned out the whole config was there except for the block device. Since I had originally hot-added the drive it picked up a different drive letter than if I had done it while the server was powered off. I probably should have used the UUID when I configured targetd but I just used the new drive letter since at this point I just wanted it to work.

So now the ESXi hosts were able to see the drive again but the datastore was still inaccessible. Rescanning the storage adapters and even rebooting a host had no effect. Finally, I tried to add a new datastore and it was able to realize there was an existing signature on the disk and import it. However, this only fixed it for the host that I did the import on and I couldn't simply run the import a second time on the other host. So I storage-vmotioned everything (except my poor EL7 template) to an nfs datastore and re-formatted the iSCSI datastore. After re-formatting and adding it as a new datastores both hosts were able to see it and the lab infrastructure was restored to full health. 

## Guacamole Issues ##

I've been trying not to just disable SELinux lately. And for the most part its fairly transparent when using pre-packaged applications. For my guacamole server I had disabled SELinux during the initial set up and never went back and corrected it. Having dealt with the nginx SELinux stuff before I thought maybe I just had to add the ports to those that nginx is allowed to proxy to with `semanage port --add --type http_port_t --proto tcp 8080`, but 8080 was already there. 

So it was time to break out the awesome `audit2why` to figure out what's actually being denied. First, find an AVC denial in your audit.log and then pipe it over to audit2why with something like `grep 1521762214.216:81 /var/log/audit/audit.log | audit2why`. This will likely give you the exact command you need to resolve the issue, in my case it was to run `setsebool -P httpd_can_network_relay 1` to allow nginx to be able to be used as a proxy. And with that change my server was fully functional again.

# For the Future #

So those are all the issues that were caused by a full shut down of the lab. They have been permanently fixed a this point, but there are some general lessons to be learned from them. The first is that I need to get better about confirming changes persist after a reboot, routine patching will be a good way to catch these. Better configuration management will also help. That is instead of just disabling a firewall on a host, I should either do it via ansible so its at least tracked or even better finish troubleshooting and resolving issues instead of moving on to other things. I would also like to get more configuration management around the vcenter side of things, to make that easier to rebuild if I need to.

In any case, the lab handled the vacation much better than some of the previous times I had to shut it down and is progressing nicely.