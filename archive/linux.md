### List Users ###

`cut -d: -f1 /etc/passwd | sort`

### Taken from Technical Notes ###

Linux
A Short Primer
Most Linux installs you’ll come across are CentOS(Red Hat) or Ubuntu based. SSh is used to remotely connect to a linux machine. Tab completion and man pages will help greatly finding commands and files you may be unfamiliar with. The linux file system differs greatly from windows. All files and folders are contained within the root or / directory. Linux is also case sensitive. 


/bin - where binaries are stored (compare to c:\program files)
/etc - configuration files (compare to appdata or programdata on windows)
/home - user specific files and configs
/opt - contains programs not installed from repositories
/sbin - system binaries
/var/log - most common location for log files


file and folder commands
cd - change directory
ls - list directory contents
rm - delete file 
touch - create empty file


http://www.thegeekstuff.com/2010/09/linux-file-system-structure/


The most common tasks you’ll be performing is modifying configuration files, patching, and troubleshooting performance/service issues. 


Linux has several commonly used text editors including vi or vim, and nano. Nano is generally easier to use while vi/vim have some additional features. 


Patching is done via the package manager. Ubuntu/Debian based systems use apt-get and Red Hat (CentOS) based distributions use yum. 


Log files can be found in /var/log. top is a very simple task manager and systemd is becoming the standard service manager. 


top 10 commands
short file system tour
logs
patching
man
text editors


http://devo.ps/blog/2013/03/06/troubleshooting-5minutes-on-a-yet-unknown-box.html
http://mylinuxbook.com/20-interesting-and-extremely-helpful-linux-command-line-tricks/
Linux on VMWare
Linux on esxi is a generally pleasant experience with only one or 2 exceptions (vmxnet nics on redhat 5 can be annoying). Here are a few tricks, some useful and some just because.
VMWare Tools
VMWare is now suggesting to use the opensource vmware tools that are oftentimes packaged with the OS, this makes updating them all the easier since it can be done as part of the guest updates instead of being initiated from vcenter or vcloud. 
yum install open-vm-tools 
Annoying warnings/messages at boot
There are 2 general warning/messages that appear when booting a linux box. Neither of them cause any problems are than the occasional inquisitive look when booting. Both are solved relatively painlessly. 
piix4_smbus 0000:00:007.3: Host SMBus controller not enabled!
This message can be resolved by blacklisting the i2c_piix4 module.
end_request i/o error dev fd0 sector 0
The message above appears even if you remove the floppy disk from the VM. You also need to disable it in the VM’s bios.
Template Creation
When creating images to be used for templates I like to to make them as clean as possible before converting them to a template. This is both to make things look clean as well as to keep the size on disk as small as possible. It may not seem like a lot but when you end up with 100 VMs from the same template things start to add up. In addition to the non-default defaults below.
yum clean all (clears package cache)
yum remove (old kernels)
history -c (remove history of all the embarrassing commands you ran)
change root password and put in template notes 
yum remove ivtv-firmware iwl*
yum install vim NetworkManager-tui
disable ipv6 (per http://blogoless.blogspot.com/2014/12/centos-7-ipv6-and-yum.html) 
vi /etc/sysctl.d/disable-ipv6.conf


net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1


Then issue sysctl -p


However you still try to contact IPv6 addresses, so the solution consists in adding this line


ip_resolve=4


to


/etc/yum.conf
Non-Default Defaults
While the OS maintainers generally have sane defaults there are some that don’t always make sense for real life. Here is a non-complete list of tweaks and optimizations that make life a little better. The below modifications assume running a minimal install of CentOS 7
Disable not-necessary services
systemctl disable kdump.service
Disable SELinux
set selinux to permissive instead of enforcing
Set I/O Scheduler
(this may not be needed) add “elevator=deadline” to kernel parameters in /etc/default/grub and run grub2-mkconfig -o /boot/grub2/grub.cfg
Filesystems and Partition Layout
Set noatime unless specifically needed


/
8-12 GB
/boot
512 MB
/home
1-10 GB
/var
4 GB (include in / and partition subdirectories if needed)
swap
1-4 GB
Determining Physical Memory Layout
dmidecode -t memory
Common Commands
sudo: super user do, basically run command as administrator(root)
ls: list contents of a directory (equivalent to DIR on windows)
fdisk: disk partitioning tool
mkfs.<fs_type>: format partition with fs_type such as vfat(fat32), ext4
apt-get: package management tool found on debian/ubuntu based systems
yum: package management for red hat and fedora based systems
man <command>: manual for a command, generally gives summary and options for the command
ssh: secure shell, remote connection tool similar to Windows RDP
bash: common shell with features such as tab completion
mount <device partition> <mount point>: mount a partition such as a usb device
umount:  unmounts a device (basically safely remove)
nano: very simple text editor
vim: powerful but annoying text editor
systemctl: systemd method of managing services (similar to services.msc)
nmap: portmapper, used to find open/closed ports on a host
ip addr: displays IP address, similar to ipconfig
ifconfig: displays network information similar to ipconfig
rm: remove (use -r option to recursively remove files in a directory), note there is no recycle bin on linux
rmdir: remove a directory 
mkdir <dir_name>: create a directory
touch <file>: create an empty file
chown: change the ownership of a file or directory
chmod: change the permissions (read,write,execute) of a file or folder
chmod +x: make a file executable
./file: run a file such as a bash script
grep: search for a string in a file
pgrep: search for a process id by the name
top (or htop): displays statistics regarding cpu and memory usage (eqivalent to task manager)
kill or pkill: kill process based on process id or name
echo "- - -" > /sys/class/scsi_host/host#/scan (scan for changes to disk)
ls -l /dev/disk/by-uuid
hostnamectl set-hostname
nmtui: network manager text based ui
dmidecode: provides hardware information from BIOS
chronyd: alternative to ntpd
Service Management (Systemd)
Systemd is quickly becoming the standard service management tool. It is intelligent in its ability to do things such as only mount nfs file systems after the network service has been started. It does add a bit of complication but overall the benefits outweigh this additional complication. 


Starting/Stopping a service:
	systemctl start <servicename>
	systemctl stop <servicename>


Enabling/Disabling a service to start at boot
	systemctl enable <servicename>
	systemctl disable <servicename>


Power management:
Systemd can also be used to manage power functions such as restarting and suspending. 
Firewalld
sort of iptables replacement/frontend
firewall-cmd --add-source=192.168.10.0/24 --zone=internal
Joining an Active Directory Domain
use cockpit gui or https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Windows_Integration_Guide/realmd-domain.html
add domain admins or other groups to sudo
%domain\ admins@domain.com ALL=(ALL) ALL
NIC Teaming
mac address? appeared to work but was using mac of individual cards and didn’t failover, now can’t get to work at all
iptables
service iptables save
BIND (DNS)
Don’t use .local, it is used by mDNS already and will cause conflicts and headaches
Postfix (sending email)
Being able to send email from a server can greatly reduce the amount of manual work on a server. Output from automated scripts/cron jobs can be sent in an email. This is commonly done using postfix and the mail command. 


1. Install mailx, cyrus-sasl*
2. Add the following to /etc/postfix.main.cf replacing the information as necessary


smtp_sasl_auth_enable = yes 
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
smtp_sasl_security_options = noanonymous 
smtp_tls_security_level = encrypt
smtp_use_tls = yes
header_size_limit = 4096000
relayhost = [smtp.sendgrid.net]:587
smtp_tls_policy_maps = hash:/etc/postfix/tls_policy


3. create /etc/postfix/sasl_passwd and put your username/passwd in it, then chown to postfix and chmod 600
[smpt.gmail.com]:587 user@gmail.com:password
4. create /etc/postfix/tls_policy and put smtp settings
[smpt.gmail.com]:587 encrypt
5. postmap /etc/postfix/sasl_passwd and postmap /etc/postfix/tls_policy
Updates/Patching
Linux handles patching and applications a bit differently from Windows. In addition to the OS each distribution also generally takes care of its own applications. This makes updating easier and also provides greater confidence that the patches will work with your system. There are several package managers but most commonly are apt-get on Debian/Ubuntu and yum on Redhat/CentOS based systems. It is also generally possible to only apply security updates in the case you want to ensure maximum stability. 


Check for updates
yum check-update
apt-get update
Install updates
yum update
apt-get upgrade


Automatic Updates


Updates can be automated using a cronjob. The following script will apply updates and send an email on what updates were applied. 


yum-check-update.sh
#!/bin/bash
#
# checks for yum updates and emails if there are any available
#
# change this to your email
#
email=youremailaddress
#
# no need to change anything below here
#
yumtmp="/tmp/yum-check-update.$$"
yum="/usr/bin/yum"


$yum check-update --security >& $yumtmp


yumstatus="$?"


hostname=$(/bin/hostname)


date=$(date)
number=$(cat $yumtmp | egrep '(.i386|.x86_64|.noarch|.src)' | wc -l)
updates=$(cat $yumtmp | egrep '(.i386|.x86_64|.noarch|.src)')
echo "
There are $number updates available on host $hostname at $date


The available updates are:
$updates
" | /bin/mail -s "UPDATE: $number updates available for $hostname" $email


rm -f /tmp/yum-check-update.*
#

```
yum-apply-update.sh


#!/bin/bash
#
# yum-apply-updates.sh
#
# installs security updates and reboots system
#
#
email=youremailaddress
#
# no need to change anything below here
#
yumtmp="/tmp/yum-check-update.$$"
yum="/usr/bin/yum"


$yum update --security -y >& $yumtmp


yumstatus="$?"


hostname=$(/bin/hostname)


date=$(date)
number=$(cat $yumtmp | egrep '(.i386|.x86_64|.noarch|.src)' | wc -l)
updates=$(cat $yumtmp | egrep '(.i386|.x86_64|.noarch|.src)')
echo "
There were $number updates installed on host $hostname at $date


The installed updates are:
$updates
" | /bin/mail -s "UPDATE: $number updates installed for $hostname" $email
```

# clean up #


`rm -f /tmp/yum-check-update.*`


#reboot system
`/sbin/shutdown -r`


spacewalk server?

### Pulp ###
Pulp touts itself as a package management tool, including repositories and what not. However, it is more of a framework than a complete tool such as spacewalk or Satellite. It requires a client installed which runs off java, which further makes this a less worthy product. 

### Directory Services (FreeIPA) ###


Adding Computer to Domain
yum install ipa-client (freeipa-client on fedora)
ipa-client-install --mkhomedir
If you forget to add the --mkhomedir option you can run authconfig --enablemkhomedir --update
Windows clients not natively supported, need to set up an AD trust


### Desktops ###

Cinnamon is meh, not really terrible but not good either

### LVM ###
```
pvcreate /dev/sdXY
vgcreate vg_name /dev/sdXY
lvcreate -l 100%FREE -n lv_name vg_name
```
Cron
Hardening

validate: http://www.cyberciti.biz/faq/linux-add-nodev-nosuid-noexec-options-to-temporary-storage-partitions/

https://wiki.archlinux.org/index.php/Security 

http://people.redhat.com/swells/scap-security-guide/RHEL/7/output/rhel7-guide.html 

### NFS ###

Kerberized nfs with freeipa https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Identity_Management_Guide/kerb-nfs.html

https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Linux_Domain_Identity_Authentication_and_Policy_Guide/kerb-nfs.html 

Link seems to be missing a step on the client, I had to create the prinicipal in freeipa gui and then retreive keytab

http://serverfault.com/questions/514118/mapping-uid-and-gid-of-local-user-to-the-mounted-nfs-share