This document will cover installing a load balanced/redundant NTP server configuration. This is primarily targeted at virtual environment where the time needs to stay in sync across local components. 

### Building the NTP Servers ###

1 Start with a standard hardened linux server
1 install ntp if not already installed
1 enable and start ntpd if not done already
1 add firewalld rules if needed firewall-cmd --permanent --add-service=ntp
1 edit /etc/ntp.conf to allow queries from trusted networks and peering from trusted hosts. Change default time servers to poll from as well if needed for compliance reasons (i.e. pull from nist instead of pool.ntp.org)
```

#allow to sync from peer
restrict 192.168.30.245 nomodify notrap

#allow others to query this server
restrict 192.168.30.0 mask 255.255.255.0 nomodify notrap nopeer

#set peer server for local redundancy
peer 192.168.30.245

```

1 restart the ntp service and confirm working