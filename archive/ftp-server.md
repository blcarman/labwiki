## Configuring a Fairly Typical FTP Server ##

1. `yum install vsftpd`
1. `firewall-cmd --add-service=ftp --permament`
1. `firewall-cmd --add-service=ftp`
1. edit /etc/vsftpd/vsftpd.conf and disable anonymous access
1. create ftp user