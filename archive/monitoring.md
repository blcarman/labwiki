I set up Observium for monitoring. It looks very promising for the lab. I'm not sure if it would scale up for a large environment and it doesn't appear to support multiple polling servers.

I should probably document the set up but it was really simple and the project itself has solid documentation, last item to address is scaling.

## LibreNMS ##

Librenms is a fork of observium. It's pretty straightforward but the docs can be lacking in places. For example the mail configuraiton is essentially ignored and expects some local relay. To get postfix working with gmail I followed a random how to but also had to install some stuff for sasl `yum install cyrus-sasl cyrus-sasl-lib cyrus-sasl-plain`