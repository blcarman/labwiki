# Mastering Docker Book Review #

There are apparently 2 different definitions of "mastering." The first is the exemplified in Sybex's mastering series in which the books are packed with so much information they could be used to fight zombies. The second are these books that actually mean "brief overview of the basics." So I wouldn't say "Mastering Docker" is a bad book, but it is poorly named. Actually I take that back, this book was really not that good. It was mostly a series of commands with explanations similar to --memory sets the memory limit. There is no coverage of best practices, how docker works, or real world scenarios.

Obviously with the way things change rapidly its difficult to write an entire book that goes in depth enough to enable someone to "master" it, but I would expect something that isn't readily available in the project's own documentation. Leave the various commandline options and what not to the project, instead write about real world use cases, common mistakes, how to put the pieces together, and how to get the most out of the application.

So long story short, this book is ok in a quick scan through, but don't expect to learn anything unique or to gain anything more than a basic working knowledge of docker.

# Openshift vs Rancher #

Rancher seems to have a strong community following but Openshift is backed by Red Hat, which is better? Should they even be compared?