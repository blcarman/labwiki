## OPNSense Firewall ##

I started this as a pre-upgrade step and realized it would probably be a good idea to set this up to just run periodically. So here we go. Now the backup API was annoying enough to figure out, the next decision/challenge is how to limit the number of copies kept. At first I figured just do it in the bash backup script, then I thought logrotate might be easier to work with since it already has all the logic, and finally I thought maybe borg would be useful here as well so I could keep monthlies for a while. Though really these backups are so small I could keep 100 or so and it wouldn't matter. Maybe I don't worry about it after all. 

What would be cooler is if I could retain a new copy only if there were changes, which a quick test shows might be possible after all. At first I was concerned that it would include some sort of timestamp that would make it always unique, but that does not seem to be the case. So we'll download the config, compare md5sums with the most recent existing copy and discard if its the same. Then I for sure shouldn't need to worry about too many copies and will even have a nice little history of work done.

```
#!/bin/bash

#API key
key=<backup_user_key>
secret=<backup_user_secret>

backup_dir="/home/borguser/firewall_backups"
firewall="<firewall_ip>"

# confirm/mkdir the backups dir
# backup config
curl -k -u "$key":"$secret" https://$firewall/api/backup/backup/download -o "$backup_dir/labfw_$(date '+%Y_%m_%d').config"

# get only the latest 2 backup files
backups=($(ls -t1 $backup_dir | head -n 2))

first_md5=($(md5sum $backup_dir/${backups[0]}))
second_md5=($(md5sum $backup_dir/${backups[1]}))

if [ $first_md5 == $second_md5 ]; then
  echo "removing latest backup since no changes"
  rm $backup_dir/${backups[0]}
fi
```

I clearly need to work on my bash scripting but I think it will get the job done. Actually looks like there is some more tweaks needed. It assumes at least 2 files exist which obviously isn't the case until its ran for at least 2 days. 

I also need to add this firewall_backups directory to the "off-site" backups.