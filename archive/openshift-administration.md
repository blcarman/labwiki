* grant user cluster admin rights: `oc create clusterrolebinding registry-controller --clusterrole=cluster-admin --user=bradmin`
* upgrade cluster: 
```
git checkout release-3.10
git pull
ansible-playbook -i inventories/lab/inventory ~/openshift-ansible/playbooks/byo/openshift-cluster/upgrades/v3_10/upgrade.yml --limit OSEv3,localhost -v

```