deployed template to oc1.home.lab

```
[OSEv3:vars]
os_firewall_use_firewalld=True
```

Openshift-ansible does not match the documentation, its looking for groups that don't exist.

`ansible-playbook -i inventories/lab/inventory ~/remote/technical/openshift_ansible/openshift-ansible/playbooks/prerequisites.yml --limit OSEv3,localhost`

pre-req playbook didn't check available memory, kind of silly

"latest" version might actually be 3.9, not 3.10, at least as far as released versions go

that moment when you realize a big opensource project has the same bad ansible practices your dev group does...

https://bugzilla.redhat.com/show_bug.cgi?id=1554031

## Attempt 2 ##

I might have messed some stuff up doing the 3.10 release before it was actually released, so re-trying with 3.9

stopped for the night at host prep storage, otherwise i'll start cutting corners, need to configure a thin pool.

overlay requires ftype=1 for xfs at /var... so I just went with devicemapper out of laziness

more info at https://linuxer.pro/2017/03/what-is-d_type-and-why-docker-overlayfs-need-it/

```
[root@oc2 ~]# docker-storage-setup 
INFO: Writing zeros to first 4MB of device /dev/sdc                                 
4+0 records in                                                                      
4+0 records out                                                                     
4194304 bytes (4.2 MB) copied, 0.0742872 s, 56.5 MB/s                               
INFO: Device node /dev/sdc1 exists.                                                 
  Physical volume "/dev/sdc1" successfully created.                                 
  Volume group "docker-vg" successfully created                                     
ERROR: XFS filesystem at /var has ftype=0, cannot use overlay backend; consider different driver or separate volume or OS reprovision 
```

set `no_proxy=.lab.bradlab.tech,home.lab,192.168.30.50,192.168.30.51,192.168.30.52,.cluster.local,.svc,localhost,127.0.0.1,172.30.0.1` in `/etc/environment`

https://github.com/openshift/openshift-ansible/issues/7808

failboating after the pre-req playbook, during the deploy playbook.

perhaps I need to add some infra nodes per the following

https://github.com/openshift/openshift-ansible/issues/2206, setting the allow scheduler thing didn't help, will try infra nodes next

## Attempt 3 ##

Here we go again. I figure its time to actually do it right. I'm downloading the new 7.5 ISO which should format xfs correctly. Then I'll also try a single master, multiple node configuration. I should probably do static IPs as well I suppose.  I might disable the memory requirements though since otherwise these are gonna be a bit heavy.

Almost got it, but forgot to put the master in the nodes group so it didn't get the sdn stuff [https://docs.okd.io/3.9/install_config/install/advanced_install.html#marking-masters-as-unschedulable-nodes](https://docs.okd.io/3.9/install_config/install/advanced_install.html#marking-masters-as-unschedulable-nodes)

Holy crap it worked. Now I'm almost afraid to do anything else with it and in any case its enough for one weekend.

## First Impressions ##

Apparently there is no password checking by default, which is just awesome.

I also messed up my wildcard dns entry by pointing to the master rather than the router nodes. So it looks like everything is working. There is definitely a lot to it, though it's unclear how much of the cli will be useful in day to day operations.

Storage is kind of weird as well. PVCs map directly to a PV, so you need storage classes and gluster/ceph/vmware to dynamically create PVs. So I have to abandon my original plan to just use nfs.

Maybe I'm missing something but there seems to be some magic missing from the Rook docs and blog posts. Nobody wants to talk about the underlying storage. Like I get that Ceph will make storage available but what storage and how? Like will a ceph pod be on every compute node that consumes it, or just a set of infra nodes like the routers?

This led me to the "Kubernetes Up and Running" book, maybe I need to get a better foundation of kubernetes in general. The book is good but definitely aimed at consuming kubernetes rather than managing it. I suppose in a way a lot of the kubernetes stuff is set up this way so you have to buy something from a provider, or be incredibly stubborn like me. A couple things I noticed with testing the commands in the book is it seems Openshift's `oc` command is a drop in replacement for `kubectl` and in fact `kubectl` is available in the Openshift cluster. Just kidding, kubectl is a link to oc. In any case I'll probably have to do more pure kubernetes if/when I plan on doing the CKA.

Another question that occurs is with as much trouble as I've had picking this stuff will the old guys be able to handle it? I really do think its a paradigm shift from the old days and that's going to be difficult for us and the dev team to handle.

There's also a question of whether Openshift and Kubernetes in general is production ready, and I don't think I can answer that question yet. But I do need to keep it in mind so I can handle it if something does go wrong.

Not a problem with openshift necessarily but gitlab requires the ability to run the container as root, which just seems stupid and lazy. They had some big discussions on it, but I feel like I must be missing something since I don't understand why they would need root to run a flipping webapp. 

### Outstanding Questions ###

* How do I add nodes to a cluster? just add to inventory and re-run deploy-cluster.yml
* What's with the regions and "infra" nodes?
* What do we use for storage backend? Ceph, gluster, vmware? gluster seems good
* permissions?
* quotas?
* heketi-cli
* monitoring

## Templates ##

Almost working, need to fix some parameters for the bookstack one, and find a less manual way.

## Registry ##

The registry works a lot better when you actually follow the documentation and use your token rather than the password you would for the webui/oc (notably since the webui doesnt care about passwords by default)

`docker login -u openshift -p $(oc whoami -t) <registry_ip>:<port>`

How to browse registry?

How to ensure base image is being pulled from private repo? Does it automatically cache it?

## Next Steps ##

I officially have bookstack running on Openshift now. Next, I'd like to move guacamole and possibly gitlab, and everything I can really. I also need to sort out monitoring and the gluster management.

## Helm ##

Pretty easy to set up following openshift blog post

However, deploying gitlab gets
```
[brad@scandium linux-amd64]$ ./helm upgrade --install gitlab gitlab/gitlab \
>   --timeout 600 \
>   --set global.hosts.domain=gitlab.apps.bradlab.tech \
>   --set global.hosts.externalIP=192.168.30.200 \
>   --set certmanager-issuer.email=email@example.com
Release "gitlab" does not exist. Installing it now.
Error: roles.rbac.authorization.k8s.io is forbidden: User "system:serviceaccount:tiller:tiller" cannot create roles.rbac.authorization.k8s.io in the namespace "test": User "system:serviceaccount:tiller:tiller" cannot create roles.rbac.authorization.k8s.io in project "test"
```

had a bunch of notes that got ate, because apparently gitlab makes bookstack cry. In any case I gave helm cluster-admin rights and gitlab now deploys but doesn't work. It's complaining about port 80 already being in use. 

```
./helm upgrade --install gitlab8 gitlab/gitlab   --timeout 600   --set global.hosts.domain=gitlab.apps.bradlab.tech   --set global.hosts.externalIP=192.168.30.200   --set certmanager-issuer.email=email@example.com \
--set gitlab.migrations.image.repository=registry.gitlab.com/gitlab-org/build/cng/gitlab-rails-ce \
--set gitlab.sidekiq.image.repository=registry.gitlab.com/gitlab-org/build/cng/gitlab-sidekiq-ce \
--set gitlab.unicorn.image.repository=registry.gitlab.com/gitlab-org/build/cng/gitlab-unicorn-ce \
--set gitlab.unicorn.workhorse.image=registry.gitlab.com/gitlab-org/build/cng/gitlab-workhorse-ce \
--set gitlab.task-runner.image.repository=registry.gitlab.com/gitlab-org/build/cng/gitlab-task-runner-ce
```

## Maintenance ##

Powered down the lab for vacation, openshift came up with no issues. Just started the manager first, then infra nodes, and finally normal nodes; but I'm not even sure the order made a difference.

## Interesting Behavior ##

Not that it's something that I would normally do in prod, I had powered down the management node for a couple weeks in between active work on the lab. I also powered down one of the compute nodes. Inevitably the bookstack pod crapped out so I powered on the manager and at first it tried to schedule it on the offline node. After several minutes it realized it was offline and re-created it elsewhere. With this in mind it might be more important to have HA on the management node to ensure other pods are recoverable.

## Other Notes ##

volumes are different from persistent volumes

## FUBAR ##

Well, openshift did not seem to like it when my nfs server had some IO latency and now it looks like we have a number of issues. Some of these may be related to attempting to upgrade to 3.10 as well. In any case the routers are no longer working, gluster seems to hate life, and re-running the deploy_cluster.yml was unsuccessful. So I think its time to rebuild the cluster and see if we can do it a bit better.

Just kidding, it turns out most of the problems were caused by dhcp. So after the storage IO hiccup and the hosts rebooted they got new addresses. So its not that the routers were broken I was just going to the wrong IP because I had statically configured dns. So after fixing that my bookstack pod was still failing, well it turns out that was caused because the IP of the node is set in `/etc/origin/node/node-config.yaml` and hadn't been updated either. So long story short don't use dhcp for openshift.