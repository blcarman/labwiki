Finally giving Titanium its own page. I've put plenty of notes about it elsewhere, but the hardware issues are finally worthy of splitting into its own page. I've been chasing a gremlin since I upgrade to ryzen (at least I'm pretty sure that's when it started). I have since replaced every part except for the case and the problem persists. 

What's going on? The main issue is idling causes crashing (doesn't reboot, just hangs with frozen screen from time of hang and no keyboard/mouse response) generally overnight but can also happen almost immediately after booting. Furthermore while testing today(11/17/18) I found if I do not login right away the keyboard misbehaves on the login screen. I'm not sure if that's a fedora bug or another symptom related to the freezes. 

I was almost certain this was a cpu hardware issue related to the early batch of ryzen 1X00 CPUs. I already swapped the cpu out once with another R5 1600 but it was also from an early batch so couldn't rule it out completely. Earlier this week I swapped out the RAM but that didn't fix the issue either. So today I ran over to Micro Center and picked up an R3 2200G and so far its not looking good. Fedora memtest86+ still fails immediately, the weird keyboard issue if I don't log in right away still exists. I'm testing out the idle after login to see if that's also a problem. I also blacklisted some potentially problematic kernel module but while it removed a warning/error in dmesg I don't think it will make a difference. I suppose I could try a B450 motherboard, but at this point I'm about ready to cut my losses and just get a docking station for the xps 13.

Trying the zenstates C6 disabling one more time. This thread seems to be the same issue, https://community.amd.com/thread/225795 but my MSI bios still doesn't have the power options mentioned. I decided to try the zenstates c6 disabling thing again, but I'm pretty sure I tried that before with no luck.

So that is the long short version of where titanium is today. I suppose we'll see if the 2200g really does lock up overnight before I start planning the next steps.

You might be asking why I don't just shut down titanium at night or when its not in use. Basically the lab is running out of memory and in theory titanium would be more efficient than the old server hardware. Ideally I could move some of the load from the servers to titanium and save some power. I would also like to upgrade the storage server which was how I justified buying new parts in the first place. Once I identifed the bad piece of hardware I could replace it and use all the extra bits for the new warehouse.

It occurs to me that since swapping RAM I've assumed the freezing still happens since Fedora's version of memtest86+ crashes, but perhaps there's just a bug in it. Though I suppose that doesn't explain the weird keyboard issue if I don't login right away.

### Cleaning Up Logs/Errors ###

This gremlin chasing has led me to start looking into things. I was hoping to let the new CPU bake for a week before I really start messing with anything else, but I can't help it. So I thought I would configure log insight and maybe catch some logs there that might not get saved to disk when it does crash. While checking if the service was installed I noticed mcelog and kdump are both failed. I believe kdump is failing because I decreased the RAM in the system, but it looks like mcelog is just not exiting correctly because AMD uses a different method. So since the devs haven't fixed it since 2014 I went ahead and disabled and masked the unit.

```
systemctl disable mcelog.service
systemctl mask mcelog.service
```

I installed Log insight and added Fedora to the OS's for the lab agent group. It should collect the usual suspects and everything in the journal.

#### KDE Logs ####

Good grief, KDE spams `/var/log/messages` incessantly. I looked up a few and for the most part it seems to be "uh the underlying stuff is weird so something needs to be done" and then nothing is ever done.

### AMD 2200G Review ###

So far decent performance for every day use however ripping a dvd seemed to take forever. Also haven't tested integrated graphics or power usage, but will do so before I swap it out.

### July 2019 Update ###

Finally think I have titanium stable. Upgraded the motherboard to a gigabyte B450 Aorus Pro and disabled global C-states in the bios(without that it crashed the first night). Also not sure if it's a coincidence but the AER pcie errors are still a thing but possibly only appeared after enabling virtualization in the bios. I did some digging and looked at `sudo lscpi -vvvv` and noticed it was downgrading the pcie slot to 2.5GT/s (i.e. pcie 1.0) and then at times also downgrading the width (from x8 to x0), but it looks like that was just power saving stuff and not an actual issue. Once I stressed the gpu a bit it picked up, and so I'm wondering if those AER errors are just remnants from some sort of power throttling. Along with the mobo upgrade I increased the RAM to 64 GB, I was able to get a matching Mushkin kit for not too bad ($135) so I jumped on it. Now that it's stable and I have the edgerouter I think I can start moving things off the lab servers to save some power. My goal is to be leave the warehouse up, but power one or both of the lab servers down unless actively needed.