This page will contain various hardening procedures for linux servers. There will be multiple categories based on level of paranoia. The goal will be to present a scriptable list that both explains why the option is set and that can be updated as needed. 

We're assuming a minimal install with separate partitions for /, /boot, /home, /var, swap, and any other system specific partitions as needed. (link to basic install doc at some point)

### Basic Security (applies to all servers) ###

#### Harden ssh access ####

disable root ssh login (make sure other admin user exists firstdisable root ssh login (make sure other admin user exists first

```

useradd -m -G wheel cloudmin
echo -e "Pa55word\nPa55word" | passwd --stdin cloudmin
echo "PermitRootLogin no" >> /etc/ssh/sshd_config
systemctl reload sshd

```

Only allow secure encryption algorithms

```

Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr

MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-ripemd160-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-256,hmac-sha2-512,hmac-ripemd160,umac-128@openssh.com

```

disable X11 Forwarding, X shouldn't exist on a server install in the first place

```

x11Forwarding no

```

considering disabling password authentication and requiring keys.

#### Set password complexity ####

Set minimum password requirements of at least 8 characters, at least 3 out of special,upper,lower,number categories, 90 day expiration period with 7 day warning

```

authconfig --passminlen=8 --passminclass=3 --update
echo "password    required    pam_pwquality.so retry=3" >> /etc/pam.d/passwd
sed -i 's/^PASS_MAX_DAYS\t99999/PASS_MAX_DAYS   90/g' /etc/login.defs

```

#### Set Account Lockout Policy ####

Set account to lockout after 3 attempts for 10 minutes. Authconfig may overwrite files, find better way to script this following direction found at 4.1.2 of RH7 hardening guide.
Add the following as lines 2 and 4 in /etc/pam.d/system-auth

```

auth        required      pam_faillock.so preauth silent audit deny=3 unlock_time=600
auth        [default=die] pam_faillock.so authfail audit deny=3 unlock_time=600

```

not the correct way, but in any case

```

sed -i '5i/auth\trequired\tpam_faillock.so preauth silent audit deny=3 unlock_time=600/' /etc/pam.d/system-auth
sed -i '7i/auth\t[default=die]\tpam_faillock.so authfail audit deny=3 unlock_time=600/' /etc/pam.d/system-auth

```

#### Disable Unnecessary Services ####

use "systemctl list-units | grep service" to determine active services and disable any that are not needed.

systemctl disable wpa_supplicant

systemctl stop wpa_supplicant



#### Uninstall Unnecessary Packages ####

```

yum remove alsa* iwl* NetworkManager-wifi

```

#### Securing the Network Stack ####

There is a longer list of settings, but most are already set by default

```

echo "net.ipv4.conf.all.accept_redirects = 0" >> /etc/sysctl.conf
echo "net.ipv4.conf.all.secure_redirects = 0" >> /etc/sysctl.conf
echo "net.ipv4.conf.all.send_redirects = 0" >> /etc/sysctl.conf

```

#### Securing Mountpoints ####

set noexec,nosuid,nodev on /home, /tmp, /var, /boot

### More Security ###

Options that start impacting usability

### Securing Apache (the basics) ###

In httpd.conf
ServerTokens Prod reveals only Apache instead of additional apache information that could reveal vulnerabilities
ServerTokens Prod

## Confirming Security ##

There isn't a magic tool that can provide you complete assurance but there is OpenSCAP that can help confirm your system is secure.

OVAL definitions: http://www.redhat.com/security/data/metrics/

```

yum install openscap-scanner
wget http://www.redhat.com/security/data/oval/com.redhat.rhsa-RHEL7.xml
oscap oval eval --results rhsa-results-oval.xml --report oval-report.html com.redhat.rhsa-RHEL7.xml

```

### References ###

Red hat security doc: https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Security_Guide/chap-Hardening_Your_System_with_Tools_and_Services.html
Arch Linux security doc: https://wiki.archlinux.org/index.php/security