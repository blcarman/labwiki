Again looking at terraform for deploying VMs. One of the problems I'm seeing though is that it expects you to already have the template/image available which of course is fairly normal for cloud providers but not so easy when you have tens of vcenters scattered about.

We can easily create templates with Packer however the question is how do we ensure the template exists before we run our terraform, and do we simply upload a previously craeted template or do we have packer create it every time. 

1. check if latest version of template exists in vcenter
   * if not, then upload/create latest version
   * packer trips an error if the template already exists `Build 'vsphere-iso' errored: el7_packer already exists, you can use -force flag to destroy it: <nil>`, but how do we know its the latest one? and if its not we need to delete it and create a new one
1. run terraform
   * how does terraform handle the template changing?
   * what if we need to force the VMs to be re-created?

## Infoblox Provider ##

https://github.com/infobloxopen/terraform-provider-infoblox

* `dnf install golang dep`
* configure golang, https://developer.fedoraproject.org/tech/languages/go/go-installation.html
* `mkdir -p $GOPATH/src/github.com/infobloxopen; cd $GOPATH/src/github.com/infobloxopen`
* `git clone https://github.com/infobloxopen/terraform-provider-infoblox.git`
* `cd $GOPATH/src/github.com/infobloxopen/terraform-provider-infoblox`
* `make build`
* if necessary `mkdir ~/.terraform.d/plugins`
* `cp terraform-provider-infoblox ~/.terraform.d/plugins/`

Documentation of the provider is a bit slim, but when I had trouble with the WAPI version a quick glance at the source and I was able to easily identify what variable to set and the available values (way easier than the shit opnsense api code), I suppose this was in part the golang but also general code quality. In any case this is going to be very cool and bodes well for the longtime goal I've had of turning the whole lab into a script.

The infoblox provider does seem to have some issues with resources that already exist, like it doesn't bother confirming them, which kind of seems to go against terraform's declarative nature. If I'm feeling adventurous maybe I'll dig into the code a bit and see if it can be improved.

This might be the dealbreaker at work, but it looks like the infoblox provider would need some tweaks to the code essentially replacing tenant_id with crm_id. It apperas fairly trivial, but I don't see the rest of the team embracing those kinds of changes. It does bring up the question of how to track updates of the upstream project after you've tweaked it, though looking at the commits there isn't any active development anyway. Looking further into it, it appears the tenant id is more than just an extensible attribute and is part of the cloud licensing. Just for fun I might still try to modify it so that the cloud licensing isn't necessary.

The infoblox provider is really starting to show its limitations. I was able to remove the cloud license check, but then I realized it can't actually configure the IPs it allocates in DNS, so it basically only does half the job. So it looks like terraform will not be getting used at work any time soon. On the plus side this could be a good project for learning Go.

# Terraforming the Lab #

I have the ultimate goal of being able to configure the entire lab with a single script. I hope terraform will be a major piece of this. As I'm learning more about Terraform I'm also realizing it might make sense to keep some things separate even if they could be combined. So I think I'll end up with the lab infrastructure modules and environments being separate from the lab applications.

Initially, I intended to start from the ground and work my way up, however it appears Cisco doesn't have a provider, which isn't terribly surprising given the old school enterprise style of Cisco. 

Terraform is quickly losing points with its incredibly shitty documentation, specifically around variables. What the fuck is the point of a terraform.tfvars in the project if the variables in it don't get passed to the child modules? And there doesn't seem to be a way to print the variables its using so you can't even tell what's going on. The "Terraform: Up and Running" book is nearly useless as well in this regard. 

I think I got the variables working but it doesn't seem right. Apparently I need 2 sets of variables. The ones at the root level and the ones at the child module. I then set the root variables to their childe module counterparts. Of course this isn't documented anywhere... I guess alternatively I don't put the values in terraform.tfvars but in the module block. I suppose this should work unless the variable is used by multiple modules as which point the double var thing would be necessary.

And with the var values sorted out I was able to create some networks in infoblox very cleanly. 

## Directory Structure ##

Like with most things, the docs don't really tell you how to do complex things in the real world. Here's a decent article that I'm using as a starting point until I can confirm its good or figure out a better way. https://medium.com/henngeblog/terraform-sane-practices-project-structure-c4347c1bc0f1

## Importing Resources ##

Supposedly terraform can import existing resources. However when I tried this with my vsphere datacenter it wasn't supported. And I think this highlights one of the biggest flaws with terraform. It is supposed to be declarative but at the same time it's not smart enough to check existing stuff unless its in the terraform state already. I don't really understand why there wouldn't be a simple check to see if a thing exists before trying to create it, outside of laziness on the part of the developers. The documentation also doesn't really provide a good overview of what resources can or cannot be imported, so I'm essentially stucking guessing. 

Another fun thing with importing portgroups. The import would succeed, but then an apply would trip an error and in vcenter there would be the following error for the task: "The configurations of the objects are in conflict.". However re-running the apply would show no issues and nothing to change.

## Cisco Devices ##

Strike 1 for terraform (or perhaps Cisco), there is no provider for Cisco, I guess there is one for ACI but that isn't useful for me. So I suppose that will have to go into ansible. 

## VMWare ##

It looks like there is very good vmware support, not just for creating VMs but managing pretty much anything in vCenter. This is going to take some trial and error to lay out correctly.

Not off to a good start with the vsphere provider:

```
Error: Error asking for user input: 1 error(s) occurred:

* module.web.provider.vsphere: fork/exec /home/brad/remote/technical/terraform/lab/.terraform/plugins/linux_amd64/terraform-provider-vsphere_v1.9.1_x4: permission denied
```

It turns out this was my fault. At first I thought it was because my terraform directory is on nfs, but the infoblox plugin was working so I thought surely something else was going on...until I remembered that I custom compiled the infoblox plugin and put it in the terraform.d in my local home directory. After moving the vsphere plugin there and deleting the copy in the nfs it now appears to be happy.

Super close to successfully building a VM but got stuck on guest customization because the template had OS set to other instead of linux.

Winning! VM build was successful after changing the template's guest type. So its been maybe a week of after work uh... work and I'm starting to get the hang of it. The errors could be a little more verbose but really that goes with everything. Documentation is definitely weak in places, it suffers from the problem of being written by people who know the software well, so they don't think about that guy starting from zero or an admin instead of dev background and it makes it a bit discouraging for others. Also the modules system kind of sucks. I guess I'm expecting something like ansible roles and its just not the same. Obviously there may be some new tricks I still need to learn but what was a real problem is the infoblox network, though maybe I need to output it from one module into the other now that I think about it, otherwise it just tries to re-create it which isn't right.

Disk adds don't require the VM to be deleted which is nice. IP changes do though I suppose that's because guest customization only runs at first boot.

### VM Attributes ###

Terraform plan kept returning the need to change the VM notes from the the template ones to blank but wouldn't actually do it, 

```
Terraform will perform the following actions:

  ~ module.web.vsphere_virtual_machine.vm
      annotation: "built by packer on 1549596940" => ""
```

And it wouldn't drop an error that it failed either, kind of odd. I fixed it by manually performing the operation and that seemed to make terraform happy.

It appears there is also a way to ignore certain attributes in changes.

## Other Providers to Check out ##

https://github.com/elliottsam/terraform-provider-windows-dns

## Providers in Root Module ##

According to the documentation I should be able to put the provider config in the root module and it will trickle down to the child modules, but I get this error so...

```
Error: Error asking for user input: 1 error(s) occurred:

* module.web.infoblox_ip_allocation.trust: configuration for module.web.provider.infoblox is not present; a provider configuration block is required for all operations
```

## Updating Resources ##

I thought by default terraform was supposed to create new then destroy the old but it appears that is not the case with vsphere stuff. Also kind of annoying but it has to rebuild the VM just to change the IP, but I guess that kind of makes sense.