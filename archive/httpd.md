# Redirect Everything to https #

requires mod_alias or alias_module

```
<VirtualHost *:80>
    ServerName www.example.com
        Redirect "/" "https://www.example.com/"
        </VirtualHost>
```
        
# Hardening HTTPD #

## Slowloris ##
        
Slowlor is a type of DOS attack that operates by opening a a large number of http connections with incomplete header info, causing the server to wait for the rest of the data and preventing a valid connection from elsewhere. There is a 3rd party apache module called antiloris, but it does not seem active any more and the preferred method for mitigating appears to be with mod_reqtimeout.

https://confluence2.cpanel.net/display/EA/How+To+Mitigate+Slowloris+Attacks