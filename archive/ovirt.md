I finally made the jump to a real hypervisor manager. Its still lacking some features and intuitiveness that vCenter has but the price is right and it is actively being developed. I'm disappointed proxmox seems to get more publicity. The documentation available for ovirt can be lacking, but this is r to convince the suits if it ever came up. 

Now here's a mess of notes I'll pretend I'm going to clean up at some point in the future. 

### Templates ###

Ovirt templates are a little awkward. From what I can tell if you create a new VM from a template it will create it as the equivalent to a linked clone in the vmware world. However, if you create a new VM and reference the template in the creation you can select an option to clone the template's disk instead. In any case this is just another in the growing list of somewhat unexpected behavior. Though its possible I've simply grown accustomed to vmware's terminology since it's what I started with. 

I'm sure this link will change, but here's the Red Hat doc on templates/clones, https://access.redhat.com/documentation/en/red-hat-virtualization/4.0-beta/virtual-machine-management-guide/710-creating-a-cloned-virtual-machine-based-on-a-template

### Cloning ###

Cloning feels a bit awkward like the template process. Basically you have an extra step to do a clone that is automatically done with vmware. To create a clone, you first have to create a snapshot. Then you can create the clone from the snapshot. I guess it adds a bit of flexibility in that you don't have to clone the live system, but it just feels a bit clunkier. Its also possible to clone powered off VMs directly. Either way one of the weird things with cloning is the disk name is not updated automatically.

### oVirt testing ###

Trying out oVirt for VM management. Installing on existing KVM host
Installed using all defaults
Apparently node requires a dedicated install similar to esxi or not, there was a bug causing issues installing on fedora

After resolving one bug to get the node installed, the host hung and reboot did not resolve, not a good start

Re-trying the install using a couple of VMs on my desktop machine to figure out how best to go about this. I might start with the engine running on a VM on my desktop and add the r610 as a host to it

And now re-trying in VM with centos 7 because fedora 24 doesn’t appear to be supported yet

Had to enable nested virtualization, https://fedoraproject.org/wiki/How_to_enable_nested_virtualization_in_KVM

Installing host from engine gui still failed, had to add the repo on the node and now install is getting farther and holy crap it worked

Now adding storage isn’t working, at least for local storage, and do to the vm network i can’t hit the nfs server…

https://access.redhat.com/documentation/en/red-hat-virtualization/4.0-beta/administration-guide/73-preparing-and-adding-local-storage

Set up a quick fake nfs server, was able to add it so should be happy, might end up moving the big ssd to the warehouse just to enable shared storage later on, hmmm

Networks were a little weird but so far the easiest to sort out

Red Hat does seem to have slightly better install docs


Bcache -> move small disks to new btrfs pool with bcache and move disks as data is moved, 2 ssd in raid 0? To saturate 10gb link?
Or do 2 1tb disks with 120gb bcache and presented as iscsi? Iscsi on btrfs?



```
successfully disabled IPv6 once putting the following lines in /etc/sysctl.conf:

```

net.ipv6.conf.all.disable_ipv6 = 1 net.ipv6.conf.default.disable_ipv6 = 1 net.ipv6.conf.lo.disable_ipv6 = 1
File "/usr/lib64/python2.7/multiprocessing/managers.py", line 774, in _callmethod

```
raise convert_to_error(kind, result)

```
  
IndexError: list assignment index out of range


Game Time! 
After failing several times I’m near the end of trying. So we’re going to give it one final go and if it doesn’t work we’ll go crawling back to vmware. 

Using Fedora 23 as my base system this time. 

eno1 configured with mgmt IP
Eno2-4 unconfigured
10gb configured on storage network

Dnf install ovirt repo, bridge-utils, screen, python2-dnf

Set selinux permissive, ipv6 disabled

Disabling firewalld, moving iso to local for engine


Ovirt
Node Creation (first node with self-hosted engine)
Install fedora 23 minimal server (24 not supported as of writing)
Update
Disable selinux
dnf install http://resources.ovirt.org/pub/yum-repo/ovirt-release40.rpm
dnf install ovirt-hosted-engine-setup
dnf install screen
Screen
Systemctl stop firewalld
Systemctl disable firewalld
hosted-engine --deploy
Failed
Followed this, https://www.ovirt.org/develop/developer-guide/vdsm/installing-vdsm-from-rpm/ for network configuration, still failed
Disabled ipv6 per http://askubuntu.com/questions/309461/how-to-disable-ipv6-permanently
In /etc/sysctl.d/ipv6.conf
net.ipv6.conf.all.disable_ipv6 = 1 net.ipv6.conf.default.disable_ipv6 = 1 net.ipv6.conf.lo.disable_ipv6 = 1
Now getting stuck at “waiting for vdsm hardware info”
Disabled tls per above ovirt article, still failed, rebooting, still failing after rebooting, going to try with centos 7.2

Self-hosted on CentOS 7.2
Install centos minimal
Yum upgrade
And same fucking result as at step 10 above where it just gets stuck waiting for the damn vdsm

Nested Self-hosted on CentOS 7.2 VM
Install centos 7.2 minimal server
Update
Disable selinux
dnf install http://resources.ovirt.org/pub/yum-repo/ovirt-release40.rpm
Followed this fully, https://www.ovirt.org/develop/developer-guide/vdsm/installing-vdsm-from-rpm/
Disabled ipv6 per http://askubuntu.com/questions/309461/how-to-disable-ipv6-permanently
In /etc/sysctl.d/ipv6.conf
net.ipv6.conf.all.disable_ipv6 = 1 net.ipv6.conf.default.disable_ipv6 = 1 net.ipv6.conf.lo.disable_ipv6 = 1
dnf install ovirt-hosted-engine-setup
dnf install screen
Screen
hosted-engine --deploy
Failed
Create fake cert files with touch
Add engine host record on host
And still failed with setup validation cannot acquire bridge address, seems to be because of manual vdsm install

Hopefully final Self-hosted on CentOS 7.2
Install minimal centos 7
Yum upgrade
Yum install bridge-utils screen
Disable selinux, ipv6
Disable NetworkManager, enable network

/var/lib/ovirt-hosted-engine-setup/answers/answers-20160708225259.conf
6. Disabled ssl in vdsm.con and ran configure --force, now seems hung on starting vdsm 
Cancelled deploy, returned nfs to orignal .20.1 ip and rebooted
Reboot resolved the vdsm hang issue, but still failing same as before disabling ssl
Changing to master/git repo, did yum upgrade, rebooted
Still failing at same spot
Will re-try with ovirt node in the morning, but can’t figure out how to download from http://www.ovirt.org/node/

Might have found the spot in vdsm.conf that was breaking things, by trying both ipv6 and 4, fedora has broken package related to dmidecode it seems, going to re-try centos

For real hopefully final self-hosted using ovirt node iso master image
http://resources.ovirt.org/pub/ovirt-master-snapshot/iso/ovirt-node-ng-installer-master/ovirt-node-ng-installer-master-2016062905.iso
If this doesn’t work, i may email their mailing list and see what i’m missing
Followed install as previous, configured both mgmt nic and storage nic
Got stuck at boot menu, awesome
Using cockpit for the hosted-engine set up, big surprise it’s stuck waiting for the vdsm to reply

Holy crap got to the engine VM install phase. Had to get vdsm configured with the hosted engine then go back and tell it to stop listening on ipv6, otherwise hosted-engine script would re-write the config
So close to working, engine vm went well, but for whatever reason it thinks the host is named engine... 
It also wouldn’t let me add the storage correctly and now won’t let me add any further storage

Stopping sanlock service appears to have pissed off the host, some sort of HA/fencing kicked in and rebooted it and now the engine vm won’t start…

Disabling ipv6 may not be necessary

No longer assuming this will be the final install.. Install
Centos, selinux permissive, hosts with vanadium and engine, networkmanager disabled, network enabled
I think this one worked, or at least I once again got the engine set up, i’m going to leave as is for now so i can add the storage when i’m not tired like last time. 

Finally, gave up on hosted-engine set up and installed it on a VM on my desktop. Now got it mostly working. A few caveats, ISOs datastore has to be reachable from engine to upload, otherwise it appears you can copy directly into the folder it creates with a guid. Also seems i found the place to activate the storage domain...so the hosted engine set up would have worked. Once i get a second host i might move over to hosted set up. Networks were configured easily but haven’t tested yet. QCOW2 VMs can be imported by tricking the system, create a new VM with a correctly sized disk, then use qemu-img convert to convert the original image to raw and overwrite the new blank disk created by ovirt, be careful to make sure you use the correct disk

Windows 10 didn’t like moving, re-installing and going to attempt to disable secure boot on new install

Big con -> you have to reboot to change ISOs that are mounted, and its in the boot options tab. Very much a poorly thought out feature

Pros 

* weird bug/bad config I had causing the hardware checksum on the virtio adapters is no longer an issue
* storage vmotion works while VM is powered on

### Adding a Second Host ###

I recently acquired an HP DL380 G7 to use as a 2nd ovirt host. I think I still like the Dells better but the price was right and it seems quiet so far, but it does take longer to boot and the insides are more clunky to work with. In any case, I'm installing CentOS 7 to a usb drive and if it works well I will likely move the dell to usb as well to save some power. I'm setting up the networking to match the dell, so 2 NICs used for the VM traffic, 1 NIC for management, and 1 10GB port for the storage connection. The storage connection is the only part that may prove interesting with ovirt as I have the dell set up to hit the nfs storage by IP and since I'm doing direct connections the HP won't be able to use that IP. Since I do have both the warehouse and fastwarehouse datastores now, I will hopefully be able to move the VMs to one, re-create the other using a DNS name that exists on both hosts and shuffle things back to keep things running smoothly.

Install went ok but having trouble adding to cluster due to the storage issues. I also forgot to disable Network Manager at the beginning thought the install does it automatically so not a big issue. I'm going to attempt to create a new cluster with the new host and then migrate things over to it. And hopefully that will make it easier to add other hosts in the future. Something still doesn't seem right, it seems to lose connection from the engine but doesn't give a solid failure. In any case I might need to start over from scratch to redo the datastores.

Well, it ended up a bit messy but I have the 2nd host in the cluster now and "vmotions" are working. The vmotions tend to bog down the management network interface though, so I need to get teaming going on that or upgrade it to 10gb as well. One downside to my experimenting is I now have a failed datacenter and locked storage domain in ovirt. For whatever reason it won't let me forcefully remove it. Maybe vCenter would have the same issues, but I feel like in my experience with vCenter it can recover from these types of things better and doesn't let you go down a path that you can't revert. So I guess we'll leave things the way they are for now, but I may scrap ovirt and go back to vmware.

### Hosted Engine ###

I still haven't been brave enough to fully tackle the hosted engine configuration. But I think I'm at least going to move it to the warehouse so my desktop can stop dragging and I can hopefully save some power. Moving the engine to the storage server was kind of a terrible idea. Its running like crap now, so I either need to figure out what's killing it or move it to the self hosted configuration.

Well, with the engine running on the warehouse things haven't been completely terrible but I would like to get it off of there since its causing some resource pressure on there. If I take backups and create a few ansible playbooks I should be able to rebuild things fairly easy if I'm not able to import the original VMs.

I realized it probably won't be a good idea to do the fake hosted engine thing because it will be a pain to do any work on the warehouse storage. I'm now considering a 3 node hyperconverged set up and a dedicated NAS for the slow storage. Or its time to downsize to a single big box...

### Next Steps ###

* add additional host(s)

I was originally concerned with this since my 10gb connections are direct and would be on different subnets, hopefully this can be avoided with entries in the hosts file on the nodes so they can use different IPs for the same storage. Or we'll do it the right way and get a fancy switch. 

### Possible Deal Breakers ###

* Currently not able to do online disk expansions for any guests (as a stupid workaround, when using thin provisioning you could make the disk arbitrarily larger than the initial partition scheme uses and thus have the ability to expand in the guest if needed while not unnecessarily giving the users on the guest more space than they need)
* Reboot required to add/remove ISOs from guest
* Certain UI decisions are not intuitive (i.e. ISO mounting only available in boot options)
* No folders or other way to organize VMs, not bad for small environments but would become unruly in a large environment

### Upgrade to 4.1 ###

Ovirt 4.1 came out a few days ago and I'm in the process of upgrading the home lab. It seems like they've made a fair number of fixes and added a couple nice features. The one noticeable piece they left out was how to upgrade the hosts themselves. The Red Hat docs almost imply the engine will upgrade the host to the latest version automatically but really it keeps it within its minor version. I had to install the new 4.1 repos and do a normal yum upgrade, nothing terribly challenging, just would have been nice to include in the doc. I should look at getting a 3rd host for the home lab and setting up glusterfs to do a hyper-converged with it. I could potentially get away with redundancy across the hosts and not within the hosts themselves to save on physical disks as well.


## Random libvirt Note From Turtl ##

live migrate without shared storage: https://hgj.hu/live-migrating-a-virtual-machine-with-libvirt-without-a-shared-storage/