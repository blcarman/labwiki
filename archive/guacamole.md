This project seriously annoys me but I can't find a decent alternative so I guess we'll deal with it. But basically I believed their shitty documentation that said I would have to build the server side from source since I didn't see any guacomle-server packages and the plain guacamole package on Fedora was a fairly older version. Well as it turns out the packages actually are available on CentOS and Fedora, so thanks for just blatantly lying to us Guacamole project.

My next beef with Guacamole is that it really doesn't understand the whole server and client terminology. So the web application is called the client, really I think it would have been better had they stuck with the client being the end user's web browser, the web front-end could be considered the client access server or just web application and it could connect to the backend guacamole server. So now that we've given up on this shit show they call documentation we'll see if it was worth all the trouble... Stay tuned kids.

We've almost got Guacamole working, at least I hope we're almost there. I've at least got the web page loading, but whenever I try to log in I get "Invalid Login" which appears to be a universal error for "security" reasons. In theory the tomcat logs should have more info but they're next to useless at this point. In any case, I can see why the project has been stuck in "incubating" for a while now and doesn't seem to be going anywhere fast. Anyways, we'll call it a day and see if I'm missing something stupid tomorrow.

tried enabling debug logging, but no change in catalina logs

hot damn! its working. It turns out the tomcat user is set with /sbin/nologin so it wasn't picking up the environment variable GUACAMOLE_HOME. So instead I linked it to tomcat's home directory

```

ln -s /etc/guacamole /usr/share/tomcat/.guacamole

```

Holy creepy font batman, ssh working after opening firewall, but font is like a note from a serial killer in a horror movie. 

yum install liberation-mono-fonts

RDP proved to be another kind of annoying piece to get set up, had to set it to tls encryption to work with windows 10. Performance seems good, youtube video played with minimal choppiness and audio just worked which was a surprise. 

VNC access is not going as well, though to be fair the vnc fail isn't all on guacamole. I've only tried on KVM guests using krfb so I'm not sure if its the kvm piece or the krfb piece that is causing the problem. On an ovirt fedora alpha guest with krfb it would just show the desktop from the initial connection, however in the local console you could see the desktop changed. I then tried with sabayon on my local desktop kvm and krfb at least worked, but was hideously slow. I then tried just opening up the KVM vnc console to the outside but that isn't working either and looks like it may relay on ssh tunneling which doesn't make sense to me if its listening on the vnc ports to the outside world. In any case guacamole is clearly not the problem solver I thought it would be. Maybe I'll look into spice again and see if anything has changed there. 

## To Do ##

*  add guacomole config files to ansible role
*  add font install to ansible yum install liberation-mono-fonts
*  harden config and put behind nginx with ssl


### From turtl ###

Securing Guacamole

Putting an SSL cert and nginx proxy in front of guacamole to make it more secure.
First, we need to install nginx. Ansible Galaxy has a fair number of nginx roles, but for what we're doing I think I'll create my own.
ansible-galaxy init nginx_server
server { listen 443 ssl; server_name guac.home.lab; ssl_certificate /etc/pki/tls/certs/guac.home.lab.crt; ssl_certificate_key /etc/pki/tls/private/guac.home.lab.key; ssl_protocols TLSv1.2; ssl_ciphers HIGH:!aNULL:!MD5;
location / { proxy_pass http://127.0.0.1:8080; proxy_buffering off; proxy_http_version 1.1; proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for; proxy_set_header Upgrade $http_upgrade; proxy_set_header Connection $http_connection; } }
I disabled selinux to get nginx working with https, need to identify what is blocking it. edit 9/24/17, found the selinux bool that needed to be set setsebool -P httpd_can_network_relay 1 and set selinux to enforcing
Also need to remove the 8080 firewall rules to enforce https