### Basic User Creation ###

This will create a user named dbadmin that can log in from any host and has full permissions to everything in mysql.  

```

CREATE USER 'dbadmin'@'%' IDENTIFIED BY 'mM!Cl#Lv7Aysi';
GRANT ALL PRIVILEGES ON *.* TO 'dbadmin'@'%';
FLUSH PRIVILEGES;

```

For a specific DB change *.* to 'specificdb'.*

### List Users ###

```

select User,Host from mysql.user;

```

### Basic Backups ###

Create a user for backups

```

GRANT LOCK TABLES, SELECT ON *.* TO 'BACKUPUSER'@'localhost' IDENTIFIED BY 'backPa55word';

```

Backup ALL the things

```

mysqldump --all-databases -u BACKUPUSER -pbackPa55word > backuptest.sql

```

### Restoring a Backup ###

```

mysql -u root -p < backuptest.sql

```

How does this handle existing data(i.e. does it overwrite it? should all data be removed from mysql before restoring?)

### Logging ###

To enable mysql to send logs to syslog add "syslog" to the my.cnf. Note mysql can only send the log files to one place so after enabling you will have to look in syslog for all logs.

Example:

```

[mysqld_safe]
syslog

```