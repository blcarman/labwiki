### Generating a CSR ###

openssl req -new -newkey rsa:2048 -nodes -keyout www.website.com.key -out www.website.com.csr

### Self-Signed Certificate ###

After generating the CSR, openssl x509 -in ssl.csr -out ssl.cert -req -signkey ssl.key -days 3650