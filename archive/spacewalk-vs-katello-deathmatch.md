The old reliable dinosaur vs the new hot shit. Is Katello ready for the big leagues yet? It's been over a year since I last used it, so maybe. Spacewalk definitely doesn't have all the features that Katello does, but its old enough that the things it does do are done well, at least that's the idea. I'm going to try to be open-minded about both products even though I feel spacewalk is passed its prime. There may also be a third hybrid option of spacewalk plus foreman.

## Spacewalk 2.8 ##

### Home Lab Setup ###

Something strange is going on with the kickstart profile key. It was using one that I'm not sure where it came from. I created a new centos 7 key manually as it seems maybe the baked in auto-generated key isn't quite complete.

### Spacewalk Kickstart ###

This error will trip even if just one package is missing. The missing packages are most likely in the spackewalk-client repo

```
The following packages are not available in any of the channels associated to this kickstart profile: pyOpenSSL, rhnlib, libxml2-python, libxml2, spacewalk-koan*.
In order for all kickstart functionality to work correctly, these packages should be present in at least one channel associated to this profile.
```

Next up on the list is a package conflict when trying to install `cobbler-loaders`, there is a longstanding red hat bug open for this with no real movement.

For some reason excluding in the epel repo file wasn't working so instead: `yum install --exclude=cobbler-epel cobbler-loaders`

Then had to fix the `/etc/xinetd.d/tftp` to enable tftp (I thought I did this as part of spacewalk setup), then start(and enable) tftp service and boom. It would appear the default pxe menu prefers booting to local disk, need to look into changing that.

And now its trying to pull something from 127.0.1.1, wth? Stupid vmware guest customization strikes again, it puts the stupid hosts entry for 127.0.1.1 which causes the autoconfig things to put the bad IP in. Had to delete the entry, then fix the cobbler config, restart cobblerd, and run cobbler sync. And with that we have a fully kickstarted system working.

Since we won't have dhcp at work, this may be useful, https://www.redhat.com/archives/spacewalk-list/2012-March/msg00208.html

Perhaps my thinking is short sighted but without dhcp I simply can't figure out a way to make this process not suck. There is absolutely no gain over VM templates and there are a ton of cons that those don't have. Even with a build network we would have to figure out some way to get the IP/hostname into the kickstart which probably means a kickstart profile per target system which is insane. So what's the solution? The one thing this should save us from is having to copy the VM template everywhere and keeping it up to date, so maybe it will still be a net improvement. So here's the proposed process:

1. build cobbler iso per vlan with a kickstart IP (would limit number of VM builds to 1 per vlan)
1. allocate IP and hostname in IPAM
1. create VM shell and boot iso
1. wait for kickstart to complete
1. apply real IP/hostname on server
1. correct hostname in spacewalk
1. complete customization

Kickstart snippets have to be added to scripts in the kickstart profile and you must check the "template" box for the actual content of the snippet to be used.

Very limited ability to modify or even see what default snippets are being used from spacewalk gui. I cannot emphasize enough how badly the cobbler kickstart stuff is done in spacewalk. It makes one wonder if this was the reason they abandoned it in favor of the foreman and friends. On the one hand they praise the ability to template things and code re-use but then they completely disallow you to pick what goes in the base profile. 

vim /var/lib/cobbler/snippets/spacewalk/redhat_register and change redhat to centos in the rpm import, seriously fucking stupid.

So finally, we have a "good enough" solution, I've got it doing a full update and copying ssh keys/sudo stuff, but I still don't like it. Next up will be using a static IP and ISO, I suppose I'll have to do that on a different subnet to ensure http/https is enough.

There does seem to be a bug when updating the advanced options of a kickstart profile in spacewalk. It seems checking the box AND changing the text for that option only the checkbox will get updated. So you have to redo the option part afterward.

We've almost got it working, but now it would seem the `/etc/yum.repos.d/` files are coming back from the dead as part of the centos-release package so we won't be able to just nuke them after all.

### Cobbler ###

Cobbler iso building fails with selinux enabled:

```
[root@spacewalk ~]# cobbler buildiso --systems="test"
task started: 2018-11-13_000537_buildiso
task started (id=Build Iso, time=Tue Nov 13 00:05:37 2018)
using/creating buildisodir: /var/cache/cobbler/buildiso
building tree for isolinux
copying miscellaneous files
copying kernels and initrds for profiles
copying kernels and initrds for systems
generating a isolinux.cfg
generating profile list
generating system list
 - ksdevice bootif set for system test
done writing config
running: mkisofs -o /root/generated.iso -r -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -V Cobbler\ Install -R -J -T /var/cache/cobbler/buildiso
received on stdout: 
received on stderr: I: -input-charset not specified, using utf-8 (detected in locale settings)
genisoimage: Permission denied. Unable to open disc image file '/root/generated.iso'.

Exception occured: <class 'cobbler.cexceptions.CX'>
Exception value: 'mkisofs failed'
Exception Info:
  File "/usr/lib/python2.7/site-packages/cobbler/utils.py", line 123, in die
    raise CX(msg)

Exception occured: <class 'cobbler.cexceptions.CX'>
Exception value: 'mkisofs failed'
Exception Info:
  File "/usr/lib/python2.7/site-packages/cobbler/remote.py", line 95, in run
    rc = self._run(self)
   File "/usr/lib/python2.7/site-packages/cobbler/remote.py", line 157, in runner
    self.options.get("no_local_hdd",False)
   File "/usr/lib/python2.7/site-packages/cobbler/api.py", line 755, in build_iso
    iso=iso, profiles=profiles, systems=systems, buildisodir=buildisodir, distro=distro, standalone=standalone, source=source, exclude_dns=exclude_dns, force_server=force_server, no_local_hdd=no_local_hdd
   File "/usr/lib/python2.7/site-packages/cobbler/action_buildiso.py", line 478, in run
    utils.die(self.logger,"mkisofs failed")
   File "/usr/lib/python2.7/site-packages/cobbler/utils.py", line 131, in die
    raise CX(msg)

!!! TASK FAILED !!!
```

If we decide to add system objects to cobbler this may be helpful: https://docs.ansible.com/ansible/devel/modules/cobbler_system_module.html. Indeed the system object worked and even took care of the hostname issue in the spacewalk registration. Though it still remains to be seen if building an ISO for every system (we could possibly do multiple systems on a single ISO but then we'd have to manually pick at boot time), would be worth it. In any case I think I have a good proof of concept that we can survive without a stupid build network until engineering gets their heads out of their asses. Also I do need to figure out how to only include the specific entry I want in the buildiso command, but its documentation is incredibly limited.

Also it would appear that even though the system is registered to spacewalk with the correct hostname the system itself is not. Though perhaps I can have it use the cobbler system properties in the kickstart profile to take care of that. And it looks like we can use the cobbler system vars in the kickstart profile. To get the actual var names do `cobbler system dumpvars --name=test`. Ironically the mailiing list actually recommended redefining these vars in the ksmeta var which is kind of ridiculous (https://www.redhat.com/archives/spacewalk-list/2014-September/msg00043.html). In any case being able to use those variables means using the cobbler systems makes a little more sense.

The other thing this may get us is the ability to keep the ansible inventory and spacewalk inventory in sync, so in order to kickstart a system you'd have to add it to ansible. Obviously someone could still bypass this but it would limit the likelihood. 

To change pxe menu boot order you can edit `/etc/cobbler/pxe/pxedefault.template` and put the pxe_menu_items before the local entry, and remove the default thing from the local boot. Just kidding this didn't seem to change anything. Correction it fixed the pxe menu kind of, order now appears correct but it still defaults to local, not sure what the deal is with the iso version. After making changes to the /etc/cobbler/pxe stuff you need to run `cobbler sync`.

To build an iso with just one profile `cobbler buildiso --profiles="labtest:1:lab" --systems="" --iso=/root/justprofile.iso`. For doing the static IP thing per system I bet we'll have to build it per system rather than per profile. Indeed need to make it per system and disable the local boot `cobbler buildiso --systems='{{ target }}' --profiles='' --iso=/root/{{ target }}.iso --no-local-hdd`

Side note when using ansible to upload the ISO and create VM you need the pyvmomi python modules installed.

I didn't try very hard but the cobbler API seems to not like using spacewalk authentication, or at least it didn't play nice with ansible. It doesn't really matter though because I don't think ansible has a moduel for cobbler buildiso.

#### Final Thoughts on Cobbler ####

It has more power than I initially realized however I still feel like Foreman is superior. The cheetah templating syntax doesn't really make sense, you can't view a lot of the cobbler info from the spacewalk gui, and it's limited to PXE/ISO booting where Foreman can create the VMs as well.

### First Impressions ###

Spacewalk is old, but its also clear that it has been polished pretty well over the years. The setup script made installation easy. There are a number of helper scripts to add common channels, sync repos, configure cobbler, etc that make it a pleasure to work with. The gui is fairly nice and well laid out for the most part. Kickstart feels bolted on to the systems section, some of the repo stuff is located oddly, probably due to being However this is also where it shows its age. There's no concept of version control so if Joe Dumbass modifies a kickstart snippet incorrectly it could be difficult to undo. This also makes moving to another system difficult. Of course you could always maintain stuff in a git repo elsewhere, but this is prone to human error when someone forgets to update one or the other. I'll have to look at cli options for updating it as well. So with these limitations when using kickstart the name of the game is to do the bare minimum to get a bootable system that can be handed off to something else for configuration management.

As far as package management is concerned this is where spacewalk really excels. It doesn't have the fancy views and dev->test->prod environment promotion that foreman does, but its also a lot simpler. I still haven't tried a recent version of foreman/katello so its possible its worked out some of the kinks. Spacewalk does also have some nice auditing features though again its centered around packages it provides.

The final "feature" that is nice is that spacewalk clients will check in every 4 hours by default to get updates to packages and config files. This could be beneficial in allowing configuration for mutliple VMs to all be done from spacewalk, even allowing people to do things without ssh access to the systems themselves. This will likely work for simple things like a dns server that only has 1 or 2 config files, but will be difficult to scale to more complex systems. Perhaps it could be used to configure the cron job for ansible-pull to do the heavy lifting.

In conclusion, spacewalk wins at repository/package management but leaves a lot to be desired on the kickstart/provisioning side of things. Ultimately I see it hosting the distro images that are referenced in kickstart but kickstart being done with another tool.

A late addition, there is apparently a cli tool spacecmd and API, so it may be possible to put kickstart stuff in git and have gitlab CI update spacewalk.

## Katello 3.9 ##

I haven't used katello in probably 18+ months, so I'm hoping its becoming more mature than it was. There were really 2 main downsides previously. First, it's complex/complicated and hadn't yet had a lot of the minor bugs worked out. Second, it was heavily integrated with puppet. Now that didn't mean you had to use puppet, it just heavily favored it and didn't work awesomely with ansible. Hopefully the ansible integration has improved now that Red Hat owns both Katello and Ansible.

See Katello Round 2 page for details

## Entitlements ##

I should probably shoehorn this in somewhere else, but I just noticed spacewalk has a "virtualization" entitlement thingy that seems to just be a checkbox. I found an mailing list chain that said it was supposedly removed but that was years ago and its still there. it also doesn't give me the ability to remove it or add my own.

# Results #

I really tried to like spacewalk. At first it seemed very mature and had some nice things, but seriously it is just so far behind katello and others that it's just not worth it. At some point people have to realize that push button simple requires complexity in the underlying system. Spacewalk is fairly simple (though even some of its UI choices are not intuitive) because it leaves you to sort out the complexity yourself. A similar argument exists for systemd vs sysvinit, systemd carries the complexity so that unit files are simple and easy to write, whereas sysvinit was simple but the init scripts themselves were unruly.

The more I work with Spacewalk the more I am disgusted by it. I realize I'm not working on the thousands of servers in the cloud scale, but Spacewalk expects too much micro-management. It's taken me a long time to understand why so many people/companies develop their own things when projects like Spacewalk exist. But now I realize with all the manual effort, the scripts needed to interact with it, and the just not quite rightness of Spacewalk I can see how it would be better to just develop a tool to do the parts you need in the way you need it to.