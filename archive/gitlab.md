### A Quick intro to Git ###

I still need to review it but this looks like a nice reference, https://github.com/k88hudson/git-flight-rules

Git has a very simple goal, provide version control over a set of files/directories. It works best with plaintext files but can also handle binary files. In addition to source code it can be useful for config files, notes, even a blog. Also git does not require a remote upstream. 

* Cloning a repo: This operation creates a local copy of a code repo

`git clone git@gitlab.home.lab:brad/ansible.git`

* Git commits: a commit is a set of changes that you are ready to apply to the repo. After the changes are staged using the add/rm/mv subcommands you can make your commit

`git commit -m "some useful message describing your changes"`

* Working on files: `git add` can be used to tell git a newly created file has been added or an existing file has been modified. Git also has commands to remove/move files so that they are removed from the git repo in addition to the local filesystem. Note: git is of course version controlled so any files that are removed will still be available in previous commits.

```
git add <files>`
git rm <file>
git mv <file>
```

* Syncing changes to/from the remote repo. `git pull` will pull changes from the remote. `git push` will push commits to the remote. It's generally a good idea to pull before committing/pushing otherwise you could run into a merge conflict due your branch being behind the remote/master.

```
git pull
git push
```

* Git branches: `git branch -a` to list. Branches can be good and bad. They're useful for working on new features that you don't want to impact existing usage, however the bigger the changes the more difficult it can be to merge your branch into master when you're ready. While working in your branch frequent `git rebase` from master will help avoid merge conflicts when you're ready. An alternative way is to merge master into your branch before merging your branch into master. By merging master into your branch first you can resolve the conflicts on your branch without worrying about breaking the master branch.

* Git merge: if you use a development branch or maybe are just working in your feature branch, when you're feature is ready you'll need to merge it into the master or production branch.

* Git tags: you can use tags to signify a release. 

* Git stash: stashing can be useful if you are constantly context switching and changing branches. Essentially it'll hide your current work allowing you to clear your workspace to work on something different without having to worry about accidentailly committing something else.

### Projects ###

Projects should be used to logically group a set of code. Generally this might be per application or maybe a group of applications that work together. 

### Repos ###

New repos can be created without having to log into gitlab Webui.

```
mkdir new_repo
touch README.md
git commit -am "intialize new repo"
git push --set-upstream git@gitlab.example.com:namespace/nonexistent-project.git master
```

### Merge Requests ###

Merge requests can be used as code reviews. Essentially the code writer is asking for someone to review their proposed code and once it meets the standards they can merge it into master for production use.

### CI/CD ###

Gitlab has CI functionality which is largely freeform and not just limited to deploying code. You can also use it for code linting(i.e standard formatting), syntax testing, notifications, etc. Gitlab CI either runs on docker or via a bash shell. You can also set up as many CI nodes as necessary, so they might be split up across projects or environments. If a CI job fails it can also reject the commit thus ensuring the master branch is always in a good state for production use.

### Issue Tracking ###

Gitlab has a basic issue tracker and kanban boards, the uber expensive version also has a roadmap feature. Some cons to kanban board is that each repo has its own as well as the project, so its possible to end up with 2 kanban boards featuring the same issues that don't match. 

### Wiki and Other Features ###

Gitlab also has several other features including a wiki and "pages". These are good enough, but probably not a worthy replacement for a dedicated wiki/documentation platform. 

### Upgrades ###

Using the omnibus packages gitlab is easily kept up to date with the standard yum update. 

### Redundancy ###

gitlab doesn't have native HA but a backup install could be configured to replicate repos if necessary(could also be used to separate lab and prod potentially). DRBD could also be used to provide some HA, but of course comes with the usual DRBD caveats.