I'm kind of cheap, so while I like the best practices approach of separating services across VMs I really don't want to pay for multiple VPS instances (or any if I can avoid it). This is where nginx comes in. Using it as a reverse proxy I can cram multiple applications on the same web server. The question will be how many of these can I cram into singly tiny VM.

## What's being hosted? ##

* the professional blog
* the non-professional rant
* personal git repositories

## What Applications? ##

* ghost for the blogs
* gitea for git stuff (I was originally looking at gitlab but it was a bit heavy for what I needed and just didn't feel right.)

## Getting Started ##

* yum upgrade
* add nginx repo
```

[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/rhel/$releasever/$basearch/
gpgcheck=0
enabled=1

```

* yum install nginx
* useradd -m git
* yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
* yum install nodejs
* curl -L https://ghost.org/zip/ghost-latest.zip -o ghost.zip
* useradd -m ghost -s /bin/nologin
* yum install vim
* vim /etc/passwd update git user's shell to /sbin/nologin
* yum install unzip
* mkdir -p /var/www/rant/ghost
* mkdir -p /var/www/blog/ghost
* unzip -uo ghost.zip -d /var/www/rant/ghost
* unzip -uo ghost.zip -d /var/www/blog/ghost
* chown -R ghost:ghost /var/www
* cd /var/www/blog/ghost
* npm install --production
* cd /var/www/rant/ghost
* npm install --production
* cp /var/www/blog/ghost/config.example.js /var/www/blog/ghost/config.js
* edit config.js with fqdn blog.bradlab.tech and port 2369
* cp /var/www/rant/ghost/config.example.js /var/www/rant/ghost/config.js
* edit config.js with fqdn rant.bradlab.tech
* yum install supervisor

```

[root@ip-172-31-31-192 ghost]# cat /etc/supervisord.d/ghost_blog.ini
[program:ghost_blog]
command = node /var/www/blog/ghost/index.js
directory = /var/www/blog/ghost
user = ghost
autostart = true
autorestart = true
stdout_logfile = /var/log/supervisor/ghost_blog.log
stderr_logfile = /var/log/supervisor/ghost_blog_err.log
environment = NODE_ENV="production"

[root@ip-172-31-31-192 ghost]# cat /etc/supervisord.d/ghost_rant.ini
[program:ghost_rant]
command = node /var/www/rant/ghost/index.js
directory = /var/www/rant/ghost
user = ghost
autostart = true
autorestart = true
stdout_logfile = /var/log/supervisor/ghost_rant.log
stderr_logfile = /var/log/supervisor/ghost_rant_err.log
environment = NODE_ENV="production"

```

* systemctl start supervisord
* systemctl enable supervisord
* mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.bak

```

[root@ip-172-31-31-192 ~]# cat /etc/nginx/conf.d/blog.conf
server {
listen 80;
server_name blog.bradlab.tech;

location / {
        proxy_set_header        X-Real-IP $remote_addr;
        proxy_set_header        host    $http_host;
        proxy_pass              http://127.0.0.1:2369;
                }
                }
[root@ip-172-31-31-192 ~]# cat /etc/nginx/conf.d/rant.conf
server {
listen 80;
server_name rant.bradlab.tech;

location / {
        proxy_set_header        X-Real-IP $remote_addr;
        proxy_set_header        Host    $http_host;
        proxy_pass              http://127.0.0.1:2368;
        }
}

```
                                                                
* systemctl start nginx
* systemctl enable nginx
* semanage port --add --type http_port_t --proto tcp 2368
* semanage port --add --type http_port_t --proto tcp 2369
* semanage port --add --type http_port_t --proto tcp 3000
*  limit access to admin portion, http://www.codyhatch.com/articles/nginx-protect-ghost-login/
* set up gitea, I didn't document very well either the first time or this last time, but the main thing is to update the gitea.service file with quotes around the environment stuff and to correct the working directory

## Migrating to Digital Ocean ##

My AWS free tier is ending so I decided to start moving things to Digital Ocean. Along the way I used terraform to build the VM and configure the firewall. I couldn't decide how to handle the let's encrypt certs so I decided to be lazy and just copied the old ones over. I also just copied the existing /var/www/blog folder and had to do a restorecon/chown on it. Finally moved dns over to digital ocean since I will log into there more frequently. Finished moving stuff, kind of did it the lazy way. Gitea restore (i.e dump and then unzip) worked super nice.