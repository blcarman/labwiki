Here we are now with Pentaho 8. It looks like a fairly modest update from version 7, at least as far as the update process goes. It still appears to use the terrible info-zip thing that doesn't make any sense but I guess they couldn't fix everything. Unfortunately it doesn't seem officially supported to make the jump from the ancient 5.4 to 8, so we'll be making a pit stop at 7.

### Preparing installation files ###

* Download pentaho 8 archive build and the upgrade utilities
* unzip on a like system to your destination (same java and os if possible)
* run ./install.sh for main server and plugins
  * plugin destination goes to pentaho-solutions/system
* create tarball of full installation
  * tar czvf biserver-ee-70.tar.gz -C /home/pentahouser/pentaho70 .

### Ansible Role ###

The ansible role has 3 different operations: deploy, maintain, and upgrade. Deploy will do exactly that, but will not start the application since it is easier to troubleshoot initial issues when you start it manually and there is an annoying eula prompt on the first run. Maintain is meant to be the configuration management function to ensure the server is not changed outside of ansible, it should be safe to run at any time but if there are any changes pentaho will need to be restarted manually. Obviously, handlers could be created to restart pentaho, but due to its tempermental nature its generally a better idea to start it manually and follow the catalina.out as it starts. Finally, the upgrade operation is used for upgrading, it will back up an existing version of pentaho, install the new version and restore the configuration backup, as well as modify any files that may need to be updated per the pentaho upgrade documentation.

For the deploy and upgrade operations I prefer to specify them as a part of the extra-vars when running the playbook so `extra-vars operation=deploy`, this way you don't accidentally perform an action. 

Now, this might seem like an awful lot of trouble for a one-off type of task like this, so why are we doing it with ansible? The main reason is predictability, there is no risk for a human to forget to update a certain file or miss a step when ansible is doing all the work for us. It also makes it easier to know what modifications to the configuration have been made in case we need to open a ticket with support. Finally, it allows us to easily spin up new test instances if we're ever concerned something may break due to a change. 

### A Real World Example ###

To start we need 3 servers: business analytics, data integration, and data repository (note in later versions of pentaho the code has been consolidated, but you can continue to run a separate data integration server). Assuming we have already prepared the base servers (i.e. the common role has been run and OS is up to date) we can set them up for pentaho in ansible. Create 3 ansible groups as follows:

```
[pentaho_all:childen}
pentaho_servers
pentaho_db_servers

[pentaho_servers]
data-analytics
data-integration-server

[pentaho_db_servers]
data-repository
```

Now that we have the groups we need to set up the group_vars and host_vars.

```
# pentaho_all group_vars
pentaho_version_number: 5.4
old_pentaho_version_number: 5.4
ssl_enabled: "yes"
pentaho_edition: EE

hibuser_pw: PASSWORD
jcr_pw: PASSWORD
pentaho_db_pw: PASSWORD

ba_server_ip: 192.168.123.10
di_server_ip: 192.168.123.11
db_server: 192.168.123.100

# data-analytics host_vars
server_type: ba
tomcat_port: 8080

# data-integration host_vars
server_type: di
tomcat_port: 9080
```

To deploy pentaho create a playbook similar to the following

```
# apply_pentaho_roles.yml
- hosts: pentaho_db_servers
  roles:
    - pentaho_db
- hosts: pentahoservers
  roles:
    - httpd_server
    - pentaho
```

Then run the playbook in deploy mode `ansible-playbook apply_pentaho_roles.yml --extra-vars "operation=deploy" --limit pentaho_all -i my_inventory_file`

Once the playbook has been run you can manually start the services and review the catalina.out files for any errors. After the application has started it should be available at `https://data-analytics` and `https://data-integration` (replacing with real FQDNs as needed). 

Now that we have a running application we can either add `operation: maintain` to the pentaho_all group_vars or as part of the command `ansible-playbook apply_pentaho_roles.yml --extra-vars "operation=maintain" --limit pentaho_all -i my_inventory_file`.

To upgrade we need to add an `old_pentaho_version_number` variable to the group_vars so that ansible knows what to backup and in case there are any changes required when updating from a specific previous version. You also need to update the `pentaho_version_number` to be the new version you are upgrading to. Then run `ansible-playbook apply_pentaho_roles.yml --extra-vars "operation=upgrade" --limit pentaho_all -i my_inventory_file`. After its done you can manually start pentaho and review the logs similarly to the deploy process. Finally you can once again run a maintain to confirm everything is as it should be.