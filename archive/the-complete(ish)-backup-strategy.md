I've done piecemeal backups for a long time. Its always been my intent to set up a real backup strategy, but let's be honest its just not that fun. In any case I've been re-organizing some things and it only makes sense to add in good backups while I'm at it. I've been using borg for some stuff for a little while though it's still just been stored on the same host, so hardware failures could still cause problems. I also don't have everything backed up. So step 1 we need to identify what should be backed up.

# What Needs to be backed up? #

* secfs exports
* bookstack
* git (this could probably be done via the secfs backups)
* google drive? (really I should probably get anything critical off of here)
* desktop config files (until we get stuff fully into ansible)
* firewall config

# How? #

* Living data gets backed up to borg nightly
* borgserver gets backed up to warehouse weekly
* borg backup gets backed up to usb monthly