IPAM and DCIM tools have been a dream for some time. It seemed like everything out there sucked or was just stuck in an excel doc. That is until Netbox, https://github.com/digitalocean/netbox. This is finally a tool written by a network engineer who can program rather than the other way around. Its the first tool I've found that can actually handle connections. It was just released so there are still some things that could be added. But wow does it get a lot of things right from the start.

### Pros ###

* Self-hosted: none of the cloud BS, I don't have to worry about suddenly having to pay or the vendor's servers getting hacked
* open source: hosted on github, I don't have to worry about it suddenly being unavailable or a vendor deciding to stop support it. If the company backing it stops I can fork it
* common sense layout: with no documentation on the interface I was able to document everything I wanted to, anyone who should be using it can with no training
### Cons ###

* no pictures for rack elevation. Not a deal breaker but it does help engineers performing remote hands on gear they are unfamiliar with.
* no pre-defined objects. This is actually good and bad. I don't get stuck with shitty pre-defined choices that I don't have deployed, but I'm also not limited to anything. Interface naming is the same way, would be nice to have pre-defined ones, but with so many vendors naming their things differently this gives the flexibility to match the physical or software labels instead of trying to fit into the DCIM's naming.
* can I set up the same IP block multiple times for different customers? not a con just a question
* export function is basic but usable, doesn't appear to have a complete dump/backup option, but I'm sure you could just do it directly from the DB backend. 
I'm sure I could add to the list of pros but those are really the make it or break it ones. The cons are so minor and both could likely be resolved at some point in the future with a feature request. 

So now that you've found the perfect IPAM/DCIM tool you just have to convince management to use it, or just start using it without asking. If nothing else I'll at least use this as an example of how things should be done so we don't have to limit ourselves to poorly written/configured .net applications.

### Deployment on CentOS 7 ###

1  yum install epel-release
1  yum install postgresql postgresql-server postgresql-contrib libpqxx-devel python-psycopg2 gcc httpd openssl-devel python-gunicorn supervisor python-pip libxml2-dev graphviz
1  postgresql-setup initdb
1  edit /var/lib/pgsql/data/pg_hba.conf and change ident to md5 for local connections using ipv4 and ipv6
1  systemctl start postgresql
1  systemctl enable postgresql
1  netbox db creation