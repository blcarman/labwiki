## "Simple" Dev/Prod Patching ##

Option 1: using yum --downloadonly pre-download the packages and the install from the locally downloaded versions
Option 2: set up a system to act as a local repo
Option 3: Red Hat Satellite or Katello but these are complex and may cost money that is not feasible for small projects

### Option 1: Downloadonly ###

This is the easiest option however it may create issues with dependencies that are pulled from the main repos instead of the locally downloaded one so may not be the correct version requested. 

### Option 2: Reposync ###

This option requires slightly more work but still much less than a full Katello/satellite deployment. It requires a separate system with the same version and subscriptions as the system you are trying to update. It is then possible to download the repos needed and manually sync to ensure only the approved updates are allowed.

### Option 3: Katello ###

The preferred option is to use a system such as Katello/Satellite. It appears Katello does support Red Hat subscriptions so a full Satellite subscription may not be needed. This option is likely overkill for a small environment of 2-4 servers, but priceless in a large environment.