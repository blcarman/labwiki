[molecule](https://molecule.readthedocs.io/en/latest/index.html) 

# Basic Usage #

Since its probably a safe bet that we already have some ansible roles, we just need to initialize the test scenario. The first scenario needs to be named "default", though I'm not really sure why.

`molecule init scenario --scenario-name default --role-name <role_name>`

If we are starting a new role from scratch we can simply run and it will create both the empty role folder structure and a default scenario.

`molecule init role -r <role_name>`

Now, we know this default scenario is not set up for systemd, so how do we make systemd part of the default? I'm not sure, but we'll return to that thought. The next molecule command we need is the actual test command.

`molecule test`

It doesn't get any easier than that does it. This will at a bare minimum run through the ansible role twice and finally run yamllint. If you have multiple scenarios you can also specify if you want to run all or a specific one with `-s <scenario_name>`.

# Common Issues #

## Systemd in Docker ##

The first issue I had and actually what led to me finding molecule was trying to use systemd inside a docker container. Since most classic ansible roles will at some point need to interact with services systemd is a common requirement. Within the molecule scenario you'll have a molecule.yml file which has the docker settings you'll need to change. First, we need to change the image to use one that has systemd. There is an official `centos/systemd` image available so let's go with that. Note, this won't work well with firewalld but we'll deal with that later.

```
platforms:
  - name: instance
    image: centos/systemd
    volume_mounts:
        - "/sys/fs/cgroup:/sys/fs/cgroup:rw"
    command: "/usr/sbin/init"
    privileged: True
```

## Angry Linting ##

If you add a molecule scenario to an existing role it leaves out the default .yamllint file and so you get yelled at about molecule syntax as well as your own. Go ahead and grab the .yamllint from https://github.com/metacloud/molecule/blob/master/.yamllint or by creating a new role with molecule and copy it into the directory of your existing role.

## FirewallD ##

FirewallD doesn't seem to play well inside docker, so for those tests it may be better to use vagrant and full VMs.

# Conclusion #

Hopefully, this gave you enough information to get started with molecule and up your ansible game. Next up will be adding tests to ensure your ansible role is doing what you want it to and multi-node/multi-role testing.