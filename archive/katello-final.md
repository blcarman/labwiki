This document will attempt to provide a start-to-finish guide to setting up Katello. I'll try not to excessively repeat the official docs.

* Start with a typical EL7 VM (4 cpu, 10GB RAM, 100 GB for /var/lib/pulp though I just lumped it in within / for laziness)
* configure foreman repos
```
yum -y localinstall http://fedorapeople.org/groups/katello/releases/yum/3.9/katello/el7/x86_64/katello-repos-latest.rpm
yum -y localinstall http://yum.theforeman.org/releases/1.20/el7/x86_64/foreman-release.rpm
yum -y localinstall https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm
yum -y localinstall http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum -y install foreman-release-scl python2-django
yum -y install ansible
```
* install it (note: I'm add the other smart proxy installs with the original installer command, have not tested this)
```
foreman-installer --scenario katello --enable-foreman-plugin-ansible --enable-foreman-proxy-plugin-ansible --enable-foreman-plugin-remote-execution \
--enable-foreman-proxy-plugin-remote-execution-ssh
```
* configure AD realm
```
yum install rubygem-smart_proxy_realm_ad_plugin krb5-workstation
```