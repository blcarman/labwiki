This page will cover my git usage from mostly a sysadmin perspective. That is I'll be using it to host scripts, ansible playbooks/roles, and maybe some docs but no real codebases. I'm currently looking at either gitlab or gitea because they can be self-hosted, and even though github is the defacto standard I don't trust it.

I'm really liking gitea, it has been rock solid since initial configuration. Originally I was going to split the repos into ansible, powershell, and dot files. But I've now realized the dot files should probably go into ansible j2 templates when possible, sensitive information can go into host_vars in the local environment.

I don't know what it is about the official git documentation, but I find it very difficult to follow. I wouldn't call it bad, just maybe it expects you to be coming from a developer view point. In any case, like most things I think git you just have to jump in and get your hands dirty. I also like having quick references to stuff that I do infrequently. So here we are with an engineer's take on git

### Basic Branching: Do I really need to rebase constantly? ###

confirm on your new branch

`git fetch`

`git rebase master`

 do a git add and commit for any merge conflicts
 
`git push`

Rebase vs merge (I hate rebasing)

### Stashing ###

`git stash --include-untracked`

`git stash list`

`git stash pop <stash>`

### Branches of Branches ###

### Grabbing stuff from Other Branches ###

`git checkout otherbranch -- dir/file`

### Bash auto completion for git ###

[https://git-scm.com/book/en/v1/Git-Basics-Tips-and-Tricks](https://git-scm.com/book/en/v1/Git-Basics-Tips-and-Tricks)