Initial testing of Rancher seems like it may be an easier entrypoint than Openshift, but not sure if it really is production ready.

Some remaining things to be done:

* determine production configuration (i.e. how to avoid port 80 conflict between rancher gui and nginx-ingress)
* glusterfs or vmware dynamic PV's
* resource limits, by default rancher doesn't seem to limit pods at all
* registry
* prometheus/monitoring
* templating/helm
* LDAP authentication (looks pretty easy to set up via gui)
* cron/scheduled tasks (oneshot pods? i think)
* backups

### Apps to Move ###

* guacamole
* librenms?
* mysql1?

### Provisioning a Rancher Cluster ###

Rancher seems focused more closely to minikube/minishift than it does Openshift. That is it seems focused on providing a develope a couple commands to run rather than creating a supportable infrastructure. 

1. create 3 nodes via katello (or whatever basic centos 7.6 image)
1. maybe disable selinux
1. install/start/enable docker
1. disable firewalld (in reality probably figure out what ports are needed)
1. quickstart method: `docker run -d --restart=unless-stopped -p 8080:80 -p 8443:443 rancher/rancher:latest` (note the ports have to be changed if you want the agent and node installed on the same server, see https://rancher.com/docs/rancher/v2.x/en/installation/single-node/)
1. HA method: 
1. set up load balancer for true ingress HA
1. add storage class for vmware dynamic provisioning
1. change ingress domain default from xip.io

### VMWare Storage ###

VMWare is definitely not a first class citizen as far as Rancher is concerned, but then again gluster and ceph are equally lacking. In any case they do have some documentation on adding a "custom" vmware provider, it just isn't a convenient dropdown in the gui.

Since I only had one etcd node I broke the cluster trying to add the vsphere config so now rebuilding an HA cluster.

```
cloud_provider:
    name: vsphere
    vsphereCloudProvider:
      global:
        insecure-flag: true
      virtual_center:
        vcenter.lab.bradlab.tech:
          user: rancher_user
          password: the_password
          datacenters:  HomeLab
      workspace:
        server: vcenter.lab.bradlab.tech
        datacenter: HomeLab
        folder: rancher
        default-datastore: warehouseVMs
```

### Catalog Applications (Helm Charts) ###

Rancher has nice integration with helm however one kind of annoying thing (that may not be rancher's fault) is that the helm chart version updates don't really have release notes, so while you can see there is an update, you don't really know what changed.

#### Bookstack Chart ####

Bookstack helm chart seems to work nicely, however overnight it does seem that the nginx ingress changed somehow to make it look for a specific container rather than the generic service/pod. I changed it back but will have to keep an eye on it to see if it happens again. The prometheus/grafana pod seemed fine.

#### Gitlab ####

Gitlab charts are kind of crap and want to do too much to the cluster, following this https://rancher.com/how-to-run-gitlab-in-rancher-1/ instead, though it appears to be built around the 1.x version of rancher.

Gitlab is not ready for kubernetes. The chart does work, but its an old 9.x version and still uses chef to configure which really defeats the entire purpose of it being inside a container. 

Just kidding they're apparently working on a cloud native helm chart that doesn't suck. I still need to get ingress working with rancher though I was able to fudge it after deploying the chart by manually doing it. I did keep getting a bad 422 error that turned out to be because I was using http to access instead of https. Also setting value for initialrootpassword did not seem to be used. In any case at least I have a running gitlab now.

### Exec Failing ###

I think I may have hit a [bug](https://bugzilla.redhat.com/show_bug.cgi?id=1655214) in the docker that centos provided. It appears to be creating a number of issues notably exec into a container fails and some stuff causing the crashloop backoff. Hopefully this explains why the gitlab chart failed...and it might even be the reason Openshift was being such a pain.

Sure enough a fresh sync of the repos and yum update appear to have fixed it.