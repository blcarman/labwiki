Here is a brief overview of my venv's based on my recent python adventures. I like the idea of being able to keep things separate and not needing to use root. There have been a few trouble spots along the way though so here's how to deal with them.

### Tips for Django ###

* install OS pre-reqs for python mysqlclient `dnf install mariadb-devel redhat-rpm-config python3-devel`
* install mysqlclient via pip `pip install mysqlclient`

### Venvs for Ansible ###

If you have ansible code that requires a specific version venvs can make this easy. You may need to enable system libraries in the venv if you use SELinux.

Python venvs do not behave on nfs.