## Lab Equipment ##

Generally it helps to have a goal in place before buying lab equipment, otherwise you may end up buying unneeded equipment or spending time setting up equipment that does not have anything to do with your lab.

For example, there is no reason to drop money on a stack of Cisco switches if you are trying to learn linux and there is no reason to get a high end server if you are only interested in learning about routing and switching. 

That said, here is my attempt at a scalable and modular lab that will allow you to build it up as you acquire skills with a focus on adding on rather than upgrading. These are meant as a guideline or starting point. There's no reason not to go big if you find a good deal or have plans to use it down the road.

### Linux(RHCSA/RHCSE) ###

Linux has an incredibly low barrier to get started. If you have a desktop with 8+ GB of RAM and that supports hardware virtualization you're good to go. If you're looking to continuously run a lot of VMs it may be worth adding further resources to your desktop or buying a dedicated server. Ebay has tons of great deals on last gen servers like Dell R610 and R710 if you go the rack-mounted route. Otherwise look for hardware that has decent linux support and build to your specs.

### Virtualization(vmware/kvm) ###

For a dirt cheap option you can consider nested virtualization,