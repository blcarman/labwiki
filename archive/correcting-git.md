My git set up has slowly turned into an unmanagable mess over the past several months. Originally everything was nice and easy with gitea, but as I was testing things out and learning it started to get a little sloppy with branches and broken pieces. I then also ended up with some stuff that I didn't necessarily want public and so I closed off the site.

Then I started looking at gitlab and trying to figure out how I wanted to include git stuff on the blog. My first thought was to use github for the public blog-ready stuff and gitlab internally. But I already have gitea running and I like my own stuff so I think I'll continue to use gitea, until I find some reason not to. But before I announce to the world that I have a gitea server and make it publicly available again I need to do a few things. 

* enable ssl with let's encrypt certificate (goes for blog as well)
* Figure out what should be public
* Create a process for ensuring the public stuff works as expected (i.e. development branch and releases)
* Re-enforce selinux (blog issue, not git, but still)
* Secure passwords and set up ssh keys
* Upgrade gitea
* Hide users page

### Let's Encrypt the Crap Out of My Server ###

OK maybe not the whole server, but at least the ssl. Certbot was crazy easy to use, just follow their instructions.

### Re-Enforce SELinux ###

This wasn't a git issue, but just laziness when I started playing with pelican. To fix I just needed to run `restorecon -r -v /var/www`. Need to figure out best way to do this when updating content. I guess I could just create a bash/python script that does an rsync and then a restorecon. 

### What Should be Public? ###

I nuked my original ansible repo and will rebuild it with only public stufff. I will either put private stuff on the internal gitlab server or create a private repo in gitea. I also created an org and built the new ansible repo under that to make it a little more professional looking. I also used an MIT license for fun.

### How to Ensure Public Stuff Works? ###

I need to create a development branch or use tags to clarify what's stable. Since I intend to put this on my resume at some point, I want to avoid stupid commits. However I also want to avoid it looking too clean though that's less of a concern.

### Passwords and SSH Keys ###

Now that I have good ssl enabled I figured I should probably fix my terrible password and use ssh keys so I don't have to login every time I push or pull. To enable ssh though, I had to allow the git user a login shell. At first I just gave it a bash shell but then I thought maybe I should just give it plain /bin/sh, but upon further research it turns out this is just a link to bash so it doesn't really matter. I also had to enable ssh in the app.ini file and boom we have ssh working.

### Upgrade Gitea ###

Not that I needed to but I went ahead and updated gitea while I was at it. Its crazy simple, just download the new version and replace the existing gitea file and then restart the service.

### Hide Users Page ###

This one is pure paranoia and just a little bit of social anxiety. I used nginx to block the users page from people so they can't see that I only  have one user as well as what that username is to try to break into the account. I re-assigned the public git repos to the Lab org so its not listed their either. I'm sure it'll still show up in the commit history and what not, but I just mostly wanted to make it less obvious.

### My Git Workspace ###

A side benefit of the work I did above is I now have my local set up consolidated in a reasonable way. Eventually I will look at using atom with rcode (or similar) so all my development happens on a remote box further keeping my desktop clean. so for now I have the following directory structure. On both repos work is primarily done in a development branch and I'll try to merge to master monthly or when a major change justifies it.

```
~/newgit/ansible
~/newgit/my_ansible
```

### Gitlab ###

Looks like this page became a little outdated. a fair bit of time ago I started playing with gitlab at home and it just kind of stuck. Now we're back to figuring out how to cleanly make things public without exposing sensitive stuff. In home gitlab I just crammed everything into a single repo thinking I would hand pick stuff to be promoted to public, but I'm a bit lazy for that. So I think for ansible I can do a separate git repo for the inventories, and throw a symlink with a git ignore in the main repo, or maybe I even move the ansible config to the home directory default.

Another option may be to use the gitlab CI to do some magic and update only things explicitly defined. I would still need to update what it syncs, but in theory I would only have to do it once.

So we'll split things into an ansible_private (inventories and config) and a plain ansible (roles). And only the plain ansible will get mirrored to the public. I think this strategy will probably work for other repos as well. I guess I could put roles in the ansible_private repo as well and just specify the 2 directories in the config.

Mirroring to gitea doesn't seem to be working very well or documented, so it may be time to use something different there too. Just kidding, new gitlab release seems to have fixed this. Finally, I'll no longer have a mess of git repos. I have to re-initialize the git repos one last time to make sure I don't accidentally include any private stuff from past commits.

#### Kanban Board ####

Gitlab has an ok kanban board though it's currently a bit buggy. It has trouble keeping track of where items are, basically it seems like their are multiple boards that can have the same item. So if you move an item to a column on the group's board it won't necessarily move it on the project's board. It's also annoyingly difficult to add comments to issues as it can't be done without going to the issue's own page. As bad as Jira's interface is for updating issues on the same page as the board it at least has the ability.