One of the things I've struggled with since I started the lab is what exactly it should contain. As my career has progressed needs have changed. Originally the lab started out with a focus on basic networking and server management. Then last year it transformed into a very linux focused lab. And now that I'm a platform engineer the lab is getting extended a bit as I venture higher into the application stack. So here is what I consider to be a solid (if not a little overkill) lab for platform/operations engineering. Basically we'll have a fairly solid infrastructure with the ability to have various applications interact with it such as you would see in the real world.

The real fun comes in figuring out how to properly set up test environments such that I can use the infrastructure for testing things without worrying about breaking it. 

### Virtualization and Storage ###

No matter what you're doing with your lab, at some level you're going to need virtualization. For me its the foundation of the lab, without it I couldn't do any of the cool stuff I want to. 

### Active Directory and LDAP ###

### Networking ###

### Automation/Orchestration ###

### Maintenance ###

This has been a big pain point in the lab. No one likes doing routine maintenance, its just not sexy. But what's even more annoying is when I go to do something in the lab and have to spend 3 days updating dependencies because I didn't update a component for 6 months.