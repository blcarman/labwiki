### Generating License ###

Apparently infoblox won't regenerate a new temp license without fully resetting the system. So take a backup first and then run `reset all licenses`.

`set temp_license` 

do both the normal dns/dhcp/grid license as well as cloud automation (2 and 14?)

default username: admin
default password: infoblox

## Initial configuration ##

After resetting the remote console will be disabled, `set remote_console` to enable it. Log into the grid and restore the backup.