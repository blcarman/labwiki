Eventually this will all be scripted with ansible and whatever provisioning tool I decide to go with.

### Basic Install ###

1. add kickstart file with "the basics" (below is a kickstart from an install, not yet customized completely)
```

#version=DEVEL

# System authorization information

auth --enableshadow --passalgo=sha512

# Use CDROM installation media

cdrom

# Use graphical install

graphical

# Run the Setup Agent on first boot

firstboot --enable

ignoredisk --only-use=sda

# Keyboard layouts

keyboard --vckeymap=us --xlayouts='us'

# System language

lang en_US.UTF-8



# Network information

network  --bootproto=dhcp --device=eno16780032 --onboot=on --ipv6=auto

network  --hostname=template.brad.lab



# Root password

rootpw --iscrypted $6$UiWYJQ1r85BQ4MXG$MTnRlz3J5ay9w/bLrK83r8z6NAKhNCVwnyVCl6dMfrYkyNikZX6iVFnvC.I4GJilLJSaO2xF76p9lhjeX3HeD1

# System services

services --disabled="chronyd"

# System timezone

timezone America/Chicago --isUtc --nontp

# System bootloader configuration

bootloader --location=mbr --boot-drive=sda

# Partition clearing information

clearpart --none --initlabel

# Disk partitioning information

part pv.250 --fstype="lvmpv" --ondisk=sda --size=15871

part /boot --fstype="xfs" --ondisk=sda --size=512

volgroup centos --pesize=4096 pv.250

logvol /home  --fstype="xfs" --size=1020 --name=home --vgname=centos

logvol /  --fstype="xfs" --size=9795 --name=root --vgname=centos

logvol /var  --fstype="xfs" --size=4092 --name=var --vgname=centos

logvol swap  --fstype="swap" --size=953 --name=swap --vgname=centos



%packages
@^minimal
@core


%end



%addon com_redhat_kdump --disable --reserve-mb='auto'



%end

```

1.  proposed kickstart file
```

#version=DEVEL

# System authorization information

auth --enableshadow --passalgo=sha512

# Use CDROM installation media

cdrom

# Use graphical install

graphical

# Run the Setup Agent on first boot

firstboot --enable

ignoredisk --only-use=sda

# Keyboard layouts

keyboard --vckeymap=us --xlayouts='us'

# System language

lang en_US.UTF-8



# Network information

network  --bootproto=dhcp --device=eno16780032 --onboot=on --ipv6=auto

network  --hostname=template.brad.lab



# Root password

rootpw --iscrypted $6$UiWYJQ1r85BQ4MXG$MTnRlz3J5ay9w/bLrK83r8z6NAKhNCVwnyVCl6dMfrYkyNikZX6iVFnvC.I4GJilLJSaO2xF76p9lhjeX3HeD1

# System services

services --disabled="chronyd"

# System timezone

timezone America/Chicago --isUtc --nontp

# System bootloader configuration

bootloader --location=mbr --boot-drive=sda

# Partition clearing information

clearpart --none --initlabel

# Disk partitioning information

part pv.250 --fstype="lvmpv" --ondisk=sda --size=15871

part /boot --fstype="xfs" --ondisk=sda --size=512

volgroup centos --pesize=4096 pv.250

logvol /home  --fstype="xfs" --size=1020 --name=home --vgname=centos

logvol /  --fstype="xfs" --size=9795 --name=root --vgname=centos

logvol /var  --fstype="xfs" --size=4092 --name=var --vgname=centos

logvol swap  --fstype="swap" --size=953 --name=swap --vgname=centos



%packages
@^minimal
@core
open-vm-tools

%end



%addon com_redhat_kdump --disable --reserve-mb='auto'



%end

```

1. fix networking stuffs if needed
1. yum upgrade -y
1. put list of basic packages in kickstart (can specify to remove or exclude from install certain packages?)
1. disable or set selinux to permissive

```
sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config
```

1. use tmpfs for /tmp
```
systemctl enable tmp.mount
```

1. enable/configure ntp (may need to set to local ntp server)

### VMWare specific ###

Remove floppy drive when creating VM

#### VMWare Tools ####

VMWare is now suggesting to use the opensource vmware tools that are oftentimes packaged with the OS, this makes updating them all the easier since it can be done as part of the guest updates instead of being initiated from vcenter or vcloud. 

yum install open-vm-tools 

Annoying warnings/messages at boot

There are 2 general warning/messages that appear when booting a linux box. Neither of them cause any problems are than the occasional inquisitive look when booting. Both are solved relatively painlessly. 

piix4_smbus 0000:00:007.3: Host SMBus controller not enabled!

This message can be resolved by blacklisting the i2c_piix4 module.

end_request i/o error dev fd0 sector 0

The message above appears even if you remove the floppy disk from the VM. You also need to disable it in the VM’s bios.

#### VMWare Guest Customization ####

I wasn't a big fan of this feature in vcloud director 5ish because it wasn't really reliable, however in vCenter 6.5 it looks like its been polished a bit. It does require perl to be installed on the guest and optionally cloud-init. You can create customization policy in the "Policies and Profiles" section. 

### Template Creation ###

When creating images to be used for templates I like to to make them as clean as possible before converting them to a template. This is both to make things look clean as well as to keep the size on disk as small as possible. It may not seem like a lot but when you end up with 100 VMs from the same template things start to add up. In addition to the non-default defaults below.

yum clean all (clears package cache)

yum remove (old kernels)

history -c (remove history of all the embarrassing commands you ran)

change root password and put in template notes 

yum remove ivtv-firmware iwl*

yum install vim NetworkManager-tui

disable ipv6 (per http://blogoless.blogspot.com/2014/12/centos-7-ipv6-and-yum.html) 

vi /etc/sysctl.d/disable-ipv6.conf



net.ipv6.conf.all.disable_ipv6 = 1

net.ipv6.conf.default.disable_ipv6 = 1



Then issue sysctl -p

However you still try to contact IPv6 addresses, so the solution consists in adding this line

ip_resolve=4

to

/etc/yum.conf

Non-Default Defaults

While the OS maintainers generally have sane defaults there are some that don’t always make sense for real life. Here is a non-complete list of tweaks and optimizations that make life a little better. The below modifications assume running a minimal install of CentOS 7

Disable not-necessary services

`systemctl disable kdump.service`

Disable SELinux

set selinux to permissive instead of enforcing

Set I/O Scheduler

(this may not be needed) add “elevator=deadline” to kernel parameters in /etc/default/grub and run grub2-mkconfig -o /boot/grub2/grub.cfg

Filesystems and Partition Layout

Set noatime unless specifically needed

<table class="wikitable">
<tr>
<th>Mountpoint
</th>
<th>Size
</th></tr>
<tr>
<td>/
</td>
<td>8-12 GB
</td></tr>
<tr>
<td>/boot
</td>
<td>512 MB
</td></tr>
<tr>
<td>/home
</td>
<td>1-10 GB
</td></tr>
<tr>
<td>/var
</td>
<td>4 GB (include in / and partition subdirectories if needed)
</td></tr>
<tr>
<td>swap
</td>
<td>1-4 GB
</td></tr></table>

Determining Physical Memory Layout

`dmidecode -t memory`