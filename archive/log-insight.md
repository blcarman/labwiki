## Nginx Dashboards ##

According to the nginx dashboard thinger it should actually be done in the apache CLF dashboards, but I wasn't getting any data. I decided to clone a widget to test stuff when I saw this message.

```
IMPORTANT: This widget requires that the %r mod_log_config option has been specified in the agent configuration with a field_decoder to extract the URL (%U).
```

There is some VMWare magic built into this thing. I created a fairly vanilla agent group with `/var/log/nginx` specified and auto-detect set and later on I see it has created a fancy nginx agent group with all kinds of fun stuff.

```
original:
%t %i %{appname}i: %a - %u [%t] %r %s %b "%{Referer}i" "%{User-agent}i"

actual log:
192.168.50.107 - - [18/Nov/2018:16:03:41 -0600] "GET /images/action-icons/guac-config-dark.png HTTP/1.1" 200 966 "https://guac.home.lab/app.css?v=0.9.14" "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0" "-"

proposed:
%a - - [%t] "%r" %s %b "%{Referer}i" "%{%User-agent}i" "-"
```

I believe the proposed above fixed it as now it's doing it correctly, but I also deleted my original nginx agent group in case that was overriding the auto-created one.