Today I learned while deploying an NSX edge that resource pool names don't need to be unique in vcenter. So in my case I was deploying an edge and in the drop down there were multiple resource pools with the same name and in no apparent order. I had to pick one and then look at the hosts it contained to find the correct one. I don't imagine this being a problem for most organizations, but does emphasize the importance of using unique names.

### Python NSX library ###

The ansible modules vmware has created for NSX don't yet include the load balancer pieces, and while someone has submitted code for this, it doesn't appear to be done well. So we're going to tinker with the python library itself and see what kind of trouble we can get into.

```
dnf install python3-libxml2 libxml2-devel libxslt-devel
pip install pynsxv
```

It appears this python library may only be compatible with python 2.7 and hasn't been updated for a while. 
```
(mypy) [brad@scandium pynsxv]$ pynsxv -h
Traceback (most recent call last):
  File "/home/brad/mypy/bin/pynsxv", line 11, in <module>
    load_entry_point('PyNSXv==0.4.1', 'console_scripts', 'pynsxv')()
  File "/home/brad/mypy/lib64/python3.6/site-packages/pkg_resources/__init__.py", line 480, in load_entry_point
    return get_distribution(dist).load_entry_point(group, name)
  File "/home/brad/mypy/lib64/python3.6/site-packages/pkg_resources/__init__.py", line 2693, in load_entry_point
    return ep.load()
  File "/home/brad/mypy/lib64/python3.6/site-packages/pkg_resources/__init__.py", line 2324, in load
    return self.resolve()
  File "/home/brad/mypy/lib64/python3.6/site-packages/pkg_resources/__init__.py", line 2330, in resolve
    module = __import__(self.module_name, fromlist=['__name__'], level=0)
  File "/home/brad/mypy/lib64/python3.6/site-packages/pynsxv/cli.py", line 28, in <module>
    import library.nsx_logical_switch as lswitch
ModuleNotFoundError: No module named 'library'
```

Working much better with python 2. I did have to configure vxlan in order to create a "transport zone", maybe some day I'll look at the non-edge parts of nsx as I think that's more what vmware is pushing.

And of course its missing one of the key things I wanted to use it for, the ability to enable/disable members of a pool. It's only able to add or delete, which might be ok, but not ideal. Looking at the API docs the API does support it, so I guess maybe we'll go that route instead. That will probably be a bit more useful/versatile in the long run as well.

Yep, screw trying to sort out the pynsxv crap, using the API directly seems to be the way to go. This may even be the thing I've been searching for to finally write my own ansible module. Since there is already a module for other nsx stuff, I could focus on the LB to start with. 

[API reference guide](https://docs.vmware.com/en/VMware-NSX-for-vSphere/6.4/nsx_64_api.pdf)