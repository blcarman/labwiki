This will probably fail spectacularly but here goes nothing. Luckily I checked the NSX compatibility before starting as I was only on 6.4.0 and 6.7 requires 6.4.1. There will also be a scary moment as to whether my hosts will handle the 6.7 upgrade since technically the cpu's are no longer supported.

The NSX upgrade went relatively smoothly. I forgot to power down VMs so the HA maintenance mode activity kind of threw a wrench into things but I got that sorted out easily enough. 

The vcenter upgrade however was a different story. It would seem it got upset about the administrator@vsphere.local password being changed or some other trivial thing with the vsphere.local scope. Luckily I just had to power off the new failed vcenter and power on the original one. For some reason the vsphere-client service failed to start so I had to stop it (even though it wasn't started) and then start it.

From `analytics_firstboot.py_8789_stderr.log`:

```
INFO:root:Register service with LS.
2018-06-30T17:33:45.883Z  Failed to register Analytics Service with Component Manager: SoapException:
faultcode: ns0:FailedAuthentication
faultstring: Invalid credentials
faultxml: <?xml version='1.0' encoding='UTF-8'?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><S:Fault xmlns:ns4="http://www.w3.org/2003/05/soap-envelope"><faultcode xmlns:ns0="http://docs.oasis-open.org/ws-sx/ws-trust/200512">ns0:FailedAuthentication</faultcode><faultstring>Invalid credentials</faultstring></S:Fault></S:Body></S:Envelope>
2018-06-30T17:33:45.921Z  Traceback (most recent call last):
  File "/usr/lib/vmware-analytics/firstboot/analytics_firstboot.py", line 161, in register_with_cm
    cloudvm_sso_cm_register(keystore, cisreg_spec, key_alias, dyn_vars, isPatch=is_patch)
  File "/usr/lib/vmware-cm/bin/cloudvmcisreg.py", line 700, in cloudvm_sso_cm_register
    serviceId = do_lsauthz_operation(cisreg_opts_dict)
  File "/usr/lib/vmware/site-packages/cis/cisreglib.py", line 1044, in do_lsauthz_operation
    ls_obj.register_service(svc_id, svc_create_spec)
  File "/usr/lib/vmware/site-packages/cis/cisreglib.py", line 340, in add_securityctx_to_requests
    with self._sso_client.securityctx_modifier(self._stub):
  File "/usr/lib/python3.5/contextlib.py", line 59, in __enter__
    return next(self.gen)
  File "/usr/lib/vmware/site-packages/cis/cisreglib.py", line 240, in securityctx_modifier
    self._update_saml_token()
  File "/usr/lib/vmware/site-packages/cis/cisreglib.py", line 223, in _update_saml_token
    self._uname, self._passwd, token_duration=120)
  File "/usr/lib/vmware/site-packages/pyVim/sso.py", line 317, in get_bearer_saml_assertion
    ssl_context)
  File "/usr/lib/vmware/site-packages/pyVim/sso.py", line 256, in perform_request
    raise SoapException(fault, *parsed_fault)
pyVim.sso.SoapException: SoapException:
faultcode: ns0:FailedAuthentication
faultstring: Invalid credentials
faultxml: <?xml version='1.0' encoding='UTF-8'?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><S:Fault xmlns:ns4="http://www.w3.org/2003/05/soap-envelope"><faultcode xmlns:ns0="http://docs.oasis-open.org/ws-sx/ws-trust/200512">ns0:FailedAuthentication</faultcode><faultstring>Invalid credentials</faultstring></S:Fault></S:Body></S:Envelope>

2018-06-30T17:33:45.928Z  Exception: Traceback (most recent call last):
  File "/usr/lib/vmware-analytics/firstboot/analytics_firstboot.py", line 161, in register_with_cm
    cloudvm_sso_cm_register(keystore, cisreg_spec, key_alias, dyn_vars, isPatch=is_patch)
  File "/usr/lib/vmware-cm/bin/cloudvmcisreg.py", line 700, in cloudvm_sso_cm_register
    serviceId = do_lsauthz_operation(cisreg_opts_dict)
  File "/usr/lib/vmware/site-packages/cis/cisreglib.py", line 1044, in do_lsauthz_operation
    ls_obj.register_service(svc_id, svc_create_spec)
  File "/usr/lib/vmware/site-packages/cis/cisreglib.py", line 340, in add_securityctx_to_requests
    with self._sso_client.securityctx_modifier(self._stub):
  File "/usr/lib/python3.5/contextlib.py", line 59, in __enter__
    return next(self.gen)
  File "/usr/lib/vmware/site-packages/cis/cisreglib.py", line 240, in securityctx_modifier
    self._update_saml_token()
  File "/usr/lib/vmware/site-packages/cis/cisreglib.py", line 223, in _update_saml_token
    self._uname, self._passwd, token_duration=120)
  File "/usr/lib/vmware/site-packages/pyVim/sso.py", line 317, in get_bearer_saml_assertion
    ssl_context)
  File "/usr/lib/vmware/site-packages/pyVim/sso.py", line 256, in perform_request
    raise SoapException(fault, *parsed_fault)
pyVim.sso.SoapException: SoapException:
faultcode: ns0:FailedAuthentication
faultstring: Invalid credentials
faultxml: <?xml version='1.0' encoding='UTF-8'?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><S:Fault xmlns:ns4="http://www.w3.org/2003/05/soap-envelope"><faultcode xmlns:ns0="http://docs.oasis-open.org/ws-sx/ws-trust/200512">ns0:FailedAuthentication</faultcode><faultstring>Invalid credentials</faultstring></S:Fault></S:Body></S:Envelope>

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "/usr/lib/vmware-analytics/firstboot/analytics_firstboot.py", line 260, in main
    fb.register_with_cm(analytics_int_http, is_patch)
  File "/usr/lib/vmware-analytics/firstboot/analytics_firstboot.py", line 172, in register_with_cm
    problem_id='install.analytics.cmregistration.failed')
cis.baseCISException.BaseInstallException: {
    "resolution": {
        "translatable": "Please search for these symptoms in the VMware Knowledge Base for any known issues and possible resolutions. If none can be found, collect a support bundle and open a support request.",
        "localized": "Please search for these symptoms in the VMware Knowledge Base for any known issues and possible resolutions. If none can be found, collect a support bundle and open a support request.",
        "id": "install.analytics.cmregistration.failed.res"
    },
    "componentKey": "analytics",
    "problemId": "install.analytics.cmregistration.failed",
    "detail": [
        {
            "translatable": "Analytics Service registration with Component Manager failed.",
            "localized": "Analytics Service registration with Component Manager failed.",
            "id": "install.analytics.cmregistration.failed"
        }
    ]
}

2018-06-30T17:33:45.928Z  VMware Analytics Service firstboot failed
```

Re-trying the upgrade 8/11/18. Upgraded to latest patch of 6.5 first. Or not, from laptop failing due to not liking the ova. 

Re-trying again 10/11/18. Downloaded new version of the ISO, but still failing the ova thing on the laptop. Desktop got further, we'll see if it gets all the way...and it worked woohoo.

Had to re-register vcenter with nsx.

Oh cool, VUM is included with VCSA now, so I can do host upgrades through vcenter.