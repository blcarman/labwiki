I lost some momentum after I got bookstack migrated. Next up is Emby and along with it a hopefully more standard approach to doing this. 

1. determine if existing docker image is usable (it's probably not)

## Use/Create Image ##

Docker hub is a lot like ansible galaxy, there's a bunch of cool stuff that is pretty much useless if you deviate the least bit from their expected usage. So I'll be doing the image myself it looks like. It also makes me wonder whether I could make a generic image that others could use, or if mine will be limited to my openshift setup.

I find it helpful to run a dummy container in podman to test commands as you write the Dockerfile. Commonly this looks like: `podman run -it alpine:latest /bin/sh`.

Once the Dockerfile is ready, from the same directory you can run `podman build -t emby .`

Push the image to the local openshift registry 

```
oc login
podman login -u bradmin -p $(oc whoami -t) --tls-verify=false docker-registry-default.apps.bradlab.tech
podman tag emby docker-registry-default.apps.bradlab.tech/test/emby:1.0
podman push --tls-verify=false docker-registry-default.apps.bradlab.tech/test/emby:1.0
```

Podman doesn't seem to re-use layers (or I'm not doing it correctly).

A little half step here, deployed the image by itself to ensure it would run. It looks good but obviously needs nfs configured for the media libraries

## Template Creation ##

I stole my bookstack template and modified it accordingly. Mainly removed the persistent volumes and add plain volumes since the nfs stuff isn't dedicated to the container in this case. 

`oc create -f emby.json`

template generates the following error:

```
Error creating: pods "emby4-1-" is forbidden: unable to validate against any security context constraint: [spec.volumes[0]:
Invalid value: "nfs": nfs volumes are not allowed to be used spec.volumes[1]: Invalid value: "nfs": nfs volumes are not
allowed to be used]
```

Had trouble getting template to use my nfs_test scc, so I added nfs to the restricted policy. This in turn created an error regarding the selinux level. I updated the emby template with the selinux level. Security context setting in template doesn't appear to be getting used, so I edited the deployment via the webui. 
Now I'm getting this error in the webui and some logs in `/var/log/messages` on ocnode3
```
Unable to mount volumes for pod "emby8-1-ld4fq_test(aa9be021-e23f-11e8-b895-005056827a3a)": timeout expired waiting for
volumes to attach/mount for pod "test"/"emby8-1-ld4fq". list of unattached/unmounted volumes=[emby8-movies emby8-shows]
```

Also noticed some other errors on ocnode3 that I thought would have disappeared after gluster sorted itself out, so I'll have to dig into that later as well.

Looks like nfs might have been contributing to the mounting issue, I had not allowed the trust vlan to mount movies/shows. Now other issues with openshift are creating different problems.