tmux config location `~/.tmux.conf`

tmux <2.1 mouse support `setw -g mode-mouse on`

tmux 2.1+ `set -g mouse on`

copy/paste: shift + select with mouse, then shift + middle click to paste (for os level)

copy: 

tmux attach -d (attach and detach all other connections)

c: new window

https://gist.github.com/MohamedAlaa/2961058

change prefix key (helpful to use differente prefix keys across local and remote tmux sessions, also recommend changing local instead of remote config so you don't have to worry about remote servers being out of sync if they use the default)

```
unbind C-b
set -g prefix C-a
bind C-a send-prefix
```

https://stackoverflow.com/questions/27613209/how-to-automatically-start-tmux-on-ssh-session

tmux cheat sheet

ctrl b - prefix key (comparable to normal mode of vim)
```
c - new window
n - next window
p - previous window
& - kill window


% - split pane vertically
" - split pain horizontally 
x - kill pane
space - toggle pane layout

```

## Agent Forwarding ##

https://werat.github.io/2017/02/04/tmux-ssh-agent-forwarding.html

I may have actually gotten ssh forwarding working correctly...I think

(don't forget to add `ForwardAgent yes` to your ssh config and `ssh-add` to your bashrc

```
# .tmux.conf
set -g update-environment -r
setenv -g SSH_AUTH_SOCK $HOME/.ssh/ssh_auth_sock
```

```
# .bashrc
if [[ -S "$SSH_AUTH_SOCK" && ! -h "$SSH_AUTH_SOCK" ]]; then
    ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock;
fi
export SSH_AUTH_SOCK=~/.ssh/ssh_auth_sock;
```