## Installing Katello 3.8 ##

```
[root@katello ~]# foreman-installer --scenario katello
Resetting puppet server version param...
Installing             Done                                               [100%] [..............................]
  Success!
  * Katello is running at https://katello.lab.bradlab.tech
      Initial credentials are admin / n4Dp5JzwXw48qTXM
  * To install an additional Foreman proxy on separate machine continue by running:

      foreman-proxy-certs-generate --foreman-proxy-fqdn "$FOREMAN_PROXY" --certs-tar "/root/$FOREMAN_PROXY-certs.tar"
  The full log is at /var/log/foreman-installer/katello.log
Upgrade Step: remove_legacy_mongo...
yum install -y -q rh-mongodb34-syspaths finished successfully!
```

### First Impressions ###

Same basic stuff as before but looks much more polished. Side menu layout instead of the bar across the top which I think works a lot better. It has added some container stuff but probably not scalable. In short, it's built to mainly do classic sysadmin stuff well while also throwing in a little bit of the future. The only real thing I would love to see is the ability to completely replace the puppet stuff with ansible counterparts. In any case it looks like it has grown up and might be ready for production use.

One of the cool things foreman has is multi-tenancy including multiple locations. However this doesn't carry over well to the katello content. So I accidentally synced the centos repos to the default instead of the lab organization. I decided to just use the default org since I really don't have a need for multiple orgs.

### Provisioning ###

Provisioning pretty much just worked out of the box. I could sync the centos kickstart tree but it also worked by just pointing to the local centos base repo.

#### Active Directory Realm ####

Now that I got LDAP authentication for the foreman gui working I thought I would make the next logical jump and set up an AD realm to join servers during provisioning. It would seem this is not quite officially supported yet and documentation is a little lacking. So I'll try to document the steps I used. It is apparently critical to use uppercase when referring to the domain. Also not sure if it was strictly necessary but as part of figuring things out I joined the katello server to the domain.

1. `yum install rubygem-smart_proxy_realm_ad_plugin krb5-workstation`
1. edit `/etc/foreman-proxy/settings.d/realm.yml`: set enabled to true and provider to `realm_ad`
1. delegate control to create/delete computer objects to Servers OU in Active Directory
1. create keytab (from Arch wiki) and chown to foreman-proxy user
```
ktutil
addent -password -p username@EXAMPLE.COM -k 1 -e RC4-HMAC
- enter password for username -
wkt username.keytab
q
```
1. edit `/etc/foreman-proxy/settings.d/realm_ad.yml` with appropriate values
1. add realm in Foreman webui
1. make sure subnet dns servers have been updated to use the domain controller(s)

and it works. Even outside katello `realm join` makes this crazy simple to do. 

#### Template Bug or Not I guess ####

One of Katello/foreman's weak points is documentation, they have a lot of the basics covered enough that you can figure it out when its working correctly. However, now I'm facing a weird bug with the individual templates rendering correctly for a host, but when put together as a single kickstart file its using the wrong template. Documentation just kind of says its "dynamically" created which seems to be true but how the hell is it using 2 different templates... Finally found the little fucker. As it turns out the "finish" template is not used unless a specific set of circumstances are met, so really its just a waste of fucking space. Pretty much ready to never forgive katello/foreman for that stupid shit.

### Registration ###

I once again forgot to sync the katello agent repo and unfortunately the documentation is a little fuzzy about it, basically you need to lop off the rpm package from the url to get the repo url, i.e. https://yum.theforeman.org/client/1.20/el7/x86_64/.

Also need to determine how to disable repos when system is registered. Outside of the sed option to changed enabled to 0, I saw another suggestion to do the blank file thing. Note, spacewalk has the same issue. I was getting a weird duplicate/out-of-date error for epel stuff that went away after I disabled the epel repo and did a `yum clean all`.

### Ansible Smart Proxy ###

Initially I wanted to set up the ansible proxy on the same host but since we have other jumpboxes at work I figured I should do that here so I get the cert stuff right. If I was really going to do this right I would go ahead and sync the foreman repo and make an activation key that had it enabled for the katello hosts, but we'll leave that for another day. just kidding, apparently it uses the foreman_installer so it's going on the katello server.

I had to manually clone the git repo and edit /etc/ansible/ansible.cfg with the roles folder, kind of unfortunate. Also the documentation says there is a variables section but its conveniently lacking in the web UI and no screenshot in the docs.

After importing roles I was getting an error that it couldn't find the roles in a bunch of weird locations. I had to add the roles_path to the foreman_proxy user's ansible.cfg. And even then we're still having some issues, but we're almost there. At some point this started working, maybe it was after I created the ssh key for the foreman-proxy and then provisioned a host. I might add a git pull cron job to make it suck less but otherwise I think we're in business. 

### Existing VMs ###

In the compute resource you can "associate VMs" to add VMs that are registered to katello but were not necessarily provisioned by it. This allows you to reboot the VM and use the console from within katello.

### Upgrading ###

Upgrading appears relatively easy, though I'm only going a single point release from 3.8 to 3.9. The foreman-installer script seems to take care of everything on its own which is pretty nice.

### Syncing Repos ###

Pulp might still have some issues. Initial sync of repos worked fine, but when I tried to sync the whole group the epel one got stuck. I cancelled and re-ran just the epel one and same deal. Looks like there might be a bug, https://pulp.plan.io/issues/2979 or https://community.theforeman.org/t/pulp-tasks-stuck-as-waiting-unstarted-incomplete/9912.

I commented out the `PULP_MAX_TASKS_PER_CHILD=2` and did a `katello-service restart`. After katello restarted I tried the sync again, this time no new packages, but same mongodb warning as earlier and referenced above. There was also a warning to run daemon-reload so I went ahead and did that, though it does seem the task itself is at least maybe working now. Will wait for it to finish or hang before restarting katello services again. Task finished so the restart must have been enough. So pulp seems like it might still have some bugs but at least it sorted itself out with minor intervention.

#### Promoting Content Views ####

While Katello does have the hammer CLI, it's a bit clunky. It has a built in function to sync repos on a schedule, but then it doesn't allow you to automatically promote them to an environment. The first problem is it can take a long time to publish a new version. Then you have to figure out what is the latest version is and finally promote it. There appeared to be a feature request for a "promote latest" option but in typical Red Hat fashion it just kind of got closed while blaming another ticket.

Aha! I can promote from an environment and the "Library" always gets the latest version so it really is as easy as I was hoping. 

```
hammer content-view publish \
 --organization "Default Organization" \
 --name "Test"

hammer content-view version promote \
 --organization "Default Organization" \
 --content-view "Test" \
 --to-lifecycle-environment "Test" \
 --from-lifecycle-environment "Library"
```

### CentOS 7.6 ###

I synced the repos to get the new centos 7.6 stuff and now builds are having some trouble. It started with an xfs error similar to the one reported [here](https://bugzilla.redhat.com/show_bug.cgi?id=1169034) which pointed to the vmlinuz and initrd not matching. Sure enough `/var/lib/tftpboot/boot` had the new vmlinuz but not initrd based on modified date. So I simply moved the vmlinuz to vmlinuz.bak and triggered a rebuild. After that we got some dracut syncing issues so I then also moved the initrd and that seems to have fixed things even though the new initrd had the same size.

At this same time I tried to do some package optimizations and cloned the default provisioning template. I tried adding `--nodefaults` to the core group but as others have reported this seems broken currently. I also tried adding `katello-host-tools` but apparently I did that before it had access to that repo so I will need to move that to the finishing template.

Aha! the anaconda installer apparently automatically adds @core and so the one listed in the kickstart is actually sort of redundant. For the `--nodefaults` to work you need to set `packages --nocore --nobase` and then the `@core nodefaults` per [this bug report](https://bugs.centos.org/view.php?id=7388).

### LDAP Authentication ###

LDAP authentication pretty straightforward but does require using full username (i.e. user@domain).

### OpenSCAP ###

At this point I think have just about everything in katello that I would want. The exception being the openscap stuff and possibly the bootdisk. I suppose I should also organize my notes into a cohesive document at some point as well.

## vs Spacewalk ##

At this point I think its safe to say that Katello beats spacewalk. It might be more complex but it also brings a lot more to the table and is mature enough at this point for production.

Though the more I use both the more I realize both are a fair amount of shitty. Katello wins in features but loses in complexity while Spacewalk has fewer features but seems to at least be mature enough that it doesn't fall over if the wind blows the wrong way. Katello's puppet can of course be replaced by ansible, so really the only thing it brings is provisioning and content views. And while it doesn't necessarily suck on provisioning it's way too heavy into the puppet to be truly good there. 

So what's the best way to do this OS management server thing? At this point I would probably recommend spacewalk (with kickstartable tree) for package management and then handle the actual provisioning/kickstart with ansible and either AWX or maybe just spin your own webui to make it look nice.

## Addendum on Provisioning ##

After trying out packer (and still in progress terraform) Katello's provisioning is not really a feature. Literally the only thing Katello brings to the table at this point is versioned repos. And really if you re-deployed VMs every month instead of updating that no longer matters either. Well, it was nice while you lasted Katello.