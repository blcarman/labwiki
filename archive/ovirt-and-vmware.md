I've been trying to decide between migrating my ovirt set up over to vmware for a little while now, but then I got to thinking why not just run vmware inside ovirt or vice versa. This shouldn't be an issue for the most part, but the one area I'm curious about is networking, that is what's the best way to mimic trunks inside the esxi host VMs. Maybe OVN can take care of that.


### Upgrading to 4.2 ###

It's still only in beta, but I figured I'd give it a try just for fun and figured if it does suck I'll just go full vmware. The ovirt documentation requires a few leaps of faith but is generally fairly reasonable. 

1. enable the new repo, `yum install http://resources.ovirt.org/pub/yum-repo/ovirt-release42-pre.rpm`
1. remove the old ovirt-engine package since it conflicts with the 4.2 one
1. run `engine-setup`