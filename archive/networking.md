I have hopefully dialed in my lab network fairly well with this iteration. That's not to say I don't see it changing, but hopefully any changes will be additive in nature. For a long time I only had a single switch and it was fine but did create some annoying limitations. Primarily it meant having to run multiple cables between the office and the basement or do without connectivity. Well that is now gone.

# VLANs #

I used to have a bit of a problem with vlans, that is I had a lot of vlans. I guess I still have a problem with vlans, but I think I've got it dialed into a fair number. At some point I'll fit this into a table but for now I've got the following vlans: home lan, workstations, trust, dmz, and wan. I still need to add a management vlan and move a few of the physical servers over to their proper vlan, but its getting close. I'll eventually add one for storage and possibly another for testing stuff but for now we have a good number. I guess as I add more firewalls into the mix they'll need their own as well.

## Management (VLAN 2) ##

In a push to separate lab infrastructure from lab usage I'm finally setting up the management vlan (and adding my network devices to monitoring). This vlan will be used for network devices, ipmi, and esxi hosts. This will also hopefully allow me to block ssh on the home network interfaces. 

## Trust (VLAN 30) ##

General server usage, still has some that need to be moved over to DMZ

## Workstations (VLAN 50) ##



# The Home Network #

For now I've got an Asus router for my general home network. I did move the wan connection to go through the SG300 to make it easier to move to either a physical or virtual opnsense at some point in the future. I'm using the home lan network as the wan network on the main lab firewall. There are a few requirements to keep the home network stable so for now it remains boring and rather limited. The switch configuration for the home network looks like:

    interface gigabitethernet1
     description modem
     spanning-tree portfast
     switchport mode access
     switchport access vlan 3
    !
    interface gigabitethernet2
     description asus_rtr_wan
     spanning-tree portfast
     switchport mode access
     switchport access vlan 3
    !
    interface gigabitethernet20
     description asus_rtr_lan
     switchport mode access
    !

# The Lab Network #

On the lab side of things I have 2 trunks, the first going to the 3560G switch in the basement and the other going to my desktop. The main reason for this is so I can run everything I need to from my desktop in case I want to cut down on power costs. My desktop's default gateway is going through the virtual lab firewall however it does have the home lan configured in case I break the lab firewall.

    interface gigabitethernet17                                
     description titanium                                      
     switchport trunk allowed vlan add 2,10,20,30,40,50        
    !                                                          
    interface gigabitethernet18                                
     description rackswitch                                    
     switchport trunk allowed vlan add 2,10,20,30,40,50        
    !

The desktop network is complex enough to justify its own post. To cover it briefly its using systemd-networkd with various layer 2 vlans and bridges for the VMs as well as some routed interfaces for vagrant.