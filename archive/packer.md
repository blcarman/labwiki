Looking at packer to create VM templates. Officially packer supports direct esxi via ssh which is kind of insane considering what they're trying to do. Alternatively they do support workstation as well which is probably the better way to do things until you try to automate that with Jenkins or similar as well. So poking around I found a plugin that theoretically uses vCenter via the API so we're going to try it out. Also one of my biggest complaints so far with Hashicorp is the oddly written documentation. They try to show that it's simple, but then they use AWS and big examples for everything. Rather than saying "here's an example builder section" that you then combine with "the next section" and showing the examples as building blocks, they just assume you want to read a novel and do things on AWS. 

https://github.com/jetbrains-infra/packer-builder-vsphere

user variables use backticks not single quotes
```
{{user `vcenter_username`}}
```

Boom, looks like we're in business and this is cool. It makes me feel like I'm in the Canada of IT Operations, like I'm a few years behind the rest of the world on stuff.

So the plan is create a packer template for building an image from scratch as well as one that can update an existing image. Then I can have gitlab or whatever update the image every month automatically.

ansible provisioner does some interesting stuff, apparently instead of just running normal ansible it does some sort of ssh proxy thinger, so I might just run it as a local shell provisioner or something. Also it'll fail if you delete the user your using for ssh as part of the provisioning (was trying to solve the problem of not leaving some automatic install user around afterward). So perhaps we run the ansible and stuff as root, but then disable password ssh at the end of provisioning?