I finally started looking into this and it apears `script` and `scriptreplay` are the best for what I'm wanting. They aren't necessarily sufficient for a compliance/security type of auditing, but for a "what the hell did I do to make that work?" situation it should work nicely. Guacamole also implements a similar method for its ssh recording so it'll behave similarly if I decide to set it up as well. The last decision that's left is whether I record all sessions or just when I want to...

Main concern with recording every session is if I go to replay one it'll essentially re-record it. However if I don't I'll probably forget to record things when I need to. Also do I script at the client/bastion or on each server individually?

Sample script to put in /etc/profile.d to enable recording on all sessions
```
if [ "x$SESSION_RECORD" = "x" ]
then
timestamp=`date "+%m%d%Y%H%M"`
output=/var/log/sessions/session.$USER.$$.$timestamp
SESSION_RECORD=started
export SESSION_RECORD
script -t -f -q 2>${output}.timing $output
exit
fi
```

References:

https://www.2daygeek.com/automatically-record-all-users-terminal-sessions-activity-linux-script-command/