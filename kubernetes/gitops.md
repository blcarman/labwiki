I started looking at gitops originally because there wasn't an easy way to automatically provision gitlab runners and I didn't want to have to give high level access to an agent.

One problem is that there is always some amount of bootstrapping/provisioning required. How do you prevent the bootstrapping process from being too manual?

There are a series of blog posts on The New Stack, https://thenewstack.io/author/viktor-farcic/, calling out some problems with gitops tools. Of course it doesn't really offer any solutions since they want you to go look at the sponsor's website and buy their product. That said it does have some good points.

Here's what I take away. Gitops is not an end to end solution (and any tool that says it does that is lying). There will always be some amount of initial setup to get the application to a state that can be further maintained by gitops. This is true for any method out there really, the trick is minimizing the manual parts of the bootstrapping process so they can be repeated. I suppose even if flux/argo were deployable by manifests that could be plugged into fleet, there is still the initial fleet configuration needed, so is it really any different having a classic CI pipeline run `flux bootstrap` vs having that CI pipeline run some api call to configure fleet? 

Basically I think my idea of structuring a git repo with a deployments folder that contains everything managed with a gitops flow, and a provisioning folder that handles all the scripts/code needed to get things ready for gitops is a fairly ok way of doing things. CI will still need high level access to do certain things, but you could separate those agents from your app build agents making it easier to secure.

I also keep seeing an argument that gitops does not require kubernetes, but they never answer the question of how to do gitops at a server level.

I've been disappointed in the lack of templating support, but now I realize maybe I should be using configmaps for those kinds of things instead. 

I think this is common across the options below, but how do handle promoting through environments? It seems like having a branch per environment is the way to go, but I'm not a huge fan of that approach since it makes it very easy to lose track or skip an environment when merging things. It's also not as easy to navigate as a directory. Also even when using environment branches there will still be a folder structure for environment customizations, so that will add another layer of complexity and make it difficult to test. It appears Argo is in the process of solving this with a [progressive rollout container](https://github.com/Skyscanner/applicationset-progressive-sync)

Since gitops is naturally a pull model it can take time waiting for the next pull cycle to run, and then you have to review logs for the task you specifically care about.

## Fleet ##

I'm sure the Rancher team has some plans for fleet but I'm kind of curious why they're rolling their own instead of extending flux or argo.

* included with rancher
* stupid simple to get started, just put manifests in a git repo and boom
* monitors git branch or tag
* no ability to control ordering/dependencies (is this a problem?)
* no ability to control retries/failures
* not able to include multiple things in a single bundle, i.e. if i have 5 helm charts each would end up with their own fleet.yaml, will likely need some sort of templating to simplify adding new clusters
* would be cool if it could get variables from secrets/configmaps, so not quite sensitive data like email addresses wouldn't have to be leaked, but I guess I can just keep my repo private
* will still have a few manifests to deploy separately as part of cluster bootstrap process (i.e. secrets)
* with all the caveats is fleet even worth it at the cluster level? are we better off keeping everything in ansible?
* stacking yamls in a single file does not appear to work (but doesn't throw an error either)

## Flux ##

* can automatically deploy new images based on tag pattern
  * what's the best way to tag images for an environment? I suppose it would just be handled by the test process
* have to use flux cli to bootstrap clusters
  * bootstrap will both setup flux on the k8s cluster and the git repo used to manage resources going forward
* flux cli is pretty nice
* no webui, but they have made some grafana dashboards to go with their prometheus metrics
* allows ordering of when resources are deployed (which was the primary reason fleet was not usable)
* `flux create` command doesn't seem to update your flux git repo even though it adds resources to the cluster, seems like the opposite of what we want, what am I missing?
* does have the ability to install subset of components (might be useful for small digitalocean droplets)

Now that I've successfully deployed an app with flux I can say I really like it. Learning curve is not terrible and there it seems to be handle all the use cases I can think of at the moment. I do still need to test out the automatic image updates but I'm sure that will go smoothly. I could move some of my rke2 auto deploying manifests to flux as well, but won't worry about it too much. 

## Argo ##

* appears to be installable via manifest but then requires registering clusters...
* but still has its own cli/webui/api and clicky clicky process to add apps/repos
* when this is released this might cause Argo to leapfrog flux https://github.com/Skyscanner/applicationset-progressive-sync

## Ansible (not really gitops) ##

* community.k8s module documentation is extremely lacking, it seems to do a mix of things on the controller and the target and does not say what all happens on the controller, nor do the examples reflect that... some of this was just bugginess in the version that comes with ansible, installing the community.k8s collection individually makes it a lot better
* immediate feedback (where possible)
