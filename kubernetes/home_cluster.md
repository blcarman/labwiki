I've been trying to get a stable cluster running at home for a while now. This shouldn't be too difficult, but I am my own worst enemy when it comes to stability. Once I get things in a good state I like to start playing fast and loose with testing (or lack thereof) and inevitably do something that breaks my cluster in a new and spectacular way. Even using gitops tools like flux, I ended up skipping any sort of test cluster... I mean the only really "production" thing I had on it was netbox. But of course to provision a fancy new replacement cluster I needed to provision some IPs in netbox...

So I moved VM IP management to dhcp reservations, and I guess I can just hardcode the cluster VIP (if I decide to even keep kube-karp). Most of the reason for netbox was to handle nebula IPs, but I will likely replace nebula with netmaker/wireguard. It also might be time to build a vanilla cluster instead of relying on RKE2/K3S.

I think I found the right compromise between an external load balancer and kube-karp, configure keepalived and haproxy as static pods on the cluster. https://github.com/kubernetes/kubeadm/blob/master/docs/ha-considerations.md#option-2-run-the-services-as-static-pods. I kind of wish I remembered the reason for using kube-karp instead of kube-vip initially, thought maybe it was host level permissions but obviously they would both need fairly similar permissions. I might have also had issues with getting it working because I was trying to use the auto-deploying manifests feature of RKE2 instead of static pods. I think kube-vip with static pods is worth a try.

kube-vip has proven to be a bad idea. I thought it would use multicast like keepalived but it seems it either uses k8s or raft in which case the peers have to be explicitly defined. So now we're on to keepalived. Also it seems like kub-vip would bind to a specific port which isn't exactly what I wanted.

I was getting ready to stand up rancher and realized with the node taints, I will need another VIP for nginx/rancher...unless I allow nginx to run on the control plane as well. I think that's the way I'm going to do it for now.


helm install rancher rancher-latest/rancher \
  --namespace cattle-system \
  --set hostname=testranch.lab.bradlab.tech \
  --set bootstrapPassword=admin
