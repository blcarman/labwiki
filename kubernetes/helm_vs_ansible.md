# Helm vs. Ansible: But Not Really #

I've deployed plenty of stuff with helm but have yet to actually write my own chart. I just keep falling back to ansible or applying static yaml files. Obviously ansible and helm are not exactly competitors though there is some overlap, thus the comparison. 

To start off, I really do like jinja2, however the only other templating language I've spent time with is ERB (embedded ruby) in chef, and that will make anything else look good. So the question will be is helm's templating on par or better than jinja2?

The next question is how much side stuff can helm do? Right now I can have ansible retreive and configure secrets as well as the app itself after deployment. Can helm do those things as well?

Ultimately I expect at the end of this I will end up using ansible to deploy the helm charts, hopefully getting the benefits of both.

## Diving In ##

First thing that jumps out, helm is using go templates. So yay its not some one-off they made and will be useful elsewhere. 

While there appear to be template "functions" it doesn't seem like they will be as full as I was hoping. That is it seems like it will still be best doing secrets separately rather than expecting helm to be able to pull them in via a helper function.

## An end to end example ##

* a local k8s for development/testing
* helm chart stored in a cloud-based registry
* nebula vpn client since I already have the yaml

## container registry ##

I decided to add in some terraform to manage the container registry. It's been a while since I've done anything with Azure so I got to start another 12 month free tier.

## Helms Alee ##

Side note Helms Alee is a great band. 