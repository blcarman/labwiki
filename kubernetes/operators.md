---
title: Kubernetes Operator Hello World
description: figuring out my first kubernetes operator
published: true
date: 2023-01-01
tags: WIP, kubernetes, operators, golang
editor: markdown
---

## Objective ##

Write the most basic operator I can to gain an understanding of how they work and compare to other tools such as terraform/ansible

## Notes ##

OK, so I want a kubernetes operator that is able to provision, update, deprovision an IP in netbox. After that, it should also periodically check netbox to ensure the netbox state hasn't changed outside of the k8s resource. Also, let's have it store some metadata about the k8s cluster it came from, so if another cluster tries to manage the same IP, it gets an error about it being managed by a different cluster.

https://developers.redhat.com/articles/2021/09/07/build-kubernetes-operator-six-steps

FATA[0003] failed to create API: unable to run post-scaffold tasks of "base.go.kubebuilder.io/v3": exit status 2

```bash
operator-sdk create api --version=v1alpha1 --kind=Traveller
Create Resource [y/n]
y
Create Controller [y/n]
y
Writing kustomize manifests for you to edit...
Writing scaffold for you to edit...
api/v1alpha1/traveller_types.go
controllers/traveller_controller.go
Update dependencies:
$ go mod tidy
go: warning: "all" matched no packages
Running make:
$ make generate
mkdir -p /home/brad/go/src/operators/bin
test -s /home/brad/go/src/operators/bin/controller-gen || GOBIN=/home/brad/go/src/operators/bin go install sigs.k8s.io/controller-tools/cmd/controller-gen@v0.10.0
/home/brad/go/src/operators/bin/controller-gen object:headerFile="hack/boilerplate.go.txt" paths="./..."
Error: err: exit status 2: stderr: panic: loadPackageData called with empty package path

goroutine 20 [running]:
cmd/go/internal/load.loadPackageData({0xb2e7d0, 0xc000028148}, {0x0, 0x0}, {0x0, 0x0}, {0xc00002a0c4, 0x1b}, {0x0, 0x0}, ...)
	/usr/local/go/src/cmd/go/internal/load/pkg.go:811 +0x67d
cmd/go/internal/load.(*preload).preloadMatches.func1({0x0, 0x0})
	/usr/local/go/src/cmd/go/internal/load/pkg.go:1044 +0xa5
created by cmd/go/internal/load.(*preload).preloadMatches
	/usr/local/go/src/cmd/go/internal/load/pkg.go:1042 +0x265
panic: ImportPaths returned empty package for pattern ./... [recovered]
	panic: ImportPaths returned empty package for pattern ./...

goroutine 1 [running]:
cmd/go/internal/load.(*preload).flush(0xc00020e560)
	/usr/local/go/src/cmd/go/internal/load/pkg.go:1086 +0x78
panic({0x97d480, 0xc00020e580})
	/usr/local/go/src/runtime/panic.go:884 +0x212
cmd/go/internal/load.PackagesAndErrors({0xb2e7d0?, 0xc000028148?}, {0x0, 0x0, 0x0, 0x1, 0x1, 0x1}, {0xc0000241d0, 0x1, ...})
	/usr/local/go/src/cmd/go/internal/load/pkg.go:2792 +0xaea
cmd/go/internal/list.runList({0xb2e7d0?, 0xc000028148}, 0xc00002c630?, {0xc0000241d0?, 0x1, 0x1})
	/usr/local/go/src/cmd/go/internal/list/list.go:605 +0xc47
main.invoke(0xe28620, {0xc000024100, 0xe, 0xe})
	/usr/local/go/src/cmd/go/main.go:225 +0x34e
main.main()
	/usr/local/go/src/cmd/go/main.go:179 +0x7d1

Usage:
  controller-gen [flags]

Examples:
	# Generate RBAC manifests and crds for all types under apis/,
	# outputting crds to /tmp/crds and everything else to stdout
	controller-gen rbac:roleName=<role name> crd paths=./apis/... output:crd:dir=/tmp/crds output:stdout

	# Generate deepcopy/runtime.Object implementations for a particular file
	controller-gen object paths=./apis/v1beta1/some_types.go

	# Generate OpenAPI v3 schemas for API packages and merge them into existing CRD manifests
	controller-gen schemapatch:manifests=./manifests output:dir=./manifests paths=./pkg/apis/... 

	# Run all the generators for a given project
	controller-gen paths=./apis/...

	# Explain the markers for generating CRDs, and their arguments
	controller-gen crd -ww


Flags:
  -h, --detailed-help count   print out more detailed help
                              (up to -hhh for the most detailed output, or -hhhh for json output)
      --help                  print out usage and a summary of options
      --version               show version
  -w, --which-markers count   print out all markers available with the requested generators
                              (up to -www for the most detailed output, or -wwww for json output)


Options


generators

+webhook                                                                                                                                           package  generates (partial) {Mutating,Validating}WebhookConfiguration objects.                        
+schemapatch[:generateEmbeddedObjectMeta=<bool>],manifests=<string>[,maxDescLen=<int>]                                                             package  patches existing CRDs with new schemata.                                                      
+rbac:roleName=<string>                                                                                                                            package  generates ClusterRole objects.                                                                
+object[:headerFile=<string>][,year=<string>]                                                                                                      package  generates code containing DeepCopy, DeepCopyInto, and DeepCopyObject method implementations.  
+crd[:allowDangerousTypes=<bool>][,crdVersions=<[]string>][,generateEmbeddedObjectMeta=<bool>][,ignoreUnexportedFields=<bool>][,maxDescLen=<int>]  package  generates CustomResourceDefinition objects.                                                   


generic

+paths=<[]string>  package  represents paths and go-style path patterns to use as package roots.   


output rules (optionally as output:<generator>:...)

+output:artifacts[:code=<string>],config=<string>  package  outputs artifacts to different locations, depending on whether they're package-associated or not.   
+output:dir=<string>                               package  outputs each artifact to the given directory, regardless of if it's package-associated or not.      
+output:none                                       package  skips outputting anything.                                                                          
+output:stdout                                     package  outputs everything to standard-out, with no separation.                                             

run `controller-gen object:headerFile=hack/boilerplate.go.txt paths=./... -w` to see all available markers, or `controller-gen object:headerFile=hack/boilerplate.go.txt paths=./... -h` for usage
make: *** [Makefile:94: generate] Error 1
Error: failed to create API: unable to run post-scaffold tasks of "base.go.kubebuilder.io/v3": exit status 2
Usage:
  operator-sdk create api [flags]

Examples:
  # Create a frigates API with Group: ship, Version: v1beta1 and Kind: Frigate
  operator-sdk create api --group ship --version v1beta1 --kind Frigate

  # Edit the API Scheme
  nano api/v1beta1/frigate_types.go

  # Edit the Controller
  nano controllers/frigate/frigate_controller.go

  # Edit the Controller Test
  nano controllers/frigate/frigate_controller_test.go

  # Generate the manifests
  make manifests

  # Install CRDs into the Kubernetes cluster using kubectl apply
  make install

  # Regenerate code and run against the Kubernetes cluster configured by ~/.kube/config
  make run


Flags:
      --controller           if set, generate the controller without prompting the user (default true)
      --force                attempt to create resource even if it already exists
      --group string         resource Group
  -h, --help                 help for api
      --kind string          resource Kind
      --make make generate   if true, run make generate after generating files (default true)
      --namespaced           resource is namespaced (default true)
      --plural string        resource irregular plural form
      --resource             if set, generate the resource without prompting the user (default true)
      --version string       resource Version

Global Flags:
      --plugins strings   plugin keys to be used for this subcommand execution
      --verbose           Enable verbose logging

FATA[0003] failed to create API: unable to run post-scaffold tasks of "base.go.kubebuilder.io/v3": exit status 2 
```

I think the Fedora silverblue/toolbox setup is fucking with me. I don't have `make` installed in the base and I don't have access to minikube from my toolbox.

after installing make

```bash
make generate 
/home/brad/go/src/test-operator/bin/controller-gen object:headerFile="hack/boilerplate.go.txt" paths="./..."
/var/home/brad/go/src/net/cgo_linux.go:12:8: no such package located
Error: not all generators ran successfully
run `controller-gen object:headerFile=hack/boilerplate.go.txt paths=./... -w` to see all available markers, or `controller-gen object:headerFile=hack/boilerplate.go.txt paths=./... -h` for usage
make: *** [Makefile:94: generate] Error 1
```

going to try installing gcc... and looks like that did the trick.

but now following the red hat article i'm still getting a weird error

```bash
operator-sdk create api --version=v1alpha1 --kind=Traveller
Create Resource [y/n]
y
Create Controller [y/n]
y
Writing kustomize manifests for you to edit...
Writing scaffold for you to edit...
api/v1alpha1/traveller_types.go
controllers/traveller_controller.go
Update dependencies:
$ go mod tidy
go: warning: "all" matched no packages
Running make:
$ make generate
mkdir -p /home/brad/go/src/operators/bin
test -s /home/brad/go/src/operators/bin/controller-gen || GOBIN=/home/brad/go/src/operators/bin go install sigs.k8s.io/controller-tools/cmd/controller-gen@v0.10.0
/home/brad/go/src/operators/bin/controller-gen object:headerFile="hack/boilerplate.go.txt" paths="./..."
Error: err: exit status 2: stderr: panic: ImportPaths returned empty package for pattern ./... [recovered]
	panic: ImportPaths returned empty package for pattern ./...

goroutine 1 [running]:
cmd/go/internal/load.(*preload).flush(0xc0002690b0)
	/usr/local/go/src/cmd/go/internal/load/pkg.go:1086 +0x78
panic({0x97d480, 0xc0002690d0})
	/usr/local/go/src/runtime/panic.go:884 +0x212
cmd/go/internal/load.PackagesAndErrors({0xb2e7d0?, 0xc00019a000?}, {0x0, 0x0, 0x0, 0x1, 0x1, 0x1}, {0xc0001b00e0, 0x1, ...})
	/usr/local/go/src/cmd/go/internal/load/pkg.go:2792 +0xaea
cmd/go/internal/list.runList({0xb2e7d0?, 0xc00019a000}, 0xc0001c4450?, {0xc0001b00e0?, 0x1, 0x1})
	/usr/local/go/src/cmd/go/internal/list/list.go:605 +0xc47
main.invoke(0xe28620, {0xc0001b0010, 0xe, 0xe})
	/usr/local/go/src/cmd/go/main.go:225 +0x34e
main.main()
	/usr/local/go/src/cmd/go/main.go:179 +0x7d1

Usage:
  controller-gen [flags]

Examples:
	# Generate RBAC manifests and crds for all types under apis/,
	# outputting crds to /tmp/crds and everything else to stdout
	controller-gen rbac:roleName=<role name> crd paths=./apis/... output:crd:dir=/tmp/crds output:stdout

	# Generate deepcopy/runtime.Object implementations for a particular file
	controller-gen object paths=./apis/v1beta1/some_types.go

	# Generate OpenAPI v3 schemas for API packages and merge them into existing CRD manifests
	controller-gen schemapatch:manifests=./manifests output:dir=./manifests paths=./pkg/apis/... 

	# Run all the generators for a given project
	controller-gen paths=./apis/...

	# Explain the markers for generating CRDs, and their arguments
	controller-gen crd -ww


Flags:
  -h, --detailed-help count   print out more detailed help
                              (up to -hhh for the most detailed output, or -hhhh for json output)
      --help                  print out usage and a summary of options
      --version               show version
  -w, --which-markers count   print out all markers available with the requested generators
                              (up to -www for the most detailed output, or -wwww for json output)


Options


generators

+webhook                                                                                                                                           package  generates (partial) {Mutating,Validating}WebhookConfiguration objects.                        
+schemapatch[:generateEmbeddedObjectMeta=<bool>],manifests=<string>[,maxDescLen=<int>]                                                             package  patches existing CRDs with new schemata.                                                      
+rbac:roleName=<string>                                                                                                                            package  generates ClusterRole objects.                                                                
+object[:headerFile=<string>][,year=<string>]                                                                                                      package  generates code containing DeepCopy, DeepCopyInto, and DeepCopyObject method implementations.  
+crd[:allowDangerousTypes=<bool>][,crdVersions=<[]string>][,generateEmbeddedObjectMeta=<bool>][,ignoreUnexportedFields=<bool>][,maxDescLen=<int>]  package  generates CustomResourceDefinition objects.                                                   


generic

+paths=<[]string>  package  represents paths and go-style path patterns to use as package roots.   


output rules (optionally as output:<generator>:...)

+output:artifacts[:code=<string>],config=<string>  package  outputs artifacts to different locations, depending on whether they're package-associated or not.   
+output:dir=<string>                               package  outputs each artifact to the given directory, regardless of if it's package-associated or not.      
+output:none                                       package  skips outputting anything.                                                                          
+output:stdout                                     package  outputs everything to standard-out, with no separation.                                             

run `controller-gen object:headerFile=hack/boilerplate.go.txt paths=./... -w` to see all available markers, or `controller-gen object:headerFile=hack/boilerplate.go.txt paths=./... -h` for usage
make: *** [Makefile:94: generate] Error 1
Error: failed to create API: unable to run post-scaffold tasks of "base.go.kubebuilder.io/v3": exit status 2
Usage:
  operator-sdk create api [flags]

Examples:
  # Create a frigates API with Group: ship, Version: v1beta1 and Kind: Frigate
  operator-sdk create api --group ship --version v1beta1 --kind Frigate

  # Edit the API Scheme
  nano api/v1beta1/frigate_types.go

  # Edit the Controller
  nano controllers/frigate/frigate_controller.go

  # Edit the Controller Test
  nano controllers/frigate/frigate_controller_test.go

  # Generate the manifests
  make manifests

  # Install CRDs into the Kubernetes cluster using kubectl apply
  make install

  # Regenerate code and run against the Kubernetes cluster configured by ~/.kube/config
  make run


Flags:
      --controller           if set, generate the controller without prompting the user (default true)
      --force                attempt to create resource even if it already exists
      --group string         resource Group
  -h, --help                 help for api
      --kind string          resource Kind
      --make make generate   if true, run make generate after generating files (default true)
      --namespaced           resource is namespaced (default true)
      --plural string        resource irregular plural form
      --resource             if set, generate the resource without prompting the user (default true)
      --version string       resource Version

Global Flags:
      --plugins strings   plugin keys to be used for this subcommand execution
      --verbose           Enable verbose logging

FATA[0005] failed to create API: unable to run post-scaffold tasks of "base.go.kubebuilder.io/v3": exit status 2
```

https://sdk.operatorframework.io/docs/building-operators/golang/tutorial/

going lower it looks like the operator sdk is a wrapper for kubebuilder https://book.kubebuilder.io/cronjob-tutorial/gvks.html

Possibly novel take, if you really want to learn kubernetes the hard way, build an operator. It'll teach you the controller pattern and touches is on a bunch of stuff that might otherwise get skipped over like webhooks and finalizers.

operator-sdk is just kubebuilder with a couple extras (and ansible support).

AWX is the notable ansible example, otherwise most other things are golang. It does not look like kubebuilder or operator-sdk support dotnet, so getting started there might be a bit more difficult, but could probably be templated ourselves.

What do operators provide that helm charts don't?

## Motivational Struggles ##

I've gotten to the point with both the operator-sdk and kubebuilder that I need to actually write some go to make my operator do a thing... and I'm just getting stuck. Am I afraid that I'm just wasting my time, since there is no way we'll use go at work? Am I afraid that even if we could pick a language I won't be able to convince the company we should develop our own operators? Should those fears prevent me from doing something?

OK forcing myself through the tutorial still doesn't seem to be helping. I'm going back to my usual, do a thing that needs doing and figure it out as I go. So I'm going to do a cloudflare dns record. And after that I'm going to look at doing an ansible one for Active Directory dns.

## lab-operator ##

The kubebuilder tutorial didn't really include a lot of details, so I went digging in cert-manager's code. I'm not sure if it's a good reference or not. There seem to be a lot of custom types defined that make it hard to follow. Not sure if it's a golang, kubernetes, or just a cert-manager pattern. I'm also not fluent enough in go to know how/what I can unwind. step-issuer appears to be a better starting point, it also uses kubebuilder so includes the hint things too.

I think I have the spec figured out, now it's time to write some real code.

It occurs to me now part of my struggle is that I'm going in the wrong direction. I've written a bunch of spec but have no idea how to test it. I very likely should have written a simple function in go to do the cloudflare thing, and then migrated into an operator.

I wonder if it would have also been easier starting with something inside the cluster like the tutorial did with the cron job. Actually I don't know about anything in the cluster either so probably not much of a difference.

## Another Attempt: April 2024 ##

```
kubebuilder init --domain brad.lab --repo gitlab.com/blcarman/labgres-operator
```

I don't really understand how the repo matters and the kubebuilder docs just shove it off to some go modules blog post that didn't seem helpful either.

```
kubebuilder create api --group labgres --version v1 --kind PostgresInstance
```

I don't know how to feel that I'm getting the same freaking error message as I did a year ago.

```
FATA failed to create API: unable to run post-scaffold tasks of "base.go.kubebuilder.io/v4": exec: "make": executable file not found in $PATH
```

I guess it's not the exact same error, just the same phase. Yay simply installing make was enough to fix this one, so maybe we are doing better this time.