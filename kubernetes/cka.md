### Prep ###

* started with K3S
* jumped to rke2/rancher
* started udemy course

## udemy course notes ##

### namespaces ###

kube-public nampespace: resources consumed by all users
isolation and resource limits

* to change namespace in kubectl context `kubectl config set-context $(kubectl config current-context) --namespace=whatever`

interesting that resourceQuota are not a property of the namespace, but defined separately, I suppose so you can target specific pods too?

### services ###

from k8s node you can directly reach the pod IP? how do we limit that?

why does service have port? (in addition to the nodeport and targetport), port will default to targetport


## kubeadm ##

* using cri-o for the fun of it... just kidding url in the install docs seems to have changed
* need to set `--pod-network-cidr=10.75.0.0/16` and update calico manifest to match (or you know just actually read the calico docs)

`kubeadm join 192.168.30.18:6443 --token p8imyg.jllvmiz6a9mg2vm6 --discovery-token-ca-cert-hash sha256:81473f588996d65a1602973b176b79468d9299186d0ffbf8c28fe10635e28cf7`

think I'm at a good stopping point for tonight at the "what's next" section of https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/#whats-next

There doesn't seem to be much more to kubeadm, guess it wasn't as complicated as I expected, there's still a lot of value in running something like RKE2 but just running vanilla k8s is less daunting than I was expecting.