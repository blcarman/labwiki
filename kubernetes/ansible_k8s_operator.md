# Ansible Kubernetes Operator #

Ansible's stateless nature is one of its biggest pros and cons. What if you could manage resources with ansible and store the state in kubernetes?

What if you could manage things like cloudflare dns, datadog alerts, and other "platform" resources as kubernetes objects? 

Kubernetes operators is an increasingly common pattern, in fact we already use them for managing knative, awx, tls certificates, and others.

Since we're already using them it's only natural that we'd make our own. Lucky for us there is already an ansible operator sdk that handles all the kubernetes magic for us.

All we have to do is tweak our existing playbooks a tiny bit to fit into this model.

Will it replace all our ansible code? Probably not, OS level tasks and similar will still probably be handled with normal ansible playbooks.

What else could we turn into k8s objects? vmware vm's? infoblox IPs? helm chart configs.

## Hello k8s ##

## Secrets ## 

https://github.com/operator-framework/operator-sdk/issues/2510


# Presentation #

## Controllers ##

* Kubernetes is built around controllers and the control loop. Just like a thermostat monitors temperature and kicks on the furnace when the desired temperature does not match the room temperature, kubernetes controllers monitor resources and bring reality closer to the desired state. 

## Intro to Operators ##

* CRDs
* operators "Kubernetes' operator pattern concept lets you extend the cluster's behaviour without modifying the code of Kubernetes itself by linking controllers to one or more custom resources." k8s doc https://kubernetes.io/docs/concepts/extend-kubernetes/operator/
* control loop: continuously monitors/reconciles to make the real world match the desired state
* ansible operator-sdk provides scaffolding/boilerplate parts of an operator, enabling you to write an operator without any knowledge of go and a minimal amount of k8s knowledge

## why? ##

* makes everything a k8s object
  * state maintained in k8s, get all the k8s goodies like events
  * we've already accepted the complexity of k8s, we can leverage that to make our lives simple elsewhere
* separates resources
  * no more giant pipelines that have to run 1000 things in order
  * apps can manage their own dependencies directly

## Hello World ##

* init the project
  * operator frameworks do the heavy lifting for us
  * ansible, python, dotnet, and of course go
* watchers (i.e. how does the operator know to do a thing when a resource is created/updated)
  * can watch both the k8s api as well as the external resource
* spec to variables
  * "The key-value pairs in the Custom Resource spec are passed to Ansible as extra variables."

* how do we pass in secrets? can attach k8s secrets to the operator pod
  can we just get k8s secrets directly

  ```yaml
  - name: Check for specified admin password configuration
    k8s_info:
      kind: Secret
      namespace: '{{ ansible_operator_meta.namespace }}'
      name: '{{ admin_password_secret }}'
    register: _custom_admin_password
    no_log: "{{ no_log }}"
    when: admin_password_secret | length
  ```

* operators can be scoped to either a namespace or cluster level
  * namespace scope is obviously more restricted/secure, but don't make sense if you're going to deploy a lot of resources for apps in different namespaces

## Practical Uses ##

* AD DNS
* cloudflare dns, page rules, whatever
* datadog alerts
* thycotic secrets, application config, folders, whatever
* database config


# The Written Way #

## Intro ##

One of the things that makes kubernetes so cool is its usage of control loops, implemented via controllers, to make sure reality matches the desired state. A thermostat is a common example of a control loop. You provide a desired temperature, then it monitors the real temperature and it turns on the AC/furnace to until the monitored temperature matches the desired temperature. And in kubernetes this translates to controllers for making sure all the pods in a deployment are running, a job controller for starting pods required by a job, and a ton of others. 

Its easy to think of kubernetes as just a platform. Just like we run VMs on vsphere, we run containers on kubernetes. But its real power is the API it provides, and the ability to extend it. Kubernetes allows us to define custom resources and our own controllers, known as operators to destinguish from the builtin contollers. These operators can watch the API for events on the resources it manages. The operator can then take action to make reality match the desired state.

The operator term comes from the fact that it used for things that have typically been done by human operators or sysadmins. The idea is that we can automate these previously human tasks through operators.

Operators are becoming more and more common. We use them for managed TLS certificates (cert-manager), deploying knative, and inside knative itself for managing the resouces associated with functions, deploying AWX, storage provisioning. So even if we don't build our own understanding how they work is critical for both supporting kubernetes itself as well as the platform components we deploy on top of it. 

## architecture ##

### Watch and Reconcile ###

An operator has 2 main components. A watcher and a reconciler. The watcher keeps an eye on events from the kubernetes api.

The reconciler runs our ansible code to do the thing when necessary. It is also possible to periodically reconcile a resource regardless of change. This can be useful for cloud resources that may be subject to human changes or config drift that needs to be trued up.

### scoping ##

Cluster vs namespace scope

Obviously greater security with namespace-scoped, and this works great for deployments, like AWX or knative. 

Cluster scoped are more suited to things that will be used by a bunch of stuff in the cluster, like certificates. it is possible to exclude namespaces, so we can restrict to what we call "user" namespaces as well.

## Why? ##

We tend to like new shiny things. So why are kubernetes operators more than just a shiny thing to put on our resume? 

We have already accepted the complexity of kubernetes. Using operators allows us to stay within that ecosystem, so overall complexity is reduced. We can view the status and specification of a custom resource the same way we do any other kubernetes resource. We already monitor kubernetes with datadog as well. And of course the objects are stored in the cluster's etcd database, so the state is taken care of.

Another benefit of staying in kubernetes is keeping the app's whole config closer together. They can be defined in a common helm chart, and/or at least deployed to the app's namespace.

Today, most of our apps depend on databases and secrets. We have big cumbersome pipelines that create all the databases in sequence. These pipelines are run separately from the app deployment and prone to failure due to the amount of things they have to do. Furthermore, a typo for one app's database might mean none of the databases get created. That said these pipelines have been and still are incredibly useful, and this can be thought of continuing to evolve on what we've already built. 

It also means you have to look in multiple git repos to find an app's dependencies. This also introduces inconsistencies where the database might be ALL CAPS, and the secret folders camel case, making automation more difficult. 

Additionally, this makes self service easier. Wouldn't it be nice if an app spec could just set "database: true" instead of having to put in a request with platform?

## Why Not? ##

Hopefully by now you're convinced to buy the new car and the extended warranty, but you might have some warning bells going off. Surely there have to be some downsides right? Of course. 

Operators have to be deployed in every cluster they will be used in. While pipelines only consume resources when they run, operators will always consume some cpu/ram as they watch for changes. The usage increases with the number of resources.

It does also appear to create some vendor lockin with kubernetes. As we'll see shortly this vendor lockin is less of a concern, since we can use ansible code just like we do today.

## Let's Do It! ##

The huge community around kubernetes, means most of the hardwork has already been done for us. Kind of like using a cloud to manage your VMs, there are a number of operator frameworks that take care of the kubernetes parts for us, we just need to provide the logic that manages the resource. These frameworks are available for all the common languages, including dotnet and python, and yes ansible as well. Disclaimer the ansible one uses go for the actual operator parts, but we don't have to touch those parts.

The defacto standard is kubebuilder, and built on top of that is the operator-sdk that supports ansible.

Initializing a resource can optionally create the associated ansible role.

It is recommended to have each operator manage a single resource/CRD.

A great example of a project using the operator-sdk with ansible is AWX (in fact if you stick around for the next talk, we'll dive into AWX). This also provides some confidence, we aren't just picking up some random guy's pet project, we're using enterprise grade frameworks. 

https://github.com/ansible/awx-operator

roles folder: for those that are already familiar with ansible you know what roles are, for others think of it like a function or unit of code. You would have a role for creating a database, or provisioning a VM, etc. Digging in you can see the ansible code is just like we'd normally have.

But what about variables? The operator takes the spec from the custom resource we defined and passes those to ansible as extra vars. Secrets for credentials can either be attached to the operator pod, or the ansible can pull them using k8s_info module.

CRD specs: https://github.com/ansible/awx-operator/blob/devel/config/crd/bases/awx.ansible.com_awxs.yaml And the best part is it includes documentation for each option. With just plain IAC, its easy to think a variable like "cluster_name" is intuitive to everyone, but often times it only makes sense while we're writing the code, and falls apart when you realize we have more than one cluster technology in play. 

I was going to show some of the scaffolded go code, but I couldn't find it, that's how hidden away it is.


## Conclusion ##

To recap, we've learned that kubernetes is built around the idea of the control loop. We can extend it by creating our own controllers known as operators. This is made easier with the use of frameworks that manage the boilerplate code for us.