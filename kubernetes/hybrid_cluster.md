# Hybrid Cluster #

I want to automatically spin up cloud nodes when my on-prem nodes are overloaded.

## Options ##

* rancher: seems to want to do at cluster level instead of node level
* cluster autoscaler: looks more promising, might have a few layers to configure

## Connectivity ##

Azure supports ipsec vpn but it costs money, can I use nebula? 

did get ipsec tunnel working, used https://help.ui.com/hc/en-us/articles/115012305347-EdgeRouter-Route-Based-Site-to-Site-VPN-to-Azure-VTI-over-IKEv2-IPsec- for the edgerouter side

rancher provisioning does not look compelling. Reading more about the cluster autoscaler, it looks like it expects the cloud provider/provisioner to do the cluster join, thus it only supports aks clusters.

hmm or maybe with vmss we just need to pass it a custom script or cloud-init https://learn.microsoft.com/en-us/azure/virtual-machine-scale-sets/virtual-machine-scale-sets-deploy-app#already-provisioned


Does aws allow something similar? What if I skip the cluster-autoscaler for the moment and just made some sort of vm deployment script that would join the cluster?

So it's becoming clear that either I'm not searching with the right words or this is not very common. 

non-goal of cluster-api "To manage a single cluster spanning multiple infrastructure providers."

So basically just run separate clusters.