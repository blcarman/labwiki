## Kubernetes the Hard Way ##

This proved less useful than expected. It does not provide any explanation for the commands/tasks it has you run. 

## Kubernetes in Action ##

* good book, a litle bit dated, but kind of impossible for a book to stay up to date with the speed of k8s changes

## K3S ##

* I like this better than the idea of minikube though it does require a tiny bit more setup to use (i.e. full VM running)
* included traefik is old, 1.7 which at this point seems like an odd choice to not be updated
* saw strange dns failures when trying to deploy newer version of traefik through the helm crd, turns out this was due to firewalld being on
* not sure how I feel about the helm chart crd 
* overall really like this for quick n dirty deploys and/or single node clusters
* documentation is a bit confusing, like can I actually pass config things to the install script or do I need to install and then config? https://rancher.com/docs/k3s/latest/en/installation/install-options/how-to-flags/ and looks like options for the actually service should be set in INSTALL_K3S_EXEC
* should cluster-init only be used once? or can we just leave it in/run the installer with it?

### k3s with HA ##

* calico
* kube-vip (https://kube-vip.io/control-plane/#k3s but need to look at new hybrid mode, https://kube-vip.io/hybrid/)
* embedded etcd
* disable included traefik
* note dhcp is bad for cert auth, at a minimum ensure static mapping

stealing some ideas from https://blog.alexellis.io/bare-metal-kubernetes-with-k3s/, notably `--tls-san`

## Full Rancher ##

* seems to prefer being installed on top of k3s now. So first we need a 3 node k3s cluster

### Lab ###

* started with el8 template, disabled included traefik (possible need to ansibleize this part)
* DONE install gitlab runner... new thought since gitlab runners are such a manual process perhaps I should stick to a few generic runners that are then given access to new stuff instead of running them everywhere, would also probably be a bit more securable.
* copy kubeconfig to titanium, no more ssh nonsense
* create test namespace
* create role for gitlab runner
* grant runner access to k3s
* CI deploy traefik (and everything), netbox, wiki, matrix
* configure backups of postgres db's for matrix/netbox/wiki
* move jellyfin from podman to k3s
* build out a full HA cluster
* nextcloud?
* draw.io
* jitsi
* authentication?
* do egress policies also block return traffic (i.e. if i put in an egress policy blocking everything but also have an ingress policy that allows traffic, would clients be able to make http requests)

## Notes ##

`kubectl get all` doesn't seem to actually get all, notably doesn't get secrets or pvc. Apparently `all` is an alias/subset, see https://github.com/kubernetes/kubectl/issues/151

https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner ugh apparently not valid with v1.20, have to do some hacky stuff,

need nfs-utils installed on the kubernetes hosts

change chart.yml to remove version limit
```
-kubeVersion: ">=1.9.0-0 <1.20.0-0"
+kubeVersion: ">=1.9.0-0"
```

run helm with fixed imaged, not sure why the official project hasn't built a new image yet. 
```
helm install nfs-subdir-external-provisioner . --set nfs.server=warehouse.lab.bradlab.tech --set nfs.path=/mnt/warehouse/lab/testkube --set image.repository=rkevin/nfs-subdir-external-provisioner --set image.tag=fix-k8s-1.20
```

## CSI Drivers ##

* installed smb CSI driver, just a normal helm chart install